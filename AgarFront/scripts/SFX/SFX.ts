﻿import {Game} from "../beforeGame/Game"
import {SoundBase} from "./SoundBase"
declare type SoundArray = { [index: string]: Phaser.Sound[] };


export enum SoundtrackTransition {
    FADE_IN_OUT,
    CUT,
    KEEP
}

export class SFX {

    protected effects: SoundArray;
    public soundtrackTransition: SoundtrackTransition = SoundtrackTransition.FADE_IN_OUT;
    protected soundtrackOneRunDuration = 10650;
    private initTime: number;
    public soundtrackName: string;
    private static instance: SFX;
    static prefix: string = "";
    public type;


    constructor(protected phaser: Phaser.Game) {
        this.effects = SoundBase.effects;
        this.initTime = Date.now();
    }

    public getSoundtrack(): Phaser.Sound {
        //console.log(this.initTime);
        return this.effects["soundtrack" + this.initTime][0];
    }

    protected setSoundtrack(name: string) {
        this.soundtrackName = name;
        let effect = this.effects[name];
        if (this.effects[SFX.prefix + "/" + name])
            effect = this.effects[SFX.prefix + "/" + name];

        this.effects["soundtrack" + this.initTime] = effect;
    }

    static get(): SFX {
        return SFX.instance;
    }



    static setSFX(newsfx: SFX) {
        if (this.instance &&  this.instance.type ==  newsfx.type)
            return;

        if (this.instance )
            this.instance.deactivate(800);

        console.log(newsfx);
        newsfx.activate(this.instance);

       // newsfx.getSoundtrack().game.sound.volume = 0.1;
        this.instance = newsfx;

    }

    public playClick() {
        this.phaser.rnd.pick(this.effects["buttons/click"]).play();
    }

    public playSingleEffect(name: string) {
        if (this.effects[SFX.prefix +"/"+ name])
            this.effects[SFX.prefix +"/"+ name][0].play();

       if (!this.effects[name])
           throw new Error("Can't find sound: " + name);
       else
           this.effects[name][0].play()
    }
    public playOneOfEffect(name: string) {
        console.log(name);
        if (!this.effects[name])
            throw new Error("Can't find sound: " + name);

        let sfx = this.phaser.rnd.pick(this.effects[name]);
       sfx.play();
       return sfx;
    }

    public playCounting(nr: number) {
       this.effects["counting"][nr].play();
   }

    protected prevWellPlayed: Phaser.Sound;
    protected currentSlonSFX: Phaser.Sound;

    public playSlon(sfx: string | Phaser.Sound, force?, probability?) {
        if (probability && Math.random() > probability)
            return;

        if (this.currentSlonSFX && this.currentSlonSFX.isPlaying && (this.currentSlonSFX!=sfx as any)) {
            if (force) {
                let current = this.currentSlonSFX;
                current.fadeTo(300, 0);
                current.onFadeComplete.addOnce(() => {
                    current.stop();
                    current.volume = current.baseVolume;
                });
            }
            else
            return;
        }

        if ((sfx as any).indexOf)
            this.currentSlonSFX = this.playOneOfEffect(sfx as any);
        else {
            this.currentSlonSFX = sfx as any;
            (sfx as any).play();
        }
    }


    public playSlonCTPMistake() {
        this.playSlon("slon/ctp_mistake", false, 0.6);
   }

   private canPlayWellPlayed(): boolean {
       return !(this.prevWellPlayed && this.prevWellPlayed.isPlaying) &&
           !(this.playedSparta && this.playedSparta.isPlaying);
   }

   public playSlonWow() {
       this.playSlon("slon/wow");
   }

   public playSlonWellPlayed(extra = false) {
       if (!this.canPlayWellPlayed())
           return;

       let id = this.phaser.rnd.pick([0,2, 1,3,4,5,6,7]);
   //    if (extra)
     ///      id = 2;
       let sfx = this.effects["slon/wellPlayed"][id];
       this.playSlon(sfx);

   }

   public playSlonNewRecord() {
       this.playSlon("slon/newRecord");
   }

   public playSlonPlayAgain() {
       this.playSlon("slon/playAgain");
   }

   public playSlonPlay() {
       let name = "slon/play";
       if (this.effects[SFX.prefix + "/slon/play"])
           this.phaser.rnd.pick(this.effects[SFX.prefix + "/slon/play"]).play();
       else
           this.phaser.rnd.pick(this.effects["slon/play"]).play();
   }

   private playedSparta: Phaser.Sound;

   public playSlonSparta() {
       let rnd = Math.random();
       if (rnd < 0.3)
           this.playedSparta = this.effects["slon/sparta"][0];
       else if (rnd < 0.6)
           this.playedSparta = this.effects["slon/sparta"][1];
       else if (rnd < 0.9 || Game.settings.under18)
           this.playedSparta = this.effects["slon/sparta"][2];
       else
           this.playedSparta = this.effects["slon/sparta"][3];

       this.playSlon(this.playedSparta, true);
   }

   public playSlonFuckup() {
       if (Game.settings.under18)
           (this.currentSlonSFX = this.effects["slon/fuckup"][3]).play();
       else {
           this.playSlon("slon/fuckup");
       }
   }   

   public playChill() {
       this.playSingleEffect("chill");
       setTimeout(() => this.playSingleEffect("freeze"), 1200);
   }

   public playPlop() {
       this.playSingleEffect("slon/rrr");
   }


   public playFlesh() {
       this.phaser.rnd.pick(this.effects["flesh"]).play();
   }

   protected lastCutSound: Phaser.Sound;
   public playCut() {
       if (this.lastCutSound && this.lastCutSound.isPlaying)
           return;

       this.lastCutSound = this.phaser.rnd.pick(this.effects["cut"]);
       this.lastCutSound.play();
   }


   public getSound(name: string) {
       if (this.effects[name].length == 1)
           return this.effects[name][0];
       else
           return this.phaser.rnd.pick(this.effects[name])
   }


   public playSoundtrack() {
       this.getSoundtrack().play();
   }

   protected fadeVolume(sfx: Phaser.Sound, from, to, duration) {
       sfx.volume = from;
       let tween = this.phaser.add.tween(sfx).to({ volume: to }, duration).start();
   }


   public playSiren(play: boolean) {
       let sound = this.getSound("siren");
       if (play) {
           sound.play();
           sound.loop = true;
           sound.fadeTo(600, sound.baseVolume);
       }
       else
           sound.fadeOut(400);
   }

   private changeSoundtrack(prev: SFX) {
       let soundtrack = this.getSoundtrack();
       console.log("Change ");
     //  soundtrack.addMarker("afterIntro", 5.32, 999);

       if (!prev || (prev.soundtrackTransition == this.soundtrackTransition && this.soundtrackTransition != SoundtrackTransition.CUT)) {
           let fadeInDuration = 900;
           soundtrack.position = 3000;
       

           soundtrack.play(null, 10000).loop = true;

           this.fadeVolume(soundtrack, 0, (soundtrack as any).baseVolume, fadeInDuration);
           return;
       }

       if (prev.soundtrackTransition == SoundtrackTransition.CUT || this.soundtrackTransition == SoundtrackTransition.CUT) {

           prev.removeSoundtrack();

           
           let marker = null;
           if (this.type == "normal") {
               marker = "start";
               if (!soundtrack.markers["start"])
                   soundtrack.addMarker("start", 0, soundtrack.totalDuration)
               if (prev.type == "rage") {
                   marker = "afterIntro";
                   if (!soundtrack.markers["afterIntro"])
                       soundtrack.addMarker("afterIntro", 5.3, soundtrack.totalDuration - 5.3)
               }

           }
          
           soundtrack.play(marker, 0, (soundtrack as any).baseVolume).loop = true;

           console.log(soundtrack);

          // soundtrack.removeMarker("afterIntro");
          // soundtrack.position = 3000;
       }

       if (prev.soundtrackTransition == SoundtrackTransition.KEEP)
           this.effects["soundtrack" + this.initTime][0] =  prev.getSoundtrack();
   }

   public removeSoundtrack() {
       this.getSoundtrack().stop();
   }
   public activate(prev: SFX) {
       this.getSoundtrack().removeMarker("afterIntro");
       this.getSoundtrack().removeMarker("start");
       this.changeSoundtrack(prev);
   }

   public stopAll() {

   }

   public pause(play: boolean = false) {
       this.getSoundtrack().volume = +(!play) * (this.getSoundtrack() as any).baseVolume;
       this.phaser.sound.volume = +(!play) * 1;
   }

   public deactivate(duration: number) {
       let soundtrack = this.getSoundtrack();

       switch (this.soundtrackTransition) {
           case SoundtrackTransition.FADE_IN_OUT:
               let fadeOutDuration = duration;
               soundtrack.fadeOut(fadeOutDuration);
               break;

           case SoundtrackTransition.KEEP:
               break;

           case SoundtrackTransition.CUT:
               soundtrack.stop();
               break;
       }
       if (this.getSound("countdown").isPlaying)
           setTimeout(() => this.getSound("countdown").stop(), 1100);
      // console.log("Deactivate");
      // if (this.effects["soundtrack" + this.initTime] && SoundtrackTransition.FADE_IN_OUT== this.soundtrackTransition)
      // delete this.effects["soundtrack" + this.initTime];
   }

    
}



export {SFXChill} from "./SFXChill"
export {SFXNormal} from "./SFXNormal"
export {SFXMenu} from "./SFXMenu"
export {SFXRage} from "./SFXRage"
