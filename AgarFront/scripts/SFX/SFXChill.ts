﻿
import {SFX, SoundtrackTransition} from "./SFX"


export class SFXChill extends SFX {

    constructor(protected phaser: Phaser.Game, private durationS: number) {
        super(phaser);
        this.type = "chill";
        this.setSoundtrack("soundtracks/normal");
        this.soundtrackTransition = SoundtrackTransition.KEEP;
    }

    public playSlon(sfx: string | Phaser.Sound, force?, probability?){
    }

    public playFlesh() {
        this.phaser.rnd.pick(this.effects["chill/flesh"]).play();
    }

    public playPlop() {
    }

    public playCut() {
        if (this.lastCutSound && this.lastCutSound.isPlaying)
            return;

        this.lastCutSound = this.phaser.rnd.pick(this.effects["chill/cut"]);
        this.lastCutSound.play();
    }

    public activate(prev: SFX) {
   
        let fadeInDur = 5250;
        setTimeout(() => {
            let soundtrack = this.getSoundtrack();
            soundtrack.play().loop = true;;
            this.fadeVolume(soundtrack, 0, (soundtrack as any).baseVolume, 5250)
        }, this.durationS * 1000 - fadeInDur);
    }
}


