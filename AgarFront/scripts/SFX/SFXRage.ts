﻿
import {SFX, SoundtrackTransition} from "./SFX"


export class SFXRage extends SFX {

    constructor(protected phaser: Phaser.Game, soundtrack: string) {
        super(phaser);
        this.setSoundtrack(soundtrack);
        this.soundtrackTransition = SoundtrackTransition.CUT;
        this.type = "rage";
    }

    public playPlop() {
    }

}


