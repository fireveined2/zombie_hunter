﻿import {SFX} from "./SFX"


export class SFXMenu extends SFX {

    constructor(protected phaser: Phaser.Game) {
        super(phaser);
        this.setSoundtrack("soundtracks/intro");
        this.type = "menu";
    }

}


