﻿
import {SFX} from "./SFX"


export class SFXNormal extends SFX {

    constructor(protected phaser: Phaser.Game) {
        super(phaser);
        this.setSoundtrack("soundtracks/normal");
        this.type = "normal";
    }



}


