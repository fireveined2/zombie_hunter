﻿import {Game} from "../beforeGame/Game"
import {Config} from "../utils/Config"


declare type SoundArray = { [index: string]: Phaser.Sound[] };

export class SoundBase {
    static effects: SoundArray = <any>[];
    private preloadFunctions: Function[] = [];
    private effects: SoundArray = <any>[];
    static instance: SoundBase;
    constructor(public phaser: Phaser.State) {
        SoundBase.instance = this;
    }


    public load() {
        let soundtracksVolume = 0.68;
        this.add("soundtracks/intro", null, soundtracksVolume);

        this.add("soundtracks/normal", null, soundtracksVolume);
        this.add("soundtracks/rage", null, soundtracksVolume + 0.2);

        this.add("map1/soundtracks/normal", null, soundtracksVolume);
        this.add("map1/soundtracks/rage", null, soundtracksVolume + 0.2);
        this.add("map1/soundtracks/ctp", null, soundtracksVolume );
        this.add("map2/soundtracks/normal", null, soundtracksVolume);
        this.add("map2/soundtracks/rage", null, soundtracksVolume + 0.2);
        this.add("map2/soundtracks/trip", null, soundtracksVolume);

        this.add("map3/soundtracks/normal", null, soundtracksVolume-0.1);
        this.add("map3/soundtracks/rage", null, soundtracksVolume + 0.3);


        this.add("cut", null, 1, 4);
        this.add("flesh", "flesh/flesh", 0.35, 8);

        this.add("chill/cut", null, 0.5, 5);
        this.add("chill/flesh", "chill/flesh", 0.25, 7);

        this.add("counting", "counting/counting", 0.6, 3);

        this.add("freeze", null, 0.4);
        this.add("chill");
        this.add("buttons/click", null,0.8, 2);
        this.add("buttons/over", null, 0.5);
        this.add("menu/coffin", null, 0.5);
        this.add("menu/chain", null, 0.5);
        this.add("menu/logo_slide");
        this.add("menu/breath", null, 0.65);

        this.add("drink", null, 1.2);
        

        this.add("countdown");
        this.add("piggy", null, 0.7);

        this.add("slon/playAgain", "slon/a", 1, 3);
        this.add("slon/play", "slon/b", 1, 7);
        this.add("slon/sparta", "slon/c", 1, 4);
        this.add("slon/wellPlayed", "slon/wellPlayed/d", 1, 8);
        this.add("slon/wow", "slon/wellPlayed/s", 1, 16);
        this.add("slon/newRecord", "slon/e", 1, 7);
        this.add("slon/fuckup", "slon/f", 1, 11);

        this.add("slon/foto", "slon/foto/foto", 1, 9);
        this.add("slon/cure", "slon/maps/cure", 1, 3);
        this.add("slon/ctp", "slon/maps/ctp", 1, 3);
        this.add("slon/ctp_mistake", "slon/maps/ctp_mistake", 1, 3);
        this.add("slon/trip", "slon/maps/trip", 1.5, 8);
        this.add("slon/map1", "slon/maps/map1", 1, 4);
        this.add("slon/map2", "slon/maps/map2", 1, 4);

        this.add("slon/tourney", "slon/tourney/Turniej ", 1, 6);
        this.add("slon/improve_score", "slon/tourney/improve_score", 1, 5);

        this.add("slon/explosion", "slon/maps/station/bottle/explosion", 1, 5);
        this.add("slon/napalm", "slon/maps/station/bottle/napalm", 1, 5);
        this.add("slon/hotdog", "slon/maps/station/hotdog/h", 1, 10);


        this.add("slon/rewards/weapon", null, 1, 2);
        this.add("slon/rewards/new_item", null, 1, 3);
        this.add("slon/rewards/corpus", null, 1, 3);
        this.add("slon/rewards/boots", null, 1, 3);
        this.add("slon/rewards/antidotum", null, 1, 4);
        this.add("slon/pig", "slon/maps/pig", 1, 2);
        this.add("slon/dog", "slon/maps/dog", 1, 3);


        this.add("slon/jedziesz");
        this.add("slon/rrr");

        this.add("explosion", null, 1.6);
        this.add("implosion", null, 1.2);
        this.add("ignite", null, 0.8);

        this.add("open_ceil");
        this.add("breaking_glass");
        this.add("burp");
        this.add("drink_and_burp");
    }

    public add(name: string, path: string = name, baseVolume: number = 1, howMany: number = 1) {
        if (path === null)
            path = name;

        let format = ".ogg";
        if (Config.Version.os == "ios")
            format = ".mp3";

        if (howMany == 1)
            this.phaser.load.audio("sfx_" + name + "1", "assets/sfx/" + path + format);
            else
        for (let i = 1; i <= howMany; i++)
            this.phaser.load.audio("sfx_" + name + i, "assets/sfx/" + path + i + format);

        if (this.preloadFunctions.length == 0)
            this.phaser.load.onLoadComplete.addOnce(this.onLoaded, this);

        let loadFnc = () => {
            this.effects[name] = [];
            for (let i = 1; i <= howMany; i++) {
                let sfx: any = this.phaser.add.sound("sfx_"+name + i, baseVolume);
                sfx.baseVolume = baseVolume;
                if (name.indexOf("soundtracks") != -1)
                    sfx.loop = true;
                this.effects[name].push(sfx);
            }
        };

        this.preloadFunctions.push(loadFnc);
    }

    static removeSound(key: string) {
        if (!this.effects[key]) {
            console.error("Cant remove sound " + key);
            return;
        }
     
        this.effects[key][0].destroy(true);
        this.instance.phaser.cache.removeSound("sfx_" + key + "1");
        delete this.effects[key];
    }

    private onLoaded() {
        for (let fnc of this.preloadFunctions)
            fnc();

        this.preloadFunctions = [];
        SoundBase.effects = this.effects;
    }
}
