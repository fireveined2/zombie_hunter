﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { shortStatNames as statNames } from "../utils/gui/CriterionRow"

import { Blade } from "./Blade"
import { FX } from "./FX"
import { SFX, SFXNormal, SFXRage, SFXChill } from "../SFX/SFX"
import { SoundBase } from "../SFX/SoundBase"
import { HUD } from "./hud/HudRender"
import { Replay } from "./Replay"
import { Player } from "../utils/Player"

import { BackgroundAnim } from "./BackgroundAnim"

import { AccesToPlay } from "../utils/Server"
import { GameStarter } from "../beforeGame/GameStarter"
import { GameLogic } from "./logic/Logic"
import { GameFabric } from "./GameFabric"
import { GameEvents } from "./logic/GameEvents"
import * as Models from "../../../Shared/Shared"

export class PvPDisplay {


    private phaser: Phaser.State;

    private criterias: Phaser.BitmapText[] = [];
    private values: Phaser.BitmapText[] = [];

    private statusShadow: Phaser.BitmapText;
    private status: Phaser.BitmapText;
    //public criterionMode: boolean = true;

    constructor(private group: Phaser.Group, public criterionMode = true) {
        this.phaser = group.game.state.getCurrentState();
        this.group = this.phaser.add.group(group);

        this.status = this.phaser.add.bitmapText(Game.width * 0.75, Game.height * 0.02, "medium", "", undefined, this.group);
        this.status.anchor.set(0, 0);
        this.status.scale.set(0.84, 0.84);
        
        this.statusShadow = this.phaser.add.bitmapText(Game.width * 0.75+1, Game.height * 0.02+1, "medium", "", undefined, this.group);
        this.statusShadow.anchor.set(0, 0);
        this.statusShadow.scale.set(0.88, 0.88);
        this.statusShadow.tint = 0;
        this.statusShadow.alpha = 0.2;
    }

    set visible(v: boolean) {
        this.group.visible = v;

    }

    public update(name, timeLeft, score) {
        let txt = "";
        if (timeLeft>0)
           txt = name + "\n" + score + " pkt" + "\nPozostalo: " + timeLeft+"s";
        else
            txt = name + "\n" + score + " pkt";
        this.status.setText(txt);
        this.statusShadow.setText(txt);

    }
}