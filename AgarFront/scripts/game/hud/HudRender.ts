﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"

import { Mug } from "./Mug"
import { Overlay } from "./Overlay"
import { Labels } from "./Labels"
import { Score } from "./Score"
import { SkillBar } from "./SkillBar"


export class HUDInfo {
    getRageLevel: () => number;
    getFreezeLevel: () => number;
    isRageEnabled: () => boolean;
}

export class HUD implements HUDInfo{

    private modeTimer: Phaser.BitmapText;
    private modeTimerName: string;
    private modeTimerEnd: number = 0;

    public score: Score;
    private timer: Phaser.BitmapText;

    private group: Phaser.Group;
    private groupSprites: Phaser.Group;

    private mug: Mug;
    private overlay: Overlay;
    public labels: Labels;
    public skills: SkillBar;

    private time: Phaser.Image;
    private fog: Phaser.Image;



    private surrender: Phaser.Image;
    private rageLevel: number;

    private freezeLevel: number=0;

    public _rageMode: boolean = false;

    private rain: Phaser.Group;
    public onEnableRageMode: Phaser.Signal;
    public onSurrender: Phaser.Signal;

    constructor(private phaser: Phaser.Game) {

        this.group = phaser.add.group();
        this.groupSprites = phaser.add.group();
        Game.group.add(this.groupSprites);

        this.rain = phaser.add.group(this.groupSprites);

        let wall = phaser.add.image(0, Game.height, "wall", 0, this.groupSprites);
        wall.anchor.y = 1;

        this.modeTimer = phaser.add.bitmapText(Game.width / 2, Game.height-38, "medium", "", undefined, this.groupSprites);
        this.modeTimer.anchor.set(0.5)


        this.fog = phaser.add.image(0, Game.height, "map_elements", "mgla", this.groupSprites);
        this.fog.anchor.y = 1;

        this.overlay = new Overlay(this, this.groupSprites);
        this.overlay.bringToTop = this.bringToTop.bind(this);

        //score
        this.score = new Score(this.phaser, this.groupSprites);
        //timer

        this.time = phaser.add.image(15 * Game.assetsSize, Game.height - 15 * Game.assetsSize, "map_elements", "time", this.groupSprites);
        this.time.anchor.y = 1;
        if (Game.settings.side == "left")
            this.time.x = Game.width - 15 * Game.assetsSize - this.time.width;


        let pos = new Phaser.Point(Game.realWidth - 130 / Game.aspectRatio, 15 / Game.aspectRatio);
        this.timer = phaser.add.bitmapText(56 * Game.assetsSize, 32 * Game.assetsSize - this.time.height, "timer", "00", undefined);
        this.timer.scale.set(0.75, 0.75);
        this.timer.anchor.set(0.5)
        this.time.addChild(this.timer);


        this.surrender = phaser.add.image(Game.width - 20, 15, "surrender", 0, this.groupSprites);
        this.surrender.anchor.set(1, 0);
        this.surrender.visible = false;
        this.surrender.inputEnabled = true;
        this.surrender.events.onInputDown.add(() => this._onSurrender());

        this.mug = new Mug(this, this.groupSprites);
        this.mug.onClick.add(() => this.onRageBarClick());
        this.onEnableRageMode = new Phaser.Signal();
        this.onSurrender = new Phaser.Signal();

        this.skills = new SkillBar(this.groupSprites);

        this.labels = new Labels(this.groupSprites);

        
    }

    public isRageEnabled(): boolean {
        return this._rageMode;
    }

    public getRageLevel(): number {
        return this.rageLevel;
    }

    public getFreezeLevel(): number {
        return this.freezeLevel;
    }

    private _onSurrender() {
        console.log("surrr");
        this.onSurrender.dispatch();
    }

    public animTimeChange(seconds) {
        let color = 0X55FF55;
        if (seconds < 0)
            color = 0xFF5555;
        FX.get().animText(73 * Game.assetsSize, Game.height - 90 * Game.assetsSize, seconds, 0.9, color);
    }

    set visible(v: boolean) {
        this.groupSprites.visible = v;
    }

    private sfxPlayed: boolean[] = [];
    public countToStart(callback: Function) {
        this.labels.onFinish = callback;
        this.labels.show("counting");
    }

    private showRageModeLabel() {
        this.labels.show("rage");
    }

    private showChillLabel() {
        this.labels.show("chill");
    }

    
    public enableSirens(enable: boolean) {
        this.overlay.sirenEnabled = enable;
    }

    private onRageBarClick() {
        if (this.freezeLevel > 0)
            return;

        this.onEnableRageMode.dispatch();
    }

    private bringToTop(image: Phaser.Image, top: boolean) {
        if (top)
            this.groupSprites.bringToTop(image);
        else {
            this.groupSprites.bringToTop(this.mug.mug);
            this.groupSprites.bringToTop(this.time);
        }

    }

    public changeFreeze(value) {
        if (this.freezeLevel < 0.01 && value > 0.01)
            this.showChillLabel();
        this.freezeLevel = value;
    }


    public enableRageMode(enable: boolean) {
        this._rageMode = enable
        this.skills.enable(!enable);
        if (enable) {
            this.showRageModeLabel();
            FX.get().rain(this.rain, 5);
        }

        if (!enable) {
            FX.get().stopRain();
        }
    }

    public starModeTimer(name: string, duration: number) {
        this.modeTimerName = name;
        this.modeTimerEnd = Date.now() + duration * 1000;
        if (duration == 0)
            this.modeTimerEnd = -Date.now();
    }

    public removeModeTimer() {
        this.modeTimerEnd = 0;
    }


    private secondsToTime(time: number): string {
        let minutes = Math.floor(time / 60).toString();
        let seconds = Math.floor(time).toString();
        if (seconds.length == 1) seconds = '0' + seconds;
        if (minutes.length == 1) minutes = '0' + minutes;
        return seconds.toString();
    }

    public updateRageLevel(level: number) {
        this.rageLevel = level;
    }


    public secondsLeft = 0;
    public updateTime(seconds: number) {
        this.secondsLeft = seconds;
        this.timer.setText(this.secondsToTime(Math.max(seconds, 0)));

        if (seconds < 5 && !SFX.get().getSound("countdown").isPlaying && seconds>1) 
            SFX.get().playSingleEffect("countdown");

        if (seconds <= 0)
            this.timer.visible = (Math.round(Date.now() / 400)%2)==0;
    }

    public update(frame: number) {
        this.phaser.world.bringToTop(this.group);
        Game.group.bringToTop(this.groupSprites);


        this.overlay.update(frame);
        this.mug.update(frame);

        this.fog.alpha = Math.sin(frame / 25) * 0.25 + 0.25;
        this.surrender.visible = Game.absence.timeSinceLastActivity() > 5000 && this.secondsLeft < 70;

        
        let modeLeft = this.modeTimerEnd - Date.now();

        // czas trwania
        if (this.modeTimerEnd < 0)
            modeLeft = Date.now() + this.modeTimerEnd;

        if (modeLeft > 0)
            this.modeTimer.setText(this.modeTimerName + ": " + this.secondsToTime(Math.ceil(modeLeft / 1000)));
        else
            this.modeTimer.setText("");
    }


}
