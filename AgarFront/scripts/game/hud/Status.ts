﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"

import { Mug } from "./Mug"
import { Overlay } from "./Overlay"
import { Labels } from "./Labels"



export class Status  {

    private bitmap: Phaser.BitmapText;
    public data: any = {};
    private text: string = "";

    constructor(private phaser: Phaser.Game, private group: Phaser.Group) {
        this.bitmap = phaser.add.bitmapText(Game.width / 2, Game.height - 38, "medium", "", undefined, this.group);
        this.bitmap.anchor.set(0.5)
        
    }


    public setText(txt: string) {
        this.text = txt;

        this.update();
    }




    public update() {
        let txt = this.text.replace("$time", this.data.time);
        txt = txt.replace("$score", this.data.score);
        this.bitmap.setText(txt);
    }
 
}
