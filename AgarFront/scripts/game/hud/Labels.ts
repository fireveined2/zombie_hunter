﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"


export class Labels{
    private group: Phaser.Group;
    private label: Phaser.Image;

    public onFinish: Function;

    constructor(group: Phaser.Group, private phaser: Phaser.State = group.game.state.getCurrentState()) {
        this.group = group;

        this.label = phaser.add.image(Game.width / 2, Game.height / 2, "chill_label", "chill002", this.group);
        this.label.anchor.set(0.5);
        this.label.visible = false;
    }

   
    public show(type: "chill" | "rage" | "counting" | "prison_break" | "trip") {
        this.label.visible = true;
        switch (type) {
            case "counting":
                this.label.loadTexture("counting");
                this.countToStart();
                break;

            case "chill":
                this.label.loadTexture("chill_label");
                if (!Game.disableAnims)
                    this.showChillLabel();
                else
                    this.showSimpleLabel(600);
                break;

            case "rage":
                this.label.loadTexture("rage_label");
                if (!Game.disableAnims)
                    this.showRageModeLabel();
                else
                    this.showSimpleLabel();
                break;
            
            case "prison_break":
                this.label.loadTexture("map1/cut_the_police");
                this.showSimpleLabel();
                break;

            case "trip":
                this.label.visible = false;
          //      this.label.loadTexture("trip_label");
                this.showTripLabel();
                break;
        }
    }

    private _onFinish() {
        if (this.onFinish)
            this.onFinish();

        delete this.onFinish;
    }

    private sfxPlayed: boolean[] = [];
    private countToStart( time = Date.now()) {
        let now = Date.now();
        let elapsed = now - time;
        let frame = Math.ceil(elapsed / 3850 * 76);
        let gfxFrame = frame;
        if (Game.disableAnims) {
            gfxFrame = Math.ceil(frame/26);
          }

        this.label.frameName = "time00" + gfxFrame;
        if (frame >= 76) {
            SFX.get().playSingleEffect("slon/jedziesz");
            this._onFinish();
            this.label.visible = false;
            return;
        }

        if (frame > 1 && !this.sfxPlayed[0])
            SFX.get().playCounting(2), this.sfxPlayed[0] = true;

        if (frame > 26 && !this.sfxPlayed[1])
            SFX.get().playCounting(1), this.sfxPlayed[1] = true;

        if (frame > 52 && !this.sfxPlayed[2])
            SFX.get().playCounting(0), this.sfxPlayed[2] = true;

        setTimeout(() => this.countToStart(time), 10);
    }


    private showRageModeLabel(time = Date.now()) {
        this.label.visible = true;
        let now = Date.now();
        let elapsed = now - time;
        let frame = Math.ceil(elapsed / (8 * 55) * 8 + 0.1) + 1;
        this.label.bringToTop();
        this.label.frameName = "RM000" + frame;
        if (frame >= 9) {
            setTimeout(() => this.label.visible = false, 450);
            return;
        }
        setTimeout(() => this.showRageModeLabel(time), 10);
    }


    private showSimpleLabel(duration: number = 520, time = Date.now()) {
        this.label.visible = true;
        let now = Date.now();
        let elapsed = now - time;
        let progress = elapsed / duration;
        let frame = Math.ceil(elapsed / (8 * 55) * 8 + 0.1) + 1;
        this.label.bringToTop();

        this.label.alpha = Math.min(progress, 1);
        this.label.scale.set(Math.min(progress, 1));
        if (progress >= 1) {
            setTimeout(() => this.label.visible = false, 400);
            return;
        }
        setTimeout(() => this.showSimpleLabel(duration, time), 10);
    }

    private showPrisonBreakLabel(time = Date.now()) {
        this.label.visible = true;
        let now = Date.now();
        let elapsed = now - time;
        let progress = elapsed /500;
        let frame = Math.ceil(elapsed / (8 * 55) * 8 + 0.1) + 1;
        this.label.bringToTop();

        this.label.alpha = Math.min(progress, 1);
        this.label.scale.set(Math.min(progress, 1));
        if (progress >= 1) {
            setTimeout(() => this.label.visible = false, 850);
            return;
        }
        setTimeout(() => this.showPrisonBreakLabel(time), 10);
    }

    private mode: Phaser.Image;
    private trip: Phaser.Image;
     
    private showTripLabel(time = Date.now()) {
        if (!this.mode) {
            this.mode = this.phaser.add.image(Game.width / 2, Game.height * 0.4, "trip_label", "mode_1", this.group);
            this.mode.anchor.set(0.5);
            this.trip = this.phaser.add.image(Game.width / 2, Game.height * 0.6, "trip_label", "trip_1", this.group);
            this.trip.anchor.set(0.5);
        }

        let now = Date.now();
        let elapsed = now - time;
        let progress = elapsed / (30 * 100);
        let frame = Math.round(progress * 30);
        frame = frame % 20;

          this.mode.position.set(Game.width / 2, Game.height * 0.4);
        this.trip.position.set(Game.width / 2, Game.height * 0.6);

        let f = frame % 2;
        this.mode.tint = (f) ? 0 : 0xFFFFFF;
        this.trip.tint = (f) ? 0xFFFFFF : 0;
        this.mode.frameName = "mode_" + (f + 1);
        this.trip.frameName = "trip_" + (f + 1);

        let alpha = Math.min(progress * 4, 1);
        let scale = Math.min(progress * 5, 1);
        if (progress > 0.8) {
            alpha = (1 - progress) * 5;
            scale = (progress) * 1, 25;
        }
        this.mode.alpha = this.trip.alpha =alpha;
        this.mode.scale.set(scale);
        this.trip.scale.set(scale);

        this.trip.angle = this.mode.angle = 0;

        if (frame == 3) {
            this.mode.scale.set(2.5);
            this.mode.alpha = 0.6;
            this.mode.angle = 50;
            this.mode.x -= 80;
        }

        if (frame == 9) {
            this.trip.scale.set(2.9);
            this.trip.alpha = 0.4;
            this.trip.angle = -30;
            this.trip.y += 80;
        }


        if (frame == 13) {
            this.mode.scale.set(1.7);
            this.mode.alpha = 0.8;
            this.mode.angle = 20;
            this.mode.x+= 80;
        }

        if (frame == 19) {
            this.trip.scale.set(2.9);
            this.trip.alpha = 0.3;
            this.trip.angle = -40;
            this.trip.x += 80;
        }

        this.mode.bringToTop();
        this.trip.bringToTop();

        if (progress >= 1) {
            setTimeout(() => { this.mode.alpha = 0, this.trip.alpha=0 }, 40);
            return;
        }
        setTimeout(() => this.showTripLabel(time), 10);
    }

    private showChillLabel(time = Date.now()) {
        this.label.visible = true;
        let now = Date.now();
        let elapsed = now - time;
        let frame = Math.ceil(elapsed / (39 * 25) * 39 + 0.1) + 1;
        this.label.bringToTop();
        this.label.frameName = "chill00" + frame;
        if (frame >= 40) {
            setTimeout(() => this.label.visible = false, 150);
            return;
        }
        setTimeout(() => this.showChillLabel(time), 10);
    }
}
