﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"

import { Mug } from "./Mug"
import { Overlay } from "./Overlay"
import { Labels } from "./Labels"
import * as GameModels from "../logic/Models"




class SkillIcon {
    private image: Phaser.Image;
    private dragImage: Phaser.Image;
    private phaser: Phaser.State;
    public onUsed: (id: number, data: GameModels.SkillUseData) => boolean;
    public onRemoved: Function;

    public onDragStart: Function;
    public onDragStop: Function;


    private enabled: boolean = true;

    constructor(group: Phaser.Group, public skill: GameModels.ISkill) {
        this.phaser = group.game.state.getCurrentState();
        this.image = this.phaser.add.image(0, 0, "skills/icon/"  + skill.icon, 0, group);
        this.image.anchor.set(0.5);
        this.image.inputEnabled = true;
       

        if (skill.castInfo.type == GameModels.SkillCastType.DRAGABLE) {
            this.image.input.enableDrag();
            this.image.events.onDragStop.add(() => this._onDragStop());
            this.image.events.onDragStart.add(() => { if (this.onDragStart) this.onDragStart() });
            this.dragImage = this.phaser.add.image(0, 0, "skills/icon/" + skill.icon, 0, group);
            this.dragImage.anchor.set(0.5);
            this.dragImage.alpha = 0.3;
        }

        if (skill.castInfo.type == GameModels.SkillCastType.SIMPLE) {
            this.image.events.onInputDown.add(() => this.onUsed(this.skill.id, {}));
        }
    }


    public enable(enable: boolean) {
        this.enabled = enable;
        if (!enable)
            this.image.input.disableDrag();
        else
            this.image.input.enableDrag();
    }
     
    private _onDragStop() {
        if (!this.enabled)
            return;

        if (this.onDragStop)
            this.onDragStop();

        let draggedBelowWall = this.image.position.y > Game.height - 70;
        if (draggedBelowWall) {
            this.cancelDrag();
            return;
        }

        if (!this.onUsed(this.skill.id, { x: this.image.x, y: this.image.y })) {
            this.cancelDrag();
            return;
        }

        if (this.skill.castInfo.removeAfterUse) {
            this.remove();
            return;
        }

        this.cancelDrag();
    }



    public remove() {
        this.image.destroy();
        this.dragImage.destroy();

        if (this.onRemoved)
            this.onRemoved(this);
    }

    private cancelDrag() {
        this.image.position.set(this.dragImage.x, this.dragImage.y);
    }

    public setPosition(x: number, y: number) {
        this.image.position.set(x, y);
        if (this.dragImage)
            this.dragImage.position.set(x, y);
    }

    public update() {
        this.dragImage.angle = Math.sin(Date.now()/700)*4;
    }
}



export class SkillBar  {

    private skills: SkillIcon[] = [];
    private group: Phaser.Group;
    private phaser: Phaser.State;
    public onSkillCasted: (id: number, data: GameModels.SkillUseData) => boolean;

    public onDragStart: Function;
    public onDragStop: Function;
    private enabled: boolean = true;

    constructor(group: Phaser.Group) {
        this.phaser = group.game.state.getCurrentState();
        this.group = this.phaser.add.group(group);
    }

    private updatePositions() {
        for (let i = 0; i < this.skills.length; i++) 
            this.skills[i].setPosition(Game.width - 220 - 80 * i, Game.height-50);
    }

    private removeSkillFromBar(skill: SkillIcon) {
        for (let i = 0; i < this.skills.length; i++)
            if (this.skills[i] == skill)
                this.skills.splice(i, 1);

        this.updatePositions();
    }



    public addSkill(skill: GameModels.ISkill) {
        if (this.skills.length > 1)
            return false;
   
        let entry = new SkillIcon(this.group, skill);
        entry.onUsed = this.onSkillCasted;
        entry.onRemoved = this.removeSkillFromBar.bind(this);
        entry.onDragStart = this.onDragStart;
        entry.onDragStop = this.onDragStop;
        entry.enable(this.enabled);
        this.skills.push(entry);
        this.updatePositions();
        return true;
    }

    public enable(enable: boolean) {
        for (let skill of this.skills)
            skill.enable(enable);
        this.enabled = enable;
    }

    public update() {
        for (let skill of this.skills)
            skill.update();
    }
}
