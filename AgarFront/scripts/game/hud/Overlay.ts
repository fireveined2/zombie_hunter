﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"
import { HUDInfo } from "./HUDRender"

export class Overlay {

    private group: Phaser.Group;
    private overlay: Phaser.Image;

    private freeze: number = 0;
    private rage: number = 0;

    public sirenEnabled: boolean = false;

    public bringToTop: (image: Phaser.Image, top: boolean) => void;

    constructor(private hud: HUDInfo, group: Phaser.Group, private phaser: Phaser.Game = group.game) {
        this.group = group;

        this.overlay = phaser.add.image(0, 0, "overlay", 0, this.group);
        this.overlay.alpha = 0;
        this.overlay.blendMode = PIXI.blendModes.OVERLAY;
        this.overlay.width = Game.width;
        this.overlay.height = Game.height;
        this.overlay.tint = 0xFF3333;
    }



    private overlayOnTop: boolean = false;
    private bringOverlayToTop(top: boolean = true) {
        if (top && !this.overlayOnTop)
            this.bringToTop(this.overlay, true);

        if (!top && this.overlayOnTop) {
            this.bringToTop(this.overlay, false);
        }

        this.overlayOnTop = top;
    }

    public update(frame: number) {
        let hudFreeze = this.hud.getFreezeLevel();
        let hudRage = this.hud.getRageLevel();

        if (hudFreeze > this.freeze + 0.1) {
            this.freeze += 1 * Config.Global.deltaGraphics;
            this.rage = 0;
        }
        if (hudFreeze < this.freeze)
            this.freeze -= 1 * Config.Global.deltaGraphics


        if (this.hud.isRageEnabled())
            hudRage = 2.5;

        if (hudRage > this.rage)
            this.rage += 0.4 * Config.Global.deltaGraphics;
        if (hudRage < this.rage)
            this.rage -= 0.4 * Config.Global.deltaGraphics;
        if (hudRage < 1 && this.rage > 1)
            this.rage -= 0.7 * Config.Global.deltaGraphics;
        if (hudRage > 0.9 && this.rage < 1)
            this.rage += 2.7 * Config.Global.delta;

        let rage = this.rage * 0.5;
        rage = rage * 0.82 + rage * 0.18 * Math.sin(frame / 4);
        rage *= 0.8;
        rage = Math.min(rage, 1);
        this.overlay.alpha = rage;

        if (this.freeze > 0.0) {
            this.overlay.alpha = this.freeze * 0.9;
            this.bringOverlayToTop(true);
        }
        else
            this.bringOverlayToTop(false);

        this.overlay.tint = Phaser.Color.getColor(255, Math.min(255, 50 + this.freeze * 700), Math.min(255, 50 + this.freeze * 700));


        if (this.sirenEnabled) {
            let frame = Date.now() / 150;
            let sin = Math.sin(frame);
            if(sin>0)
                this.overlay.tint = Phaser.Color.getColor(55, 55, 255);
            else
                this.overlay.tint = Phaser.Color.getColor(255, 55, 55);
                this.overlay.alpha = Math.abs(sin)*0.5;
        }

    }
}
