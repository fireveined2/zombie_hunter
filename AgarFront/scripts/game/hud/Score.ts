﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"

import { Mug } from "./Mug"
import { Overlay } from "./Overlay"
import { Labels } from "./Labels"



export class Score  {

    private score: Phaser.BitmapText;
    private cumulatedPoints: Phaser.BitmapText;
    private cumulated: number = -1;
    private cumulate: boolean = false;
    private cumulateLabel: string= "";


    constructor(private phaser: Phaser.Game, private group: Phaser.Group) {
        this.score = phaser.add.bitmapText(33 * Game.assetsSize, 35 * Game.assetsSize, "medium", "PKT: 0", undefined, this.group);
        this.score.anchor.y = 0.5;

        this.cumulatedPoints = phaser.add.bitmapText(92 * Game.assetsSize, 65 * Game.assetsSize, "medium", "", undefined, this.group);
        this.cumulatedPoints.anchor.y = 0.5;
    //    this.cumulatedPoints.scale.set(0.8, 0.8 );
    }

  

    public animLostPoints(points) {
        FX.get().score(73 * Game.assetsSize, 52 * Game.assetsSize, -points, 0.57);
    }

     
    private points = 0;
    public updatePoints(points: number): number {
        let diff = points - this.points;

        if (this.cumulate) {
            diff = points - (this.points + this.cumulated);
            if (this.cumulatedPoints.scale.x < 0.5)
                this.cumulatedPoints.scale.set(1,1);

            this.cumulated += diff;
            this.score.setText("PKT: " + Math.round(this.points).toString() + " / " + Math.round(this.cumulated).toString());
          //  FX.get().score(109 * Game.assetsSize, 94 * Game.assetsSize, diff, 0.15, false, true);
            return diff;
        }
        this.points = points - this.cumulated;

        if (diff < 0)
            this.animLostPoints(diff);
        this.score.setText("PKT: " + Math.round(points).toString());
        return diff;
    }

    public cumulatePoints(cumulate: boolean, label?: string) {

      //  if(label)
          //  this.cumulateLabel = label;

        if (!cumulate && this.cumulate) {
            this.transferCumulatedPoints();
        }
        this.cumulate = cumulate;

    }

    private transferCumulatedPoints(val = this.cumulated * 0.025) {
        let duration = 1000;

        let func = () => {
            val = Math.min(val, this.cumulated);
            this.cumulated -= val;
            this.points += val;

            this.score.setText("PKT: " + Math.round(this.points).toString() + " / " + Math.round(this.cumulated).toString());


            if (this.cumulated > 0)
                setTimeout(func, 50);
            else {
                this.cumulated = 0;
                this.score.setText("PKT: " + Math.round(this.points).toString());
            }
        }
        
        func();
    }


    public update(frame: number) {
     
    }


}
