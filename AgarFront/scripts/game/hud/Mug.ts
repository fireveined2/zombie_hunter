﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { FX } from "../FX"
import { SFX } from "../../SFX/SFX"
import { HUDInfo } from "./HUDRender"

export class Mug {
    private group: Phaser.Group;

    public mug: Phaser.Image;
    private mugBlood: Phaser.Image;
    private mugBloodMask: Phaser.Graphics;
    private mugGlow: Phaser.Image;
    private mugFrozen: Phaser.Image;
    private mugFront: Phaser.Image;
    private mugBloodMaskPolygon;


    private level = 0;

    public onClick: Phaser.Signal;


    constructor(private hud: HUDInfo, group: Phaser.Group, private phaser: Phaser.Game = group.game) {

        this.group = group;
        this.mugBloodMaskPolygon = new Phaser.Polygon([new Phaser.Point(0, -5),
            new Phaser.Point(2 / Game.aspectRatio, 67 / Game.aspectRatio),
            new Phaser.Point(13 / Game.aspectRatio, 94 / Game.aspectRatio),
            new Phaser.Point(56 / Game.aspectRatio, 94 / Game.aspectRatio),
            new Phaser.Point(60 / Game.aspectRatio, 67 / Game.aspectRatio),
            new Phaser.Point(60 / Game.aspectRatio, -5 / Game.aspectRatio)]);


        this.mug = phaser.add.image(Game.width - 104 * Game.assetsSize, Game.height - 128 * Game.assetsSize, "map_elements", "mug_back", this.group);
        this.mug.inputEnabled = true;
        this.mug.events.onInputDown.add(() => { console.log("Mug clicked"); if (this.hud.getRageLevel() == 1) { this.onClick.dispatch();  } }, this);

        if (Game.settings.side == "left")
            this.mug.x = 104 * Game.assetsSize - this.mug.width;

        this.mugGlow = phaser.add.image(-32, -24, "map_elements", "mug_glow");
        this.mugGlow.anchor.set(0.5);
        this.mugGlow.x = -28 + this.mugGlow.width / 2;
        this.mugGlow.y = -24 + this.mugGlow.height / 2;
        this.mug.addChild(this.mugGlow);

        let mugBlack = phaser.add.image(-20, 20, "map_elements", "mug_black");
        this.mug.addChild(mugBlack);

        this.mugBlood = phaser.add.image(3, 4, "map_elements", "mug_blood");
        this.mug.addChild(this.mugBlood);

        this.mugBloodMask = phaser.add.graphics(0, 0);
        this.mugBloodMask.beginFill(0xffffff);
        this.mugBloodMask.drawRect(0, 0, 60, 21);
        this.mugBlood.mask = this.mugBloodMask;

        this.mugFront = phaser.add.image(0, 12, "map_elements", "mug_front");
       this.mug.addChild(this.mugFront);

        this.mugFrozen = phaser.add.image(0, 0, "mug_frozen");
        this.mug.addChild(this.mugFrozen);
        this.mugFrozen.alpha = 0;



        this.onClick = new Phaser.Signal();
    }

    public remove() {
        this.onClick.removeAll();
    }

  
   

    public getLevel(): number {
        return this.level;
    }

    public update(frame: number) {
        if (this.hud.getRageLevel() == 1) {
            if (!this.mugBlood.filters ) {
                this.mugBlood.filters = [FX.glowFilter];
                this.mugGlow.scale.setTo(1.2);
                this.mugGlow.filters = [FX.glowFilter];
            }
        }
        else
            this.mugBlood.filters = null;

        if (this.hud.getRageLevel() > this.level)
            this.level += 0.3 * Config.Global.deltaGraphics;
        if (this.hud.getRageLevel() < this.level)
            this.level -= 0.3 * Config.Global.deltaGraphics;

        this.mugBlood.y = 98 - (21 + 74 * Math.min(this.level, 1));
        this.mugGlow.alpha = Math.abs(Math.sin(frame / 15)) * Math.min(this.level, 1) * 0.4 + Math.min(this.level, 1) * 0.6 * (1 - this.hud.getFreezeLevel());;
        this.mugGlow.scale.set(0.7 + 0.6 * this.mugGlow.alpha);
//        console.log(this.mugGlow.scale.x);
        this.mugFrozen.alpha = this.hud.getFreezeLevel() * 0.9;

        this.mugBloodMask.clear();
        this.mugBloodMask.beginFill(0xffffff);
        this.mugBloodMask.x = this.mugFront.worldPosition.x;
        this.mugBloodMask.y = this.mugFront.worldPosition.y;
        this.mugBloodMask.drawPolygon(this.mugBloodMaskPolygon);
        this.mugBloodMask.endFill();


    }
}
