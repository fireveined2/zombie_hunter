﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { FX } from "./FX"




export class BackgroundAnim {

    private group: Phaser.Group;
    private background: Phaser.Image;

    private zombieHand: Phaser.Image;
    private zombieArm: Phaser.Image;
    private zombieHead: Phaser.Image;
    private zombieBody: Phaser.Image;


    private door: Phaser.Image;
    private ceil: Phaser.Image;
    private zombie: Phaser.Image;

    private fog: Phaser.Image;
    private greenOverlay: Phaser.Image;

    private wires: Phaser.Image;
    private neon: Phaser.Image;


    public speed = 1;


    constructor(private phaser: Phaser.State, private map: number, arenaBg: boolean = false) {
        this.group = phaser.add.group(Game.group);

        if (!arenaBg)
        this.background = phaser.add.image(0, 0, "background" + map, 0, this.group);
        else
            this.background = phaser.add.image(0, 0, "arena/bg", 0, this.group);
        if (!arenaBg) {

            if (map == 0) {
                this.zombieBody = phaser.add.image(450, 190, "map_elements", "body", this.group);
                this.zombieHand = phaser.add.image(398, 184, "map_elements", "fingers001", this.group);
                this.zombieHead = phaser.add.image(538, 125, "map_elements", "head001", this.group);
                this.zombieArm = phaser.add.image(670, 230, "map_elements", "hand", this.group);
                this.zombieArm.anchor.x = 0.5;
                this.animHand(1);
                this.animHead(1);
            }

            if (map == 1) {
                this.door = phaser.add.image(25, 54, "map1/elements", "drzwi0010", this.group);
                this.animDoor(10);

                this.ceil = phaser.add.image(Game.width + 8, Game.height / 2, "map1/elements", "krata", this.group);
                this.ceil.anchor.set(1, 0.5);

                //  this.ceil = phaser.add.image(Game.width + 8, Game.height / 2, "map1/elements", "zo", this.group);
                //  this.ceil.anchor.set(1, 0.5);
            }

            if (map == 2) {
                this.background.anchor.set(1, 0.5);
                this.background.position.set(Game.width, Game.height / 2);

                this.neon = phaser.add.image(this.background.left + 147, this.background.top - 5, "map2/elements", "neon_off", this.group);
                this.animNeon(1);
                this.neon.scale.set(1 / 0.6);

                this.door = phaser.add.image(this.background.left + 241, this.background.top + 122, "map2/elements", "drzwi0001", this.group);
                this.animDoorHospital(1);
                this.door.scale.set(1 / 0.6);

            }


            if (map == 3) {


                this.neon = phaser.add.image(this.background.left + 4, this.background.top + 80, "map3/light", 0, this.group);
                this.animStationLight(1);

                this.wires = phaser.add.image(this.background.left + 380, this.background.top + 170, "map3/elements", "kable001", this.group);
                this.animWires(1);
            }
        }
    }

    public openCeil(open: boolean) {
        return this.phaser.add.tween(this.ceil.anchor).to({ x: (+(!open)*1) }, 1000).start();
    }

    private doorAnimHandle;
    private handAnimHandle;
    private headAnimHandle;

    private animHand(frame) {
        if (frame == 24) {
            this.handAnimHandle = setTimeout(() => this.animHand(1), Math.random()*4000+700);
            return;
        }

        this.zombieHand.frameName = "fingers00" + frame;
        this.handAnimHandle = setTimeout(() => this.animHand(frame + 1), 45 / Math.max(this.speed, 0.7));
    }

    private animWires(frame) {
        if (frame == 80) {
            this.handAnimHandle = setTimeout(() => this.animWires(1),45);
            return;
        }

        this.wires.frameName = "kable00" + frame;
        this.handAnimHandle = setTimeout(() => this.animWires(frame + 1), 45 / Math.max(this.speed, 0.7));
    }

    private animHead(frame, backward = false) {
        if (frame == 24) {
            setTimeout(() => this.animHead(22, true), 40);
            return;
        }

        if (frame == 0) {
            this.headAnimHandle = setTimeout(() => this.animHead(1, false), Math.random() * 4000 + 700);
            return;
        }

        this.zombieHead.frameName = "head00" + frame;
        this.headAnimHandle = setTimeout(() => this.animHead(backward ? frame - 1 : frame + 1, backward), 40 / Math.max(this.speed, 0.7));
    }


    private animDoor(frame) {
        if (frame == 29) {
            this.doorAnimHandle = setTimeout(() => this.animDoor(10), Math.random() * 4000 + 700);
            return;
        }

        this.door.frameName = "drzwi00" + frame;
        this.doorAnimHandle = setTimeout(() => this.animDoor(frame + 1), 45 / Math.max(this.speed, 0.7));
    }

    private animDoorHospital(frame) {
        if (frame == 40) {
            this.doorAnimHandle = setTimeout(() => this.animDoorHospital(1),50);
            return;
        }
        if(frame<10)
            this.door.frameName = "drzwi000" + frame;
        else
            this.door.frameName = "drzwi00" + frame;
        this.doorAnimHandle = setTimeout(() => this.animDoorHospital(frame + 1), 50 / Math.max(this.speed, 0.7));
    }

    private neonTimeout: any;
    private animNeon(frame: number) {
        if (frame == 30) {
            this.neonTimeout = setTimeout(() => this.animNeon(1), 2000 + Math.random()*3000);
            return;
        }
        this.neon.frameName = "neon_off";

        if (frame < 3 || (frame > 7 && frame < 13) || (frame > 18 && frame <21))
            this.neon.frameName = "neon_on";


        this.neonTimeout = setTimeout(() => this.animNeon(frame + 1), 50 / Math.max(this.speed, 0.7));
    }


    private animStationLight(frame: number) {
        if (frame == 30) {
            this.neonTimeout = setTimeout(() => this.animStationLight(1), 2000 + Math.random() * 3000);
            return;
        }
        this.neon.alpha = 0;

        if (frame < 3 || (frame > 7 && frame < 13) || (frame > 18 && frame < 21))
            this.neon.alpha = 1;


        this.neonTimeout = setTimeout(() => this.animStationLight(frame + 1), 50 / Math.max(this.speed, 0.7));
    }
    private handFrame = 0;
    public update(elapsed) {
        if (this.map == 0 && this.zombieArm) {
            
            this.handFrame += elapsed * this.speed;
            this.zombieArm.angle = Math.sin(this.handFrame / 30) * 30;
        }
    }

    public remove() {
        clearTimeout(this.headAnimHandle);
        clearTimeout(this.handAnimHandle);
        clearTimeout(this.doorAnimHandle);
        clearTimeout(this.neonTimeout);
    }
}
