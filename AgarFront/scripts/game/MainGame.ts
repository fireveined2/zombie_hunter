﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"

import { Blade } from "./Blade"
import { FX } from "./FX"
import { SFX, SFXNormal, SFXRage, SFXChill } from "../SFX/SFX"
import { SoundBase } from "../SFX/SoundBase"
import { HUD } from "./hud/HudRender"
import { Replay } from "./Replay"
import { Player } from "../utils/Player"
import { Challenges } from "../utils/Challenges"

import { BackgroundAnim } from "./BackgroundAnim"

import { Status } from "./CornerStatus"
import { socketClient } from "../utils/server/Socket"
import { AccesToPlay } from "../utils/Server"
import { GameStarter } from "../beforeGame/GameStarter"
import { GameLogic } from "./logic/Logic"
import { GameFabric } from "./GameFabric"
import { GameEvents } from "./logic/GameEvents"
import * as Models from "../../../Shared/Shared"
import * as GameModels from "./logic/Models"
import { PvPDisplay } from "./PvPDisplay"

export class MainGame extends Phaser.State {


    private logic: GameLogic;
    private group: Phaser.Group;
    private spritePool: Phaser.Image[];
    private HUD: HUD;
    private replay: Replay;
    private bg: BackgroundAnim;
    private fabric: GameFabric;
    private acces: AccesToPlay;
    private ready: boolean = false;
    private status: Status;

    private pvp: boolean;
    private pvpUpdateInterval: any;
    private score: number = 0;

    public getFreeSprite(key: any): Phaser.Image {
        for (let sprite of this.spritePool)
            if (!sprite.visible) {
                sprite.visible = true;
                sprite.alpha = 1;
                sprite.loadTexture(key);
                return sprite;
            }

        let sprite = this.add.image(0, 0, key, 0, this.group);
        this.spritePool.push(sprite);
        return sprite;
    }

    public preload() {


    }
    private arena = false;


    public init(acces: AccesToPlay, pvp: boolean) {
        this.pvp = pvp;
        if (pvp)
        this.pvpUpdateInterval = setInterval(() => {
            socketClient.pvp.sendUpdate(this.score, Math.round(this.HUD.secondsLeft));
        }, 1000);

        Game.backButtonCallback = () => { };
        this.acces = acces;

        //  this.replay = new Replay(null);
        if (this.replay) {

            this.acces.config = JSON.parse(`{ "id": 4,
  "seed": 74663,
  "duration": 60,
  "map": 2,
  "bonus": { "negativElementPercent": 2.9 } } `);
            Game.height = 468.75;
        }

        this.ready = true;
        Game.instance.resize(1);

        this.spritePool = [];



        SFX.prefix = "map" + this.acces.config.map;



        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        let arena = false;
        if (acces.challengeId) {
            let challenge = Challenges.get(acces.challengeId);
            if (challenge && challenge.data.type == Models.ChallengeType.TOURNEY)
            arena = true;
        }
        this.arena = arena;
        this.bg = new BackgroundAnim(this, this.acces.config.map, arena);
        new FX(this.game);
        FX.get().initPool(50);




        let fabric = this.fabric = new GameFabric(this.game, this.group, this.getFreeSprite.bind(this));


        this.logic = new GameLogic(fabric, Game.width, Game.height);
        this.HUD = new HUD(this.game);

        let pvpDisplay: PvPDisplay;
        if (this.pvp)
            pvpDisplay = new PvPDisplay(this.group);

        let training = !acces.challengeId;
        let status = this.status = new Status(this.group, !training);
        let crits = [];
        let stats;
        if (training)
            crits = [{ stat: "exp", value: 500 }, { stat: "respect", value: 100 }];
        else {
            crits = [{ stat: "cutFlesh", value: 500 }, { stat: "biggestCut", value: 100 }];
            //  let challenge = Challenges.get(acces.challengeId);
            //  crits = challenge.data.criteria;
            //    status.sumStatsWith = challenge.prevStats ? challenge.prevStats : <any>{};
        }
        this.logic.onUpdate = (now, elapsed) => {

            if (!this.pvp) {
                let stats = <any>{};
                if (!training)
                    stats = GameLogic.stats;

                else {
                    stats.exp = GameLogic.stats.cutFlesh;
                    let hp = 1;
                    stats.respect = Math.round(Math.round(GameLogic.stats.score / 1000) * Math.min(Player.get().data.hp + 0.2, 1));
                }

                status.updateStats(crits as any, stats);
                status.update(elapsed);
            } else {
                pvpDisplay.update(socketClient.pvp.enemy.name, socketClient.pvp.enemy.timeLeft, socketClient.pvp.enemy.score);

            }

            if (this.replay && this.logic.gameRunning)
                this.replay.update(now);

            this.HUD.update(now);
            FX.get().update(elapsed);
            this.bg.update(elapsed);
        }

        this.HUD.onEnableRageMode.add(() => GameEvents.emit("userEnabledRage"));
        this.HUD.onSurrender.add(() => GameEvents.emit("userSurrender"));




        // 
        GameEvents.on("durationChanged", (seconds) => {
            this.HUD.animTimeChange(seconds);
            if (seconds > 0 && SFX.get().getSound("countdown").isPlaying)
                SFX.get().getSound("countdown").stop();

            if (seconds < 0) {
                if (this.acces.config.map == Models.Map.POLICE)
                    SFX.get().playSlon("slon/dog");

                if (this.acces.config.map == Models.Map.STATION)
                    SFX.get().playSlon("slon/hotdog");
            }
        });


        GameEvents.on("time", this.HUD.updateTime.bind(this.HUD));
        GameEvents.on("updatedRageLevel", this.HUD.updateRageLevel.bind(this.HUD));
        GameEvents.on("updatedFreezeLevel", (level) => {
            this.HUD.changeFreeze(level);
            this.bg.speed = 1 - level;
        });

        GameEvents.on("mode.rage", (frame, duration) => {
            this.HUD.score.cumulatePoints(true, "Rage Mode");
            SFX.get().playSingleEffect("drink_and_burp");

            SFX.get().playSlonSparta();
            SFX.setSFX(new SFXRage(Game.group.game, "soundtracks/rage"));

            Game.inputCollector.push("rage", frame);
            this.HUD.enableRageMode(true);
            if (this.acces.config.map == Models.Map.STATION)
                this.HUD.starModeTimer("Rage", Config.framesToSeconds(duration));
            else
                this.HUD.starModeTimer("Rage Mode", Config.framesToSeconds(duration));
        })

        GameEvents.on("mode.normal", () => {
            SFX.setSFX(new SFXNormal(Game.group.game));
            if (this.HUD.isRageEnabled())
                SFX.get().playSingleEffect("burp");
            this.HUD.enableRageMode(false);
            this.HUD.score.cumulatePoints(false);
        })

        GameEvents.on("mode.chill", (frame, duration) => {
            this.HUD.score.cumulatePoints(true, "Chill");
            SFX.get().playChill();
            SFX.setSFX(new SFXChill(Game.group.game, Config.framesToSeconds(duration)));
            this.HUD.starModeTimer("Chill", Config.framesToSeconds(duration));
        })

        GameEvents.on("ctpHumanCut", (frame) => {
            SFX.get().playSlonCTPMistake();
        });

        GameEvents.on("mode.prison_break", (frame, start: boolean) => {

            let startCtp = () => {
                this.HUD.labels.show("prison_break");
                SFX.get().playSingleEffect("open_ceil");
                this.HUD.starModeTimer("Prison Break", 0);
            };

            if (start) {
                if (!Player.get().hasSeenTip("ctp")) {
                    this.hideHUD(true);
                    setTimeout(() => this.showTip("ctp", startCtp), 1);
                    Player.get().markTipAsSeen("ctp");
                }
                else

                    startCtp();
            }



            if (start) {
                SFX.setSFX(new SFXRage(Game.group.game, "map1/soundtracks/ctp"));
                SFX.get().playSingleEffect("open_ceil");
                if (Math.random() > 0.6)
                    SFX.get().playSlon("slon/ctp");
            }
            else {
                SFX.get().playSingleEffect("open_ceil");
            }

            this.bg.openCeil(start).onComplete.addOnce(() => {
                // SFX.get().playSiren(start);
                this.HUD.enableSirens(start);
                this.HUD.score.cumulatePoints(start, "Prison Break");
            });

            if (start) {
                if (SFX.get().getSound("countdown").isPlaying)
                    SFX.get().getSound("countdown").stop();
            }
            else
                this.HUD.removeModeTimer();
        })

        GameEvents.on("mode.drugs", (frame, start: boolean, duration: number) => {
            let startTrip = () => {
                setTimeout(() => SFX.get().playOneOfEffect("slon/trip"), 3000 + Math.random() * 4000);
                fabric.getBlade().mirrorInput = start;
                FX.get().showDrugsFilter(start);
                this.HUD.labels.show("trip");
                this.HUD.starModeTimer("Trip", Config.framesToSeconds(duration));
            };

            if (start) {
                if (!Player.get().hasSeenTip("trip")) {
                    this.hideHUD(true);
                    setTimeout(() => this.showTip("trip", startTrip), 1);
                    Player.get().markTipAsSeen("trip");
                }
                else
                    startTrip();
            }

            SFX.get().playSingleEffect("breaking_glass");

            if (!start) {
                fabric.getBlade().mirrorInput = false;
                FX.get().showDrugsFilter(false);
            }


            this.HUD.score.cumulatePoints(start, "Trip");



            if (start) {
                SFX.setSFX(new SFXRage(Game.group.game, "map2/soundtracks/trip"));

                if (SFX.get().getSound("countdown").isPlaying)
                    SFX.get().getSound("countdown").stop();
            }
            else
                this.HUD.removeModeTimer();
        })


        GameEvents.on("combo", (combo) => {
            if (combo == 5)
                SFX.get().playSlon("slon/wow");


            if (combo == 25)
                SFX.get().playSlonWellPlayed();
        })

        Game.inputCollector.reset();



        GameEvents.on("scored", (points, pos, size) => {
            this.score = points;
            Game.absence.markAsActive();
            let diff = this.HUD.score.updatePoints(points);
            if (pos && (this.acces.config.map != Models.Map.STATION || diff > 7))
                FX.get().score(pos.x, pos.y, diff, size);
        });

        GameEvents.on("ignited", (points, pos) => {
            let diff = this.HUD.score.updatePoints(points);
            if (pos && (this.acces.config.map != Models.Map.STATION || diff > 7))
                FX.get().score(pos.x, pos.y, diff, 1);
        });

        Game.resume = this.resume.bind(this);
        Game.pause = this.pause.bind(this);

        this.logic.init(this.acces);

        this.logic.onEnd = this.backToMainMenu.bind(this);

        this.start();
        FX.fade(this.game, "in", 800, () => this.startCounting(), 300);


    }

    private startCounting() {

        let startCounting = () => {
            this.HUD.countToStart(() => { GameEvents.emit("startAfterCounting"); });
            SFX.setSFX(new SFXNormal(this.game));;

        };
        let mapTip = "map" + this.acces.config.map;
        if (!Player.get().hasSeenTip(mapTip)) {
            this.hideHUD(true);
            setTimeout(() => {
                this.showTip("map" + this.acces.config.map, startCounting);
                Player.get().markTipAsSeen(mapTip);
            }, 700);
        }
        else
            startCounting();
    }

    public create() {


    }


    public start() {
        if (this.replay)
            this.initReplay("input.txt");
        else
            this.run();
    }

    public makeSkillAction(action: GameModels.ISkillAction, useData: GameModels.SkillUseData) {
        let data = action.data;
        switch (action.type) {
            case GameModels.SkillActionType.PLAY_SFX:
                SFX.get().playOneOfEffect(data.name);
                break;

            case GameModels.SkillActionType.PLAY_SLON_SFX:
                if (Math.random() > 0.5)
                    SFX.get().playSlon(data.name);
                break;

            case GameModels.SkillActionType.SHOW_EFFECT:
                FX.get().effect(data.name as any, useData.x, useData.y);
                break;

            case GameModels.SkillActionType.SAVE_INPUT:
                Game.inputCollector.push(data.name as any, this.logic.getCurrentFrame(), useData.x, useData.y);
                break;

            default:
                console.warn("Uknown skill action: " + action);
        }
    }


    public run() {
        this.logic.run();
        this.logic.skills.onUnknownAction = this.makeSkillAction.bind(this);
        this.HUD.skills.onSkillCasted = this.logic.skills.useSkill.bind(this.logic.skills);
        this.HUD.skills.onDragStart = () => this.fabric.getBlade().disabled = true;
        this.HUD.skills.onDragStop = () => this.fabric.getBlade().disabled = false;

     
        if (this.acces.config.map == 3) {
            let skills = this.logic.skills.get();
        
            this.HUD.skills.addSkill(skills[0]);
            this.HUD.skills.addSkill(skills[2]);

        }

        if (this.acces.config.map == Models.Map.POLICE) {
            this.logic.getGenerator("humans").onCut.add((cuttable, pos, frame) => {
                FX.get().blood(pos.x, pos.y, cuttable.getSize(), false);
                SFX.get().playFlesh();
            });
        }

        if (this.acces.config.map == Models.Map.STATION) {
            this.logic.getGenerator("barrel_petrol").onCut.add((cuttable, pos, frame) => {
                if (cuttable.getSize() == 1) {
                    this.HUD.skills.addSkill(this.logic.skills.get()[1]);
                    SFX.get().playClick();
                }
            });

            this.logic.getGenerator("barrel_explode").onCut.add((cuttable, pos, frame) => {
                if (cuttable.getSize() == 1) {
                    this.HUD.skills.addSkill(this.logic.skills.get()[0]);
                    SFX.get().playClick();
                }
            });
            this.logic.getGenerator("barrel_implode").onCut.add((cuttable, pos, frame) => {
                if (cuttable.getSize() == 1) {
                    this.HUD.skills.addSkill(this.logic.skills.get()[2]);
                    SFX.get().playClick();
                }
            });

        }


        this.logic.forEachNewInput = Game.inputCollector.addCut.bind(Game.inputCollector);;
        this.logic.getGenerator("flesh").onCut.add((cuttable, pos, frame) => {
            FX.get().blood(pos.x, pos.y, cuttable.getSize(), false);
            SFX.get().playFlesh();
        });

        this.logic.getGenerator("chill").onCut.add((cuttable, pos, frame) => {
            FX.get().blood(pos.x, pos.y, cuttable.getSize(), false);
            SFX.get().playFlesh();
        });

        this.logic.getGenerator("bomb").onCut.add((cuttable, pos, frame) => {
            FX.get().blood(pos.x, pos.y, cuttable.getSize(), true);
            if (cuttable.getSize() != 1)
                SFX.get().playFlesh();
            else {
                if (this.acces.config.map != 1 && this.acces.config.map != Models.Map.STATION)
                    SFX.get().playPlop();
            }
        });

        this.logic.getGenerator("piggy").onCut.add((cuttable, pos, frame) => {
            FX.get().blood(pos.x, pos.y, cuttable.getSize(), true);
            SFX.get().playFlesh();
            if (cuttable.getSize() == 1)
                SFX.get().playSlon("slon/pig");
        });
    }

    private initReplay(fileName: string) {
        this.replay.load(fileName, () => {
            this.run();
            this.replay.start(this.logic.addInput.bind(this.logic), () => GameEvents.emit("userEnabledRage"));
        });
    }

    public pause() {
        if (!this.ready)
            return;

        this.logic.pause();
    }

    public resume() {
        if (!this.ready)
            return;
        this.logic.resume();
    }

    public preRender() {
        if (!this.ready || this.logic.isPaused())
            return;
        this.logic.preRender()
    };

    private lastTime = -1;
    public update() {
        if (!this.ready || this.logic.isPaused())
            return;
        if (this.lastTime == -1)
            this.lastTime = Date.now();
        let elapsed = Date.now() - this.lastTime;
        this.lastTime = Date.now();
        this.logic.update();
        FX.get().frequentUpdate(Math.min(elapsed, 100));
    }

    public remove() {
        if (!this.ready)
            return;
        SFX.prefix = "";
        this.game.world.removeAll(true);
        this.bg.remove();
        SFX.get().stopAll();
        Game.pause = null;
        Game.resume = null;
        this.fabric.createBlade().destroy();

        clearInterval(this.pvpUpdateInterval);
    }
    public backToMainMenu(frame: number, secondsLeft: number, accesToPlay: AccesToPlay) {

        this.fabric.createBlade().disabled = true;

        this.logic.delayStop(frame + Config.secondsToFrames(3), () => {

            Game.stats = JSON.parse(JSON.stringify(GameLogic.stats));
            this.logic.remove();
            GameEvents.removeAllListeners();

        });

        setTimeout(() => {
            SFX.get().deactivate(1000);
            let afterFade = () => {

                this.remove();
                Game.instance.resize(1.9);
                if (!this.pvp)
                    this.game.state.start('FinalScore', true, false, Game.stats.score, secondsLeft, accesToPlay);
                else
                    this.game.state.start('PvPFinalScore', true, false, Game.stats.score, secondsLeft, accesToPlay);
                console.log(GameLogic.debugPointsInfo);
            };
            FX.fade(this.game, "out", 1000, afterFade);
        }, 2300);
    }


    private showTip(name: string, callback: Function) {

        SFX.get().playClick();
        this.pause();
        this.hideHUD(true);
        let tip = this.add.image(Game.width / 2, Game.height / 2, "tip/" + name, 0, this.group);
        tip.anchor.set(0.5);

        let style = { font: "24px CosmicTwo", fill: "white" };
        let text = this.add.text(Game.width / 2, 10, "Kliknij, aby powrócić do gry", style, this.group);
        text.anchor.x = 0.5;
        text.setShadow(3, 3, 0, 3, true);
        text.visible = false;
        setTimeout(() => {
            text.visible = true;
            this.input.onDown.addOnce(removeTip);
        }, 2000);

        let removeTip = () => {

            text.destroy();
            tip.destroy();
            this.hideHUD(false);
            this.resume();
            callback();
        }
    }

    private hideHUD(hide: boolean) {
        this.HUD.visible = this.status.visible = !hide;
    }
}