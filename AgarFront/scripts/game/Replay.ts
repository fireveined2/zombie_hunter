﻿
import { Config } from "../utils/Config"
import { Collector, Type, Sample } from "./InputCollector"
import { Phaser } from "../utils/phaser"
export class Replay {


    private addPoint: (pos: Phaser.Point, newSlide?: boolean) => void;
    private enableRage: Function;
    public useSkill: (id: number, x: number, y: number)=>void;
    private buffer: Sample[];

    constructor(buffer: any) {
        this.buffer = [];
        if (buffer) {
            let input = new Collector();
            this.buffer = input.decompress(buffer);
        }
        if (!this.buffer)
            this.buffer = [];
        this.prevFrame = 0;
    }

    public load(filename: string, startCallback: () => void) {
        if (filename) {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', 'replays/' + filename, true);
            //   xhr.responseType = 'arraybuffer';
            xhr.addEventListener("error", (error) => { console.log("Error loading replay!") }, false);
            xhr.onload = event => {
                this.buffer = new Collector().decompress(xhr.response);
                startCallback();
            }
            xhr.send();
        }
        else {
            startCallback();
        }
    }



    public start(addPointFunc: (pos: Phaser.Point, newSlide?: boolean) => void, enableRage: Function) {
        this.addPoint = addPointFunc;
        this.enableRage = enableRage;
    }
   

    private pushed = 0;
    private prevFrame = 0;
    public update(frame: number) {
        
        if (this.buffer.length == 0)
            return;

        while (this.buffer.length >0 && this.buffer[0].frame <= frame) {
            let item = this.buffer.shift();

            if (item.type == Type.CUT) {
                this.addPoint(new Phaser.Point(item.x, item.y));
                this.prevFrame = item.frame;
                this.pushed++;

            }

            if (item.type == Type.ENABLE_RAGE) {
                this.enableRage();
            }

            if (item.type == Type.BOTTLE_EXPLODEM) 
                this.useSkill(0, item.x, item.y);

            if (item.type == Type.BOTTLE_IMPLODEM)
                this.useSkill(2, item.x, item.y);

            if (item.type == Type.BOTTLE_NAPALM)
                this.useSkill(1, item.x, item.y);

          
        }

        
    }

}