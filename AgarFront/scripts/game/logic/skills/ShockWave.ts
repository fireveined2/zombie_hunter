﻿
import { Cuttable, ICuttable, ICuttableInterface } from "../cuttable/Cuttable"
import { Config } from "../../../utils/Config"
import { RND } from "../../../utils/Random"
import { GameLogic } from "../Logic"
import { ISkillController } from "./Controller"
import { CuttableGenerator, FactorySettings, ICuttableFactory } from "../cuttable/Generator"
import { Phaser } from "../../../utils/phaser"
export interface ShockWaveOptions {
    power: number;
    x: number;
    y: number;
}

export class ShockWave implements ISkillController {

    public alive: boolean = true;


    constructor(public name: string, frame: number, private pools: ICuttableInterface[][], private options: ShockWaveOptions = { power: 1, x: GameLogic.gameWidth / 2, y: GameLogic.gameHeight / 2 }) {

        while (true) {
            let divided = false;
            for (let pool of pools)
                for (let obj of pool) {
                    if (obj.alive && obj.getDivisionsNum() < 4 && obj.isOverTheWall() && obj.getBaseKey().indexOf("bomb") == -1) {
                        obj.hit(new Phaser.Point(obj.body.x, obj.body.y), 1, frame);
                        divided = true;
                    }
                }
            if (!divided)
                break;
        }

        for (let pool of pools)
            for (let obj of pool) {
                if (!obj.isOverTheWall() && obj.getSize()==1)
                    continue;
                let x = obj.body.x - options.x;
                let y = obj.body.y - options.y;
                let vec = new Phaser.Point(x, y);
                let normalized = vec.normalize();
                obj.move(normalized.x * (380  + obj.body.x % 300), normalized.y * (700 + obj.body.x%300));
            }

        this.alive = false;
    }

    public kill() {
    }


    public update(frame: number) {
      
    }

    public onStart: Function;

}