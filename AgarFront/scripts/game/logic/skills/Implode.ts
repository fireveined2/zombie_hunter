﻿
import { Cuttable, ICuttable, ICuttableInterface } from "../cuttable/Cuttable"
import { Config } from "../../../utils/Config"
import { RND } from "../../../utils/Random"
import { GameLogic } from "../Logic"
import { ISkillController } from "./Controller"
import { CuttableGenerator, FactorySettings, ICuttableFactory } from "../cuttable/Generator"
import { Phaser } from "../../../utils/phaser"

export interface ImplodeOptions {
    power: number;
    duration: number;
    x: number;
    y: number;
}

export class Implode implements ISkillController {

    public alive: boolean = true;
    private duration: number;
    private endFrame: number = 0;
    private pos: Phaser.Point;

    constructor(public name: string, frame: number, private pools: ICuttableInterface[][], private options: ImplodeOptions = { power: 1, duration: 5, x: GameLogic.gameWidth / 2, y: GameLogic.gameHeight / 2 }) {
        this.alive = true;
        this.pos = new Phaser.Point(options.x, options.y);
        this.endFrame = frame + Config.secondsToFrames(options.duration);
        this.duration = this.endFrame - frame;
    }

    public kill() {
        this.alive = false;
    }



    public update(frame: number) {
        let progress = (this.duration - (this.endFrame - frame)) / this.duration;
        let power = progress>=1?0:Math.sqrt(1 - progress);

        if(frame%2)
        for (let pool of this.pools)
            for (let obj of pool) {
                if (!obj.alive || obj.getBaseKey().indexOf("bomb") != -1)
                    continue;

                let x = this.options.x - obj.body.x;
                let y = this.options.y - obj.body.y;
                let distance = this.pos.distance(new Phaser.Point(obj.body.x, obj.body.y));

                let curSpeed = obj.getSpeed();
                let xs = x / 1.5 * power*2 - curSpeed.x / 20*2;
                let ys = y / 1.5 * power*2 + curSpeed.y/20*2;
                obj.move(xs, ys);
                if (distance < 250)
                    obj.rotateMultipler = (250 - distance) / 100 * power; 
            }

        if (progress > 1)
            this.alive = false;
    }

    public onStart: Function;

}