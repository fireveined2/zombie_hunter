﻿                                          
import { Cuttable, ICuttable, ICuttableInterface } from "../cuttable/Cuttable"
import { Config } from "../../../utils/Config"
import { RND } from "../../../utils/Random"
import { GameLogic } from "../Logic"
import { ISkillController } from "./Controller"
import { CuttableGenerator, FactorySettings, ICuttableFactory } from "../cuttable/Generator"
import { Phaser } from "../../../utils/phaser"

export interface IgniteOptions {
    power: number;
    x: number;
    y: number;
}

export class Ignite implements ISkillController {

    public alive: boolean = true;


    private fireMap: boolean[][];
    private fireMapCeilSize = 10
    constructor(public name: string, frame: number, private pools: ICuttableInterface[][], private options: IgniteOptions = { power: 1, x: GameLogic.gameWidth / 2, y: GameLogic.gameHeight / 2 }, public onIgnited: Function) {
        //   this.tree = new Phaser.QuadTree(0, 0, GameLogic.gameWidth, GameLogic.gameHeight, 10, 4, 0);;
       // this.fireMap = [];


        for (let pool of pools)
            for (let obj of pool) {
                if (!obj.alive || obj.isBurning())
                    continue;

                let x = obj.body.x;
                let y = obj.body.y;
                let vec = new Phaser.Point(x, y);
                if (vec.distance(new Phaser.Point(options.x, options.y)) < 300) {
                   
                    if (this.onIgnited && this.onIgnited(obj))
                        obj.ignite();
                }
            }

        this.alive = true;

      
    }

    public kill() {
    }

    private resetFireMap() {
        for (let i = 0; i < GameLogic.gameWidth / this.fireMapCeilSize; i++) {
            this.fireMap[i] = [];
            for (let y = 0; y < GameLogic.gameHeight / this.fireMapCeilSize; i++)
                this.fireMap[i][y] = false;
        }
    }

    private fillFireMap(pools: any[][]) {
        for (let pool of pools)
            for (let obj of pool) {
                if (!obj.alive || !obj.isBurning())
                    continue;

        
            }


        for (let i = 0; i < GameLogic.gameWidth / this.fireMapCeilSize; i++) {
            this.fireMap[i] = [];
            for (let y = 0; y < GameLogic.gameHeight / this.fireMapCeilSize; i++)
                this.fireMap[i][y] = false;
        }
    }

    private igniteElementsAtPosition(x: number, y: number, source) {
        let point = new Phaser.Point(x,y);
        for (let pool of this.pools)
            for (let obj of pool) {
                if (!obj.alive || obj == source || obj.isBurning())
                    continue;

                if (point.distance(new Phaser.Point(obj.body.x, obj.body.y)) < 100) {
                    if (this.onIgnited && this.onIgnited(obj))
                        obj.ignite();
                }
            }
    }

    private dur =0 ;

    public update(frame: number) {
        let ignitedElements = 0;


        let interval = 7;
        if (Config.Global.delta < Config.Global.deltaGraphics)
            interval = 12;

        if (frame % interval  == 1) {
     //       this.resetFireMap();

            for (let pool of this.pools)
                for (let obj of pool) {
                    if (!obj.alive || !obj.isBurning())
                        continue;

                    this.igniteElementsAtPosition(obj.body.x, obj.body.y, obj);
                    ignitedElements++;
                }



            if (!ignitedElements) {
                this.alive = false;
                return;
            }

        }
    }

    public onStart: Function;

}