﻿import { Phaser } from "../../utils/phaser"
import { Config } from "../../utils/Config"
import { CuttableGenerator, FactorySettings, ICuttableFactory } from "./cuttable/Generator"
import * as GeneratorControllers from "./cuttable/Controller"
import { IGeneratorController } from "./cuttable/Controller"
import { ICuttableInterface } from "./cuttable/Cuttable"
import { IBlade } from "./Blade"
import { ModeManager, Modes } from "./Mode"

import { CollisionDetector } from "./Collision"
import { GameStats } from "./GameStats"



import { BonusHandler } from "./BonusHandler"
import { Time } from "./Time"

import { IGameFabric } from "./IGameFabric"
import * as Models from "../../../../Shared/Shared"
import { GameEvents } from "./GameEvents"
import { SkillHandler } from "./SkillHandler"

export class GameLogic {
    public generators: CuttableGenerator[] = [];
    public controllers: IGeneratorController[] = [];

    private bonusHandler: BonusHandler;
    private blade: IBlade;
    private collisions: CollisionDetector;
    private mode: ModeManager;

    private timer: Time;

    static stats: GameStats = new GameStats();
    static gameWidth: number;
    static gameHeight: number;
    static mapName: string;
    static bonus = <Models.IGameBonus>{};
    static debugPointsInfo: number[] = [];

    private updateHandle: any;
    public gameRunning: boolean = false;
    private accesToPlay: Models.AccesToPlay;

    public onEnd: Function;


    private stopFrame: number = -1;
    private stopCallback: Function;

    public skills: SkillHandler;

    constructor(private fabric: IGameFabric, width: number, height: number, private speed: number = 1) {
        GameLogic.gameWidth = width;
        GameLogic.gameHeight = height;
    }

    private arenaBg: boolean = false;
    public init(acces: Models.AccesToPlay) {
        this.accesToPlay = acces;

    }

    public run() {
        this.start(this.accesToPlay);
    }


    
    private start(acces: Models.AccesToPlay) {
        GameLogic.debugPointsInfo = [];
        GameLogic.stats.reset();
       
        let config = acces.config;
        GameLogic.bonus = config.bonus;

        if (!GameLogic.bonus.basePossibleCuts)
            GameLogic.bonus.basePossibleCuts = 2;
        Config.Cuttable.maxDivisions = GameLogic.bonus.basePossibleCuts;

        if (acces.config.map == Models.Map.STATION) {
            if (!GameLogic.bonus.rageDuration)
                GameLogic.bonus.rageDuration = 0;
            GameLogic.bonus.rageDuration -= 4;

            if (!GameLogic.bonus.duration)
                GameLogic.bonus.duration = 0;
            GameLogic.bonus.duration += 10;

            if (!GameLogic.bonus.rageLoadPercent)
                GameLogic.bonus.rageLoadPercent = 0;
            GameLogic.bonus.rageLoadPercent -= 0.3;

        }


        Config.reset();
       // config.duration = 20;
        console.log("Starting game with seed: " + config.seed);

        this.gameRunning = false;

        GameLogic.mapName ="map"+ config.map;



        let modes = ["chill", "normal", "rage", "prison_break", "drugs"];
        for(let mode of modes)
            GameEvents.on(<any>("mode." + mode), (...args) => {
                for (let gen of this.generators)
                    gen.setMode(mode);
            })

        let factory = this.fabric.createCuttableFactory();
        let fleshGenerator = new CuttableGenerator("flesh", config.seed, CuttableGenerator.defaultSettings, factory);
        fleshGenerator.setElements("flesh");

        let extraThrowNum = 4;
        if (acces.config.map == Models.Map.STATION)
            extraThrowNum = 6;

        fleshGenerator.changeSettings("default", { frequency: 20, cuttablesInExtraThrow: extraThrowNum, extraThrowFrequency: 7 });
        fleshGenerator.changeSettings("rage", { frequency: 4, cuttablesInExtraThrow: 3, extraThrowFrequency: 4 });
        fleshGenerator.changeSettings("prison_break", { controllerOnly: true });
        fleshGenerator.changeSettings("drugs", { controllerOnly: true });

        if (GameLogic.bonus.zombiePercent)
            fleshGenerator.constFrequencyMultipler = 1 + GameLogic.bonus.zombiePercent;

        let bombGenerator = new CuttableGenerator("bomb", config.seed+5200, CuttableGenerator.defaultSettings, factory);
        bombGenerator.setElements("bombs");
        bombGenerator.changeSettings("default", { frequency: 185, cuttablesInExtraThrow: 2, extraThrowFrequency: 8 });
        bombGenerator.changeSettings("rage", { frequency: 130, cuttablesInExtraThrow: 1, extraThrowFrequency: 99 });
        bombGenerator.changeSettings("prison_break", { percentageFreq: 0 });
        bombGenerator.changeSettings("drugs", { controllerOnly: true });
        if (GameLogic.bonus.negativElementPercent)
            bombGenerator.constFrequencyMultipler = 1 + GameLogic.bonus.negativElementPercent;

        let bonusGenerator = new CuttableGenerator("chill", config.seed+1100, CuttableGenerator.defaultSettings, factory);
        bonusGenerator.setElements("chill");
        bonusGenerator.changeSettings("default", { percentageFreq: 0 });
        bonusGenerator.changeSettings("normal", { percentageFreq: Config.Bonus.data[0].probability });
        if (GameLogic.bonus.chillPercent)
            bonusGenerator.constFrequencyMultipler = 1 + GameLogic.bonus.chillPercent;


        let piggyGenerator = new CuttableGenerator("piggy", config.seed + 2100, CuttableGenerator.defaultSettings, factory);
        piggyGenerator.setElements("piggy");
        piggyGenerator.changeSettings("default", { percentageFreq: Config.Cuttable.PiggyProbability });
        piggyGenerator.changeSettings("prison_break", { percentageFreq: 0 });
        piggyGenerator.changeSettings("drugs", { percentageFreq: 0 });

        this.generators = [fleshGenerator, bonusGenerator, bombGenerator, piggyGenerator];

   



        this.bonusHandler = new BonusHandler(fleshGenerator.add.bind(fleshGenerator));

        this.blade = this.fabric.createBlade();
        GameEvents.on("userSurrender", () => this.endGame(this.timer.prevFrame));
        this.mode = new ModeManager(Modes.normal, this.generators, this.bonusHandler);


        this.skills = new SkillHandler(this);
        this.skills.addControllerFnc = this.addController.bind(this);
        this.skills.onIgnite = this.mode.onIgnited.bind(this.mode);

        fleshGenerator.onCut.add(this.mode.onFleshCut.bind(this.mode));
     /* fleshGenerator.onDeadUncut = () => {
            GameLogic.stats.missedFlesh++;
           // console.log(GameLogic.stats.missedFlesh);
        }
       */ 

        bonusGenerator.onCut.add((obj, pos, frame) => {
            this.mode.onFleshCut(obj, pos, frame);
            if (obj.getSize() == 1 && this.bonusHandler.canEnableAnotherMode(frame))
                this.bonusHandler.onChill(obj, pos, frame);;
        });


        piggyGenerator.onCut.add(this.mode.onFleshCut.bind(this.mode));
        piggyGenerator.fallFromTop = true;

        bombGenerator.onCut.add((obj, pos, frame) => {
            let cutPonies = this.mode.onBombCut(obj, pos, frame);
            if (cutPonies) {
                let minusTime = Math.pow(2, cutPonies);
                this.timer.addTime(-minusTime);
            }
        });

        if (config.map == Models.Map.POLICE) 
            this.initMap1();

        if (config.map == Models.Map.HOSPITAL)
            this.initMap2();

        if (config.map == Models.Map.STATION)
            this.initMap3();

        GameEvents.on("userEnabledRage", () => {
            if (this.bonusHandler.canEnableAnotherMode(this.timer.prevFrame))
            this.mode.set(Modes.rage)
        });

        GameEvents.on("mode.rage", (frame, duration) => {
            this.timer.addTime(Config.framesToSeconds(duration));
            this.bonusHandler.rageEndFrame = frame + duration;
            this.mode.bombsDeactivated = true;
        });


        GameEvents.on("mode.normal", () => {
            this.mode.bombsDeactivated = false;
        });

        this.collisions = new CollisionDetector();

        this.timer = new Time(config.duration + (config.bonus.duration|0), this.speed);
        this.timer.onEnd.addOnce((frame) => this.endGame(frame));


        GameEvents.on("startAfterCounting", () =>{
            this.timer.run();
            this.gameRunning = true;
            this.blade.run();


            for (let i = 0; i < 7; i++)
                fleshGenerator.delayThrow(3 * i);
        });

   

            this.updateHandle = setInterval(this.update.bind(this), 4);
            GameEvents.emit("gameStarted");


            for (let generator of this.generators)
                generator.prepare();

    }

    private addController(controller: GeneratorControllers.IGeneratorController) {
        this.controllers.push(controller);
    }

    private initMap1() {
        let config = this.accesToPlay.config;
        let factory = this.fabric.createCuttableFactory();
        let humanGenerator = new CuttableGenerator("humans", config.seed + 2100, CuttableGenerator.defaultSettings, factory);
        humanGenerator.setElements("humans");
        humanGenerator.changeSettings("default", { percentageFreq: 0 });
        humanGenerator.changeSettings("prison_break", { controllerOnly: true });
        this.generators.push(humanGenerator);

        humanGenerator.onCut.add((obj, pos, frame) => {
            this.bonusHandler.endPrisonBreak(frame);
            GameEvents.emit("ctpHumanCut", frame);
        });

        let keysGenerator = new CuttableGenerator("prison_break", config.seed + 5100, CuttableGenerator.defaultSettings, factory);
        keysGenerator.setElements("prison_break");

        let freq = 0.023;
        if (GameLogic.bonus.ctpPercent)
            freq *= (1 + GameLogic.bonus.ctpPercent);
        keysGenerator.changeSettings("normal", { percentageFreq: freq });

        keysGenerator.changeSettings("default", { percentageFreq: 0 });

        keysGenerator.onCut.add((obj, pos, frame) => {
            this.mode.onFleshCut(obj, pos, frame);
            if (obj.getSize() == 1 && this.bonusHandler.canEnableAnotherMode(frame))
                this.bonusHandler.onPrisonBreak(obj, pos, frame);;
        });


        this.generators.push(keysGenerator);

        let countFlesh = (flesh: ICuttableInterface) => {
            if (flesh.getSize() == 1)
                GameLogic.stats.cutInCtps++;
        }

        GameEvents.on("mode.prison_break", (frame, enable: boolean) => {
            let fleshGenerator = this.getGenerator("flesh");
            this.timer.timeElapsingPaused = enable;
            this.mode.bombsDeactivated = enable;
            for (let gen of this.generators)
                gen.fallFromRight = enable;

            if (enable) {
            

                fleshGenerator.onDeadUncut = () => {
                    this.bonusHandler.endPrisonBreak(this.timer.prevFrame + 1);
                }

                let cnt = new GeneratorControllers.CutThePolice("ctp", this.getGenerator.bind(this));
                this.controllers.push(cnt);
                cnt.onStart = () => {
                    GameLogic.stats.ctps++
                    fleshGenerator.onCut.add(countFlesh);
                    Config.Global.gravity /= 2;
                    this.mode.scoreMultipler = 6;
                    if (GameLogic.bonus.pointsPercentInPrisonBreak)
                        this.mode.scoreMultipler *= 1 + GameLogic.bonus.pointsPercentInPrisonBreak;
                }
                //   this.mode.cumulatePoints(true);
            }
            else {
                fleshGenerator.onCut.remove(countFlesh);
                fleshGenerator.onDeadUncut = null;
                this.mode.scoreMultipler = 1;
                Config.Global.gravity *= 2;
                this.getController("ctp").kill();

            }
        });
    }



    private initMap2() {
        let config = this.accesToPlay.config;
        let factory = this.fabric.createCuttableFactory();
        let drugsGenerator = new CuttableGenerator("drugs", config.seed + 2100, CuttableGenerator.defaultSettings, factory);
        drugsGenerator.setElements("drugs");
        drugsGenerator.changeSettings("default", { percentageFreq: 0 });

        let freq = 0.025;
        if (GameLogic.bonus.tripPercent)
            freq *= (1 + GameLogic.bonus.tripPercent);
        drugsGenerator.changeSettings("normal", { percentageFreq: freq });

        this.generators.push(drugsGenerator);

        drugsGenerator.onCut.add((obj, pos, frame) => {
            if (obj.getSize() == 1 && this.bonusHandler.canEnableAnotherMode(frame))
                this.bonusHandler.onDrugs(obj, pos, frame);
        });

        GameEvents.on("mode.drugs", (frame, enable: boolean, duration) => {
            this.mode.bombsDeactivated = enable;
            let fleshGenerator = this.getGenerator("flesh");
            this.mode.cumulatePoints(enable);
            if (enable) {
                GameLogic.stats.trips += Config.framesToSeconds(duration);
                let cnt = new GeneratorControllers.Trip("trip", this.getGenerator.bind(this), Config.framesToSeconds(duration));
                cnt.changeGameSpeedFunc = (speed: number) => { this.bonusHandler.gameSpeed = speed };

                this.controllers.push(cnt);
                cnt.onStart = () => {
                    Config.Cuttable.maxDivisions = 4;
                    this.mode.scoreMultipler = 10;
                    if (GameLogic.bonus.pointsPercentInTrip)
                        this.mode.scoreMultipler *= 1 + GameLogic.bonus.pointsPercentInTrip;
                }          
            }
            else {
                this.mode.scoreMultipler = 1;
                Config.Cuttable.maxDivisions = GameLogic.bonus.basePossibleCuts;
                this.getController("trip").kill();
            }
        });
    }


    private initMap3() {
        let config = this.accesToPlay.config;
        let factory = this.fabric.createCuttableFactory();

        let freq = 0.1;
   
        let generator = new CuttableGenerator("barrel_explode", config.seed + 2100, CuttableGenerator.defaultSettings, factory);
        generator.setElements("barrel_explode");
        generator.changeSettings("default", { percentageFreq: 0.09 });
        this.generators.push(generator);

        
        generator = new CuttableGenerator("barrel_implode", config.seed + 55100, CuttableGenerator.defaultSettings, factory);
        generator.setElements("barrel_implode");
        generator.changeSettings("default", { percentageFreq: 0.05 });
        this.generators.push(generator);
        
        generator = new CuttableGenerator("barrel_petrol", config.seed + 11100, CuttableGenerator.defaultSettings, factory);
        generator.setElements("barrel_petrol");
        generator.changeSettings("default", { percentageFreq: 0.07});
        this.generators.push(generator);
    }

    public getGenerator(name: string): CuttableGenerator {
        for (let gen of this.generators)
            if (gen.name == name)
                return gen;

        throw new Error("Can't find generator " + name);
    }

    public getController(name: string): IGeneratorController {
        for (let gen of this.controllers)
            if (gen.name == name)
                return gen;

        throw new Error("Can't find controller " + name);
    }

    set forEachNewInput(func: (x: number, y: number, frame: number) => void) {
        this.blade.forEachNewInput = func;
    }




    public preRender() {
        if (this.blade && this.gameRunning)
            this.blade.render();
    }


    private updateCollisions(now: number, elapsed: number) {
        let cut = this.blade.update(now, elapsed);
     
        if (cut) {
            let pools = [];
            for (let gen of this.generators)
                pools.push(gen.pool);
            this.collisions.checkCollision(now, pools, cut);
        }
    }

    private handleSlowmotion(gameSpeed: number) {
        Config.Global.delta = Config.Global.deltaGraphics * gameSpeed;

        for (let gen of this.generators)
            gen.frequencyMultipler = Math.max(gameSpeed, 0.5);
        if (this.bonusHandler.gameFrozen)
            GameEvents.emit("updatedFreezeLevel", 1 - gameSpeed);
        this.timer.setGameSpeed(gameSpeed)
    }

    public log(data) {
        console.log(data);
    }

    public onUpdate: (frame: number, elapsed: number)=>void;


    public addInput(pos: Phaser.Point) {
        this.blade.addPoint(pos);
    }


    public getCurrentFrame() {
        return this.timer.prevFrame;
    }

    public update() {

        if (!this.timer)
            return; 

        let now = this.timer.frame;
        let elapsed = this.timer.elapsed;
        let prev = this.timer.prevFrame;


        if (this.gameRunning) {
            for (let i = prev; i < now; i++) {
                if (i == this.stopFrame && this.stopCallback)
                    this.stopCallback();
                if (i >= this.stopFrame && this.stopFrame != -1) {
                    this.remove();
                    return;
                }

                if (i % 25 == 0)
                    GameLogic.debugPointsInfo.push(GameLogic.stats.score);

                if (this.onUpdate)
                    this.onUpdate(i, 1);
               
                 this.updateCollisions(i, 1);

                 for (let a = 0; a < this.controllers.length; a++) {
                     let controller = this.controllers[a];
                     if (!controller.alive) {
                         this.controllers.splice(a, 1);
                          a--;
                         continue;
                     }
                     controller.update(i);
                 }


                for (let gen of this.generators)
                    gen.update(i);

                this.mode.update(i);
                this.bonusHandler.update(i);
                this.handleSlowmotion(this.bonusHandler.gameSpeed);
                this.timer.update(i+1);

            }

        }
        else { 
            this.onUpdate(now, elapsed);
             this.timer.update(now);
        }
    
    }


    private endGame(frame) {
        this.onEnd(frame, this.timer.secondsLeft, this.accesToPlay);
        for (let gen of this.generators)
            gen.disableTillFrame(frame+10000);
    }

    public delayStop(frame: number, callback?: Function) {
        this.stopCallback = callback;
        this.stopFrame = frame;
    }

    public remove() {

        clearInterval(this.updateHandle);
        GameLogic.stats.score = this.mode.score;
        Config.Global.delta = Config.Global.deltaGraphics;
    }

    public pause() {

        clearInterval(this.updateHandle);
        this.updateHandle = -1;
    }

    public isPaused() {
        return this.updateHandle == -1;
    }

    public resume() {
        this.updateHandle = setInterval(this.update.bind(this), 4);
        this.timer.resumeAfterPause();
    }
}