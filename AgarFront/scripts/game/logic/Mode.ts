﻿import { GameLogic } from "./Logic"
import { Config } from "../../utils/Config"

import { FactorySettings, CuttableGenerator } from "./cuttable/Generator"

import { ICuttableInterface } from "./cuttable/Cuttable"

import { BonusHandler } from "./BonusHandler"
import { GameEvents } from "./GameEvents"
import { Phaser } from "../../utils/phaser"

export enum Modes {
    normal, 
    rage
}


interface ModeSettings {
    fleshSettings: FactorySettings;
    bombsSettings: FactorySettings;
    scoreMultipler: number;
    rageDecreasePerSecond: number;
}
let settings: ModeSettings[] = [];

settings[Modes.normal] = <ModeSettings>{
    fleshSettings: <FactorySettings>{ frequency: 20, cuttablesInExtraThrow: 4, extraThrowFrequency: 7 },
    bombsSettings: <FactorySettings>{ frequency: 185, cuttablesInExtraThrow: 2, extraThrowFrequency: 8 },
    scoreMultipler: 1,
    rageDecreasePerSecond: 0.07
};


settings[Modes.rage] = <ModeSettings>{
    fleshSettings: <FactorySettings>{ frequency: 4, cuttablesInExtraThrow: 3, extraThrowFrequency: 4 },
    bombsSettings: <FactorySettings>{ frequency: 130, cuttablesInExtraThrow: 1, extraThrowFrequency: 99 },
    scoreMultipler: 1.2,
    rageDecreasePerSecond: 0.0
};

interface ICombo {
    lastCutTime: number;
    value: number;
    duration: number;
}

export class ModeManager {

    private generators: CuttableGenerator[];

    private current: Modes;
    private bonusHandler: BonusHandler;

    private _score: number;
    private _rageLevel: number;
    private rageEndFrane: number;
    private frame: number;


    private settings: ModeSettings;
    private combo: ICombo = <ICombo>{ value: 1, duration:0 };
    private pointsInCurrentCombo = 0;
    private cutPonies = 0;

    public scoreMultipler = 1;
    private _cumulatePoints: boolean = false;
    public cumulatedPoints = 0;

    public cumulatePoints(cumulate: boolean): number {
        this._cumulatePoints = cumulate;
        if (cumulate)
            this.cumulatedPoints = 0;
        else
            return this.cumulatedPoints;
    }

    constructor(type: Modes, generators: CuttableGenerator[], bonsuHandler: BonusHandler) {
        this.generators = generators;
        this.bonusHandler = bonsuHandler;

        this.set(type);

        this.score = 0;
        this.rageLevel = 0;

        //    this.fleshFactory.onMissedFlesh = this.onMissedFlesh.bind(this);
  
    }

    public isRageActive() {
        return this.current == Modes.rage;
    }

    public set(mode: Modes) {
        if (mode == Modes.rage) {
            if (this.current != Modes.normal || this._rageLevel < 0.9)
                return; 

            GameLogic.stats.rages++;
            let duration = Config.Mechanics.rageModeDuration;
            if (GameLogic.bonus.rageDuration)
                duration += Config.secondsToFrames(GameLogic.bonus.rageDuration); 

            this.rageEndFrane = this.frame + duration;
            GameEvents.emit("mode.rage", this.frame + 1, duration);


        }
        else {
            if (this.current == Modes.rage) {
                GameEvents.emit("mode.normal", this.frame+1);
                this.rageLevel = 0;



            }

        }


        this.current = mode;
        this.settings = settings[mode];
     //   this.fleshFactory.setSettings(this.settings.fleshSettings);
    //    this.bombFactory.setSettings(this.settings.bombsSettings);
    }

    set score(score: number) {
        this._score = Math.round(score);
        GameLogic.stats.score = this._score;
      //  this.hud.updatePoints(score);
    }

    get score(): number {
        return this._score;
    }


    set rageLevel(rage: number) {
        if (this.bonusHandler.gameFrozen)
            return;

        this._rageLevel = Math.min(rage, 1);
        GameEvents.emit("updatedRageLevel", this._rageLevel);
       // this.hud.updateRageLevel(this._rageLevel);
    }

    get rageLevel(): number {
        return this._rageLevel;
    }


    public update(frame: number) {
        this.frame = frame;

        if (this.rageLevel > 0 && this.rageLevel <= 1) {
            if (this.current == Modes.normal) {
                if (this.rageLevel < 1)
                    this.rageLevel -= this.settings.rageDecreasePerSecond * Config.Global.delta;
            }
            else
                this.rageLevel -= 1 / Config.Mechanics.rageModeDuration;
        }

        if (frame >= this.rageEndFrane && this.current == Modes.rage)
            this.set(Modes.normal);

    }

    private calculateCombo(frame: number) {
        let elapsed = frame - this.combo.lastCutTime;
        if (elapsed <= Config.Mechanics.maxElapsedFramesForCombo) {
            this.combo.duration += elapsed;
        //    if (this.combo.value < 25)
                this.combo.value++;

            GameEvents.emit("combo", this.combo.value);

            if (this.combo.value > GameLogic.stats.biggestCut)
                GameLogic.stats.biggestCut = this.combo.value;

            if (this.combo.duration > GameLogic.stats.longestCut)
                GameLogic.stats.longestCut = this.combo.duration;

        }
        else {
            this.pointsInCurrentCombo = 0;
            this.combo.value = 1;
            this.combo.duration = 0;
        }
        this.combo.lastCutTime = frame;
    }
    public onIgnited(obj: ICuttableInterface) {
        if (this.current == Modes.rage)
            return false;

        let score = 30 * Math.pow(obj.getSize(), 1.5);

        if (GameLogic.bonus.pointsPercent)
            score *= 1 + GameLogic.bonus.pointsPercent;

        this.pointsInCurrentCombo += score;
        this.score += score;
        if (this._cumulatePoints)
            this.cumulatedPoints += score;

        GameEvents.emit("ignited", this._score, new Phaser.Point(obj.body.x, obj.body.y));
        return true;
    }
  
    public onFleshCut(flesh: ICuttableInterface, pos: Phaser.Point, frame: number) {
        

        let basePoints = Config.Cuttable.points[flesh.getBaseKey()];
        let fleshSize = flesh.getSize()
        if (fleshSize == 1) {
            GameLogic.stats.cutFlesh++;
            if (!GameLogic.stats.cutElements[basePoints])
                GameLogic.stats.cutElements[basePoints] = 0;
            GameLogic.stats.cutElements[basePoints]++;
        }
        if (fleshSize == 1) 
            this.calculateCombo(frame);

        let score = Config.Mechanics.score(this.combo.value, basePoints) * this.settings.scoreMultipler * fleshSize * this.scoreMultipler;



        let size = Math.min(1 + this.combo.value * 0.04, 1.5) * fleshSize;

        if (flesh.getBaseKey(true)=="flesh1"  && fleshSize == 1) {
           GameLogic.stats.pigs++;
           score = 500, size = 1.55;
           if (GameLogic.bonus.pointsForPiggy)
               score += GameLogic.bonus.pointsForPiggy;
       }

        if (GameLogic.bonus.pointsPerCut)
            score += GameLogic.bonus.pointsPerCut;

        if (GameLogic.bonus.pointsPercent)
            score *= 1 + GameLogic.bonus.pointsPercent;

        if (GameLogic.bonus.pointsPercentInRage && this.current == Modes.rage)
            score *= 1 + GameLogic.bonus.pointsPercentInRage;

   
            this.score += score;
            if (this._cumulatePoints)
                this.cumulatedPoints += score;

            GameEvents.emit("scored", this._score, pos, size);
  

        this.pointsInCurrentCombo += score;

        if (this.current == Modes.normal) {
            let rage = Config.Mechanics.basicRageMultiplier;
            if (frame - this.rageEndFrane < 60)
                rage *= 0.1;
            if (GameLogic.bonus.rageLoadPercent)
                rage *= (1 + GameLogic.bonus.rageLoadPercent);
            this.rageLevel += rage;
        }
    }
    public bombsDeactivated = false;

    public onBombCut(bomb: ICuttableInterface, pos: Phaser.Point, frame: number): number {
     
        if (bomb.getSize() != 1 || this.bonusHandler.gameFrozen)
            return;

        if (!this._cumulatePoints && !this.bombsDeactivated)
        GameLogic.stats.cutPonies++;


        if (this.current == Modes.normal) {

            let points = 0;
            let elapsed = frame - this.combo.lastCutTime;
            if (elapsed <= Config.Mechanics.maxElapsedFramesForCombo) {
                points = this.pointsInCurrentCombo;
                this.pointsInCurrentCombo = 0;
            }
            if (this._cumulatePoints) {
                points = this.cumulatedPoints;
                this.cumulatedPoints = 0;
            }

            if (Config.Version.www) {
                points = points + (this.score - points) * 0.2;
                //FX.get().score(pos.x, pos.y, -20, 1.55, true);
            }
            else 
              

                this.score -= points;
            if (points > 0)
                GameEvents.emit("scored", this._score, pos, 1.14);

           // SFX.get().playPlop();
         //   this.hud.animLostPoints(points);

            return (this._cumulatePoints || this.bombsDeactivated) ? 0 : GameLogic.stats.cutPonies;
        }
    }

}