﻿export interface SkillUseData {
    x?: number;
    y?: number;
}


export enum SkillCastType {
    DRAGABLE,
    SIMPLE
}


export enum SkillActionType {
    USE_CONTROLLER,
    PLAY_SLON_SFX,
    PLAY_SFX,
    SHOW_EFFECT,
    SAVE_INPUT
}

export interface ISkillActionData{
    name: string;
}
export interface ISkillAction {
    type: SkillActionType;
    data: ISkillActionData;
}

export interface ISkill {
    id: number;
    icon: number;
    actions: ISkillAction[];
    castInfo: ISkillCastInfo;
}

export interface ISkillCastInfo {
    removeAfterUse?: boolean;
    type: SkillCastType;
}