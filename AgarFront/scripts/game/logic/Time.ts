﻿import { GameLogic } from "./Logic"
import { Config } from "../../utils/Config"


import { BonusHandler } from "./BonusHandler"

import { GameEvents } from "./GameEvents"
import { Phaser } from "../../utils/phaser"
export class Time {

    private endFrame: number;
    public prevFrame: number;
    private startTime: number;
   
    private ended: boolean = false;
    private duration;
    public onEnd: Phaser.Signal;

    public timeElapsingPaused: boolean = false;

    constructor(duration: number, private speed: number) {
        this.ended = false;
        this.duration = duration;
        this.startTime = 0;
        this.prevFrame = this.frame;
        this.onEnd = new Phaser.Signal();

        GameLogic.stats.duration = duration;
    }

    public run() {
        this.startTime = Date.now();
        this.prevFrame = this.frame;
        this.endFrame = this.frame + (this.duration) * 1000 / Config.Global.frameTime;
    }

    public addTime(seconds: number) {
        GameLogic.stats.duration += seconds;
        this.endFrame += seconds * 1000 / Config.Global.frameTime;
        GameEvents.emit("durationChanged", seconds);
    }

    get secondsLeft(): number {
        if (this.startTime == 0)
            return this.duration;

        return (this.endFrame - this.prevFrame) * Config.Global.frameTime / 1000 + 0.5;
    }

    get frame(): number {
        return Math.floor((Date.now() - this.startTime) / (Config.Global.frameTime * this.speed));
    }

    get elapsed(): number {
        return this.frame - this.prevFrame;
    }

    private prevSeconds = 0;
    public update(frame: number) {
        if (this.timeElapsingPaused)
            this.endFrame++;

        let secs = this.secondsLeft;
        if (secs<=0.5  && this.ended == false) {
            this.onEnd.dispatch(frame);
            this.ended = true;
        }
        if (this.prevSeconds != secs) {
            GameEvents.emit("time", secs);
            this.prevSeconds = secs;
        }


        this.prevFrame = frame;
    }

    public setGameSpeed(gameSpeed: number) {
        if (gameSpeed < 1)
            this.endFrame++;
    }

    public resumeAfterPause() {
        let elapsed = this.frame - this.prevFrame;
        this.startTime += elapsed * Config.Global.frameTime;
   //     this.endFrame += elapsed;
        this.prevFrame = this.frame;
    }

}