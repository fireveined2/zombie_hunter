﻿import { IBlade } from "./Blade"
import { ICuttableFactory } from "./cuttable/Generator"

export interface IGameFabric {
    createBlade(): IBlade;
    createCuttableFactory(): ICuttableFactory;
}

