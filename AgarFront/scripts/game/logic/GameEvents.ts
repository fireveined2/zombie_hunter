﻿
import { Phaser } from "../../utils/phaser"
import { Event } from "../../utils/typescript.events"
import * as GameModels from "./Models"

 interface GameEvents extends Event{

     //   emit(name: string, ...arg: any[]);
     emit(name: "mode.normal", frame: number);
     on(name: "mode.normal", callback: (frame: number) => void);

     emit(name: "mode.rage", frame: number, durationFrames: number);
     on(name: "mode.rage", callback: (frame: number, durationFrames: number) => void);

     emit(name: "mode.chill", frame: number, duration: number);
     on(name: "mode.chill", callback: (frame: number, duration: number) => void);

     emit(name: "mode.prison_break", frame: number, start: boolean);
     on(name: "mode.prison_break", callback: (frame: number, start: boolean) => void);

     emit(name: "ctpHumanCut", frame: number);
     on(name: "ctpHumanCut", callback: (frame: number) => void);

     emit(name: "mode.drugs", frame: number, start: boolean, duration?: number);
     on(name: "mode.drugs", callback: (frame: number, start: boolean, duration?: number) => void);

     emit(name: "skillUsed", skillId: number, data: GameModels.SkillUseData);
     on(name: "skillUsed", callback: (skillId: number, data: GameModels.SkillUseData) => void);

     emit(name: "ignited", score: number, pos: Phaser.Point);
     on(name: "ignited", callback: (score: number, pos: Phaser.Point) => void);


     emit(name: "bonus", frame: number, bonusName: string);
     on(name: "bonus", callback: (frame: number, name: string) => void);

     emit(name: "bonusEnd", frame: number, bonusName: string);
     on(name: "bonusEnd", callback: (frame: number, name: string) => void);

     emit(name: "updatedRageLevel", rage: number);
     on(name: "updatedRageLevel", callback: (rage: number) => void);

    emit(name: "updatedFreezeLevel", freeze: number);
    on(name: "updatedFreezeLevel", callback: (freeze: number) => void);

    emit(name: "durationChanged", delta: number);
    on(name: "durationChanged", callback: (delta: number) => void);

    emit(name: "scored", score: number, pos?: Phaser.Point, cutElementSize?: number);
    on(name: "scored", callback: (score: number, pos?: Phaser.Point, cutElementSize?: number) => void);

    
    emit(name: "time", secondsLeft: number);
    on(name: "time", callback: (secondsLeft: number) => void);

    emit(name: "combo", combo: number);
    on(name: "combo", callback: (combo: number) => void);

    emit(name: "gameStarted" );
    on(name: "gameStarted", callback: () => void);




     /////
    emit(name: "userEnabledRage");
    on(name: "userEnabledRage", callback: () => void);

    emit(name: "userSurrender");
    on(name: "userSurrender", callback: () => void);

    emit(name: "startAfterCounting");
    on(name: "startAfterCounting", callback: () => void);
}

 export var GameEvents: GameEvents = new Event() as any;
