﻿import { Config } from "../../utils/Config"
import { GameEvents } from "./GameEvents"
import { GameLogic } from "./Logic"

import { Phaser } from "../../utils/phaser"
export class BonusHandler {

    public gameSpeed: number = 1;

    private freezeStartFrame: number = 0;
    private freezeEndFrame: number = 0;

    private breakStartFrame: number = -1;
    private breakEndFrame: number = -1;

    private drugsStartFrame: number = -1;
    private drugsEndFrame: number = -1;



    public rageEndFrame: number;

    private addFlesh: Function;
    public onChillEnd: Function;
    public onDrugsEnd: Function;

    constructor(addFlesh: Function) {
        this.addFlesh = addFlesh;
    }

    public onChill(cuttable: any, pos: any, frame: number) {
        let bonus = Config.Bonus.data[0];

        if (0 == Config.Bonus.freeze && frame > this.freezeEndFrame) {
            GameLogic.stats.chills++;
            this.freezeStartFrame = frame + 22;

            let duration = bonus.duration;
            if (GameLogic.bonus.chillDuration)
                duration += GameLogic.bonus.chillDuration;

            this.freezeEndFrame = frame +duration / Config.Global.deltaGraphics;
            GameEvents.emit("mode.chill", frame, duration / Config.Global.deltaGraphics   );

            this.addFlesh(frame);
            this.addFlesh(frame);
        }
    }

    public endPrisonBreak(frame) {
        if (this.breakEndFrame > frame+2)
        this.breakEndFrame = frame+1;
    }

    public onPrisonBreak(cuttable: any, pos: any, frame: number) {
        if (frame > this.freezeEndFrame) {
            
            this.breakStartFrame = frame + 22;
            let duration = 40;
            this.breakEndFrame = frame + duration / Config.Global.deltaGraphics;
            GameEvents.emit("mode.prison_break", frame, true);
        }
    }

    public onDrugs(cuttable: any, pos: any, frame: number) {
        if (frame > this.drugsEndFrame) {

            this.drugsStartFrame = frame + 22;

            let duration = 15;
            if (GameLogic.bonus.tripDuration)
                duration += GameLogic.bonus.tripDuration;

            this.drugsEndFrame = frame + duration / Config.Global.deltaGraphics;
            GameEvents.emit("mode.drugs", frame, true, duration / Config.Global.deltaGraphics );
        }
    }

    public canEnableAnotherMode(frame: number): boolean {
        if (frame < this.freezeEndFrame)
            return false

        if (frame < this.breakEndFrame)
            return false;

        if (frame < this.rageEndFrame)
            return false;

        if (frame < this.drugsEndFrame)
            return false;

        return true;
    }

    public gameFrozen: boolean = false;

    public update(frame: number) {
       
        if (frame < this.freezeEndFrame) {
            let duration = this.freezeEndFrame - this.freezeStartFrame;
            let progress = (frame - this.freezeStartFrame) / duration;
            if (progress < 0.6)
                this.gameSpeed = 0.15;
            else 
                this.gameSpeed = 0.15 + (progress - 0.6) * 0.85 / 0.4;
            Config.Cuttable.maxDivisions = 5;
            this.gameFrozen = true;
        }
        else {
            if (Config.Cuttable.maxDivisions == 5) {
                Config.Cuttable.maxDivisions = GameLogic.bonus.basePossibleCuts;
                GameEvents.emit("mode.normal", frame);
                this.gameSpeed = 1;
            }
            if (frame == this.freezeEndFrame + 1)
                this.gameFrozen = false;
  
        }




        if (frame < this.breakEndFrame) {
            let duration = this.breakEndFrame - this.breakStartFrame;
            let progress = (frame - this.breakStartFrame) / duration;
        }
        else {
            if (frame == this.breakEndFrame) {
                GameEvents.emit("mode.prison_break", frame, false);
                GameEvents.emit("mode.normal", frame);
            }

        }


        if (frame < this.drugsEndFrame) {
            let duration = this.drugsEndFrame - this.drugsStartFrame;
            let progress = (frame - this.drugsStartFrame) / duration;
        }
        else {
            if (frame == this.drugsEndFrame) {
                GameEvents.emit("mode.drugs", frame, false);
                GameEvents.emit("mode.normal", frame);
                this.gameSpeed = 1;
            }

        }


    }


}





















