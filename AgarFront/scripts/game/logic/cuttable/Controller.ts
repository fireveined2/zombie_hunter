﻿export { Controller as CutThePolice } from "./controllers/CutThePolice"
export { Controller as Trip } from "./controllers/Trip"

export interface IGeneratorController {
    alive: boolean;
    name: string;
    kill();
    onStart: Function;
    update(frame: number);
}

