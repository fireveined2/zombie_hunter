﻿
import { Cuttable, ICuttable, ICuttableInterface } from "./Cuttable"
import { Config } from "../../../utils/Config"
import { RND } from "../../../utils/Random"
import { GameLogic } from "../Logic"
import { Phaser } from "../../../utils/phaser"

export interface FactorySettings {
    frequency?: number;
    percentageFreq?: number;
    controllerOnly?: boolean;

    cuttablesInExtraThrow?: number;
    extraThrowFrequency?: number;


}

export interface ICuttableFactory {
    create(data: ICuttable, cuttable?: ICuttableInterface): ICuttableInterface;
}


export  class CuttableGenerator {


    protected rnd: RND;

    public name: string;
    protected addNextFrame: number;

    private throwsToExtraThrow: number;

    private createdCuttable: ICuttable = <ICuttable>{};

    public pool: ICuttableInterface[];
    public frequencyMultipler = 1;
    public constFrequencyMultipler = 1;

    public onIgnite: Phaser.Signal;
    public onCut: Phaser.Signal;
    public onDeadUncut: (me: Cuttable) => void;
    private disabledTillFrame: number=0;

    private elements: string[] =[];

    private factory: ICuttableFactory;
    public onFirstAdd: () => void;

    protected settings: FactorySettings;
    static defaultSettings: FactorySettings = {
        frequency: 54,
        extraThrowFrequency: 7,
        cuttablesInExtraThrow: 2    
    };

    private isDisabled(frame): boolean {
        return (this.settings.frequency == 0 || frame < this.disabledTillFrame) && !this.settings.controllerOnly;
    }


    public settingsBase: {[mode: string]: FactorySettings} = {};

    public fallFromRight: boolean = false;
    public fallFromTop: boolean = false;
    public xSpeedMultipler = 1;

    public setSettings(settings: FactorySettings) {
        this.settings = settings;
        this.throwsToExtraThrow = settings.extraThrowFrequency;
        this.addNextFrame = -1;
        this.plannedThrows = [];
    }

    public disableTillFrame(frame: number) {
        this.disabledTillFrame = frame;
        this.plannedThrows = [];
    }

    public changeSettings(name: string, settings: FactorySettings) {
        this.settingsBase[name] = settings;
        this.setMode("normal");
    }

    public setElements(name: string) {
        let map = GameLogic.mapName;

        if (name == "flesh")
            for (let i = 2; i <= Config.Cuttable.fleshTypes[map]; i++)
                this.elements.push(map+"/flesh" + i);

        if (name == "bombs") {
            for (let i = 1; i <= Config.Cuttable.bombTypes[map]; i++)
                this.elements.push(map + "/bomb" + i);
        }    
        if (name == "chill")
            this.elements.push(map +"/bonus1");

        if (name == "prison_break")
            this.elements.push(map + "/bonus2");

        if (name == "humans")
            for (let i = 1; i <=4; i++)
            this.elements.push(map + "/human"+i);

        if (name == "drugs")
            this.elements.push(map + "/bonus2");

        if (name == "barrel_explode")
            this.elements.push(map + "/bonus4");

        if (name == "barrel_implode")
            this.elements.push(map + "/bonus2");

        if (name == "barrel_petrol")
            this.elements.push(map + "/bonus3");


        if (name == "piggy")
            this.elements.push(map +"/flesh1"); 
    }
    
    protected getFromPool(): ICuttableInterface {
        for (let obj of this.pool) {
            if (!obj.alive) {
                obj.revive();
                return obj;
            }
        }

        return null;
    }

    constructor(name: string, seed: number, settings: FactorySettings = CuttableGenerator.defaultSettings, factory: ICuttableFactory) {
        this.name = name;
        this.rnd = new RND(seed);
        this.factory = factory;

        this.settings = settings;
        this.settingsBase["default"] = settings;
        this.onCut = new Phaser.Signal;
        this.onIgnite = new Phaser.Signal;
        this.addNextFrame = -1;
        this.pool = [];
    }

    
    protected createCuttable(): ICuttable {
        let x = this.rnd.between(-GameLogic.gameWidth * 0.1, GameLogic.gameWidth * 1.1)
        let xp = x / GameLogic.gameWidth - 0.5;
        let x_speed = this.rnd.between(300, 710) * -xp + this.rnd.between(-100, 100);

  
     //   x_speed *= Math.sqrt(Game.assetsSize);

        let angle = this.rnd.between(0, 360) //* Math.sqrt(Game.assetsSize);
        let speed = this.rnd.between(Config.Cuttable.minSpeed, Config.Cuttable.maxSpeed)// * Math.sqrt(Game.assetsSize);
       

        if (this.fallFromTop) {
            this.createdCuttable.y = -80;
            speed = 0;
        }
        else
            this.createdCuttable.y = GameLogic.gameHeight + 100;



        if (this.fallFromRight) {
            x = GameLogic.gameWidth * 1.05;
            x_speed = -this.rnd.between(620, 740) * this.xSpeedMultipler;
            /*
            this.createdCuttable.y = this.rnd.between(-GameLogic.gameHeight * 0.1, GameLogic.gameHeight * 0.7)
            let xp = 0.9 - this.createdCuttable.y / GameLogic.gameHeight;
            speed = this.rnd.between(Config.Cuttable.minSpeed, Config.Cuttable.maxSpeed) * xp;
            */
            this.createdCuttable.y = GameLogic.gameHeight * 0.4 + this.rnd.between(-110,140);
            speed = 190  +this.rnd.between(-40, 40);
        }

  

        this.createdCuttable.x = x;
        this.createdCuttable.speed = speed;
        this.createdCuttable.angle = angle;
        this.createdCuttable.x_speed = x_speed;

        this.createdCuttable.frame = 0;
        this.createdCuttable.key = this.getRandomKey();
        return this.createdCuttable;
    }

    private getRandomKey() {
        return this.rnd.pick(this.elements );
    }

    public add(frame: number, obj?: ICuttable) {
        if (this.isDisabled(frame))
            return;

        if (!obj)
            obj = this.createCuttable();

        let pool = this.getFromPool();
        let cuttable = this.factory.create(obj, pool);
        cuttable.onSplit = this.split.bind(this);
        cuttable.onCut.removeAll();
        cuttable.onCut.add((...params) => this.onCut.dispatch(...params));

        if (this.onDeadUncut)
            cuttable.onDeadUncutted = (obj) => { if (this.onDeadUncut) this.onDeadUncut(obj); }
        else
            cuttable.onDeadUncutted = null;

         if (!pool)
            this.pool.push(cuttable);

         if (this.onFirstAdd) {
             this.onFirstAdd();
             this.onFirstAdd = null;
         }
         return cuttable;
    }

    private makeExtraThrow(frame: number) {
        this.throwsToExtraThrow = this.settings.extraThrowFrequency;

        for (let i = 0; i < this.settings.cuttablesInExtraThrow - 1; i++)
            this.delayThrow(frame + i*12);
    }


    public prepare() {
        for (let i = 0; i < this.elements.length * 3; i++) {
            let c = this.add(1111110);
            c.getEdge(1);
            c.getEdge(0);
            c.getEdge(2);
            c.getEdge(3);
        }

            for (let elem of this.pool)
                elem.kill();
     
    }

    public delayThrow(frame: number) {
        this.plannedThrows.push(frame);
    }

    private plannedThrows: number[] = [];


    public update(frame: number) {
        while (this.plannedThrows[0] < frame) {
            this.add(frame);
            this.plannedThrows.splice(0, 1);
        }

        if (this.settings.percentageFreq === undefined) {
            if (this.addNextFrame == -1)
                this.addNextFrame = frame + this.settings.frequency * this.rnd.double() + 0.35;

            if (frame >= this.addNextFrame) {
                if (!this.isDisabled(frame) && !this.settings.controllerOnly) {
                    this.add(frame);

                    this.throwsToExtraThrow--;
                    if (this.throwsToExtraThrow < 1)
                        this.makeExtraThrow(frame);
                }
                this.addNextFrame = frame + this.settings.frequency / this.frequencyMultipler / this.constFrequencyMultipler;
            }
        }
        else {
            let nr = this.rnd.double();
            if (!this.isDisabled(frame) && !this.settings.controllerOnly)
                if (this.settings.percentageFreq * this.frequencyMultipler * this.constFrequencyMultipler > nr * 1000 / Config.Global.frameTime)
                 this.add(frame);
        }



        for (let cuttable of this.pool)
            if (cuttable.alive)
            cuttable.update(frame);
    }


    public setMode(mode: string) {
        if (this.settingsBase[mode])
            this.setSettings(this.settingsBase[mode]);
        else
            if (this.settingsBase["default"])
            this.setSettings(this.settingsBase["default"]);
    }

    protected split(frame: number, obj: ICuttable) {
         this.add(frame, obj);
        
    }
}