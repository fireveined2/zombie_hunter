﻿
import { Cuttable, ICuttable, ICuttableInterface } from "../Cuttable"
import { Config } from "../../../../utils/Config"
import { RND } from "../../../../utils/Random"
import { GameLogic } from "../../Logic"
import { Phaser } from "../../../../utils/phaser"
import { CuttableGenerator, FactorySettings, ICuttableFactory } from "../Generator"
import { IGeneratorController } from "../Controller"

export class Controller implements IGeneratorController {

    private flesh: CuttableGenerator;
    private humans: CuttableGenerator;
    public alive: boolean = true;
    private startFrame = 0;
    private nextThrowFrame = 0;
    private throws = 0;
    constructor(public name: string, get: (name: string) => CuttableGenerator) {
        this.flesh = get("flesh");
        this.humans = get("humans");
        this.alive = true;

        this.throws = 0;
        this.startFrame = 0;
        this.nextThrowFrame = 0;
 
    }

    public kill() {
        this.alive = false;
        this.flesh.xSpeedMultipler = 1;
        this.humans.xSpeedMultipler = 1;
    }

    public update(frame: number) {
        if (!this.startFrame) {
            this.startFrame = frame;
            this.nextThrowFrame = frame;
        }

        let duration = Config.framesToSeconds(frame - this.startFrame, true);
        if (duration < 2)
            return;

        if (this.onStart) {
            this.onStart(frame);
            delete this.onStart;
        }

        let delayAfterHuman = Math.max(0.9 - 0.1 * this.throws, 0.4);
        let delayBetweenFlesh = Math.max(1 - 0.1 * this.throws, 0.3);
        let delayAfterNextThrow = Math.max(1 - 0.3 * this.throws, 0.35);
        let gap = frame % 3 == 0 ? 0 : Math.max(1 - 0.1 * this.throws, 0.35);

        let speed = 1 + 0.07 * this.throws;

        if (frame > this.nextThrowFrame) {
            this.flesh.xSpeedMultipler = speed;
            this.humans.xSpeedMultipler = speed;


            this.throws++;
            this.humans.add(frame);
            let zombies = frame % (6 - Math.min(Math.round(this.throws / 2), 5)) + 1;
         
            for (let i = 0; i < zombies; i++) {
                if (gap && i > 0)
                    this.flesh.delayThrow(frame + Config.secondsToFrames(delayAfterHuman + delayBetweenFlesh * i + gap));
                else
                    this.flesh.delayThrow(frame + Config.secondsToFrames(delayAfterHuman + delayBetweenFlesh * i));
            }
            this.nextThrowFrame = frame + Config.secondsToFrames(delayAfterHuman + delayBetweenFlesh * (zombies - 1) + delayAfterNextThrow + gap);
        }
    }

    public onStart: Function;

}