﻿
import { Cuttable, ICuttable, ICuttableInterface } from "../Cuttable"
import { Config } from "../../../../utils/Config"
import { RND } from "../../../../utils/Random"
import { GameLogic } from "../../Logic"
import { Phaser } from "../../../../utils/phaser"
import { CuttableGenerator, FactorySettings, ICuttableFactory } from "../Generator"
import { IGeneratorController } from "../Controller"

export class Controller implements IGeneratorController {

    private bombs: CuttableGenerator;

    private flesh: CuttableGenerator;
    public alive: boolean = true;
    private startFrame = 0;
    private prevFrame = 0;
    public changeGameSpeedFunc: (speed: number) => void;


    constructor(public name: string, get: (name: string) => CuttableGenerator, private duration: number) {
        this.flesh = get("flesh");
        this.alive = true;

        this.bombs = get("bomb");
        this.elements = [this.bombs.pool, this.flesh.pool];
    }

    public kill() {
        this.alive = false;
        this.flesh.xSpeedMultipler = 1;
        this.bombs.fallFromTop = false;
        this.changeGameSpeedFunc(1);
        this.flesh.fallFromTop = this.flesh.fallFromRight = false;
        delete this.changeGameSpeedFunc;
    }

    private nextThrowFrame = 0;
    private elements: ICuttableInterface[][] = [];



    public update(frame: number) {
        this.prevFrame = frame;
        if (!this.startFrame) {
            this.startFrame = frame;
            this.nextThrowFrame = frame;
        }

        let duration = Config.framesToSeconds(frame - this.startFrame, true);
        if (duration < 2)
            return;

        let progress = (duration - 2) / (this.duration - 2);

   
        let power = Math.min(2 - Math.abs(progress - 0.5) * 4, 0.9);;
        let speed = 1 * (1 - power) + Math.abs(Math.sin(duration * duration * (3 + power)) * power) * 0.3;
        if (speed >= 1) speed = 0.99;
        this.changeGameSpeedFunc(speed);

        for (let pool of this.elements)
            for (let i = 0; i < pool.length; i++) {
                if (!pool[i].alive)
                    continue;
            pool[i].scale = Math.sin(duration * duration) * 0.4 * power + 1;
            pool[i].rotateMultipler = 6 + Math.sin(duration) * power;
        }

        if (frame > this.nextThrowFrame) {
            if (frame % 5 <2)
                this.flesh.fallFromTop = true, this.flesh.fallFromRight = false;
            if (frame % 5 == 3)
                this.flesh.fallFromRight = true;
            if (frame % 5 == 4)
                this.flesh.fallFromTop = this.flesh.fallFromRight = false;

            this.flesh.add(frame);
            this.bombs.fallFromTop = true;
            if (frame % 8 == 3)
            this.bombs.delayThrow(frame +20);

            this.nextThrowFrame = Config.secondsToFrames(1) + frame + this.elements.length;

        }
        

        if (this.onStart) {
            this.onStart(frame);
            delete this.onStart;
        }

        
    }

    public onStart: Function;

}