﻿
import { Config } from "../../../utils/Config"
import { CuttablesDatabase } from "../../../beforeGame/CuttablesDatabase"
import { GameLogic } from "../Logic"
import { Phaser } from "../../../utils/phaser"

export interface ICuttable {
    x: number;
    x_speed: number;
    speed: number;
    angle: number;

    y?: number;
    part?: "none" | "H" | "V";
    key?: string;
    frame?: number;
}


export interface ICuttableBody {

    x: number;
    y: number;
    width: number;
    height: number;
    anchor: Phaser.Point;

    key: string;
    frame: number;
    angle: number;
}


export interface ICuttableInterface{
    key: string;
    scale: number;
    onDeadUncutted: (me: Cuttable) => void;
    onSplit: (frame: number, part: ICuttable) => void;
    body: ICuttableBody;
    onCut: Phaser.Signal;
    onIgnite: Phaser.Signal;
    create(x: number, x_v: number, speed: number, width: number, height: number, key: string, y?: number);
    createFromObj(obj: ICuttable);

    getSize(): number
    getDivisionsNum(): number
    getBaseKey(withoutMap?: boolean): string;
    getEdge(number: number): Phaser.Line;
    update(frame?: number);
    alive: boolean;
    revive();
    kill();
    isCuttable: boolean;
    hit(pos: Phaser.Point, edge: number, frame?: number);
    clone(): ICuttable;
    rotateMultipler; 

    isOverTheWall(): boolean;

    move(x_speed: number, y_speed: number);

    isBurning(): boolean;
    ignite(): void;
    getSpeed(): { x: number, y: number };
}

export class Cuttable implements ICuttableInterface {
    public scale = 1;
    protected _alive: boolean = false;
    //protected image: Phaser.Image;
    protected speed: number;
    protected initialSpeed: number;
    protected x_speed: number;
    public rotateMultipler = 1;
    private vertices: Phaser.Point[];
    private edge: Phaser.Line;

    private immune: number = 0;
    public key: string;

    public onDeadUncutted: (me: Cuttable) => void;
    public onSplit: (frame: number, part: ICuttable) => void;

    public body: ICuttableBody;

    public onKill: Function;
    public onCut: Phaser.Signal;
    public onIgnite: Phaser.Signal;
    private _isBurning: boolean;



    constructor() {
        this.body = <ICuttableBody>{ anchor: new Phaser.Point() };
        this.onCut = new Phaser.Signal();
        this.onIgnite = new Phaser.Signal();
        this.scale = 1;
    }

    public getSpeed() {
        return { x: this.x_speed, y: this.speed };
    }
    public isBurning() {
        return this._isBurning;
    }

    public ignite() {
        this._isBurning = true;
    }

    public move(xSpeed: number, ySpeed: number) {
        this.x_speed += xSpeed;
        this.speed += -ySpeed;
    }

    public isOverTheWall(): boolean {
        let wallLevel = GameLogic.gameHeight - 70;
        if (!this.vertices)
            return true;

        for (let vertice of this.vertices) {
            if (vertice.y < wallLevel)
                return true;
        }
        return false;
    }

    public create(x: number, x_v: number, speed: number, width: number, height: number, key: string, y?: number) {
        this.body.x = x;
        this.body.y = y === undefined ? GameLogic.gameHeight + 100 : y;
        this.body.width = width;
        this.body.height = height;
        this.body.anchor.setTo(0.5);
        this.body.frame = 0;
        this.body.key = key;
        this.body.angle =x%360;
        this.x_speed = x_v;
        this.key = key;

        this.speed = this.initialSpeed = speed;
        this._alive = true;
    }

    public createFromObj(obj: ICuttable) {
        this.body.x = obj.x;
        this.body.y = obj.y
        this.body.anchor.setTo(0.5);
        this.x_speed = obj.x_speed
        this.body.angle = obj.angle;
        this.body.frame = obj.frame;
        this.key = obj.key;
        this.body.key = obj.key; 

        let frame = CuttablesDatabase.instance.get(this.body.key as string).frames[this.body.frame];
        this.body.width = frame.width;
        this.body.height = frame.height;

        this.speed = this.initialSpeed = obj.speed;
        this._alive = true;
        this.immune = -1;

  

        if (obj.frame > 2) {
            this.body.anchor.x = 0.5;
            this.body.anchor.y = 0;
        } else {
            this.body.anchor.y = 0.5;
            this.body.anchor.x = 0;
        }

        this.setAnchor();
    }

    public getSize(): number{
        return 1 / this.getDivisionsNum();
    }

    public getDivisionsNum(): number {
        let size = 1;
        if (this.body.frame != 0)
            size++;
        for (let char of this.key)
            if (char == "!")
                size++;
        return size;
    }

    public getBaseKey(withoutMap: boolean = false) {
        let base = this.body.key.split("!")[0];
        if (!withoutMap)
            return base;

        return base.split("/")[1];
    }

    private setAnchor() {
        let baseName = this.getBaseKey();
        let rect: Phaser.Rectangle = CuttablesDatabase.instance.get(baseName).frames[0];
        if (Config.Cuttable.sizes[this.key])
            rect = Config.Cuttable.sizes[this.key];
        let cx = rect.centerX;
        let cy = rect.centerY;

        let frame = CuttablesDatabase.instance.get(this.body.key as string).frames[this.body.frame];
        let x = frame.x;
        let y = frame.y;
        let width = frame.width;
        this.body.width = frame.width;
        this.body.height = frame.height;
        let height = frame.height;
        let ax = (cx - x) / width;
        let ay = (cy - y) / height;

        this.body.anchor.set(ax, ay);
    }

    private calculateVertices() {
        if (!this.vertices) {
            this.vertices = [];
            for (let i = 0; i < 4; i++)
                this.vertices[i] = new Phaser.Point(0,0);
        }
        let ax = this.body.anchor.x;
        let ay = this.body.anchor.y;
        this.vertices[0].x = this.body.x - this.body.width *ax;
        this.vertices[0].y = this.body.y - this.body.height *ay;
        this.vertices[1].x = this.body.x + this.body.width *(1-ax);
        this.vertices[1].y = this.body.y - this.body.height *ay;
        this.vertices[2].x = this.body.x + this.body.width * (1 - ax);
        this.vertices[2].y = this.body.y + this.body.height * (1 - ay);
        this.vertices[3].x = this.body.x - this.body.width * ax;
        this.vertices[3].y = this.body.y + this.body.height * (1 - ay);


        //TODO Przepisać, żeby nie tworzyć za każdym razem nowych obiektów
        for (let i = 0; i < 4; i++)
            this.vertices[i] = this.vertices[i].rotate(this.body.x, this.body.y, this.body.angle, true);
    }


    public getEdge(number: number): Phaser.Line{
        if (!this.edge) 
            this.edge = new Phaser.Line(0,0);
        
        this.calculateVertices();
        let v1 = this.vertices[number];
        let v2 = this.vertices[number == 3 ? 0 : number + 1]
        this.edge.setTo(v1.x, v1.y, v2.x, v2.y);
        return this.edge;
    }


    protected anim(body: ICuttableBody, speed: number, xspeed: number, angle: number) {
        let dt = Config.Global.delta;
        body.angle += angle * dt * this.rotateMultipler*1.7;
        body.y -= speed * dt;
        body.x += xspeed * dt;

    }



    public update(frame?: number) {
        if (this.immune == -1)
            this.immune = frame + 10;
        if (frame > this.immune)
            this.immune = 0;

        if (!this.alive)
            return;

        this.speed -= Config.Global.gravity * Config.Global.delta;
        this.anim(this.body, this.speed, this.x_speed, this.initialSpeed / 10);

        if (this.body.y > GameLogic.gameHeight + 150 && this.speed < 0)
            this.kill();

        let sp = Config.Global.delta / Config.Global.deltaGraphics;
    }

    get alive(): boolean {
        return this._alive;
    }



    public revive() {
        this._alive = true;
        this.scale = 1;
        this.rotateMultipler = 1;
    }

    public kill() {

        if (this.onDeadUncutted && this.isCuttable) {
            this.onDeadUncutted(this);
        }
        this._isBurning = false;
        this._alive = false;
        if (this.onKill)
            this.onKill();
    }

    get isCuttable(): boolean {
        let cuttable = this.alive && !this.immune && this.getDivisionsNum() < Config.Cuttable.maxDivisions
        return cuttable;
    }



    public cut(frame: number, horizontal: boolean) {
       // if (this.getDivisionsNum() >= Config.Cuttable.maxDivisions && Config.Cuttable.maxDivisions>2) {
         //   this.kill();
      //      return;
      //  }

        if (this.body.frame != 0) 
            this.key += "!" + this.body.frame.toString();
        
       // this.image.loadTexture(this.key);
        this.body.key = this.key
        this.body.frame = horizontal ? 3 : 1;

        let clone = this.clone();
        clone.x_speed *= 1.15;
        clone.speed *= 1.08;
        clone.frame = this.body.frame as number + 1;

        this.speed *= 0.86;
        this.x_speed *= 0.86;

        if (this.onSplit) 
            this.onSplit(frame, clone);

        this.setAnchor();

      
    }


    public hit(pos: Phaser.Point, edge: number, frame: number) {
        this.onCut.dispatch(this, pos, frame);
        this.cut(frame, edge == 1 || edge == 3);
        this.immune = frame + 10;

        if (pos.x > this.body.x)
            this.x_speed -= 70;
        if (pos.x < this.body.x)
            this.x_speed += 70;

        if (pos.y > this.body.y)
            this.speed -= 70;
        if (pos.y < this.body.y)
            this.speed += 70;

   
    }


    public clone(): ICuttable {
        return <ICuttable>{
            x: this.body.x,
            y: this.body.y,
            x_speed: this.x_speed,
            angle: this.body.angle,
            speed: this.speed,
            key: this.key
        }; 
    }
    
}