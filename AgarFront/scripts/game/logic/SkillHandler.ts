﻿import { Config } from "../../utils/Config"
import { GameEvents } from "./GameEvents"
import { GameLogic } from "./Logic"
import * as GameModels from "./Models"
import * as SkillControllers from "./skills/Controller"

export class SkillHandler {

    private skills: GameModels.ISkill[] = [];
    public onUnknownAction: (action: GameModels.ISkillAction, useData: GameModels.SkillUseData) => void;

    public addControllerFnc: Function;
    public onIgnite: Function;

    constructor(private logic: GameLogic) {

        let skill: GameModels.ISkill = {
            id: 0,
            icon: 1,
            castInfo: {
                removeAfterUse: true,
                type: GameModels.SkillCastType.DRAGABLE
            },
            actions: [
                {
                    type: GameModels.SkillActionType.SAVE_INPUT,
                    data: {
                        name: "bottle_explode"
                    }
                },
                {
                type: GameModels.SkillActionType.PLAY_SLON_SFX,
                data: {
                    name: "slon/explosion"
                }
            },
            {
                type: GameModels.SkillActionType.PLAY_SFX,
                data: {
                    name: "explosion"
                }
            },
            {
                type: GameModels.SkillActionType.USE_CONTROLLER,
                data: {
                    name: "ShockWave"
                }
            },
            {
                type: GameModels.SkillActionType.SHOW_EFFECT,
                data: {
                    name: "boom"
                }
            }
            ]

        };
        this.skills.push(skill);


        skill = {
            id: 1,
            icon: 2,
            castInfo: {
                removeAfterUse: true,
                type: GameModels.SkillCastType.DRAGABLE
            },
            actions: [
                {
                    type: GameModels.SkillActionType.SAVE_INPUT,
                    data: {
                        name: "bottle_napalm"
                    }
                },{
                type: GameModels.SkillActionType.PLAY_SFX,
                data: {
                    name: "explosion"
                }
            },{
                type: GameModels.SkillActionType.PLAY_SLON_SFX,
                data: {
                    name: "slon/napalm"
                }
            }, {
                type: GameModels.SkillActionType.USE_CONTROLLER,
                data: {
                    name: "Ignite"
                }
            },
            {
                type: GameModels.SkillActionType.SHOW_EFFECT,
                data: {
                    name: "big_boom"
                }
            }]

        };

        this.skills.push(skill);



        skill = {
            id: 2,
            icon: 3,
            castInfo: {
                removeAfterUse: true,
                type: GameModels.SkillCastType.DRAGABLE
            },
            actions: [
                {
                    type: GameModels.SkillActionType.SAVE_INPUT,
                    data: {
                        name: "bottle_implode"
                    }
                },
                {
                type: GameModels.SkillActionType.PLAY_SFX,
                data: {
                    name: "implosion"
                }
            },{
                type: GameModels.SkillActionType.PLAY_SLON_SFX,
                data: {
                    name: "slon/explosion"
                }
            }, {
                type: GameModels.SkillActionType.USE_CONTROLLER,
                data: {
                    name: "Implode"
                }
            }, {
                type: GameModels.SkillActionType.SHOW_EFFECT,
                data: {
                    name: "implode"
                }
            }
           ]

        };

        this.skills.push(skill);
    }

    public get() {
        return this.skills;
    }

    public useSkill(skillId: number, data: GameModels.SkillUseData): boolean {
        let skill = this.skills[skillId];
        if (!skill) {
            console.warn("Can't find skill " + skillId);
            return;
        }
        console.log(skillId);
        for (let action of skill.actions)
            this.makeAction(action, data);


        return true;
    }

    private makeAction(action: GameModels.ISkillAction, useData: GameModels.SkillUseData) {
        let data = action.data;
 
        switch (action.type) {
            case GameModels.SkillActionType.USE_CONTROLLER:
                this.useController(data, useData);
                break;


            default:
                if (this.onUnknownAction)
                    this.onUnknownAction(action, useData);
        }
    }

    private useController(data: GameModels.ISkillActionData, useData: GameModels.SkillUseData) {
        let frame = this.logic.getCurrentFrame();

        let pools = [];
        for (let gen of this.logic.generators)
            pools.push(gen.pool);

        switch (data.name) {
            case "ShockWave":
                let cnt = new SkillControllers.ShockWave("shock_wave", frame, pools, { power: 1, x: useData.x, y: useData.y });
                //   this.addControllerFnc(cnt);
                break;

            case "Ignite":
                let ignite = new SkillControllers.Ignite("ignite", frame, pools, { power: 1, x: useData.x, y: useData.y }, this.onIgnite);
                this.addControllerFnc(ignite);
                break;

            case "Implode":
                let implode = new SkillControllers.Implode("implode", frame, pools, { power: 1, duration: 5, x: useData.x, y: useData.y });
                this.addControllerFnc(implode);
                break;

            default:
                console.warn("Unknown controller " + data.name);

        }
    }


}





















