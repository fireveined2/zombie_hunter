﻿import * as Models from "../../../../Shared/Shared"
import { Phaser } from "../../utils/phaser"

export class GameStats implements Models.GameStats {
    score: number;

    cutPonies: number;
    missedPonies: number;

    cutFlesh: number;
    missedFlesh: number;

    ctps: number;
    cutInCtps: number;
    trips: number;
    rages: number;
    chills: number;
    duration: number;
    pigs: number;

    cutsCount: number;
    biggestCut: number;
    longestCut: number;
    highestScoreForCut: number;
    cutElements: {};
    bladeRoad: number;

    constructor() {
        this.reset();
    }

    public reset() {
        this.score = 0;
        this.cutPonies = 0;
        this.missedPonies = 0;
        this.cutFlesh = 0;
        this.missedFlesh = 0;
        this.rages = 0;
        this.ctps = 0;
        this.cutInCtps = 0;
        this.trips = 0;
        this.pigs = 0;
        this.chills = 0;
        this.duration = 0;
        this.cutsCount = 0;
        this.biggestCut = 0;
        this.longestCut = 0;
        this.highestScoreForCut = 0;
        this.cutElements = {};
        this.bladeRoad = 0;
    }
}