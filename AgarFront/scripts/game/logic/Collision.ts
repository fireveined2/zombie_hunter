﻿import { GameLogic } from "./Logic"
import { Config } from "../../utils/Config"
import { Cuttable, ICuttableInterface } from "./cuttable/Cuttable"
import { Phaser } from "../../utils/phaser"

export class CollisionDetector {


    private blade: Phaser.Line;

    constructor() {
        this.blade = new Phaser.Line(0, 0);
    }


    public checkCollision(frame: number, cuttables: ICuttableInterface[][], points: Phaser.Point[]) {
        for (let a = 0; a< points.length - 1; a++) {
            this.blade.setTo(points[a].x, points[a].y, points[a + 1].x, points[a + 1].y);
            for (let container of cuttables)
                for (let cuttable of container)
                    if (cuttable.isCuttable)
                        for (let i = 0; i < 4; i++) {
                            let point = cuttable.getEdge(i).intersects(this.blade);
                            if (point) {
                                this.onHit(frame, cuttable, point, i);
                                i = 5;
                            }
                        }
        }
    }

    private onHit(frame: number, cuttable: ICuttableInterface, point: Phaser.Point, edge: number) {
        cuttable.hit(point, edge, frame);
    }

}