﻿import { Config } from "../../utils/Config"
import { GameLogic } from "./Logic"
import { Phaser } from "../../utils/phaser"

export interface IBlade {
    forEachNewInput: (x: number, y: number, frame: number) => void;
    run();
    disableMouseInput: boolean;
    update(frame: number, elapsed: number): Phaser.Point[];
    render();
    addPoint(pos: Phaser.Point, newSlide?: boolean)
}


export interface Segment {
    point: Phaser.Point;
    distance: number;
    alpha: number;
    ownLength: number;
    time: number;
    checked?: boolean;
    new?: boolean;
}

export interface IBladeController {
    disableMouseInput(disable: boolean); 
}

export interface IBladeView {
    clearCanvas();
    update(segments: Segment[]);
    render(segments: Segment[]);
}

export class BladeLogic {


    public segments: Segment[];
    private erasingPoints: boolean = false;
    private cutDistance: number = 0;

    public keyUp: boolean;

    private _disableInput: boolean = false;
    public forEachNewInput: (x: number, y: number, frame: number) => void;
    private renderer: IBladeView;
    private haveNewInput = false;

    constructor(renderer: IBladeView) {
        this.segments = [];
        this.renderer = renderer;
    }

    public run() {
        this.disableMouseInput = false;
    }

    set disableMouseInput(disable: boolean) {
        if (disable) 
            this.keyUp = false;
    
        this._disableInput = disable;
    }

    private in = 0;
    public addPoint(pos: Phaser.Point, newSlide?: boolean) {

        this.haveNewInput = true;
        if (this.keyUp || newSlide) {
            this.keyUp = false;
            this.segments = [];
        }

        let distance = this.cutDistance + 650;
        let length = 1;


        if (this.segments.length > 0) {
     
            length = this.segments[this.segments.length - 1].point.distance(pos);
       
  
            GameLogic.stats.bladeRoad += length;
            distance = this.segments[this.segments.length - 1].distance + 110 + length;
            if (length < 5)
                return;
        }
        else
            this.segments = [];

        let segment = <Segment>{
            point: pos,
            alpha: 1,
            distance: distance,
            ownLength: length,
            time: Date.now(),
            new: true
        }
        this.segments.push(segment);
    }


    private updatePoints(elapsed) {
        this.cutDistance += elapsed * Config.Global.deltaGraphics * (this.segments.length * 1200 + 2400) * (Config.Version.www ? 1.2 : 1);
        if (this.segments.length > 0)
            this.cutDistance = Math.min(this.cutDistance, this.segments[this.segments.length - 1].distance);

        let segments = this.segments;
        for (let i = 0; i < segments.length; i++) {
            let prevDistance = i > 0 ? segments[i - 1].distance : 1;
            segments[i].alpha = 1 - ((this.cutDistance - prevDistance) / segments[i].ownLength);
        }

        while (this.segments.length > 1 && this.segments[1].alpha <= 0) {
            this.segments.shift();
            if (this.segments.length == 1) {
                this.segments = [];
                this.renderer.clearCanvas();
            }
        }

    }



   

    private getCut(frame: number): Phaser.Point[] {
        if (this.segments.length < 2)
            return null;

        let array: Phaser.Point[] = [];
        for (let i = 0; i < this.segments.length; i++)
            if (!this.segments[i].checked) {
   
                array.push(this.segments[i].point);
                if (i != this.segments.length - 1) {
                    this.segments[i].checked = true;
                    this.in++;
            
                }
            }
        return array;
    }

    public update(frame: number, elapsed: number): Phaser.Point[] {
        if (this.haveNewInput)
        {
            if (this.forEachNewInput)
                for (let segment of this.segments) {
                    if (segment.new)
                        this.forEachNewInput(segment.point.x, segment.point.y, frame);
                    segment.new = false;
                }
        }

        this.updatePoints(elapsed);
        this.renderer.update(this.segments);
                         
        if (this.haveNewInput) {
            this.haveNewInput = false;
            return this.getCut(frame);
        }
        return null;
    }

    public render() {
        this.renderer.render(this.segments);   
    }
}