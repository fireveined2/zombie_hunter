﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { CuttablesDatabase } from "../beforeGame/CuttablesDatabase"
import { FX } from "./FX"
import { SFX} from "../SFX/SFX"
import { Cuttable as CuttableModel, ICuttableBody, ICuttable, ICuttableInterface } from "./logic/cuttable/Cuttable"

export class Cuttable implements ICuttableInterface {

    set scale(scale: number) {
        this.image.scale.set(scale);
    }

    protected image: Phaser.Image;
    protected phaser: Phaser.Game;

    private flames: Phaser.Image[];

    protected parent: Phaser.Group;


    private getSprite: (key) => Phaser.Image;


    public onSplit: (frame: number, part: ICuttable) => void;

    public model: CuttableModel;
    public onCut: Phaser.Signal;
    public onIgnite: Phaser.Signal;

    set onDeadUncutted(func: (me: ICuttableInterface) => void) {
        this.model.onDeadUncutted = func;
    }

    get body(): ICuttableBody {
        return this.model.body;
    }

    public move(xSpeed: number, ySpeed: number) {
        this.model.move(xSpeed, ySpeed);
    }
    public isOverTheWall(): boolean {
        return this.model.isOverTheWall();
    }

    public isBurning() {
        return this.model.isBurning();
    }

    public ignite() {
        if (!this.flames) {
            let num = Math.ceil(Math.random() * 2);
            if (this.getSize() != 1)
                num = 0;

            if (this.getSize() < 0.45)
                num = 0;
            this.flames = [];

            for (let i = 0; i < num; i++) {
                let x = Math.random() * (this.body.width * 0.7) - this.body.width * 0.35;
                let y = Math.random() * (this.body.height * 0.7) - this.body.height * 0.35;
                let flame = FX.get().makeFlame(x, y);
                this.image.addChild(flame);
                this.flames.push(flame);
            }
        }
        if (this.getSize() > 0.45 && Math.random() < this.getSize())
            SFX.get().playSingleEffect("ignite");

        this.model.ignite();
    }

    public getSpeed() {
        return this.model.getSpeed();
    }

    constructor(getSprite: (key) => Phaser.Image) {
        this.phaser = Game.group.game;
        this.getSprite = getSprite;

        this.model = new CuttableModel();
        this.onCut = this.model.onCut;
        this.onIgnite = this.model.onIgnite;
        this.model.onKill = () => this.kill(true);
    }

    set key(k) {
        this.model.key = k;
    }

    get ket(): string {
        return this.model.key;
    }

    public create(x: number, x_v: number, speed: number, width: number, height: number, key: string, y?: number) {
        this.model.create(x, x_v, speed, width, height, key, y);
        this.image = this.getSprite(key);


        //this.image.x
        this.image.x = x;
        this.image.y = y === undefined ? Game.height + 100 : y;
        this.image.width = width;
        this.image.height = height;
        this.image.anchor.setTo(0.5);
        this.image.key = key;
    }


    public createFromObj(obj: ICuttable) {
        this.model.createFromObj(obj);
        this.image = this.getSprite(obj.key);

        this.image.x = obj.x;
        this.image.y = obj.y
        this.image.anchor = this.model.body.anchor;

        this.image.angle = obj.angle;
        this.image.frame = obj.frame;

    }

    public getSize(): number {
        return this.model.getSize();
    }

    public getDivisionsNum(): number {
        return this.model.getDivisionsNum();
    }

    public getBaseKey() {
        return this.model.getBaseKey();
    }


    public getEdge(number: number): Phaser.Line {
        return this.model.getEdge(number);
    }


    public update(frame?: number) {
        if (!this.alive)
            return;

        this.model.update(frame);

        this.image.x = this.model.body.x;
        this.image.y = this.model.body.y;
        this.image.angle = this.model.body.angle;

        if (this.isBurning()) {
            this.image.tint = 0xFFCCCC;

            for (let flame of this.flames) 
                flame.angle = -this.image.angle;
        }
    }

    get alive(): boolean {
        return this.model.alive;
    }

    set rotateMultipler(r: number) {
        this.model.rotateMultipler = r;
    }

    public revive() {
        this.scale = 1;
        this.model.revive();
    }

    public kill(fromModel: boolean = false) {

        if (!fromModel)
            this.model.kill();
        this.image.tint = 0xFFFFFF;
        this.image.visible = false;
        if (this.image.parent)
            this.image.parent.removeChild(this.image);

        if (this.flames) {
            for (let flame of this.flames) {
                this.image.removeChild(flame);
                flame.destroy();
            }
            delete this.flames;
        }
    }

    get isCuttable(): boolean {
        return this.model.isCuttable;
    }






    public hit(pos: Phaser.Point, edge: number, frame?: number) {

        this.model.onSplit = this.onSplit;
        this.model.hit(pos, edge, frame);


        this.image.loadTexture(this.model.body.key);
        this.image.frame = this.model.body.frame;
        this.image.anchor = this.model.body.anchor;
    }


    public clone(): ICuttable {
        return this.model.clone();
    }

}