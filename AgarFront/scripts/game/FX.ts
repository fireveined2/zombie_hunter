﻿import {Game} from "../beforeGame/Game"
import {Config} from "../utils/Config"

export class FX {
    static FX: FX;
    static glowFilter: Phaser.Filter;

    private drugsFilter: Phaser.Filter;
    private drugsFilterActive: boolean = false;
    private drugsOveray: Phaser.Image;

    static bloodOverlayFilter: Phaser.Filter;

    private pool: Phaser.Image[] = [];
    private textPool: Phaser.BitmapText[] = [];

    private group: Phaser.Group;
    private groupOnTop: Phaser.Group;



    
    constructor(private phaser: Phaser.Game) {
        let bladeFragment = [
            "precision lowp float;",
            "varying vec2 vTextureCoord;",
            "varying vec4 vColor;",
            'uniform sampler2D uSampler;',

            'void main() {',
            'vec4 sum = vec4(0);',
            'vec2 texcoord = vTextureCoord;',
            'for(int xx = -4; xx <= 4; xx++) {',
            'for(int yy = -3; yy <= 3; yy++) {',
            'float dist = sqrt(float(xx*xx) + float(yy*yy));',
            'float factor = 0.0;',
            'if (dist == 0.0) {',
            'factor = 2.0;',
            '} else {',
            'factor = 2.0/abs(float(dist));',
            '}',
            'sum += texture2D(uSampler, texcoord + vec2(xx, yy) * 0.002) * factor;',
            '}',
            '}',
            'gl_FragColor = sum * 0.035 + texture2D(uSampler, texcoord);',
            '}'
        ];


        if (Config.Version.mobile) {
            bladeFragment = [
                "precision lowp float;",
                "varying vec2 vTextureCoord;",
                "varying vec4 vColor;",
                'uniform sampler2D uSampler;',

                'void main() {',
                'gl_FragColor =   vec4(1.2,1.2,2.2,1.2) * texture2D(uSampler, vTextureCoord);',
                '}'
            ];

        }


        this.drugsFilter = new Phaser.Filter(this.phaser, {power:{ type: '1f', value: 0.0 }}, this.phaser.cache.getShader('shaders/drugs'));
        FX.glowFilter = new Phaser.Filter(this.phaser, null, bladeFragment);


        let tripFragment = [
            "precision lowp float;",
            "varying vec2 vTextureCoord;",
            'uniform sampler2D uSampler;',
            'uniform float intens;',
            "uniform vec2  resolution;",

            'void main() {',
            "vec2 position = ( gl_FragCoord.xy / resolution.xy * 2.0 - 1.0 );",
            'gl_FragColor = texture2D(uSampler, texcoord);',
            '}'
        ];
       // FX.bloodOverlayFilter = new Phaser.Filter(this.phaser, null, overlayFragment);
        //  FX.bloodOverlayFilter.setResolution(Game.realWidth, Game.realHeigt);
        this.group = phaser.add.group(Game.group);
        this.groupOnTop = phaser.add.group(Game.group);

        if (FX.FX)
            FX.FX.remove();

        FX.FX = this;
        this.pool = [];
        this.textPool = [];
    }

    public showDrugsFilter(show: boolean = true) {
        if (show) 
            this.phaser.world.filters = [this.drugsFilter];
           // this.drugsOveray = this.drugsFilter.addToWorld();
    

        this.drugsFilterActive = show;
    }

    static get(): FX {
        return FX.FX;
    }

    private dropGroup: Phaser.Group;
    private addDrop() {
        let drop = this.getAviableImage("blood");
        drop.frameName = "rain1";
        drop.y = -20;
        drop.x = Math.random() * (Game.width+150)+20;
        drop.name = "drop";
        drop.alpha = 0.5;
        drop.scale.set(Math.random() * 0.65 + 0.35);
        drop.angle = 0;
        this.dropGroup.add(drop);
    }

    public stopRain() {
        if (this.dropGroup)
        this.dropGroup.visible = false;
    }
    public makeFlame(x, y) {
        let flame = this.phaser.state.getCurrentState().add.image(x, y, "effects/fire", "12");
        flame.anchor.set(0.5);
        flame.animations.add("burn", Phaser.Animation.generateFrameNames('1', 0, 6), 17 + Math.round(Math.random() * 10), true);
        flame.scale.set(0.6 + Math.random()*0.4);
        flame.play("burn");
        return flame;
    } 

    public effect(type: "boom" | "implode" | "big_boom", x, y) {
        let maxFrames = 0;
        let scale = 0;
        if (type == "boom") {
            maxFrames = 5;
            scale = 1 / 0.4;
        }

        if (type == "implode") {
            maxFrames = 14;
            scale = 2;
        }
        if (type == "big_boom") {
            maxFrames = 6;
            scale = 1/0.75;
        }


        let boom = this.phaser.state.getCurrentState().add.image(x, y, "effects/" + type, "0", this.groupOnTop);
        boom.scale.set(scale);
        boom.anchor.set(0.5);
        let start = Date.now();
        let update = () => {
            let frame = Math.floor((Date.now() - start) / 45);
            boom.frameName = frame.toString();
            if (frame > maxFrames) {
                boom.destroy();
                return;
            }
            setTimeout(() => update(), 10);
        }
        update();
    }

    public rain(group: Phaser.Group, power: number = 6) {
        let p = Game.settings.effectLevel;
        if (p == 0) p = 0.9;
            power *= p;

        if (this.dropGroup)
            this.dropGroup.destroy();

        this.dropGroup = this.phaser.add.group();
        group.add(this.dropGroup);
        for (let i = 0; i < power; i++) {
            setTimeout(() => this.addDrop(), i*100);
           // ;
        }
    }

    public animWindow(group?: Phaser.Group, dx=0, dy=0) {
        let window = this.phaser.state.getCurrentState().add.image(196+dx, 150+dy, "window", 0, group?group:this.groupOnTop);
        window.alpha = 0;
        let updateWindow = () => {
            if (!window.alive)
                return;

            window.alpha = Math.sin(Date.now() / 500);
            setTimeout(updateWindow, 30);
        } 
        updateWindow();
        Game.group.bringToTop(this.groupOnTop);
    }


    private getAviableText(): Phaser.BitmapText {

        for (let obj of this.textPool)
            if (!obj.visible) {
                obj.visible = true;
                obj.alpha = 1;
                obj.scale.set(1, 1);
                obj.tint = 0xFFFFFF;
     
                return obj;

            }

        let obj = this.phaser.add.bitmapText(0, 0, "big", "", undefined,  this.groupOnTop);
        this.textPool.push(obj);
        obj.anchor.setTo(0.5);
        return obj;
    }

    private getAviableImage(key: string): Phaser.Image {
        for (let obj of this.pool) {
            if (!obj.alive)
                continue;
            if (!obj.visible) {
                obj.visible = true;
                obj.alpha = 1;
                obj.scale.set(1);
                obj.tint = 0xffffff;
                obj.name = "";
                obj.loadTexture(key);
                this.group.remove(obj, false);
                this.groupOnTop.remove(obj, false);
                if (this.dropGroup)
                    this.dropGroup.remove(obj, false);

      
                return obj;
            }
        }
        let obj = this.phaser.add.image(0, 0, key, 0);
        this.pool.push(obj);
        obj.anchor.setTo(0.5);
        return obj;
    }

    public initPool(num: number) {
        for (let i = 0; i < num; i++) {
            let obj = this.phaser.add.image(0, 0, "blood", 0);
            this.pool.push(obj);
            obj.anchor.setTo(0.5);
            obj.scale.set(1);
            obj.tint = 0xffffff;
            obj.name = "";
            obj.visible = false;

            if (i % 3 == 0) {
                let obj = this.phaser.add.bitmapText(0, 0, "big", "", undefined, this.groupOnTop);
                this.textPool.push(obj);
                obj.anchor.setTo(0.5);
                obj.visible = false;
            }

            }

    }

    private mark(x: number, y: number, size: number, key: string, tint = 0xffffff) {
        let img = this.getAviableImage(key);
        this.group.add(img);
        if (key == "blood") {
            img.frameName = "blood0005";
            img.name = "blood";
            img.scale.set(0.7);

        }
        img.scale.setTo(size*0.7);
        img.x = x;
        img.y = y;
        img.angle = Math.random() * 360;
        img.tint = tint;
    }

    private animBlood(startTime: number, img: Phaser.Image, red?) {
        
        let elapsed = Date.now() - startTime;
        let frame = Math.ceil(elapsed / 350 * 7+0.1);
      
        if (frame > 7) {
            img.visible = false;
            img.alpha = 0;
            return;
        }
        img.frameName = "blood000" + frame;
        if (red)
            img.tint = 0xFF2222;
        setTimeout(() => this.animBlood(startTime, img), 10);
    }

    public blood(x: number, y: number, size?: number, red = false) {
        if (!size)
            size = 1;
        let baseSize = size;
        size += Math.random() * size / 2 - size / 4;
      //  if (Game.settings.effectLevel != 0) 
            this.mark(x, y, size, "blood", red ? 0xff2222 : 0xffffff);
        
        if (baseSize < 0.6)
            return;

        let img = this.getAviableImage("blood");
        this.groupOnTop.add(img);
        img.name = "blood";
        img.scale.set(size);
        img.x = x;
        img.y = y;
        this.animBlood(Date.now(), img, red);
    }


    public score(x: number, y: number, score: number, size: number = 1, percent: boolean = false, goDown: boolean = false) {
        let style = { font: "30pt Arial" };
        let txt = this.getAviableText();
        //txt.setStyle(style);
        txt.position.set(x,y);
        txt.anchor.setTo(0.5, 0.5);
        txt.scale.set(size + 0.4, size + 0.4);
        txt.setText(Math.round(score).toString());


        if (score < 0) {
            txt.tint = 0xFF5555;
            if (!percent)
                txt.name = "minus";
        }
        if (goDown) {
            txt.name = "minus";;
            txt.tint = 0x55FF55;
        }
        if (percent)
            txt.setText(Math.round(score).toString() + "%");
    }

    public animText(x: number, y: number, number: number, size: number, color?: number) {
        let style = { font: "30pt Arial" };
        let txt = this.getAviableText();
        //txt.setStyle(style);
        txt.position.set(x, y);
        txt.anchor.setTo(0.5, 0.5);
        txt.scale.set(size + 0.4, size + 0.4);
        txt.setText(number.toString());

        //   if (number < 0)
        if (color)
        txt.tint = color;

     }


    private animDrop(startTime: number = Date.now(), img: Phaser.Image) {
        let elapsed = Date.now() - startTime;
        let frame = Math.ceil(elapsed / 180 * 4+0.1);
        if (frame > 4) {
            img.visible = false;
            img.alpha = 0;
            if (img.parent && img.parent.worldVisible)
             this.addDrop();
            return;
        }
        img.frameName = "rain" + frame;
        setTimeout(() => this.animDrop(startTime, img), 20);
    }

    private remove() {
        for (let obj of this.pool)
            obj.destroy();

        for (let obj of this.textPool)
            obj.destroy();
    }

    public frequentUpdate(elapsedMs: number) {
        if (this.drugsFilterActive && this.drugsFilter.uniforms.power.value < 0.02)
            this.drugsFilter.uniforms.power.value += 0.0005 * elapsedMs/40;

        if (!this.drugsFilterActive && this.drugsFilter.uniforms.power.value > 0) {
            this.drugsFilter.uniforms.power.value -= 0.0004 * elapsedMs/40;
            if (this.drugsFilter.uniforms.power.value <= 0)
                this.phaser.world.filters = null;
        }
        if (this.drugsFilter.uniforms.power.value > 0)
            this.drugsFilter.update();
    }


    public update(elapsed: number) {

     
        for (let obj of this.pool) {
            if (!obj.visible)
                continue;

            if (obj.name == "drop") {
                if (obj.y < Game.height - (obj.scale.x * 23 +60)) {
                    obj.x -= (320 + obj.scale.x * 180) * Config.Global.delta * elapsed;
                    obj.y += (780 + obj.scale.x *600) * Config.Global.delta * elapsed;
                } else {
                   this.animDrop(Date.now(), obj);
                    obj.name = "";
                }
            }

            if (obj.name == "blood"){
                obj.alpha -= 0.3 * Config.Global.deltaGraphics * elapsed;
                if (obj.alpha < 0)
                    obj.visible = false;
            }

        }

        for (let txt of this.textPool) {
                if (!txt.visible)
                continue;

                let fadeFactor = 1;
                if (Game.settings.effectLevel == 0)
                    fadeFactor = 1.2;

                this.groupOnTop.bringToTop(txt);
                if (txt.name == "minus") {
                    txt.alpha -= 1.15 * fadeFactor * Config.Global.deltaGraphics * elapsed;
                let scale = txt.scale.x - Config.Global.deltaGraphics * elapsed * 0.2;
                txt.scale.set(scale, scale);
                txt.position.y += 70 / Game.assetsScale * Config.Global.deltaGraphics * elapsed;
            }
            else {
                    txt.alpha -= 1 * fadeFactor* Config.Global.deltaGraphics * elapsed;
                let scale = txt.scale.x - Config.Global.deltaGraphics * elapsed * 0.5;
                txt.scale.set(scale, scale);
                txt.position.y -= 200 / Game.assetsScale * Config.Global.deltaGraphics * elapsed;
            }

            if (txt.alpha < 0)
                txt.visible = false;
        }

        Game.group.bringToTop(this.groupOnTop);
    }

    static createTextOverlay(phaser: Phaser.Game, text: string) {
        let overlay = phaser.add.graphics(0, 0);
        overlay.scale.set(1 / Game.aspectRatio, 1 / Game.aspectRatio);
        overlay.beginFill(0, 0.6);
        overlay.drawRect(0, 0, Game.width, Game.height);
        overlay.alpha = 1;
        overlay.inputEnabled = true;

        let style = { font: "32px CosmicTwo", fill: "white", align: "center" };
        let txt = phaser.add.text(Game.width / 2, Game.height/2, text, style);
        txt.anchor.set(0.5);
        overlay.addChild(txt);

        return overlay;
    }

    static showPauseOverlay(phaser: Phaser.Game, resume: Function) {
        let overlay = phaser.add.graphics(0, 0);
        overlay.scale.set(1 / Game.aspectRatio, 1 / Game.aspectRatio);
        overlay.beginFill(0, 0.5);
        overlay.drawRect(0, 0, Game.width, Game.height);
        overlay.alpha = 1;

        let pause = phaser.add.bitmapText(Game.width / 2, Game.height *0.3, "big", "PAUZA");
        pause.anchor.set(0.5);
        pause.scale.set(2,2);
        overlay.addChild(pause);

        let resumetxt = phaser.add.bitmapText(Game.width / 2, Game.height *0.3+ pause.height, "big", "Kliknij, aby wznowic rozgrywke");
        resumetxt.anchor.set(0.5);
        overlay.addChild(resumetxt);


        phaser.input.onDown.addOnce(() => {
            overlay.destroy();
            resume();
        });
    }

    static createOverlay(phaser: Phaser.State): Phaser.Graphics {
        let overlay = phaser.add.graphics(0, 0);
        overlay.scale.set(1 / Game.aspectRatio, 1 / Game.aspectRatio);
        overlay.beginFill(0);
        overlay.drawRect(0, 0, Game.width, Game.height);
        overlay.alpha = 0;
        overlay.inputEnabled = true;
        return overlay;
    }


    static fade(phaser: Phaser.Game, type: "out" | "in", durationMs: number = 250, onComplete?: Function, blackScreenDurationMs?: number): Phaser.Graphics {
        let overlay = phaser.add.graphics(0, 0);
        overlay.scale.set(1 / Game.aspectRatio, 1 / Game.aspectRatio);
        overlay.beginFill(0);
        overlay.drawRect(0, 0, Game.width, Game.height);
        overlay.inputEnabled = true;

        let makeTween = () => {
            let tween: Phaser.Tween;
            if (type == "out")
                tween = phaser.add.tween(overlay).from({ alpha: 0 }, durationMs, Phaser.Easing.Default, true);
            else
                tween = phaser.add.tween(overlay).to({ alpha: 0 }, durationMs, Phaser.Easing.Default, true);

            tween.onComplete.addOnce(() => {
                if (blackScreenDurationMs && type=="out")
                    setTimeout(() => {
                        if (onComplete)
                            onComplete(overlay);
                        else
                            overlay.destroy()
                    }, blackScreenDurationMs);
                else {
                    if (onComplete) {
                        onComplete(overlay);
                        setTimeout(() => overlay.destroy(), 20);
                    }
                    else
                        overlay.destroy()
                }
            });
        }

        if (blackScreenDurationMs && type == "in")
            setTimeout(makeTween, blackScreenDurationMs);
        else
            makeTween();
        return overlay;
    }

  
}


