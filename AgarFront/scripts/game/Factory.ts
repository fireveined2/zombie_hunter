﻿
import { FactorySettings, ICuttableFactory } from "./logic/cuttable/Generator"
import { Config } from "../utils/Config"
import { ICuttableInterface, ICuttable } from "./logic/cuttable/Cuttable"
import { CuttablesDatabase } from "../beforeGame/CuttablesDatabase"
import { Game } from "../beforeGame/Game"
import { Cuttable } from "./Cuttable"

export class CuttableFactory implements ICuttableFactory {

  //  public onMissedFlesh: (me?: Flesh) => void;
    protected getSprite: (key: any) => Phaser.Image;
    protected group: Phaser.Group;
    protected phaser: Phaser.Game;

    constructor( getSprite: (key: any) => Phaser.Image, group?: Phaser.Group) {
        this.getSprite = (type) => { let img = getSprite(type); this.group.add(img); return img; };
        this.phaser = Game.group.game;

        if (!group) {
            this.group = this.phaser.add.group();
            Game.group.add(this.group);
        }
        else
            this.group = group;
    }


    public create(obj: ICuttable, cuttable?: ICuttableInterface): ICuttableInterface {
        if (!cuttable) 
            cuttable = new Cuttable(this.getSprite);

        cuttable.createFromObj(obj);   
        return cuttable;
    }

}