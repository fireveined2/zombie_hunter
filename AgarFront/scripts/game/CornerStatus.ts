﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { shortStatNames as statNames } from "../utils/gui/CriterionRow"

import { Blade } from "./Blade"
import { FX } from "./FX"
import { SFX, SFXNormal, SFXRage, SFXChill } from "../SFX/SFX"
import { SoundBase } from "../SFX/SoundBase"
import { HUD } from "./hud/HudRender"
import { Replay } from "./Replay"
import { Player } from "../utils/Player"

import { BackgroundAnim } from "./BackgroundAnim"

import { AccesToPlay } from "../utils/Server"
import { GameStarter } from "../beforeGame/GameStarter"
import { GameLogic } from "./logic/Logic"
import { GameFabric } from "./GameFabric"
import { GameEvents } from "./logic/GameEvents"
import * as Models from "../../../Shared/Shared"

export class Status {


    private phaser: Phaser.State;

    private criterias: Phaser.BitmapText[] = [];
    private values: Phaser.BitmapText[]=[];
    private criteriaValues: Phaser.BitmapText[] = [];
    public sumStatsWith = <Models.GameStats>{};

    //public criterionMode: boolean = true;
   
    constructor(private group: Phaser.Group, public criterionMode = true) {
        this.phaser = group.game.state.getCurrentState();
        this.group = this.phaser.add.group(group);
        let x =  0.9;
        if (!criterionMode)
            x = 0.89;

        let marginY = Game.height*0.07;
        for (let i = 0; i < 5; i++) {
            this.criterias[i] =  this.phaser.add.bitmapText(Game.width *x, Game.height * 0.02 + marginY*i, "medium", "", undefined, this.group);
            this.criterias[i].anchor.set(1, 0);
            this.criterias[i].scale.set(0.8, 0.8);

            this.values[i] = this.phaser.add.bitmapText(Game.width *(x+0.01), Game.height * 0.02 + marginY * i, "medium", "  ", undefined, this.group);
            this.values[i].anchor.set(0, 0.5);
            this.values[i].y = Game.height * 0.02 + marginY * i + this.values[i].height / 2;
            this.values[i].scale.set(0.8, 0.8);

            this.criteriaValues[i] = this.phaser.add.bitmapText(Game.width * 0.99, Game.height * 0.02 + marginY * i, "medium", "", undefined, this.group);
            this.criteriaValues[i].anchor.set(0, 0);
            this.criteriaValues[i].scale.set(0.8, 0.8);
        }
    }

    set visible(v: boolean) {
        this.group.visible = v;

    }

    private prevStats = {};
    public updateStats(criterias: Models.ChallengeCriteria[], stats: Models.GameStats) {
        let criteriasText = "";
        let statsText = "";
        for (let i = 0; i < criterias.length; i++) {
            let stat = criterias[i].stat;
            if (stat == 'duration')
                continue;
   
            let value = stats[stat];
            if (this.sumStatsWith[stat])
                value += this.sumStatsWith[stat];

            if (value !== this.prevStats[stat]) {

            this.criterias[i].setText(statNames[stat]);

            if (this.criterionMode) 
            this.values[i].setText(value);
                else
                this.values[i].setText("+"+value);


            if (this.criterionMode) {
             //   this.criteriaValues[i].setText("/" + criterias[i].value);
              //  this.criteriaValues[i].x = this.values[i].right + 7;
            }
          
                this.values[i].scale.set(1.1,1.1);
            }

            this.prevStats[stat] = value;
        }

    }

    public update(elapsedFrames: number) {
        for (let i = 0; i < this.values.length; i++) {
            this.values[i].y = Game.height * 0.02 + Game.height * 0.07 * i + this.values[i].height / 2;
            let scale = this.values[i].scale.x;
            if (scale>0.8)
            this.values[i].scale.set(scale-0.06, scale-0.06);
        }

    }
}