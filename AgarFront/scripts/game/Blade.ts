﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { FX } from "./FX"
import { SFX } from "../SFX/SFX"

import { BladeView } from "./blade/View"
import { BladeLogic, IBlade } from "./logic/Blade"
import { BladeController } from "./blade/Controller"


export class Blade implements IBlade {

    private model: BladeLogic;
    private controller: BladeController;
    private view: BladeView;
    public mirrorInput: boolean = false;
    public disabled: boolean = false;
    set forEachNewInput(func: (x: number, y: number, frame: number) => void) {
        this.model.forEachNewInput = func;
    }

    private scale = Config.Version.www ? 0.6 : 0.5;
    constructor(game: Phaser.Game) {
        this.view = new BladeView(game);
        this.model = new BladeLogic(this.view);
        this.controller = new BladeController(game, this.model, this.addPoint.bind(this));;
        this.disabled = false;
    }

    public run() {
        this.controller.disableMouseInput(false)
    }


    set disableMouseInput(disable: boolean) {
        this.controller.disableMouseInput(disable);
        this.disabled = disable;
    }

 
    public update(frame: number, elapsed: number): Phaser.Point[] {
        return this.model.update(frame, elapsed);
    }

    public render() {
        this.model.render();
    }

    public destroy() {
        this.view.destroy();
    }

    public addPoint(pos: Phaser.Point, newSlide?: boolean) {
        if (this.disabled)
            return; 
        if (this.mirrorInput) {
            pos.x = Game.width - pos.x;
            pos.y = Game.height - pos.y;

        }
        this.model.addPoint(pos, newSlide);
    }
}