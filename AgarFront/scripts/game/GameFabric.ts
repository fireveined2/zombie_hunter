﻿import { Blade } from "./Blade"
import { CuttableFactory } from "./Factory"



export class GameFabric {

    constructor(public phaser: Phaser.Game, public group: Phaser.Group, private getFreeSprite: (key: string) => Phaser.Image) {

    }
    private blade: Blade;
    createBlade(): Blade {
        if (!this.blade)
            this.blade = new Blade(this.phaser)
        return this.blade;
    }

    getBlade() {
        return this.createBlade();
    }

    private factory: CuttableFactory;
    createCuttableFactory(): CuttableFactory {
        if (!this.factory)
            this.factory = new CuttableFactory(this.getFreeSprite, this.group)
        return this.factory;
    }

    getFactory() {
        return this.createCuttableFactory();
    }

}