﻿  
import { Config } from "../utils/Config"
declare var Buffer;
export enum Type {
    CUT,
    ENABLE_RAGE,
    BOTTLE_EXPLODEM,
    BOTTLE_IMPLODEM,
    BOTTLE_NAPALM
}

export interface Sample {
    type: Type;
    x: number;
    y: number;
    frame: number;
} 

var ab2str_arraymanipulation = function (buf) {
    var bufView = new Uint8Array(buf);
    var unis = [];
    for (var i = 0; i < bufView.length; i++) {
        unis.push(bufView[i]);
    }
    return String.fromCharCode.apply(null, unis);
}



var str2ab_arraymanipulation = function (str) {
    var buf = new ArrayBuffer(str.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0; i < str.length; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

export class Collector {

    private samples: Sample[] = [];

    constructor() {
        this.reset();
    }
    public reset() {
        this.samples = [];
    }

    static oldInput: Sample[];

    public push(type: "cut" | "rage" | "bottle_explode" | "bottle_implode" | "bottle_napalm", frame: number, x?: number, y?: number) {
        let typeId = Type.CUT;
        if (type == "rage")
            typeId = Type.ENABLE_RAGE;

        if (type == "bottle_explode")
            typeId = Type.BOTTLE_EXPLODEM;
        if (type == "bottle_implode")
            typeId = Type.BOTTLE_IMPLODEM;
        if (type == "bottle_napalm")
            typeId = Type.BOTTLE_NAPALM;

        this.samples.push(<Sample>{ type: typeId, x: x, y: y, frame: frame });
    }

    public addCut(x: number, y: number, frame: number) {
        this.push("cut", frame, x, y);
    }

    public getCompressed(): ArrayBuffer {
        let size = 0;
        for (let sample of this.samples) {
            if (sample.type == Type.CUT)
                size += 8;
            else
                size += 4;
        }

        //version code
        size += 2;
  

        let buffer = new Buffer(size);

        //version
        buffer.writeUInt16LE(61000, 0);

        let offset = 2;
        for (let sample of this.samples) {
            buffer.writeUInt32LE(sample.type + sample.frame * 8, offset);
            offset +=4;

            if (sample.type == Type.CUT) {         
                buffer.writeInt16LE(sample.x, offset);
                offset += 2;
                buffer.writeInt16LE(sample.y, offset);
                offset += 2;
            }
        }
        let buf = ab2str_arraymanipulation(buffer);
   
        return buf;
    }

    public decompress(input: ArrayBuffer): Sample[] {

        let inp = str2ab_arraymanipulation(input);
        let buffer = new Buffer(inp);
        let samples: Sample[] = [];

        let factor = 10;
        let offset = 0;
        let frameSize = 16;
            while (offset < buffer.length) {
                let sample = <Sample>{}
                if (frameSize == 16) {
                    sample.type = buffer.readUInt16LE(offset);
                    offset += 2;
                }
                else {
                    sample.type = buffer.readUInt32LE(offset);
                    offset += 4;
                }
                if (offset == 2 && sample.type == 50000) {
                    factor = 4;
                    sample.type = buffer.readUInt16LE(offset);
                    offset += 2;
                }

                if (offset == 2 && sample.type == 60000) {
                    factor = 4;
                    frameSize = 32;
                    sample.type = buffer.readUInt32LE(offset);
                    offset += 4;
                }


                if (offset == 2 && sample.type == 61000) {
                    factor = 8;
                    frameSize = 32;
                    sample.type = buffer.readUInt32LE(offset);
                    offset += 4;
                }

                sample.frame = Math.floor(sample.type / factor);
                sample.type = sample.type % factor;
               

                if (sample.type == Type.CUT) {
                    sample.x = buffer.readInt16LE(offset);
                    offset += 2;
                    sample.y = buffer.readInt16LE(offset);
                    offset += 2;
                }
                samples.push(sample);
        }

            Collector.oldInput = JSON.parse(JSON.stringify(samples));
        return samples;
    }
}