﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { SFX } from "../../SFX/SFX"
import { IBladeView, Segment } from "../logic/Blade"
declare var InstallTrigger;

export class BladeView implements IBladeView{

    private bitmap: Phaser.BitmapData;
    private phaser: Phaser.Game;
    
    private scale = Config.Version.www ? 0.6 : 0.5;

    constructor(phaser: Phaser.Game) {
        this.phaser = phaser;
        this.bitmap = phaser.add.bitmapData(Game.width * this.scale, Game.height * this.scale);
        let sprite = this.bitmap.addToWorld(0, 0);
        
        sprite.scale.set(1/this.scale);

        let pattern = this.phaser.add.bitmapData();
        pattern.load("bladePattern");

        if (!this.canvasPattern)
            this.canvasPattern = this.canvas.createPattern(pattern.canvas as any, "repeat");

        if (!this.canvasPatternChill)
            this.canvasPatternChill = this.canvas.createPattern(pattern.load("bladePatternChill").canvas as any, "repeat");


        Game.group.add(sprite);
    }

    get canvas(): CanvasRenderingContext2D {
        return this.bitmap.context;
    }

    public clearCanvas() {
        this.canvas.clearRect(0, 0, this.bitmap.width, this.bitmap.height);
    }
            

    public destroy() {
        this.bitmap.destroy();
    }

    private drawBackground(segments: Segment[], size: number) {
        let canvas = this.canvas;
        canvas.lineWidth = size * Game.assetsSize;;
        canvas.lineCap = "round";

        for (let i = 1; i < segments.length; i++) {
            let prev = segments[i - 1];
            let cur = segments[i];
            let gradient = canvas.createLinearGradient(prev.point.x * this.scale, prev.point.y * this.scale, cur.point.x * this.scale, cur.point.y * this.scale);
            gradient.addColorStop(0, "rgba(0,0,0," + prev.alpha + ")");
            gradient.addColorStop(1, "rgba(0,0,0," + cur.alpha + ")");
            canvas.strokeStyle = gradient;

            canvas.beginPath();
            canvas.moveTo(prev.point.x * this.scale, prev.point.y * this.scale);
            canvas.lineTo(cur.point.x * this.scale, cur.point.y * this.scale);
            canvas.stroke();
        }
    }

    private canvasPatternChill: CanvasPattern;
    private canvasPattern: CanvasPattern;
    private drawBlade(segments: Segment[], size: number) {
        if (segments.length < 2)
            return;
        let canvas = this.canvas;

        let chill = Config.Global.delta != Config.Global.deltaGraphics;
        if (!chill)
            canvas.strokeStyle = this.canvasPattern;
        else
            canvas.strokeStyle = this.canvasPatternChill;

        canvas.lineWidth = size * Game.assetsSize;
        canvas.lineJoin = "round";
        canvas.lineCap = "square";
        canvas.globalCompositeOperation = "source-in";

        canvas.moveTo(segments[0].point.x * this.scale, segments[0].point.y * this.scale);
        for (let i = 1; i < segments.length; i++) {
            let cur = segments[i].point;
            canvas.lineTo(cur.x * this.scale, cur.y * this.scale);
        }
        canvas.stroke();
        canvas.globalCompositeOperation = "source-over";
    }


   

    public render(segments: Segment[]) {
        if (segments.length <= 1)
            return;
        this.drawBackground(segments, 17 * this.scale);
        this.drawBlade(segments, 10 * this.scale);
        this.bitmap.dirty = true;
    }

   

    private updateBladeSound(segments: Segment[]) {
        let l = segments.length;
        if (l < 5)
            return;

        let dist = segments[l - 1].distance - segments[l - 5].distance;
        let elapsed = (segments[l - 1].time - segments[l - 5].time) / 1000;
        if (dist / elapsed > Config.SFX.bladeSoundMinDistancePerSec)
            SFX.get().playCut();
    }

    public update(segments: Segment[]) {
        this.updateBladeSound(segments);

    }

}