﻿import { Game } from "../../beforeGame/Game"
import { Config } from "../../utils/Config"
import { SFX } from "../../SFX/SFX"
import { BladeLogic, IBladeController } from "../logic/Blade"
import { BladeView } from "./View"

declare var InstallTrigger;

export class BladeController implements IBladeController{
    constructor(private phaser: Phaser.Game, private model: BladeLogic, private addPoint: Function) {

    }

    public run() {
        this.disableMouseInput(false);
    }

    private mouseHandleFuncBinded = this.handleMouseMove.bind(this);
    public disableMouseInput(disable: boolean) {
        if (disable) {
            if (Config.Version.www)
                this.phaser.canvas.removeEventListener("mousemove", this.mouseHandleFuncBinded);
            else
                this.phaser.input.deleteMoveCallback(this.handleInput, this);
            this.model.keyUp = false;
        }
        else {
            if (Config.Version.www)
                this.phaser.canvas.addEventListener("mousemove", this.mouseHandleFuncBinded);
            else
                this.phaser.input.addMoveCallback(this.handleInput, this);
        } 
    }

 


    private fixWhich(e) {
        e.clicked = e.which;
        if (!e.which && e.button) {
            if (e.button & 1) e.clicked = 1      
            else if (e.button & 4) e.clicked = 2 
            else if (e.button & 2) e.clicked = 3 
        }
        if (typeof InstallTrigger !== 'undefined')
            if (!(e.buttons & 1))
                e.clicked = 0;
    }

    private handleMouseMove(event) {
        this.fixWhich(event);
        if (event.clicked != 1) {
            this.model.keyUp = true;
            return;
        }
        let rect = this.phaser.canvas.getBoundingClientRect();
        let x = event.clientX - rect.left;
        let y = event.clientY - rect.top;
         //TODO nie tworzyć za kazdym razem nowych punktów
        let pos = new Phaser.Point(Math.round(x * Game.assetsScale), Math.round(y * Game.assetsScale));
        pos.x = Math.round(pos.x / 2) * 2;
        pos.y = Math.round(pos.y / 2) * 2;
        this.addPoint(pos);
    }

    private handleInput(pointer: Phaser.Pointer, x: number, y: number) {
         //TODO nie tworzyć za kazdym razem nowych punktów
        let pos = new Phaser.Point(Math.round(x * Game.assetsScale), Math.round(y * Game.assetsScale));
        pos.x = Math.round(pos.x);
        pos.y = Math.round(pos.y);
        this.addPoint(pos);
    }
}