﻿import * as Models from "../../../Shared/Shared"
import { SFX } from "../SFX/SFX"
import { Game } from "../beforeGame/Game"

export interface IHeroAnimElement {
    anchor: Phaser.Point;
    pos: Phaser.Point;
    anim?: boolean;

    anchorOnArm?: boolean;
}

 var Item = Models.ItemType;

export class HeroAnim{


    static disableSecondaryWeaponAnim = undefined;
    private hero: Phaser.Group;
    private head: Phaser.Image;
    private primaryWeapon: Phaser.Image;
    private secondaryWeapon: Phaser.Image;
    private secondaryWeaponPos: Phaser.Point;
    private boots: Phaser.Image;

    private corpus: Phaser.Image;
    private rightArm: Phaser.Image;
    private legs: Phaser.Image;

    private body: Phaser.Image;

    private leftArm: Phaser.Image;
    private blood: Phaser.Image;

    private elements: { [index: string]: IHeroAnimElement } = {} as any;

    private functions: { time: number, function: Function }[] = [];

    private alive: boolean = true;

    private initItems() {
        let name = "hero0/"
        let elem: IHeroAnimElement;


        //boots
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 240) }
        this.elements["hero0/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 240) }
        this.elements["hero1/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 240) }
        this.elements["hero2/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-24, 231) }
        this.elements["hero3/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-27, 237) }
        this.elements["hero4/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-44, 231) }
        this.elements["hero6/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 240) }
        this.elements["hero9/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 217) }
        this.elements["hero10/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 217) }
        this.elements["hero11/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 221) }
        this.elements["hero12/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-43, 220) }
        this.elements["hero13/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 217) }
        this.elements["hero14/" + "boots"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11), pos: new Phaser.Point(-40, 217) }
        this.elements["hero15/" + "boots"] = elem;

        //left arm
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero0/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero1/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero2/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero9/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero10/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero11/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero12/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero13/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero14/" + "leftArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero15/" + "leftArm"] = elem;

        //right arm
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-45, 18) }
        this.elements["hero0/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero1/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero2/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero9/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero10/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero11/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero12/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero13/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero14/" + "rightArm"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 20), pos: new Phaser.Point(-50, 18) }
        this.elements["hero15/" + "rightArm"] = elem;

        //legs
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-57, 121) }
        this.elements["hero0/" +  "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-53, 121) }
        this.elements["hero1/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-45, 121) }
        this.elements["hero2/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-45, 121) }
        this.elements["hero9/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-45, 121) }
        this.elements["hero10/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-45, 121) }
        this.elements["hero11/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-53, 116) }
        this.elements["hero12/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-61, 106) }
        this.elements["hero13/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-45, 121) }
        this.elements["hero14/" + "legs"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7), pos: new Phaser.Point(-45, 121) }
        this.elements["hero15/" + "legs"] = elem;

        //body
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(64, 12), pos: new Phaser.Point(0, 0) }
        this.elements["hero0/" +  "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(64, 12), pos: new Phaser.Point(0, 0) }
        this.elements["hero1/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(0, 5) }
        this.elements["hero2/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(0, 5) }
        this.elements["hero9/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(0, 5) }
        this.elements["hero10/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(0, 5) }
        this.elements["hero11/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(10, 5) }
        this.elements["hero12/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(10, 5) }
        this.elements["hero13/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(0, 5) }
        this.elements["hero14/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(0, 5) }
        this.elements["hero15/" + "body"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(78, 28), pos: new Phaser.Point(9, 5) }




        //primary weapon
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(45, 60), pos: new Phaser.Point(-132, 29) }
        this.elements["hero0/" +  "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(45, 60), pos: new Phaser.Point(-125, 12) }
        this.elements["hero1/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(45, 60), pos: new Phaser.Point(-130, 5) }
        this.elements["hero2/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(45, 60), pos: new Phaser.Point(-125, 12) }
        this.elements["hero3/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(45, 60), pos: new Phaser.Point(-125, 12) }
        this.elements["hero4/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(48, 80), pos: new Phaser.Point(-129, 12) }
        this.elements["hero5/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(48, 80), pos: new Phaser.Point(-129, 12) }
        this.elements["hero6/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(40, 85), pos: new Phaser.Point(-129, 12) }
        this.elements["hero7/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(43, 110), pos: new Phaser.Point(-129, 12) }
        this.elements["hero8/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(43, 110), pos: new Phaser.Point(-129, 41) }
        this.elements["hero9/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(43, 110), pos: new Phaser.Point(-129, 41) }
        this.elements["hero10/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(48, 110), pos: new Phaser.Point(-131, 32) }
        this.elements["hero11/" + "primaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(48, 110), pos: new Phaser.Point(-132, 24) }
        this.elements["hero14/" + "primaryWeapon"] = elem;

        //head
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(85, 130), pos: new Phaser.Point(-2, -2) }
        this.elements["hero0/" +  "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(85, 130), pos: new Phaser.Point(-2, 3) }
        this.elements["hero1/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(85, 130), pos: new Phaser.Point(2, 7) }
        this.elements["hero2/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(56, 106), pos: new Phaser.Point(-2, 3) }
        this.elements["hero3/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 124), pos: new Phaser.Point(2, 7) }
        this.elements["hero4/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(73, 140), pos: new Phaser.Point(-10, -12) }
        this.elements["hero6/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 164), pos: new Phaser.Point(-8, -12) }
        this.elements["hero7/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(74, 124), pos: new Phaser.Point(2, 15) }
        this.elements["hero9/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 119), pos: new Phaser.Point(-14, 7) }
        this.elements["hero10/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(83, 169), pos: new Phaser.Point(-14, -12) }
        this.elements["hero11/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 119), pos: new Phaser.Point(-14, -12) }
        this.elements["hero12/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 119), pos: new Phaser.Point(-14, -12) }
        this.elements["hero13/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 119), pos: new Phaser.Point(-14, -12) }
        this.elements["hero14/" + "head"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(68, 119), pos: new Phaser.Point(-14, -12) }
        this.elements["hero15/" + "head"] = elem;

        //secondary weapomn
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(120, 86), pos: new Phaser.Point(58, 87), anim: !HeroAnim.disableSecondaryWeaponAnim, anchorOnArm: true}
        this.elements["hero0/" + "secondaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(120, 86), pos: new Phaser.Point(58, 87), anchorOnArm: true }
        this.elements["hero1/" + "secondaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero2/" + "secondaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero3/" + "secondaryWeapon"] = elem;
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1), pos: new Phaser.Point(40, 17) }
        this.elements["hero4/" + "secondaryWeapon"] = elem;


    } 


    constructor(private phaser: Phaser.State, parent: Phaser.Group) {
        if (HeroAnim.disableSecondaryWeaponAnim === undefined)
            HeroAnim.disableSecondaryWeaponAnim = (Game.settings.effectLevel == 0);

        this.initItems();
        let name = "hero0/"
        let elem: IHeroAnimElement;

        this.hero = phaser.add.group(parent);

        this.blood = this.phaser.add.image(0, 260, "hero/blood", 0, this.hero);
        this.blood.anchor.set(0.5);
        this.blood.visible = false;


        //boots
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(35, 11) }
        let boots = this.boots = phaser.add.image(-40, 247, name + "boots", 0, this.hero);
   

        //left arm
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(4, 1) }
        let arm = this.leftArm = phaser.add.image(40, 17, name + "leftArm", 0, this.hero);


        //right arm
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(100, 22) }
        this.rightArm = phaser.add.image(-50, 13, name + "rightArm", 0, this.hero);

        //legs
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(14, 7) }
        this.legs = phaser.add.image(-53, 121, name + "legs", 0, this.hero);


        //body
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(64, 12) }
        let body = this.body = phaser.add.image(0, 0, name + "blood/body", 0, this.hero);
       

        //primary weapon
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(33, 80) }
        let weapon = this.primaryWeapon = phaser.add.image(-137, 29, name + "blood/primaryWeapon", 0, this.hero);
 

        //head
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(85, 130) }
        let head = this.head = phaser.add.image(-2, 3, name + "head", 0, this.hero);


        //secondary weapomn
        elem = <IHeroAnimElement>{ anchor: new Phaser.Point(120, 86) }
        this.secondaryWeapon = phaser.add.image(81, 113, name + "secondaryWeapon", 0, this.hero);
        this.secondaryWeaponPos = new Phaser.Point(62, 85);

        this.setBasicSet();

        this.animHead();
        this.animPrimaryWeapon();
        if (!HeroAnim.disableSecondaryWeaponAnim)
            this.moveLeftArm();
    }

    public kill() {
        this.alive = false;
    }

    public getTop() {
        return this.head.top + this.hero.y;
    }

    public setBasicSet(bloodVersion: boolean = false) {
        let type = Models.ItemType;
        this.setItem({ type: type.HEAD, set: 0} as any, bloodVersion);
        this.setItem({ type: type.BOOTS, set:0 } as any, bloodVersion);
        this.setItem({ type: type.CORPUS, set: 0} as any, bloodVersion);
        this.setItem({ type: type.WEAPON1, set: 0 } as any, bloodVersion);
        this.setItem({ type: type.WEAPON2, set:0} as any, bloodVersion);
    }


    public setItemsFromSetIds(ids: number[], bloodVersion = false) {
        for (let i = 0; i < ids.length; i++) {
            this.setItem({ type: i, set: ids[i]} as any, bloodVersion);
        }
    }

    public setItems(items: Models.IItem[], bloodVersion: boolean = false) {
        for (let item of items)
            this.setItem(item, bloodVersion);
    }

    public setItem(item: Models.IItem, bloodVersion: boolean = false) {
        let name = "hero" + item.set + "/";
        if (bloodVersion) {
            if (item.type != Models.ItemType.WEAPON2)
            name += "blood/";
            this.blood.visible = true;
        }


        let type = Models.ItemType;
        let elems: string[] = [];

        switch (item.type) {
            case type.HEAD:
                elems.push("head");
                break;

            case type.BOOTS:
                elems.push("boots");
                break;

            case type.CORPUS:
                elems.push("body", "leftArm", "rightArm", "legs");
                break;

            case type.WEAPON1:
                elems.push("primaryWeapon");
                break;

            case type.WEAPON2:
                elems.push("secondaryWeapon");
                break;
        }

        for (let elem of elems) {
            let image = this[elem] as Phaser.Image;


      // jeśli nie ma wersji z krwią, załaduj normalną
            let textureName = name + elem;
            if (!this.phaser.cache.checkImageKey(textureName))
                textureName = textureName.replace("blood/", "");;

                image.loadTexture(textureName);
                image.name = textureName;
           

            let imageName = name.replace("blood/","");
            this.setAnchorFor(image, this.elements[imageName+ elem].anchor);
            image.data = this.elements[imageName + elem];

            if (elem == "secondaryWeapon") {
                let element = this.elements[imageName + elem];
                this.secondaryWeaponPos = element.pos;
                if (element.anim)
                image.frameName = "bron0001";
            }
            else
                image.position = this.elements[imageName + elem].pos;

        }   
    }

    get height() {
        return this.boots.y + (1 - this.boots.anchor.y) * this.boots.height;
    }

    set visible(v: boolean) {
        this.hero.visible = v;
    }
    get scale() {
       return  this.hero.scale;
    }

    public updateAnim() {
        let time = Date.now();

        let headFrame = time / 40;

        for (let i = 0; i < this.functions.length; i++) {
            let fnc = this.functions[i];
            if (fnc.time > time)
                continue;

            fnc.function();
            this.functions.splice(i, 1);
            i--;
        }


        this.body.scale.set(Math.sin(time/400)*0.008+1);
        this.animSecondaryWeapon(time);
    }

    private animHead() {
        this.head.angle = -7;
        let t = this.phaser.add.tween(this.head).to({ angle: 5 }, 400).start();
        let t2 = this.phaser.add.tween(this.head).to({ angle: -6}, 550).delay(2200);
        t.onComplete.add(() => t2.start());
        t2.onComplete.add(() => t.delay(Math.random()*2000+800).start());

        let blink = () => {
            this.blink(300);
            this.planFunction(450, () => this.blink(250));
            this.planFunction(Math.random()*2000+1500, blink);
        }
        this.planFunction(100, blink);
    }

    private planFunction(delay: number, fnc: Function) {
        if (!this.alive)
            return;
         
        this.functions.push({ time: delay + Date.now(), function: fnc });
    }


    private blink(duration: number, callback?: Function) {
        if (!this.alive)
            return;

        if (this.head.name.indexOf("blink") != -1)
            return;
        if (this.phaser.cache.checkImageKey(this.head.name + "_blink"))
          this.head.loadTexture(this.head.name + "_blink");

        setTimeout(() => {
            if (!this.alive)
                return;
            this.head.loadTexture(this.head.name);
            if (callback)
                callback();
        }, duration);
    }

    private animPrimaryWeapon() {
        let startAngle = -9;
        this.primaryWeapon.angle = startAngle;
        let t = this.phaser.add.tween(this.primaryWeapon).to({ angle: startAngle - 8 }, 400, null, false, 1800 + Math.random()*2000)
            .to({ angle: startAngle  }, 300, null, false)
            .to({ angle: startAngle - 7 }, 300, null, false, 200)
            .to({ angle: startAngle }, 230, null, false)
            .to({ angle: startAngle - 3 }, 250, null, false, 200)
            .to({ angle: startAngle }, 180, null, false)
            .start()
            .loop();
    }

    private weaponAnimStartTime = 0;
    private animSecondaryWeapon(time: number) {
      
        let pos = new Phaser.Point(this.secondaryWeaponPos.x, this.secondaryWeaponPos.y).rotate(0, 0, this.leftArm.angle, true);

        if (this.secondaryWeapon.data.anchorOnArm) {
            this.secondaryWeapon.x = pos.x + this.leftArm.x;
            this.secondaryWeapon.y = pos.y + this.leftArm.y;
        } else {
            this.secondaryWeapon.x = pos.x;
            this.secondaryWeapon.y = pos.y;
        }
        this.secondaryWeapon.angle = this.leftArm.angle;

        if (this.secondaryWeapon.data.anim) {
            let frame = Math.round((Date.now() - this.weaponAnimStartTime) / 40);;
            if (frame > 84)
                frame = 84;

            let name = "bron00";
            if (frame < 10)
                name += "0" + frame;
            else
                name += frame;
            this.secondaryWeapon.frameName = name;
        }
    }

    private moveLeftArm() {
        if (!this.secondaryWeapon.data.anim)
            return;

        let move = () => {
            if (!this.secondaryWeapon.data.anim)
                return;

            SFX.get().playSingleEffect("menu/chain");
            this.weaponAnimStartTime = Date.now();
            let t = this.phaser.add.tween(this.leftArm).to({ angle: -4 }, 120, Phaser.Easing.Sinusoidal.In)
                .to({ angle: 4 }, 120)
                .to({ angle: -6 }, 130)
                .to({ angle: 7 }, 140)
                .to({ angle: -5 }, 110)
                .to({ angle: 6 }, 160)
                .to({ angle: -7 }, 90)
                .to({ angle: 3 }, 210)
                .start()
            this.planFunction(7000, move);
        }
        this.planFunction(5000, move);
    }

    private setAnchorFor(img: Phaser.Image, anchor: Phaser.Point) {
        img.anchor.x = anchor.x / img.width;
        img.anchor.y = anchor.y / img.height;
    }


    get position() {
        return this.hero.position;
    }
}