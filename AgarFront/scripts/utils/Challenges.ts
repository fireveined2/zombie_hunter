﻿
import * as Server from "./Server"
import "../../../Shared/Shared"
export * from "../../../Shared/Shared"
import { Player } from "./Player"

export class Challenges {

    static data: Server.IChallengeData[] = [];
    static quests: Server.IChallengeData[][] = [];

    static tourneys: Server.IChallengeData[] = [];

    static downloaded: boolean = false;
    static onUpdated: Function;

    static download(token: string = "") {
        Server.Request.getChallenges(token, Challenges.update.bind(this), Challenges.onError.bind(this));
    }

    private static parse(challenge: Server.IChallengeData) {
        if (!challenge.data.prize)
            challenge.data.prize = <any>{
                id: 0,
                name: "Respekt",
                type: -1,
                icon: 0,
                bonus: {}
            }
    }

    static reset() {
        this.data = [];
        this.quests = [];
        this.tourneys = [];

    }
    private static updateOne(challenge: Server.IChallengeData) {
        this.parse(challenge);
        let array;
        let quest = challenge.data.quest;
        if (challenge.data.type == Server.ChallengeType.TOURNEY)
            array = this.tourneys;
        else
            if (quest == -1)
                array = this.data;
            else {
                if (!this.quests[quest])
                    this.quests[quest] = [];
                array = this.quests[quest];
            }

        for (let i = 0; i < array.length; i++)
            if (array[i].data.id == challenge.data.id) {
                array[i] = challenge;
                return;
            }
        array.push(challenge);
    }

    static update(data: Server.IChallengeData[]) {
  
        let normalChallenges = 0;
        for (let challenge of data) {
            this.updateOne(challenge);
            if (challenge.data.quest == -1)
                normalChallenges++;
        }

        if (normalChallenges > 4) {
            this.sort(this.data);
            for (let quests of this.quests)
                this.sort(quests);
        }

        if (this.onUpdated) {
            this.onUpdated();
            delete this.onUpdated;
        }
        Challenges.downloaded = true;
        console.log(data);
    }

    static sort(data: Server.IChallengeData[]) {
        if (!data || !data.length)
            return;

        console.log("sort");
        let lvl = Player.get().data.level;
        let respect = Player.get().data.respect;

        function compareNumbers(a: Server.IChallengeData, b: Server.IChallengeData) {

            let a1 = (a.data.minLevel <= lvl && a.data.cost <= respect);
            let b1 = (b.data.minLevel <= lvl && b.data.cost <= respect);
            if (a.data.type == 3)
                return -1;

            if (b.data.type == 3)
                return 1;

            if (a1 == b1) {
                return a.data.type - b.data.type;
            }

            return (+b1) - (+a1);  // (!a1)?-1:((!b1)?1:0);
        }
        data = data.sort(compareNumbers);
        console.log(data);
    }

    static remove(id: number) {
        let seekAndDestroy = (array: Server.IChallengeData[]) => {
            for (let i = 0; i < array.length; i++)
                if (array[i].data.id == id)
                    array.splice(i, 1);
        }

        seekAndDestroy(this.data);
        seekAndDestroy(this.tourneys);
        for (let map in this.quests)
            seekAndDestroy(this.quests[map]);
    }

    static get(id: number): Server.IChallengeData {
        if (!this.data)
            return;

        for (let ch of this.tourneys)
            if (ch.data.id == id)
                return ch;


        for (let ch of this.data)
            if (ch.data.id == id)
                return ch;

        for (let key in this.quests) {
            let quest = this.quests[key];
            for (let ch of quest)
                if (ch.data.id == id)
                    return ch;
        }
    }
    private static onError(error: Server.RequestError) {

    }
}