﻿import { Game } from "../beforeGame/Game"
import { Config } from "./Config"
export class WebPage {

    private static loadPage(url: string, callback: Function, changeFunction?: (content: string) => string) {
        var xmlhttp = new XMLHttpRequest();
        this.loaded = true;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4) {
               
                /*
                var newdiv = document.createElement("div");
                newdiv.innerHTML = xmlhttp.responseText;
                var container = document.getElementById("content");
                container.appendChild(newdiv);
                */
                let text = xmlhttp.responseText;
                if (changeFunction)
                    text = changeFunction(xmlhttp.responseText);
                   document.getElementById('content').innerHTML += text;
                    setTimeout(() => callback(), 10);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }


    static load(url: string, callback: (elem: HTMLElement) => void, portrait: boolean = true, useReturnButton: boolean = false) {
        this.loadPage(url, () => {
            document.getElementById('game-window').style.visibility = 'hidden';
            document.getElementById('game-window').style.position = 'absolute';
            document.getElementById('game-window').style.pointerEvents = 'none';
            document.getElementById('content').style.position = 'absolute';
            document.getElementById('content').style.top = '0';
            document.getElementById('content').style.left = '0';
            document.getElementById('content').style.pointerEvents = 'auto';

            Game.group.game.world.visible = false;
            callback(document.getElementById('content'));

            if (portrait && !Config.Version.www)
                (window.screen as any).orientation.lock('portrait');

            if (useReturnButton) {
                $("#return").click(<any>(() => this.unload()));
                $(".return").click(<any>(() => this.unload()));
                $("#content").delegate(".return", "click", <any>(() => this.unload()));
            }
            });
        return document.getElementById('content')
    }


    static loadAndChangeContent(changeFunction: (content: string)=>string, url: string, callback: (elem: HTMLElement) => void, portrait: boolean = true, useReturnButton: boolean = false) {
        this.loadPage(url, () => {
            document.getElementById('game-window').style.visibility = 'hidden';
            document.getElementById('game-window').style.position = 'absolute';
            document.getElementById('game-window').style.pointerEvents = 'none';
            document.getElementById('content').style.position = 'absolute';
            document.getElementById('content').style.top = '0';
            document.getElementById('content').style.left = '0';
            document.getElementById('content').style.pointerEvents = 'auto';
            Game.group.game.world.visible = false;
            callback(document.getElementById('content'));

            if (portrait && !Config.Version.www)
                (window.screen as any).orientation.lock('portrait');

            if (useReturnButton) {
                $("#return").click(<any>(() => this.unload()));
                $(".return").click(<any>(() => this.unload()));
                $("#content").delegate(".return", "click", <any>(() => this.unload()));
            }
        }, changeFunction);
        return document.getElementById('content')
    }



    static loadInBackground(url: string, callback: () => void) {
        this.loadPage(url, () => {
            document.getElementById('content').style.position = 'absolute';
            document.getElementById('game-window').style.position = 'absolute';

            document.getElementById('game-window').style.pointerEvents = 'none';
            document.getElementById('content').style.pointerEvents = 'auto';
            callback();


            if (!this.handleWallClick) {
          
                this.handleWallClick = this._handleWallClick.bind(this);
               if (!Config.Version.www)
                    window.addEventListener("touchstart", this.handleWallClick, true);
                else
                   window.addEventListener("mousedown", this.handleWallClick, true);
            }
        });

        Game.group.game.input.onDown.add(() => {
            console.log("CLick!", Game.group.game.input.activePointer.position);

        });

        return document.getElementById('content')
    }

    private static  handleWallClick;

    private static _handleWallClick(ev) {
            let e: any = ev;
            if ((e as any).processed)
                return;

            if (document.getElementById('game-window').style.pointerEvents != "none")
                return; 
            let y = 0;
            if (!Config.Version.www)
                y = (e as any).touches[0].screenY;
            else
                y = e.pageY - $("canvas").get(0).getBoundingClientRect().top;

            let height = window.innerHeight;

            if (Config.Version.www)
                height = 600;

            console.log(y, height);
            if (y / height > (Game.height - 70) / Game.height) {

          

                document.getElementById('game-window').style.pointerEvents = 'auto';
                Game.group.game.input.mouse._onMouseDown(e);
                 //   Game.group.game.input.mouse._onMouseUp(e);
                    if ((<any>Game.group.game.input.touch)._onTouchStart) {
                        (<any>Game.group.game.input.touch)._onTouchStart(e);
                        (<any>Game.group.game.input.touch)._onTouchEnd(e);

                    
          

                }

   
  
                setTimeout(() => {
                    if (document.getElementById('content').style.pointerEvents != "none")
                    document.getElementById('game-window').style.pointerEvents = 'none';
                }, 200);


            }
        }

    static openInSystemBrowser(url: string) {
        (cordova as any).InAppBrowser.open(url, "_system");
    }

    private static loaded: boolean = false;
    static unloadIfLoaded() {
        if (this.loaded)
            this.unload();
    }

    static isCurerentlyLoadedInBackground() {
        return this.handleWallClick != undefined;
    }

    static unload() {
        if (this.handleWallClick) {
            window.removeEventListener("touchstart", this.handleWallClick);
            window.removeEventListener("mousedown", this.handleWallClick);
        }
        this.handleWallClick = undefined;

        document.getElementById('game-window').style.pointerEvents = 'auto';
        let returnBtn = document.getElementById('return');
        if (returnBtn) 
            returnBtn.onclick = null;
        Game.group.game.world.visible = true;
        document.getElementById('game-window').style.visibility = 'visible';
        document.getElementById('game-window').style.position = 'initial';
       // document.getElementById('content').style.display = 'none';
        document.getElementById('content').innerHTML = "";
        document.getElementById('content').style.pointerEvents = 'none';


      if (!Config.Version.www)
 (window.screen as any).orientation.lock('landscape');
    }
}

declare var Touch: any;
declare var TouchEvent: any;
/* eventType is 'touchstart', 'touchmove', 'touchend'... */
function sendTouchEvent(x, y, element, eventType) {
    const touchObj = new Touch({
        identifier: Date.now(),
        target: element,
        clientX: x,
        clientY: y,
        radiusX: 2.5,
        radiusY: 2.5,
        rotationAngle: 10,
        force: 0.5,
    });

    const touchEvent = new TouchEvent(eventType, {
        cancelable: true,
        bubbles: true,
        touches: [touchObj],
        targetTouches: [],
        changedTouches: [touchObj],
        shiftKey: true,
    });

    element.dispatchEvent(touchEvent);
}
