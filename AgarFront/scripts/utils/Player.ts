﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { GameStats } from "../game/logic/GameStats"
import { GameLogic } from "../game/logic/Logic"
import { Challenges } from "../utils/Challenges"
import { Messages } from "../utils/Messages"

import * as GUI from "../utils/gui/gui"

import { SFX } from "../SFX/SFX"
import { GameSettings } from "../beforeGame/Settings"

import { ZHConfig, updaterConfig } from "./ZHConfig"

import * as Server from "../utils/Server"
import { socketClient } from "./server/Socket"

export class Player {

    private socket
    private static instance: Player;
    private storage: Storage;

    public logged: boolean = false;
    public username: string;

    private preparedAccesToPlay: Server.AccesToPlay;
    private challengeAccesToPlay: Server.AccesToPlay;

    private token: string;

    public data: Server.PlayerData;

    private usedItems: number[] = [];

    constructor() {
        this.storage = window.localStorage;

        if (!this.data)
            this.data = <Server.PlayerData>{
                hp: 1,
                exp: 1,
                hcn: 0,
                items: [],
                respect: 0,
                username: "Gracz",
                level: 1,
                wearedItemsids: []
            }
    }

    public logout() {
        this.logged = false;
        this.token = "";
        this.data = <Server.PlayerData>{
            hp: 1,
            exp: 1,
            hcn: 0,
            items: [],
            respect: 0,
            username: "Gracz",
            level: 1,
            wearedItemsids: []
        }
        socketClient.disconnect();
    }

    get respect(): number {
        return this.data.respect;
    }

    private heal(value: number) {
        this.data.hp = Math.min(1, this.data.hp + value);
    }

    public useItem(item: Server.IItem) {
        for (let stat in item.bonus)
            if (stat == "heal")
                this.heal(item.bonus[stat]);

        let items = this.data.items;
        let index = items.indexOf(item);
        items.splice(index, 1);
        this.usedItems.push(item.id);
    }

    get status(): { logged: boolean, online: boolean, onlineAndLogged: boolean } {
        let online = false;
        if (navigator.network && navigator.network.connection.type != Connection.NONE)
            online = true;
        if (!navigator.network)
            online = true;

        let a = <any>{ logged: this.logged, online: online };
        a.onlineAndLogged = a.online && a.logged;
        return a;
    }


    public records = {
        set: (map: number, record: number) => {
            this.storage.setItem("map" + map + "/record", record.toString());
        },
        get: (map: number) => {
            let d = this.storage.getItem("map" + map + "/record");
            return parseInt(d ? d : "0");
        }
    };

    public isMapUnlocked(map) {
        for (let item of this.data.items)
            for (let stat in item.bonus)
                if (stat == "unlockMap" && item.bonus[stat] == map)
                    return true;

        return false;
    }

    public getWearedItem(type: Server.ItemType) {
        for (let item of this.data.items)
            if (item.id == this.data.wearedItemsids[type])
                return item;
    }

    public getAllWearedItems() {
        let type = Server.ItemType;
        let types = [type.HEAD, type.CORPUS, type.BOOTS, type.WEAPON1, type.WEAPON2];
        let items: Server.IItem[] = [];
        for (let typ of types) {
            let item = this.getWearedItem(typ);
            if (item)
                items.push(item);
        }
        return items;
    }

    public putOnItem(item: Server.IItem) {
        this.data.wearedItemsids[item.type] = item.id;
    }

    public saveEQ() {
        let change = <Server.IEQChange>{
            usedItemsIds: this.usedItems,
            wearedItemsIds: this.data.wearedItemsids
        }
        Server.Request.saveEQ(this.token, change, () => { }, () => { })
        this.usedItems = [];
    }

    public saveSettings(settings: GameSettings) {
        this.storage.setItem("effects", settings.effectLevel.toString());
        this.storage.setItem("side", settings.side);
        this.storage.setItem("under18", settings.under18 ? "1" : "0");
    }

    public loadSettings(): GameSettings {
        if (!this.storage.getItem("side"))
            return {
                effectLevel: Config.Version.www ? 2 : 0,
                side: "right",
                under18: true
            };

        if (this.storage.getItem("effectsLevelReseted") != "true") {
            this.storage.setItem("effects", "0");
            this.storage.setItem("effectsLevelReseted", "true");
        }

        let settings = <GameSettings>{};
        settings.effectLevel = parseInt(this.storage.getItem("effects"));
        settings.side = this.storage.getItem("side") as any;
        settings.under18 = this.storage.getItem("under18") == "1" ? true : false;
        return settings;
    }


    public getRecord(): number {
        return parseInt(this.storage.getItem("record")) | 0;
    }

    public setRecord(record: number) {
        this.storage.setItem("record", record.toString());
    }


    private onSuccesfulLogin(token: string) {
        console.log(token);
        this.token = token;
        socketClient.connect(token);
    }

    get savedCredentials(): { email: string, password: string } {
        return {
            email: this.storage.getItem("credentials_email"),
            password: this.storage.getItem("credentials_password")
        };
    }

    public saveCredentials(mail: string, password: string) {
        this.storage.setItem("credentials_email", mail);
        this.storage.setItem("credentials_password", password);
    }

    public confirmReadingMessage(message: number) {
        Server.Request.confirmReadingMessage(this.token, message);
    }

    public login(credentials: Server.ICredentials, onSuccess: (token: string) => void, onError: (error: Server.ServerRequestError) => void, onQueueMessage: (type: Server.QueueMessage, data: any) => void) {
        if (this.logged) {
            console.warn("User already logged");
            return;
        }

        Server.Request.login(credentials, (token) => {
            this.onSuccesfulLogin(token);
            onSuccess(token);
        }, onError, onQueueMessage);
    }



    public register(data: Server.IRegisterData, onSuccess: (token: string) => void, onError: (error: Server.ServerRequestError) => void, onQueueMessage: (type: Server.QueueMessage, data: any) => void) {
        data.username = data.usernameFull
        Server.Request.register(data, (token) => {
            this.onSuccesfulLogin(token);
            onSuccess(token);
        }, onError, onQueueMessage);
    }


    private onDataDownloaded(data: Server.PlayerData) {
        this.logged = true;
        this.data = data;
        Messages.update(data.messages);

        if (data.configUpdate)
            updaterConfig(data.configUpdate);

        console.log(data);

        if (data.banned)
            setTimeout(() => GUI.Message.simple("Zablokowany dostęp!", "Jedna z Twoich gier nie przeszła weryfikacji. Do czasu wyjaśnienia dostęp do zadań i wyzwań jest zablokowany."), 1000);

        if (data.grantedTourneyEntrances) {
            setTimeout(() => new GUI.Popup(`Za zakup HCN zyskałeś dodatkowe ${data.grantedTourneyEntrances} darmowych wejściówek na Turniej!`), 1200);
        }

        if (data.pendingRewards && data.pendingRewards.length > 0) {
            let openNext = () => {
                let elem = data.pendingRewards.shift();
                if (!elem)
                    return;

                if (elem.type == Server.IRewardType.MYSTERY_BOX)
                    GUI.Message.mysteryBox(elem.name, elem.icon, openNext);
                if (elem.type == Server.IRewardType.DEFAULT)
                    GUI.Message.reward(elem.name, elem.icon, openNext);
            }
            setTimeout(openNext, 1200);
        }
    }

    public lastGameRequest: Server.IGameAccesRequest = {};

    public startChallenge(request: Server.IGameAccesRequest, onSucces: (acces: Server.AccesToPlay) => void, onError: (error: Server.ChallengeError) => void) {
        Server.Request.startChallenge(this.token, request, (acces) => {
            socketClient.game.start();
            this.challengeAccesToPlay = acces;
            this.lastGameRequest = request;
            onSucces(acces);
        }, onError);
    }

    public resetScore(challenge: number, onSucces: () => void, onError: (error: Server.RequestError) => void) {
        Server.Request.resetScore(this.token, challenge, onSucces, onError);
    }

    public startPvP(request: Server.IGameAccesRequest, onSucces: (acces: Server.AccesToPlay) => void, onError: (error: Server.ChallengeError) => void) {
        Server.Request.startPvP(this.token, request, (acces) => {
            socketClient.game.start();
            this.challengeAccesToPlay = acces;
            this.lastGameRequest = request;
            onSucces(acces);
        }, onError);
    }

    public startTraining(map: number, onSucces: (acces: Server.AccesToPlay) => void, onError: (error: Server.ChallengeError) => void) {
        if (!this.status.onlineAndLogged || this.data.banned)
            setTimeout(() => {
                let acces = this.getOfflineAccesToPlay(map);
                this.challengeAccesToPlay = acces;
                onSucces(acces);
                Player.get().sendDebugMsg("offline_game");
            }, 0);
        else
            Server.Request.startTraining(this.token, map, (acces) => { this.challengeAccesToPlay = acces; onSucces(acces) }, () => {
                let acces = this.getOfflineAccesToPlay(map);
                this.challengeAccesToPlay = acces;
                socketClient.game.start();
                onSucces(acces);
            });
    }

    private requestNewAccesToTraining() {
        if (!this.data.banned)
            Server.Request.getAccesToPlay(this.token, (acces) => this.preparedAccesToPlay = acces)
    }


    private getOfflineAccesToPlay(map: number): Server.AccesToPlay {
        let config = <Server.GameConfig>{
            seed: Math.random() * 32345 + 12334,
            duration: 90,
            bonus: {},
            map: map
        }
        let acces = <Server.AccesToPlay>{
            config: config,
            offline: true
        }
        return acces;
    }


    private onRequestError(error: Server.RequestError) {
        if (error == Server.RequestError.CONNECTION_TIMEOUT)
            return;
    }

    public downloadPlayerData(onSuccess?: () => void, onError?: (error: Server.RequestError) => void) {
        let updateCounter = 0;
        this.requestNewAccesToTraining();
        Challenges.reset();
        Challenges.download(this.token);
        Challenges.onUpdated = () => {
            updateCounter++;
            if (updateCounter == 2 && onSuccess)
                onSuccess();
        }
        Server.Request.getPlayerData(this.token, (data) => {
            this.onDataDownloaded(data);

            updateCounter++;
            if (updateCounter == 2 && onSuccess)
                onSuccess();
        }, (error) => {
            this.onRequestError(error);
            if (onError)
                onError(error);
        });
    }

    public sendDebugMsg(msg: "offline_game" | "game_loaded") {
        Server.Request.debug(msg);
    }

    static get(): Player {
        if (!Player.instance)
            Player.instance = new Player();

        return Player.instance;
    }

    public getRank(): number {
        return 0;
    }

    public getRankName(): string {
        return ["Nowicjusz"][this.getRank()];
    }

    public generateGameData(stats: Server.GameStats) {
        let data = <Server.GameData>{
            stats: stats,
            input: Game.inputCollector.getCompressed(),
            screenHeight: Game.height,
            screenWidth: Game.width,
            debugPointsInfo: GameLogic.debugPointsInfo
        };
        return data;
    }
    public sendGameData(stats: Server.GameStats, onSuccess: (results: Server.GameResults) => void, onError: (error: Server.RequestError) => void) {
        let data = this.generateGameData(stats);

        if (this.challengeAccesToPlay) {
            if (this.lastGameRequest.pvpId) {
                data.challenge = 0;
                data.seed = this.challengeAccesToPlay.config.seed;
                data.pvpId = this.lastGameRequest.pvpId;
            }
            else
            if (this.challengeAccesToPlay.challengeId) {
                data.challenge = this.challengeAccesToPlay.challengeId;
                data.seed = this.challengeAccesToPlay.config.seed;
            }
            else {
                data.trainingMap = this.challengeAccesToPlay.config.map;
                data.challenge = 0;
            }
            delete this.challengeAccesToPlay;
        }

        Server.Request.postGame(this.token,
            data,
            (results) => { this.onGameResults(results); onSuccess(results); },
            (error) => { onError(error); },
        );
    }

    public getRanking(callback: (rows: Server.RankingRow[]) => void) {
        Server.Request.getRanking(this.token, callback, () => new GUI.Popup("Brak połączenia z serwerem"));
    }

    public makeDonation(msg: string, hcn: number, callback: () => void) {
        Server.Request.makeDonate(this.token, msg, hcn, () => { callback(); this.data.hcn = Math.max(0, this.data.hcn - hcn) }, () => new GUI.Popup("Nie można wysłać wiadomości"));
    }

    private onGameResults(results: Server.GameResults) {
        this.onDataDownloaded(results.playerUpdate);
        Challenges.update(results.challengesUpdate);
        socketClient.game.end();
    }

    public hasSeenTip(name: string) {
        return this.storage.getItem(name) == "true";
    }
    public markTipAsSeen(name: string) {
        this.storage.setItem(name, "true");
    }

    public showRankLabel(group: Phaser.Group): Phaser.Group {
        if (!this.logged)
            return;

        let phaser = group.game;
        let parent = phaser.add.group(group);

        let bg = phaser.add.image(Game.width + 50, 10, "rank_bg", 0, parent);
        bg.scale.x = -1;
        bg.anchor.x = 1;
        let rank;

        rank = phaser.add.bitmapText(bg.left + 15, 23 * Game.assetsSize, "medium", this.data.username, undefined, parent);
        rank.scale.set(0.8, 0.8);
        rank.anchor.y = 0.5;

        return parent;
    }



    public showNameLabel(group: Phaser.Group): Phaser.Group {
        if (!this.logged)
            return;

        let phaser = group.game;
        let parent = phaser.add.group(group);
        let name = this.data.username;
        let bg = phaser.add.image(Game.width + 70, 10, "rank_bg", 0, parent);
        bg.anchor.x = 1;

        let rank;
        rank = phaser.add.bitmapText(Game.width - 15, 8 + bg.height / 2, "medium", this.data.username, undefined, parent);
        rank.scale.set(0.92, 0.92);
        rank.anchor.y = 0.5;
        rank.anchor.x = 1;

        return parent;
    }

}