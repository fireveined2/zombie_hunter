﻿import { Game } from "../beforeGame/Game"
import * as Models from "../../../Shared/Shared"
import { Player } from "./Player"
import * as GUI from "./gui/gui"

export class Messages {


    private static pending: Models.IUserMessage[] = [];
    private icon: Phaser.Image;




    constructor(private phaser: Phaser.State, private parent: Phaser.Group) {
        if (Messages.pending.length == 0)
            return;

        this.icon = phaser.add.image(Game.width * 0.99, Game.height - 70, "buttons/new_message", 0, this.parent);
        this.icon.anchor.set(1, 1);
        this.icon.inputEnabled = true;
        this.icon.events.onInputDown.add(()=>this.openOne());;
    }


    private static open(message: Models.IUserMessage) {
        
        if (message.title && message.title.length > 0)
            GUI.Message.simple(message.title, message.message, () => Player.get().confirmReadingMessage(message.id));
        else
            new GUI.Message(message.message, () => Player.get().confirmReadingMessage(message.id));

    }
 


    private openOne() {
        let message = Messages.pending.shift();
        if (!message)
            return;
        Messages.open(message)


        if (Messages.pending.length == 0)
            this.icon.visible = false;
    }

    static update(messages: Models.IUserMessage[]) {
        this.pending = messages;
        for (let i = 0; i < messages.length; i++) {
            let message = messages[i];
            if (message.openImmediately) {
                messages.splice(i, 1);
                Messages.open(message);
            }
        }
    }
}