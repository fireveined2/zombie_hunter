﻿import * as Models from "./Models"


var request: request.SuperAgentStatic = require('superagent');

export class Request {

    private static post(url: string, data: any, end: (error, req: request.Response) => void) {
        request.post(url)
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(data))
            .timeout(5000)
            .end((error, req) => {
                end(error, req);
            })

    }

    static login(credentials: Models.ICredentials, onSucces: (token: string) => void, onError?: (error: Models.LoginError) => void) {
        Request.post("https://hophands.pl/mobile/user/sign-in", credentials, (error, req) => {
            if (onError && error) {
                if (error.timeout)
                    onError(Models.LoginError.CONNECTION_TIMEOUT);
                else
                    onError(Models.LoginError.WRONG_CREDENTIALS);
                return;
            }

                onSucces(req.body.token);
        })
    }



}

