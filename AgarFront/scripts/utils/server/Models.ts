﻿
import  "../../../../Shared/Shared"
export * from "../../../../Shared/Shared"

export interface ICredentials {
    email: string;
    password: string;
}

export interface IRegisterData {
    email: string;
    password: string;
    usernameFull: string;
    gender: number;
    newsletter: boolean;
    rules: number;
    dateOfBirth: string;

    username?: string;
}

export enum LoginError {
    CONNECTION_TIMEOUT=1,
    WRONG_CREDENTIALS
}

export enum RegisterError {
    CONNECTION_TIMEOUT = 1,
    USERNAME_ALREADY_USED,
    EMAIL_ALREADY_USED
}

export enum RequestError {
    CONNECTION_TIMEOUT=1,
    WRONG_TOKEN
}


