﻿import * as Models from "./Models"
import { Config } from "../Config"

var request: request.SuperAgentStatic = require('superagent');
import io = require("socket.io-client");

export class Request {

    static version: string = Config.Version.ver;

    //private static GameServerHost = "";
    private static GameServerHost = Config.Global.serverAdress;
    private static QueueHost = Config.Global.serverAdress;

    //http://176.119.56.118:9241
    //http://192.168.1.100:9241
    private static post(url: string, data: any, end: (error, res: request.Response) => void, timeout = 25) {
        console.log("Post: " + url);
        console.log(data);
        let os = Config.Version.os[0];
        if (data["token"] && data["token"].length > 1)
            data["token"] = this.version + os + "|" + data["token"];
        request.post(url)
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(data))
            //  .withCredentials()
            .timeout(timeout * 1000)
            .end((error, req) => {
                end(error, req);
                console.log(error, req);
            })
    }

    private static get(url: string, data: any, end: (error, res: request.Response) => void) {
        console.log("Get: " + url);
        console.log(data);
        let os = Config.Version.os[0];
        if (data["token"] && data["token"].length > 1)
            data["token"] = this.version + os + "|" + data["token"];
        console.log(data["token"]);
        request.get(url)
            .query(data)
            .timeout(25000)
            .end((error, req) => {
                end(error, req);
            })

    }

    private static loginOldWay = false;


    static login(credentials: Models.ICredentials, onSucces: (token: string) => void, onError: (error: Models.ServerRequestError) => void, onQueueMessage: (type: Models.QueueMessage, data: any) => void) {
        if (!this.loginOldWay)
            this.addToQueue(credentials, onSucces, onError, onQueueMessage);
        else {


            Request.post("https://hophands.pl/mobile/user/sign-in", credentials, (error, req) => {
                if (onError && error) {
                    console.log(error);
                    if (error.timeout)
                        onError(Models.ServerRequestError.SERVER_TIMEOUT);
                    else
                        onError(Models.ServerRequestError.BAD_CREDENTIALS);
                    return;
                }

                let token = req.body.token;
                console.log("Token: " + token);
                if (token)
                    onSucces(req.body.token);
                else
                    onError(Models.ServerRequestError.BAD_CREDENTIALS);
            })


        }
    }

    private static addToQueue(data: any, onSucces, onError, onQueueStatusChange) {
        let socket = io(this.GameServerHost, { 'forceNew': true });
        socket.removeAllListeners();

        let timeout = setTimeout(() => {
            onError(Models.ServerRequestError.GAME_SERVER_TIMEOUT); this.loginOldWay = true
        }, 10 * 1000);

        let start = () => {
            console.log("Connected");
            clearTimeout(timeout);
            socket.emit("req", { data: data, os: Config.Version.os, version: this.version, osVersion: typeof device !== "undefined" ? device.version : "???", starterVersion: Config.Version.starterVersion });

            socket.on(Models.QueueMessage.CHANGE_POS, (data) => onQueueStatusChange(Models.QueueMessage.CHANGE_POS, data));
            socket.on(Models.QueueMessage.WAIT, (data) => onQueueStatusChange(Models.QueueMessage.WAIT, data));
            socket.on("token", (token) => { onSucces(token); socket.removeAllListeners(); socket.disconnect() });
            socket.on("error", (error) => { onError(error); socket.removeAllListeners(); socket.disconnect() });
        }

        socket.on("connect", () => {
            start();
        })

        socket.on("reconnect", () => {
            start();
        })
    }

    static register(data: Models.IRegisterData, onSucces: (token: string) => void, onError: (error: Models.ServerRequestError) => void, onQueueMessage: (type: Models.QueueMessage, data: any) => void) {
        this.addToQueue(data, onSucces, onError, onQueueMessage);
    }


    static getPlayerData(token: string, onSucces: (data: Models.PlayerData) => void, onError?: (error: Models.RequestError) => void) {
        Request.get(Request.GameServerHost + "/users/me",
            { token: token },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }

    private static hasErrors(error): Models.RequestError {
        if (!error)
            return null;

        console.log(error);
        if (error.timeout)
            return Models.RequestError.CONNECTION_TIMEOUT;
        else
            return Models.RequestError.WRONG_TOKEN;

    }

    static getRanking(token: string = "", onSucces: (data: Models.RankingRow[]) => void, onError?: (error: Models.RequestError) => void) {
        Request.get(Request.GameServerHost + "/ranking",
            { token: token },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }


    static getChallenges(token: string = "", onSucces: (data: Models.IChallengeData[]) => void, onError?: (error: Models.RequestError) => void) {
        Request.get(Request.GameServerHost + "/challenges",
            { token: token },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }


    static getAccesToPlay(token: string, onSucces: (acces: Models.AccesToPlay) => void, onError?: (error: Models.RequestError) => void) {
        Request.get(Request.GameServerHost + "/game",
            { token: token },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }

    static makeDonate(token: string, msg: string, hcn: number, onSucces: () => void, onError: (error: Models.ChallengeError) => void) {
        Request.post(Request.GameServerHost + "/donate",
            { token: token, msg: msg, hcn: hcn},
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces();
            });
    }

    static startPvP(token: string, game: Models.IGameAccesRequest, onSucces: (acces: Models.AccesToPlay) => void, onError: (error: Models.ChallengeError) => void) {
        Request.get(Request.GameServerHost + "/pvp/" + game.pvpId,
            { token: token, request: game },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }

    static startChallenge(token: string, game: Models.IGameAccesRequest, onSucces: (acces: Models.AccesToPlay) => void, onError: (error: Models.ChallengeError) => void) {
        Request.get(Request.GameServerHost + "/game/" + game.challengeId,
            { token: token, request: game },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }

    static startTraining(token: string, map: number, onSucces: (acces: Models.AccesToPlay) => void, onError: (error: Models.ChallengeError) => void) {
        Request.get(Request.GameServerHost + "/game/0/" + map,
            { token: token, request: <Models.IGameAccesRequest>{ trainingMap: map, challengeId: 0 } },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces(res.body);
            });
    }

    static postGame(token: string, gameData: Models.GameData, onSucces: (data: Models.GameResults) => void, onError?: (error: Models.RequestError) => void) {
        let adr = "";
        if (gameData.challenge)
            adr = "/" + gameData.challenge;
        else {
            adr = "/0/" + gameData.trainingMap;
        }
        //  setInterval(() => {
        let time = Date.now();
        Request.post(Request.GameServerHost + "/game" + adr,
            { token: token, gameData: gameData },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }
                console.log("Time: " + (Date.now() - time) + "ms");
                onSucces(res.body);
            }, 8);

        console.log("One game sent!");
        //  }, 450);
    }


    static saveEQ(token: string, change: Models.IEQChange, onSucces: () => void, onError?: (error: Models.RequestError) => void) {
        Request.post(Request.GameServerHost + "/eq",
            { token: token, change: change },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces();
            });
    }


    static resetScore(token: string, challenge: number, onSucces: () => void, onError?: (error: Models.RequestError) => void) {
        Request.post(Request.GameServerHost + "/reset/" + challenge,
            { challenge: challenge, token:token },
            (error, res) => {
                error = this.hasErrors(error);
                if (error && onError) {
                    onError(error);
                    return;
                }

                onSucces();
            }, 8);
    }

    static confirmReadingMessage(token: string, id: number) {
        Request.post(Request.GameServerHost + "/message",
            { token: token, message: id },
            (error, res) => {
                error = this.hasErrors(error);
            });
    }

    private static lastDebugMessageTime = Date.now();
    static debug(type: "game_loaded" | "offline_game" | "loader_error" | "js_error", param?) {
        if (Date.now() - this.lastDebugMessageTime < 8000)
            return;
        this.lastDebugMessageTime = Date.now();
        if (type == "js_error")
            console.warn(param);
        Request.post(Request.GameServerHost + "/debug",
            { type: type, params: param },
            (error, res) => {
                error = this.hasErrors(error);
            });
    }


}

