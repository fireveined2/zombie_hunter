﻿import * as Models from "./Models"
import * as SocketModels from "../../../../Shared/SocketModels"
import io = require("socket.io-client");
import { Config } from "../Config"

class PvP {
    constructor(private socket: SocketIOClient.Socket) {

    }

    public init() {
        this.socket.on(SocketModels.Events.Invite, (data: SocketModels.IInvitation) => {
            if (!socketClient.game.inGame)
                this.challenge.on(data.sender);
            else
                this.challenge.decline();
                console.log("PvP: ", data);
        });

        this.socket.on(SocketModels.Events.PvPUpdate, (update: SocketModels.IPvPUpdate) => {
            this.enemy.score = update.score;
            this.enemy.timeLeft = update.timeLeft;
            if (this.onEnemyUpdate)
                this.onEnemyUpdate(update);
        });

    }
    public close() {
        this.socket.removeEventListener(SocketModels.Events.PvPResults);
    }

    public onEnemyUpdate: (update: SocketModels.IPvPUpdate) => void;
    public enemy = <{ score: number, timeLeft: number, name: string }>{};
    public challenge = {
        make: (id: number, onAccepcted: Function, onDeclined: Function) => {
            this.socket.emit(SocketModels.Events.Invite, id);
            this.socket.once(SocketModels.Events.InvitationResponse, (answer: SocketModels.InvitationAnswer) => {
                if (answer == SocketModels.InvitationAnswer.YES)
                    onAccepcted();
                else
                    onDeclined();
            });
        },

        cancel: () => {
            this.socket.emit(SocketModels.Events.InvitationCancel);
        },

        accept: () => {
            this.socket.emit(SocketModels.Events.InvitationResponse, SocketModels.InvitationAnswer.YES);
        },

        decline: () => {
            this.socket.emit(SocketModels.Events.InvitationResponse, SocketModels.InvitationAnswer.NO);
        },

        on: (player: Models.PlayerInfo) => { },
        onCancel: (callback: Function) => {
            this.socket.removeEventListener(SocketModels.Events.InvitationCancel);
            this.socket.once(SocketModels.Events.InvitationCancel, callback);
        }
    };


    public onInitData(callback: (data: SocketModels.IPvPInitData) => void) {
        this.socket.removeEventListener(SocketModels.Events.PvPInitData);
        this.socket.once(SocketModels.Events.PvPInitData, callback);
        this.enemy.score = 0;
        this.enemy.timeLeft = 0;
    }

    public sendUpdate(score: number, timeLeft: number) {
        this.socket.emit(SocketModels.Events.PvPUpdate, <SocketModels.IPvPUpdate>{ score: score, timeLeft: timeLeft });
    }

    public sendData(data: Models.GameData, onResults: (results: SocketModels.IPvPResults) => void) {
        this.socket.removeEventListener(SocketModels.Events.PvPResults);
        this.socket.once(SocketModels.Events.PvPResults, onResults);
        this.socket.emit(SocketModels.Events.GameData, data);
        socketClient.game.end();
    }
}

 class SocketClient {

    private socket: SocketIOClient.Socket;

    private static instance: SocketClient;
    public pvp: PvP;
    static get() {
        if (!this.instance)
            this.instance = new SocketClient();

        return this.instance; 
    }
    constructor() {


    }
   
    public connect(token: string) {
        if (this.socket)
            return;
        this.socket = io(Config.Global.serverAdress.replace("4", "5"), { 'forceNew': true });
        this.pvp = new PvP(this.socket);

        this.socket.on("connect", () => {
            this.pvp.init();
            setTimeout(()=>this.auth(token), 5000);
        });
    }


    public disconnect() {
        this.socket.disconnect();
        delete this.socket;
    }


    private auth(token: string) {
        this.socket.emit(SocketModels.Events.Auth, { token: token });
    }


    get game() {
        let inGame = false;
        return {
            inGame: inGame,
            start: () => {
                if (!inGame && this.socket)
                this.socket.emit(SocketModels.Events.StartedGame);
                inGame = true;
            },

            end: () => {

                if (inGame && this.socket)
                    this.socket.emit(SocketModels.Events.EndedGame);
                inGame = false;
            }
        }
    }


    

}

 export var socketClient: SocketClient = new SocketClient();