﻿import { Game } from "../beforeGame/Game"
import * as Models from "../../../Shared/Shared"
import { Player } from "./Player"
import * as GUI from "./gui/gui"

export class Donate {


    constructor(private phaser: Phaser.State, private parent: Phaser.Group) {
        
        this.phaser = Game.group.game.state.getCurrentState();
        if (!this.phaser.cache.checkImageKey("buttons/cancel"))
            return; 

        let overlay = this.phaser.add.image(0, -50, "gui/info_popup_bg", 0, Game.group);
        overlay.inputEnabled = true;



        let element = document.getElementById("message") as HTMLDivElement;
        element.style.visibility = "visible";
       // element.style.pointerEvents = "none";
        element.innerHTML = `<div style='text-align: center;'>
<h2>Donacja</h2>
Poniżej możesz wpisać wiadomość, która zostanie wysłana do centrum dowodzenia stream'em. Jeśli Twój przekaz nas zainteresuje, Słoń odpowie komentarzem, a może nawet wyzwie Cię na pojedynek...
<textarea  id='donate_msg' placeholder='Treść wiadomości' maxlength="255" style='width: 90%; height: 30vh; margin: 10px;text-align: center;
background-color: rgba(255,255,255,0.5);font-family: CosmicTwo; border-radius: 5px;'> </textarea>

<input id="donate_hcn" max='${Player.get().data.hcn}' placeholder ='Liczba HCN (max. ${Player.get().data.hcn})' 
style='width: 90%; text-align: center;background-color: rgba(255,255,255,0.5);font-family: CosmicTwo; border-radius: 5px;'>
</div>`;

        let remove = () => {
            overlay.destroy();
            ok.destroy();
            cancel.destroy();
            element.innerHTML = "";
        }

        let ok = this.phaser.add.image(Game.width *0.35, 410, "buttons/ok", 0, Game.group);
        ok.anchor.set(0.5, 0.5);
        ok.inputEnabled = true;
        ok.events.onInputDown.addOnce(() => setTimeout(() => {
            let msg = $("#donate_msg").val();
            let hcn = $("#donate_hcn").val() || 0;
            console.log(msg, hcn);
            if(msg.length>0)
            Player.get().makeDonation(msg, hcn, () => {
                new GUI.Popup("Wiadomość została wysłana!");
                });

            remove();
        }, 30));


        let cancel = this.phaser.add.image(Game.width *0.65, 410, "buttons/cancel", 0, Game.group);
        cancel.anchor.set(0.5, 0.5);
        cancel.inputEnabled = true;
        cancel.events.onInputDown.addOnce(() => setTimeout(() => {
            remove();
        }, 30));
    }


   

   
}