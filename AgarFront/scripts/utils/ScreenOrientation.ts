﻿import { Game } from "../beforeGame/Game"
export class ScreenOrientation {
    static setLandscape() {

        (window.screen as any).orientation.lock('landscape');
        (document.getElementById('game-window').style as any).height = (screen.height > screen.width) ? screen.width : screen.height;
        (document.getElementById('game-window').style as any).width = (screen.width > screen.height) ? screen.height : screen.width;

        let w = Game.width;
        let h = Game.height;

        Game.group.game.scale.setGameSize(Game.realHeigt, Game.realWidth);
        Game.width = h;
        Game.height = w;
        Game.group.game.scale.refresh();
    }

    static setPortrait(game: Phaser.Game) {
        (window.screen as any).orientation.lock('portrait');
        (document.getElementById('game-window').style as any).width = (screen.height > screen.width) ? screen.width : screen.height;
        (document.getElementById('game-window').style as any).height = (screen.width > screen.height) ? screen.height : screen.width;

        let w = Game.width;
        let h = Game.height;

        game.scale.setGameSize(Game.realWidth, Game.realHeigt);
        Game.width = h;
        Game.height = w;
        game.scale.refresh();

    }
}
