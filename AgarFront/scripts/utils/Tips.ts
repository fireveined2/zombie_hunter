﻿
import * as Server from "./Server"
import "../../../Shared/Shared"
export * from  "../../../Shared/Shared"
import { Player} from "./Player"
import * as GUI from "./gui/gui"
 

export class Tips {

    private static tips: string[] = [];
    private static funTipsStartIndex = 0;

    private static init() {
        this.add("Przecinanie kucyków, misiów i psów skraca czas rozgrywki");
        this.add("Przy tafieniu kucyka, misia lub psa, tracisz punkty za obecene cięcie");
        this.add("Podczas Rage Mode możesz ciąć wszystko!");
        this.add("Nie przegap zamarzniętego mózgu - tryb CHILL zatrzyma czas, co pozwoli Ci zdobyć dodatkowe punkty");
        this.add("Wypijanie krwi z kufla zwiększa szanse na otrzymanie fiolki z antidotum");
        this.add("Przedmioty zdobyte podczas rozgrywki znajdziesz w ekwipunku");
        this.add("Zaraza postępuje powoli, ale nieubłaganie - stan zdrowia bohatera pogarsza się z każdym dniem");
        this.add("Zaraza postępuje szybciej podczas wysiłku fizycznego - stan zdrowia bohatera pogarsza się po kazdym polowaniu");
        this.add("Jeśli zaraza opanuje bohatera w co najmniej 20%, otrzymywany Respekt za rozgrywkę jest pomniejszany o wartość zakażenia");
        this.add("Korzystaj z antidotum, aby poprawić stan zdrowia bohatera. Fiolki z antidotum znajdziesz w Ekwipunku");
        this.add("Jeśli brakuje Ci Respektu do podjęcia wyzwania, poluj na zombie na podstawowych siedliskach. Pamiętaj, że stan zdrowia wpływa na otrzymywany Respekt");
        this.add("Pokaż jak ubrałeś bohatera! Gdy udostępnisz ekran w sieci i oznaczysz HopHands, możesz otrzymać losową nagrodę");
        this.add("Gdy otworzysz celę na Komisariacie - skup się! Musisz przeciąć każdego zombiaka i unikać elementów ludzkich");
        this.add("Chemikalia w Szpitalu wpływają na postrzeganie rzeczywistości. Kiedy świat jest pokolorowany, każde cięcie sprawia 10 razy więcej przyjemności. Niestety nie jest to takie łatwe gdy ostrze nie chce się słuchać…");
        this.add("Kiedy podczas TRIP MODE przetniesz misia, tracisz wszystkie punkty. Nie rób tego");
        this.add("Dopasuj jakość efektów graficznych w Ustawieniach do możliwości Twojego urządzenia mobilnego");
        this.add("Czy wiesz, że w Ustawieniach możesz włączyć treści +18?");
        this.add("Im wyższy poziom Łowcy, tym więcej dodatkowych treści - dźwięków, utworów, map, przedmiotów, zombie");
        this.add("Gdy nad siedliskiem zombie wyświetli się wykrzyknik, oznacza to, że masz do wykonania nowe Zadanie");
        this.add("Nowy poziom Łowcy możesz zdobyć dopiero, gdy wykonasz wszystkie zadania");
        this.add("Przecięce świni daje dodatkowe punkty");
        this.add("Aby odblokować dostęp do nowego siedliska, musisz posiadać w ekwipunku odpowiednią Głowę");
        this.add("Kliknij nazwę zadania na liście zadań, aby wyświetlić dodatkowe informacje");
        this.add("Bonusy z przedmiotów nie działają podczas wyzwań i zadań");
        this.add("W wyzwaniach One Shot płatna jest każda próba - musisz dać z siebie wszystko w każdej grze");
        this.add("W wyzwaniach sumowanych płatna jest tylko pierwsza próba - graj, aż uda Ci się spełnić wszystkie wymagania");

        this.funTipsStartIndex = this.tips.length;
        this.add("Jeśli cięcie zombie zaczyna Cię denerwować - ZMIEŃ PRACĘ");
        this.add("Granie na toalecie negatywnie wpływa na zdrowie");
        this.add("Nie zapominaj o higienie - regularne picie krwi zombie prowadzi do powstawania płytki nazębnej");
        this.add("Zauważyłeś, że Twój Łowca przytył?");
        this.add("Zombie polują na mózgi - jesteś bezpieczny!");


    }


    private static add(tip: string) {
        this.tips.push(tip);
    }
    static showRandom(callback: Function) {
        if (this.tips.length == 0)
            this.init();

        
        let length = this.tips.length;
        if (!window.localStorage.getItem("seenTips"))
            length = this.funTipsStartIndex;
        let tip = Phaser.ArrayUtils.getRandomItem(this.tips, 0, length);
        new GUI.BeforeGameTip("Porada " + Math.round(Math.random() * 1000 + 1), tip, callback);
        window.localStorage.setItem("seenTips", "true");
    }

}