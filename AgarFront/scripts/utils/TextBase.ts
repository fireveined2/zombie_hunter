﻿export var ChallengeDescribe = [];
ChallengeDescribe[0] = `W wyzwaniu typu One Shot możesz polegać tylko na sobie - brany pod uwagę jest tylko jeden wynik uzyskany podczas jednej próby. Każde nieudane podejście kosztuje tyle samo.`;
ChallengeDescribe[1] = `Wyzwanie Sumowane to takie, w którym gracz może brać udział określoną ilość razy, a wyniki z wszystkich prób są sumowane do ogólnego wyniku. Tylko pierwsze podejście jest płatne.`;
ChallengeDescribe[2] = `Wyzwanie Sumowane to takie, w którym gracz może brać udział określoną ilość razy, a wyniki z wszystkich prób są sumowane do ogólnego wyniku. Tylko pierwsze podejście jest płatne.`;
ChallengeDescribe[3] = `W tym typie wyzwania mierzysz się z innymi graczami. Każda podejście jest płatne, a nagrody wygrywają trzy osoby o najwyższych wynikach.`;


export class TextDatabase {

    static getMapName(map: number) {
        if (map == 0)
            return "CMENTARZ";
        if (map == 1)
            return "KOMISARIAT";
        if (map == 2)
            return "SZPITAL";
        if (map == 3)
            return "HOPSTOP";
    }

    static getChallengeTypeDescribe(type: number) {
        return ChallengeDescribe[type];
    }

}