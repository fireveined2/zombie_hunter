﻿import { Game } from "../beforeGame/Game"
import * as Models from "../../../Shared/Shared"
import { SFX } from "../SFX/SFX"
import { ZHConfig } from "./ZHConfig"
import { Payments } from "../beforeGame/Payments/Payments"
import { Player } from "./Player"
import * as GUI from "../utils/gui/gui"
import { WebPage } from "./WebPage"
import { Config } from "./Config"

interface WallOptions {
    empty?: boolean;
    tourneySetup?: boolean;

}
export class Wall {

    private wall: Phaser.Image;
    private button: Phaser.Image;
    private hpText: Phaser.Text;
    private hpBar: Phaser.Image;
    private hpBarWidth: number;
    public onButtonClick: Function;

    public hideHCN: Function;

    private hcn: Phaser.Text;
    private hcn_describe: Phaser.Text;


    constructor(private phaser: Phaser.State, private parent: Phaser.Group, player: Models.PlayerData, options = <WallOptions>{}) {
        let wall: Phaser.Image;

        if (options.empty) {
            wall = this.wall = phaser.add.image(Game.width / 2, Game.height, "wall", 0, this.parent);
            this.wall.y = Game.height - wall.height;
            wall.anchor.x = 0.5;
            return;
        }

        if (!player) {
            wall = this.wall = phaser.add.image(Game.width/2, Game.height, "wall", 0, this.parent);
            this.wall.y = Game.height - wall.height;
            wall.anchor.x = 0.5;

            let ranking = this.button = phaser.add.image(0, 73, "buttons/ranking");
            ranking.inputEnabled = true;
            ranking.events.onInputDown.add(() => { this._buttonClick();});
            ranking.anchor.set(0.5);
           
            wall.addChild(ranking);
            return;
        }

        wall = this.wall = phaser.add.image(0, Game.height, "wall", 0, this.parent);
        this.wall.y = Game.height - wall.height;

        let ranking = this.button = phaser.add.image(Game.width / 2, 73, "buttons/ranking");
        ranking.inputEnabled = true;
        ranking.events.onInputDown.add(() => {
            SFX.get().getSound("buttons/click").play();
            this._buttonClick();
        });
        ranking.anchor.set(0.5);
        wall.addChild(ranking);

        let leftButton = this.leftButton = phaser.add.image(Game.width *0.1, 73, "buttons/ranking");
        leftButton.inputEnabled = true;
        leftButton.events.onInputDown.add(() => {
            SFX.get().getSound("buttons/click").play();
            if (this.onLeftButtonClick)
                this.onLeftButtonClick();
        });
        leftButton.anchor.set(0.5);
        leftButton.visible = false;
        wall.addChild(leftButton);

        this.leftButtonBlinkImg = this.phaser.add.image(leftButton.x, leftButton.y, leftButton.key + "_black");
        this.leftButtonBlinkImg.anchor = leftButton.anchor;
        this.leftButtonBlinkImg.alpha = 0;
        wall.addChild(this.leftButtonBlinkImg);

        let rightButton = this.rightButton = phaser.add.image(Game.width * 0.9, 73, "buttons/ranking");
        rightButton.inputEnabled = true;
        rightButton.events.onInputDown.add(() => {
            SFX.get().getSound("buttons/click").play();
            if (this.onRightButtonClick)
                this.onRightButtonClick();
        });
        rightButton.anchor.set(0.5);
        rightButton.visible = false;
        wall.addChild(rightButton);

        this.rightButtonBlinkImg = this.phaser.add.image(rightButton.x, rightButton.y, rightButton.key + "_black");
        this.rightButtonBlinkImg.anchor = rightButton.anchor;
        this.rightButtonBlinkImg.alpha = 0;
        wall.addChild(this.rightButtonBlinkImg);
  

        let style = <Phaser.PhaserTextStyle>{ font: "21px CosmicTwo", fill: "rgb(133,160, 108)" };
        if (!options.tourneySetup) {
            let hpIcon = phaser.add.image(Game.width * 0.97, 57, "icons/hp");
            hpIcon.anchor.set(1, 0.5);
            wall.addChild(hpIcon);


            let hpText = this.hpText = phaser.add.text(hpIcon.centerX, hpIcon.y + 10, Math.round((1 - player.hp) * 100) + "%", style);
            hpText.anchor.set(0.5, 0);
            wall.addChild(hpText);

            let hpBar = phaser.add.image(hpIcon.left - 15, 73, "bars/hp");
            hpBar.anchor.set(1, 0.5);
            wall.addChild(hpBar);

            let hpBarFill = this.hpBar = phaser.add.image(-hpBar.width, 0, "bars/hp_fill");
            this.hpBarWidth = hpBarFill.width;
            hpBarFill.anchor.set(0, 0.5);
            this.fillBar(1 - player.hp, hpBarFill);
            hpBar.addChild(hpBarFill);
        }

        style = <Phaser.PhaserTextStyle>{ font: "23px CosmicTwo", fill: "rgb(133,160, 108)" };
        let hcn = this.hcn = phaser.add.text(Game.width * 0.68, 65, player.hcn.toString(), style);
        hcn.anchor.set(0.5);
        hcn.inputEnabled = true;
        hcn.events.onInputDown.add(() => this.openPayments());
        wall.addChild(hcn);

        style = <Phaser.PhaserTextStyle>{ font: "18px CosmicTwo", fill: "rgb(133,160, 108)" };
        let hcnDesc = this.hcn_describe = phaser.add.text(Game.width * 0.68, 84, "hcn", style);
        hcnDesc.anchor.set(0.5);
        hcnDesc.events.onInputDown.add(() => this.openPayments());
        wall.addChild(hcnDesc);
        this.hideHCN = () => {
            hcnDesc.visible = false;
            hcn.visible = false;
        }

        if (!options.tourneySetup) {
            style = <Phaser.PhaserTextStyle>{ font: "23px CosmicTwo", fill: "rgb(133,160, 108)" };
            let respect = phaser.add.text(Game.width * 0.32, 65, player.respect.toString(), style);
            respect.anchor.set(0.5);
            wall.addChild(respect);

            style = <Phaser.PhaserTextStyle>{ font: "18px CosmicTwo", fill: "rgb(133,160, 108)" };
            let respectDesc = phaser.add.text(Game.width * 0.32, 84, "respektu", style);
            respectDesc.anchor.set(0.5);
            wall.addChild(respectDesc);



            style = <Phaser.PhaserTextStyle>{ font: "32px CosmicTwo", fill: "rgb(133,160, 108)" };
            let lvl = phaser.add.text(Game.width * 0.02, 74, player.level.toString(), style);
            lvl.anchor.set(0, 0.5);
            wall.addChild(lvl);

            style = <Phaser.PhaserTextStyle>{ font: "20px CosmicTwo", fill: "rgb(133,160, 108)" };
            let lvlDesc = phaser.add.text(Game.width * 0.11, 59, "poziom", style);
            lvlDesc.anchor.set(0, 0.5);
            wall.addChild(lvlDesc);

            let expBar = phaser.add.image(lvl.right + 12, 82, "bars/exp");
            expBar.anchor.set(0, 0.5);
            wall.addChild(expBar);
            let expBarFill = phaser.add.image(0, 0, "bars/exp_fill");
            expBarFill.anchor.set(0, 0.5);
            this.fillBar(player.exp / ZHConfig.expLevels[player.level], expBarFill);
            expBar.addChild(expBarFill);
        }
        Game.backButtonCallback = null;
    }

    private openPayments() {

        let onClose = () => {}

        if (WebPage.isCurerentlyLoadedInBackground()) {
            $("#game-window").css("pointer-events", "auto");
            setTimeout(() => $("#game-window").css("pointer-events", "auto"), 200);
            setTimeout(() => $("#game-window").css("pointer-events", "auto"), 500);
            onClose = () =>  $("#game-window").css("pointer-events", "none"); 
        }

        if (!Player.get().logged) {
            new GUI.Popup("Aby zakupić HCNy musisz być zalogowany!").onClose = onClose;
            return;
        }
        new Payments(Game.group).onCloseCallback = onClose;
    } 

    public setTourneyButton(cost: number, userHCN: number, callback: Function, tourneyIsFinished: boolean) {
        if (tourneyIsFinished)
            return;

        let style = <Phaser.PhaserTextStyle>{ font: "22px CosmicTwo", fill: "rgb(133,160, 108)" };
        let respect = this.phaser.add.text(Game.width * 0.30, 74, "Weź udział!", style);
        respect.anchor.set(0.5);
        this.wall.addChild(respect);
        respect.inputEnabled = true;
        respect.events.onInputDown.add(callback);
    }

    private onLeftButtonClick: Function;
    private onRightButtonClick: Function;
    private leftButton: Phaser.Image;
    private rightButton: Phaser.Image;
    private leftButtonBlinking = Config.Version.starterVersion>1.1;
    private rightButtonBlinking = Config.Version.starterVersion > 1.1;
    private leftButtonBlinkImg: Phaser.Image;
    private rightButtonBlinkImg: Phaser.Image;

    public setLeftButton(img: "list" | "top3_players", onClick: Function) {
        this.leftButton.visible = true;
        this.leftButton.loadTexture("buttons/" + img);
        this.leftButtonBlinkImg.loadTexture("buttons/" + img + "_black");
        this.onLeftButtonClick = () => { onClick(); this.leftButtonBlinking = false; };
    } 

    public blinkButton(side: "left" | "right", blink: boolean = true) {
        if (side == "left")
            this.leftButtonBlinking = blink
        else
            this.rightButtonBlinking = blink;
    }

    public setRightButton(img: "prize" | "prizes" , onClick: Function) {
        this.rightButton.visible = true;
        this.rightButton.loadTexture("buttons/" + img);
        this.rightButtonBlinkImg.loadTexture("buttons/" + img + "_black");
        this.onRightButtonClick = () => { onClick(); this.rightButtonBlinking = false; };
    } 

    public enableHCNDetails() {
        this.hcn.x -= 40;
        this.hcn_describe.x -=40;

        let style = <Phaser.PhaserTextStyle>{ font: "18px CosmicTwo", fill: "rgb(133,160, 108)", align: "center" };
        let buy = this.phaser.add.text(Game.width * 0.75-10, 74, "Doładuj\nkonto", style);
        buy.anchor.set(0.5);
        this.wall.addChild(buy);
        buy.inputEnabled = true;
        buy.events.onInputDown.add(() => this.openPayments());
    }

    public setMenuButton(button: "back" | "turnieje" = "back") {
        this.button.loadTexture("buttons/" + button);
        Game.backButtonCallback = () => this.onButtonClick();
    }

    private _buttonClick() {
        if (this.onButtonClick) this.onButtonClick();
    
    }
    public updatePlayerData(player: Models.PlayerData) {
        this.hpText.setText(Math.round((1 - player.hp) * 100) + "%");
        this.fillBar(1 - player.hp, this.hpBar, this.hpBarWidth);
    }

    public update() {
        if (this.leftButtonBlinking) {
            this.leftButtonBlinkImg.alpha = 1 - this.leftButton.alpha;
            this.leftButton.alpha = Math.abs(Math.sin(Date.now() / 700)) * 0.5 + 0.5;
        }
        else {
            this.leftButton.alpha = 1;
            this.leftButtonBlinkImg.alpha = 0;
        }

        if (this.rightButtonBlinking) {
            this.rightButtonBlinkImg.alpha = 1 - this.rightButton.alpha;
            this.rightButton.alpha = Math.abs(Math.sin(Date.now() / 660+0.2))*0.5 + 0.5;
        }
        else {
            this.rightButton.alpha = 1;
            this.rightButtonBlinkImg.alpha = 0;
        }
    }

    private fillBar(factor: number, bar: Phaser.Image, width?: number) {

        let crop = new Phaser.Rectangle(0, 0, factor * (width?width:bar.width), bar.height);
        bar.crop(crop);
    }


    set visible(v: boolean) {
        this.wall.visible = v;
    }

}