﻿export class UserAbsence {

    private lastActiveTime;

    public markAsActive() {
        this.lastActiveTime = Date.now();
    }

    public timeSinceLastActivity(): number {
        return Date.now() - this.lastActiveTime;
    }
}