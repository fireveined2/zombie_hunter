﻿import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { GameStats } from "../game/logic/GameStats"
import { Challenges } from "../utils/Challenges"
import { Messages } from "../utils/Messages"

import * as GUI from "../utils/gui/gui"

import { SFX } from "../SFX/SFX"
import { GameSettings } from "../beforeGame/Settings"

import * as Server from "../utils/Server"


export var ZHConfig: Server.ZHConfig = { expLevels: [100, 100, 100, 100, 100], minVersion: "0.1", streamEnabled: false, happyHours: false };
export function updaterConfig(config: Server.ZHConfig) {
    console.log(config);
    if (parseFloat(config.minVersion) > parseFloat(Server.Request.version))
        setTimeout(()=>new GUI.Popup("Twoja wersja gry jest nieaktualna. Zaktualizuj aplikację, aby kontynuować polowanie"), 1000);
    ZHConfig = config;
}