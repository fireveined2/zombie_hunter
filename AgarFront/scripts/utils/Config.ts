﻿import { Phaser } from "./phaser"
interface IBonus {
    probability: number; // 
    duration: number;

}

export namespace Config {
    export class Version {
        static mobile = true;
        static www = true;
        static os: "ios" | "android" | "windows" = "ios";
        static ver = "0.29";
        static starterVersion = typeof window!=="undefined"?parseFloat((window as any).starterVersion):0;
    }

    export class Global {
      
  static serverAdress = "http://192.168.1.100:9241";
   //  static serverAdress = "https://ams.hophands.pl:9241";
        //

        static skipLobby: boolean = true;
        static frameTime: number = 40;
        static delta: number = Global.frameTime / 1000;
        static deltaGraphics: number = Global.frameTime / 1000;

        static gravity: number = 900 +  -20;

        static durationS: number = 10;
    }

    export function secondsToFrames(sec: number): number {
        return Math.round(sec * 1000 / Config.Global.frameTime);
    }

    export function framesToSeconds(frames: number, dontRound: boolean = false): number {
        if (!dontRound)
            return Math.round(frames * Config.Global.frameTime / 1000);
        else
            return frames * Config.Global.frameTime / 1000;
    }
    export class Display {
        static targetWidth = Config.Version.mobile ? 800 : 800;
        static targetHeight = Config.Version.mobile ? 533 : 600;

    }


    export function reset() {
        Global.gravity = 900 +  -20;
        Cuttable.maxDivisions = 2;
    }

    export class Cuttable {
        static maxSpeed = 930 + 30;
        static minSpeed = 850 +  0;
        static fleshTypes: number[] = []
        static bombTypes: number[] =[]
        static bonusTypes: number[] = []

        static PiggyProbability =  0.015;

        static sizes: Phaser.Rectangle[] = [];
        static frames: Phaser.Rectangle[][] = [];

        static points: number[] = [];
        static maxDivisions: number = 2;
    }

    export class Bonus {
        static freeze = 0;

        static data: IBonus[] = [{
            probability: 0.02,
            duration: 12
        }]; 

        
    }

    export class SFX {
        static bladeSoundMinDistancePerSec: number = Config.Version.mobile ? 700 : 1000;
    }

    export class Mechanics {
        static maxElapsedFramesForCombo: number = 23;
        static rageModeDuration: number = Config.secondsToFrames(10); //seconds
        static comboBonusRageMultiplier: number = 0;
        static basicRageMultiplier: number = 0.071;

        static score(comboValue: number, basePoints: number) {
            if (comboValue == 10)
                return 200;

            if (comboValue == 1)
                return basePoints;
            if (comboValue == 2)
                return Math.round(basePoints*1.2);
            if (comboValue == 3)
                return Math.round(basePoints * 1.3);

            return Math.round(basePoints * 1.5);
          //  return 10 * Math.pow(comboValue, 0.5);
        }

    }


}


