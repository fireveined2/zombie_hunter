﻿import { CriterionRow } from "./CriterionRow";
import * as Models from "../../../../Shared/Shared";
import { AnimableElement } from "./gui"


export class CriteriaList implements AnimableElement {

    private criteria: Models.ChallengeCriteria[];
    private stats: Models.GameStats;

    private rows: CriterionRow[] = [];
    private phaser: Phaser.State;
    private group: Phaser.Group;

    constructor(parent: Phaser.Group, x: number, y: number, criteria: Models.ChallengeCriteria[], stats?: Models.GameStats, private oneLine: boolean = false) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        this.group.position.set(x, y);
        this.criteria = criteria;
        this.stats = stats;

        if (criteria)
        this.create(criteria, stats);
    }


    public create(criteria: Models.ChallengeCriteria[], stats: Models.GameStats) {
        this.criteria = criteria;
        this.stats = stats;

        for (let i = 0; i < this.criteria.length; i++) {
            let criterion = this.criteria[i];
            let y = this.rows.length * (this.oneLine?30:58);

            let stat = 0;
            if (this.stats) {
                let name = criterion.stat;
                if (criterion.stat == "cur_score")
                    name = "score";
                stat = this.stats[name];

            }
            let row = new CriterionRow(this.group, 0, y, criterion, stat, this.oneLine);
            this.rows.push(row);
        }
    }

    public addSimple(name, value) {
            let y = this.rows.length * (this.oneLine ? 30 : 58);
            let row = new CriterionRow(this.group, 0, y, null, null, this.oneLine);
            row.setSimple(name, value);
            this.rows.push(row);
            return row.row;
        
    }

    set visible(visible) {
        this.group.visible = visible;
    }

    public anim(next?: AnimableElement[], onEnd?: Function) {
        this.visible = true;
        for (let row of this.rows)
            row.prepareToAnim();

       this.rows[0].anim(this.rows.filter((val, index) => index != 0), onEnd);
    }

}

