﻿

export class Button {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;

    protected text: Phaser.Text;

    public onClick: Function;

    constructor(text: string, x: number, y: number, parent: Phaser.Group, onClick?: Function) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;

        this.onClick = onClick;
        this.text = this.phaser.add.text(x, y, text, { "fill": "white" }, this.group);
        this.text.inputEnabled = true;
        this.text.events.onInputDown.add(() => this._onClick());
    }

    public setText(txt: string) {
        this.text.setText(txt);
    }
    protected _onClick() {
        if (this.onClick)
            this.onClick();
    }

    get anchor(): Phaser.Point {
        return this.text.anchor;
    }

    set visible(v: boolean) {
        this.text.visible = v;
    }

    get element() {
        return this.text;
    }
}