﻿import { Popup } from "./Popup";

export class QuestionMark {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;

    protected image: Phaser.Image;

    public onClick: Function;

    constructor(x: number, y: number, text: string, parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;


        this.image = this.phaser.add.image(x, y, "hud/question_mark", 0, this.group);
        this.image.anchor.set(0.5);
        this.image.inputEnabled = true;
        this.image.events.onInputDown.add(() => {
            new Popup(text);
        });

    }
    get element() {
        return this.image;
    }
}