﻿

import * as Models from "../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../Shared/Challenges";
import { AnimableElement } from "./gui"

export interface ListRowConfig {
    header: string;
    content: string;
    oneLine?: boolean;
    color?: string;
}

export class ListRow implements AnimableElement {

    private phaser: Phaser.State;
    private group: Phaser.Group;

    private row: Phaser.Text;
    private describe: Phaser.Text;



    constructor(parent: Phaser.Group, x: number, y: number, private config: ListRowConfig) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);


        if (!config.oneLine) {
            let style = { font: "21px CosmicTwo", fill: "white" };
            this.describe = this.phaser.add.text(x, y, "", style, this.group);
            this.describe.anchor.set(0.5);

            style = { font: "19px CosmicTwo", fill: "rgb(255,220,195)" };
            this.row = this.phaser.add.text(x, y + 26, "", style, this.group);
            this.row.anchor.set(0.5);


        }
        else {
            let style = { font: "18px CosmicTwo", fill: "white" };
            this.describe = this.phaser.add.text(x, y, "", style, this.group);
            this.describe.anchor.set(1,0);

            style = { font: "21px CosmicTwo", fill: "white" };
            this.row = this.phaser.add.text(x + 10, y, "", style, this.group);
            this.row.anchor.set(0,0);
        }
               
        this.setData();
    }

    get height(): number {
        if (this.config.oneLine)
            return 38;
        else
        return 78;
    }

    public setData() {
        this.describe.setText(this.config.header);
        this.row.setText(this.config.content);
    }


    public prepareToAnim() {
        this.group.alpha = 0;
    }

    public anim(next?: AnimableElement[], onEnd?: Function) {
        let tween = this.phaser.add.tween(this.group).to({ alpha: 1 }, 600).start();
        if (next)
            tween.onComplete.addOnce(() => {

                if (next && next.length > 0)
                    next.shift().anim(next, onEnd);
                else if (onEnd)
                    onEnd();

            });
    }
}

