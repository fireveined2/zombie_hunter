﻿import { Button } from "./gui"
import { Game } from "../../beforeGame/Game"
import { SFX } from "../../SFX/SFX"
export class Popup {


    protected group: Phaser.Group;

    protected text: Phaser.Text;
    protected exit: Phaser.Image;

    public onClose: Function;

    constructor(text: string, private phaser?: Phaser.Game, fontSize?: number,  onClose?: Function) {
        console.log("open");
        this.phaser = Game.group.game;
        let overlay = this.phaser.add.image(Game.width / 2, Game.height / 2, "gui/popup", 0, Game.group);
        overlay.anchor.set(0.5);
        overlay.inputEnabled = true;

        if (!fontSize) {
            fontSize = 35;
            if (text.length > 80)
                fontSize = 20;// + 15 - (text.length / 57)
            if (text.length > 179)
                fontSize = 17;// + 15 - (text.length / 57)
        }
        this.onClose = onClose;
        let style = <Phaser.PhaserTextStyle>{ "fill": "white", font: fontSize+"px CosmicTwo", align: "center", wordWrap: true, wordWrapWidth: 410 };
        this.text = this.phaser.add.text(0, -overlay.height *0.13, text, style);
        this.text.anchor.set(0.5, 0.5);
        overlay.addChild(this.text);

        this.exit = this.phaser.add.image(0, overlay.height*0.27, "buttons/ok", 0, Game.group);
        this.exit.anchor.set(0.5, 0.5);
        this.exit.inputEnabled = true;
        this.exit.events.onInputDown.addOnce(() => setTimeout(() => {
            overlay.destroy();
            //   title.destroy();
            //  desc.destroy()
            this.exit.destroy();
            this._onClose()
        }, 30));
        overlay.addChild(this.exit);
    }

    protected _onClose() {
        SFX.get().playSingleEffect("buttons/click");
        if (this.onClose)
            this.onClose();
    }



    //static youHaveToBeLogged() {
      //  new Popup("Musisz być zalogowany!", Game.group);
  //  }

}