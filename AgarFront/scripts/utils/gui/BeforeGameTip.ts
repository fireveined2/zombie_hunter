﻿
import { Game } from "../../beforeGame/Game"
import * as Models from "../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../Shared/Challenges";
import { AnimableElement } from "./gui"
import { SFX } from "../../SFX/SFX"

export class BeforeGameTip {
    private phaser: Phaser.State;
    public group: Phaser.Group;

    constructor(title: string, describe: string, onClose: Function, imageKey?: string) {
        this.phaser = Game.group.game.state.getCurrentState();
        this.group = this.phaser.add.group();
        this.group.scale.set(Game.group.scale.x);

       
        let style = <Phaser.PhaserTextStyle>{ font: "25px CosmicTwo", fill: "white" };
        let tit = this.phaser.add.text(Game.width / 2, Game.height * 0.3, title, style, this.group);
        tit.anchor.set(0.5);
        tit.transformCallback = () => {
            this.phaser.world.bringToTop(this.group);
        }

        style = <Phaser.PhaserTextStyle>{ font: "22px CosmicTwo", fill: "white", wordWrap: true, wordWrapWidth: 500, align: "center" };
        let desc = this.phaser.add.text(Game.width / 2, Game.height * 0.47, describe, style, this.group);
        desc.anchor.set(0.5, 0.5);

        tit.y = desc.top - Game.height * 0.1;


        style = <Phaser.PhaserTextStyle>{ font: "19px CosmicTwo", fill: "white", wordWrap: true, wordWrapWidth: 500, align: "center" };
        let reminder = this.phaser.add.text(Game.width / 2, Game.height - 10, "Kliknij, aby rozpocząć rozgrywkę", style, this.group);
        reminder.anchor.set(0.5,1);
        let animText = () =>  reminder.alpha = Math.round(Math.abs(Math.sin(Date.now() / 700)));
        let interval = setInterval(animText, 40);

        if (imageKey) {
            let img = this.phaser.add.image(Game.width / 2, desc.bottom + 30, imageKey, 0, this.group);
            img.anchor.set(0.5, 0);
        }

        this.phaser.input.onDown.addOnce(() => {
            this.group.destroy();
            clearInterval(interval);
            onClose();

        });


        this.group.alpha = 0;
        this.phaser.add.tween(this.group).to({ alpha: 1 }, 500).start();
    }
}


