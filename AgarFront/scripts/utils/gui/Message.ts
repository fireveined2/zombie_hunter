﻿import { Button } from "./gui"
import { Game } from "../../beforeGame/Game"
import { SFX } from "../../SFX/SFX"
import { Player } from "../Player"
import { Challenges } from "../Challenges"


export class Message {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;

    protected text: Phaser.Text;
    protected exit: Phaser.Image;

    public onClose: Function;

    protected element: HTMLDivElement;

    constructor(content: string, onClose?: Function) {
        this.phaser = Game.group.game.state.getCurrentState();

        let overlay = this.phaser.add.image(0, -50, "gui/info_popup_bg", 0, Game.group);
        overlay.inputEnabled = true;

        this.onClose = onClose;
        /*      let style = <Phaser.PhaserTextStyle>{ font: "23px Candara", fill: "black", wordWrap: true, wordWrapWidth: 430 };
              let title = this.phaser.add.text(Game.width / 2, Game.height * 0.045, titleText, style, Game.group);
              title.anchor.set(0.5, 0);
          //    overlay.addChild(title);
      
              style = <Phaser.PhaserTextStyle>{ font: "17px Candara", fill: "black", wordWrap: true, wordWrapWidth: 430 };
              let desc = this.phaser.add.text(Game.width / 2, Game.height * 0.22, content, style, Game.group);
              desc.anchor.set(0.5,0);
            //  overlay.addChild(desc);
              */
        this.exit = this.phaser.add.image(Game.width / 2, 410, "buttons/ok", 0, Game.group);
        this.exit.anchor.set(0.5, 0.5);
        this.exit.inputEnabled = true;
        this.exit.events.onInputDown.addOnce(() => setTimeout(() => {
            overlay.destroy();
            //   title.destroy();
            //  desc.destroy()
            this.exit.destroy();
            this._onClose()
        }, 30));

        this.element = document.getElementById("message") as HTMLDivElement;
        this.element.style.visibility = "visible";
        this.element.style.pointerEvents = "none";
        this.element.innerHTML = content;

    }

    protected _onClose() {
        SFX.get().playSingleEffect("buttons/click");
        this.element.style.visibility = "hidden";
        if (this.onClose)
            this.onClose();
    }

    static youHaveToBeLogged() {
        Message.simple("Zaloguj się!", `Zaloguj się, aby skorzystać z ekwipunku, wziąć udział w kampanii i uzyskać dostęp do nowych map, rozbudowanego systemu rozwoju bohatera, turniejów o nagrody i premiowanych wyzwań.<br><br>Rejestracja na serwisie HopHands oraz gra Zombie Huntera jest darmowa.`);
    }


    static simple(titleText: string, content: string, onClose?: Function, smallFont?: boolean) {
        if (!smallFont)
            new Message("<h2 style='text-align: center; '>" + titleText + "</h2> <p style='font-size: 130%; weight:bold;'>" + content + "</p>", onClose);
        else
            new Message("<h2 style='text-align: center; '>" + titleText + "</h2> <p style='font-size: 110%; weight:bold; font-family: Candara; text-align: center'>" + content + "</p>", onClose);
    }

    static tournamentInfo() {

        Message.simple("Tuuuuurniej!", `By wziąć udział w turnieju, należy zakupić "wpisowe", czyli tzw. wirtualną walutę HitCoins. Jednorazowa próba to koszt 10 HitCoinów, czyli 1 zł. Na stronie można zakupić większe pakiety waluty, dzięki którym jednostkowy koszt jest znacznie mniejszy.
Turniej trwa do 18 czerwca. <br><br> Najlepsi gracze (ale nie tylko) zostaną nagrodzeni wartościowymi upominkami. Pierwsza nagroda to nowy smartfon Samsung Galaxy S8. <br><br> Więcej informacji odnośnie warunków udziału w turnieju i lista wszystkich nagród w lobby turniejowym.`, null, true);
        if (Player.get().logged && Challenges.tourneys[0]) {
            if (Challenges.tourneys[0].data.tourneyData.myScore)
                SFX.get().playSlon("slon/improve_score");
            else
                SFX.get().playSlon("slon/tourney");
        }
        else
            SFX.get().playSlon("slon/tourney");
    }

    static mysteryBox(name: string, icon: number, onClose) {
        new Message(`<style> 
                h3, h2 {
                text-align: center;
                margin: 10px;
                }
                img {
                    display: block;
                    margin: auto;
                    width: 18%;
                }
                </style>
                <h2> MysteryBox! </h2>

                <h3>W MysteryBoxie znalazłeś nowy przedmiot:<br>
                <b>${name}</b></h3><br>
                <img src='assets/s1/items/${icon}.png'>
                <br>
                <h3> Nagrodę znajdziesz w swoim ekwipunku! </h3>`, onClose);
    }

    static reward(name: string, icon: number, onClose) {
        new Message(`<style> 
                h3, h2 {
                text-align: center;
                margin: 10px;
                }
                img {
                    display: block;
                    margin: auto;
                    width: 18%;
                }
                </style>
                <h2> ${name} </h2>

                <h3>Dostałeś nowy przedmiot:<br>
                <b>${name}</b></h3><br>
                <img src='assets/s1/items/${icon}.png'>
                <br>
                <h3> Nagrodę znajdziesz w swoim ekwipunku! </h3>`, onClose);
    }

    static inviteRewardInfo() {
        Message.simple("Darmowe granie za zapraszanie", `Zaproś znajomych do turnieju, a za każdą osobę dostaniesz 2 darmowe próby w turnieju. <br><br> Nick zaproszonej osoby przekaż w wiadomości prywatnej na Facebooku HopHands`, null, true);
    }

    private static messagesSeen = [false, false];

    static showRandomInfoMessage() {
        let i1 = parseInt(window.localStorage.getItem("messages/messageInfo0"));
        if (!i1) i1 = 0;
        let i2 = parseInt(window.localStorage.getItem("messages/messageInfo1"));
        if (!i2) i2 = 0;

        let infoCount = [i1, i2];

        let info = Math.round(Math.random());

        if (infoCount[info] > 2 || this.messagesSeen[info])
            info = 1 - info;

        if (infoCount[info] > 2 || this.messagesSeen[info])
            return;


        this.messagesSeen[info] = true;
        if (info == 0)
            this.tournamentInfo();

        if (info == 1)
            this.inviteRewardInfo();

        console.log(infoCount);
        window.localStorage.setItem("messages/messageInfo" + info, (infoCount[info] + 1).toString());

    }
}