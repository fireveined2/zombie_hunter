﻿import { ListRow } from "./ListRow";
import * as Models from "../../../../Shared/Shared";
import { AnimableElement } from "./gui"

export class ListRowInitData {
    header: string;
    content: string;
}

export class List implements AnimableElement {



    private rows: ListRow[] = [];
    private phaser: Phaser.State;
    private group: Phaser.Group;

    constructor(parent: Phaser.Group, x: number, y: number, rows: ListRowInitData[]) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        this.group.position.set(x, y);


        this.create(rows);
    }


    private create(rows: ListRowInitData[]) {
        for (let i = 0; i < rows.length; i++) {
            let y = 0;
            if (i > 0)
                y += i *58;

            let row = new ListRow(this.group, 0, y, { header: rows[i].header, content: rows[i].content });
            this.rows.push(row);
        }
    }

    set visible(visible) {
        this.group.visible = visible;
    }

    public anim(next?: AnimableElement[], onEnd?: Function) {
        this.visible = true;
        for (let row of this.rows)
            row.prepareToAnim();

       this.rows[0].anim(this.rows.filter((val, index) => index != 0), onEnd);
    }

}

