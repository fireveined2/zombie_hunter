﻿

import * as Models from "../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../Shared/Challenges";
import { AnimableElement } from "./gui"
import { SFX } from "../../SFX/SFX"

export class ArrowSelector {

    private phaser: Phaser.State;
    public group: Phaser.Group;



    private top: Phaser.Image;
    private bot: Phaser.Image;

    private enabled = [true, true];

    constructor(parent: Phaser.Group, x: number, y: number, private onSelect: (arrow: "bot"| "top")=>void) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);;
        this.group.position.set(0,0);

        this.top = this.phaser.add.image(x, y-25, "hud/arrow", 0, this.group);
        this.top.anchor.set(0.5);
        this.top.inputEnabled = true;
        this.top.events.onInputDown.add(() => this.click("top"));
        this.top.scale.y = -1;


        this.bot = this.phaser.add.image(x, y + 25, "hud/arrow", 0, this.group);
        this.bot.anchor.set(0.5);
        this.bot.inputEnabled = true;
        this.bot.events.onInputDown.add(() => this.click("bot"));
    }
    private click(arrow: "bot" | "top") {
        if (arrow == "bot" && !this.enabled[1])
            return;
        if (arrow == "top" && !this.enabled[0])
            return;

        SFX.get().playClick();
        this.onSelect(arrow);
    }

    public enableTop(enable: boolean) {
        this.enabled[0] = enable;
        this.top.tint = enable ? 0xFFFFFF : 0x777777;
    }

    public enableBot(enable: boolean) {
        this.enabled[1] = enable;
        this.bot.tint = enable ? 0xFFFFFF : 0x777777;
    }
}

