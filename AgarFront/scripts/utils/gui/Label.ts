﻿

export class Label {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;

    protected txt: Phaser.Text;
    private label: Phaser.Image;

    constructor(text: string, x: number, y: number, parent?: Phaser.Group, txtSize: number = 38, color = "black") {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;


        let style = { font: txtSize + "px CosmicTwo", fill: color };
        this.txt = this.phaser.add.text(5, 9, text, style, this.group);
        this.label = this.phaser.add.image(x, y, "gui/label", 0, this.group)
        let sx = this.label.width / (this.txt.width + 10);
        let sy = this.label.height / (this.txt.height + 10);
        this.label.width /=sx;
        this.label.height /= sy;
        if (this.group)
        this.group.remove(this.txt);
        this.label.addChild(this.txt);
        this.txt.width *= sx;
        this.txt.height *= sy;
        this.txt.anchor = this.label.anchor;

    }
    private init(text: string, x: number, y: number, parent?: Phaser.Group, txtSize: number = 38) {

    }
    get bottom() {
        return this.txt.bottom;
    }


    get anchor() {
        return this.label.anchor;
    }

    get element() {
        return this.label;
    }

    public destroy() {
        this.element.destroy();
    }


}