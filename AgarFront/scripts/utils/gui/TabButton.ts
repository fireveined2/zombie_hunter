﻿

export class TabButton {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;

    protected image: Phaser.Image;

    public onClick: Function;

    constructor(text: string, x: number, y: number, parent: Phaser.Group, onClick?: Function) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;

        this.onClick = onClick;
        let key = "btn_events";
        if (text == "Ostatnia gra")
            key = "btn_lastGame";
        if (text == "Nagrody")
            key = "btn_rewards";

        this.image = this.phaser.add.image(x, y, "results/"+key, 0, this.group);
        this.image.inputEnabled = true;
        this.image.events.onInputDown.add(() => this._onClick());
        this.active(false);
    }
    get element() {
        return this.image;
    }

    protected _onClick() {
        if (this.onClick)
            this.onClick();
    }

    public active(active: boolean) {
        if (active)
            this.image.tint = 0xAAFFAA;
        else
            this.image.tint = 0xFFFFFF;
    }

    set visible(v) {
        this.image.visible = v;
    }
}