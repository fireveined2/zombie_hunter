﻿

export class BorderedText {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;

    protected txt: Phaser.Text;

    public onClick: Function;

    constructor(text: string, x: number, y: number, parent: Phaser.Group, onClick?: Function) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;

        this.onClick = onClick;

        let style = { font: "22px CosmicTwo", fill: "white" };
        this.txt = this.phaser.add.text(x, y, text, style, this.group);

   /*     let graphics = this.phaser.add.graphics(0, 0)
        graphics.lineStyle(1, 0xffffff, 1)
        graphics.drawRect(-4, -4, this.txt.width + 8, this.txt.height+8)
        graphics.inputEnabled = true;
        graphics.events.onInputDown.add(() => this._onClick());
        this.txt.addChild(graphics);
        */
    }
    get element() {
        return this.txt;
    }

    get bottom() {
        return this.txt.bottom;
    }
    protected _onClick() {
        if (this.onClick)
            this.onClick();
    }
}