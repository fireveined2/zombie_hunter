﻿

import * as Models from "../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../Shared/Challenges";
import { AnimableElement } from "./gui"


export class RespectField{

    private phaser: Phaser.State;
    private group: Phaser.Group;



    private punishment: Phaser.Text;
    private reward: Phaser.Text;
    private img: Phaser.Image;



    constructor(parent: Phaser.Group, x: number, y: number, reward: number, punishment: number) {
        this.phaser = parent.game.state.getCurrentState();
        this.group =parent;

        this.img = this.phaser.add.image(x, y, "gui/respect", 0, this.group);

        let style = { font: "19px Candara", fill: "white" };
        this.reward = this.phaser.add.text(this.img.width * 0.8, this.img.height * -0.3, reward.toString(), style, this.group);
        this.reward.anchor.set(0.5);

        style = { font: "19px Candara", fill: "white" };
        this.punishment = this.phaser.add.text(this.img.width * 0.8, this.img.height * 0.5, punishment.toString(), style, this.group);
        this.punishment.anchor.set(0.5);

        this.img.addChild(this.reward);
        this.img.addChild(this.punishment);  
    }

    get anchor() {
        return this.img.anchor;
    }
   
}

