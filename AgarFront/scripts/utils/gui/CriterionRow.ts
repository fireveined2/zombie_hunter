﻿

import * as Models from "../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../Shared/Challenges";
import { AnimableElement } from "./gui"

var stats: Models.GameStats;


export var statNames: { [stat: string]: string } = {};
statNames["score"] = "Wynik";
statNames["cur_score"] = "Obecny wynik";
statNames["duration"] = "Długość gry"  
statNames["biggestCut"] = "Największe combo"
statNames["cutFlesh"] = "Przecięte zombie"    
statNames["rages"] = "Rage Mode'y";
statNames["chills"] = "Chille";
statNames["cutInCtps"] = "Złapanych w CutThePolice";
statNames["cutPonies"] = "Przecięte kucyki";

statNames["missedFlesh"] = "Przepuszczone zombie";


statNames["missedPonies"] = "Pominięte kucyki";
statNames["trips"] = "Długość tripa";
statNames["ctps"] = "Cut The Police";
statNames["highscore"] = "Twój rekord";
statNames["pigs"] = "Przecięte świnie";
statNames["numberOfTries"] = "Ilość rozgrywek";

statNames["smallElements"] = "Małe elementy";
statNames["mediumElements"] = "Średnie elementy";
statNames["bigElements"] = "Duże elementy";


export var shortStatNames: { [stat: string]: string } = {};
shortStatNames["score"] = "Wynik";
shortStatNames["cur_score"] = "Obecny wynik";
shortStatNames["duration"] = "Długosc"
shortStatNames["biggestCut"] = "Najwieksze combo"
shortStatNames["cutFlesh"] = "Przeciete zombie"
shortStatNames["rages"] = "Rage Mode'y";
shortStatNames["chills"] = "Chille";
shortStatNames["cutInCtps"] = "Zlapanych w CutThePolice";
shortStatNames["cutPonies"] = "Przecięte kucyki";
shortStatNames["missedPonies"] = "Pominiete kucyki";
shortStatNames["trips"] = "Długosc tripa";
shortStatNames["ctps"] = "Cut The Police";
shortStatNames["highscore"] = "Twoj rekord";
shortStatNames["position"] = "Twoja pozycja";
shortStatNames["challenge"] = "Wyzwanie";
shortStatNames["missing_points"] = "Do miejsca wyżej brakuje";
shortStatNames["numberOfTries"] = "Ilość rozgrywek";

shortStatNames["respect"] = "RESPEKT";
shortStatNames["exp"] = "EXP";

var operatorChar: string[] = [];
operatorChar[Models.ChallengeCriteriaOperator.EQUAL] = "=";
operatorChar[Models.ChallengeCriteriaOperator.MORE] = ">";
operatorChar[Models.ChallengeCriteriaOperator.LESS] = "<";

export class CriterionRow implements AnimableElement {

    private phaser: Phaser.State;
    private group: Phaser.Group;

    private line: Phaser.Image;
    public row: Phaser.Text;
    private describe: Phaser.Text;

    private criterion: Models.ChallengeCriteria;
    private curValue: number;


    constructor(parent: Phaser.Group, x: number, y: number, criterion: Models.ChallengeCriteria, curValue: number, private oneLine: boolean =false) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        this.criterion = criterion;
        this.curValue = curValue;


        //this.line = this.phaser.add.image(x, y, "gui/criterion_line", 0, this.group);
        if (!oneLine) {
            let style = { font: "18px CosmicTwo", fill: "white" };
            this.describe = this.phaser.add.text(x, y, "", style, this.group);
            this.describe.anchor.set(0.5);

            style = { font: "21px CosmicTwo", fill: "white" };
            this.row = this.phaser.add.text(x, y + 26, "", style, this.group);
            this.row.anchor.set(0.5);
        }
        else {
            let style = { font: "18px CosmicTwo", fill: "white" };
            this.describe = this.phaser.add.text(x, y, "", style, this.group);
            this.describe.anchor.set(1,0);

            style = { font: "21px CosmicTwo", fill: "white" };
            this.row = this.phaser.add.text(x + 10, y, "", style, this.group);
            this.row.anchor.set(0,0);
        }

        if (criterion)
        this.setData(criterion, curValue);
    }

    get height(): number {
        if (this.oneLine)
            return 38;
        else
        return 78;
    }

    public setData(criterion: Models.ChallengeCriteria, curValue: number) {
        if (curValue !== undefined) {
            let succes = ChallengeUtils.matchCriterion(criterion, curValue);
            if (succes)
                this.row.fill = "green";
            else
                this.row.fill = "red";
        }
        else
            curValue = 0;

        this.criterion = criterion;
        curValue = Math.round(curValue)
        this.curValue = curValue;
        let txt = curValue ? curValue.toString() : "0";
        if (criterion.value !== undefined) {
            txt += " " + operatorChar[criterion.operator] + " ";
            txt += criterion.value;
        }

        this.row.setText(txt);



        this.updateHeader(statNames[criterion.stat]);
    }

    public setSimple(name, value) {
        this.row.fill = "green";
        this.updateHeader(name);
        this.row.setText(value);
    }

    private updateHeader(header: string) {
        if(header)
        this.describe.setText(header);

     //   let x = this.describe.x - this.describe.width/2 - this.line.worldPosition.x;
      //  let cropRect = new Phaser.Rectangle(x-8, 0, this.describe.width+16, this.line.height);
      //  this.line.crop(cropRect);
    }

    public prepareToAnim() {
        this.group.alpha = 0;
    }

    public anim(next?: AnimableElement[], onEnd?: Function) {
        let tween = this.phaser.add.tween(this.group).to({ alpha: 1 }, 600).start();
        if (next)
            tween.onComplete.addOnce(() => {

                if (next && next.length > 0)
                    next.shift().anim(next, onEnd);
                else if (onEnd)
                    onEnd();

            });
    }
}

