﻿export class SleepManager {

    static keepAwake() {
        if (!window.plugins)
            return;
        if (!(<any>window.plugins).insomnia)
            return;
        
        (<any>window.plugins).insomnia.keepAwake(this.succes, this.error)
    }

    static allowSleep() {
        if (!window.plugins)
            return;
        if (!(<any>window.plugins).insomnia)
            return;

        (<any>window.plugins).insomnia.allowSleepAgain(this.succes, this.error)
    }

    private static succes() {
        console.log("Sleep settings changes");
    }

    private static error(err) {
        console.warn("Can't change sleep settings!", err);
    }
}