﻿
export class Notyfications {

    private static localNotification;
    static disabled: boolean = false;
    static listen() {
        if (this.localNotification === undefined) {
            this.localNotification = ((<any>cordova).plugins && !(<any>cordova).plugins.notification) ? (<any>cordova).plugins.notification.local : null;
        }

        (window as any).FirebasePlugin.onNotificationOpen((notification) => {
            if (this.disabled)
                return;

            console.log(notification);
            this.onNotyfication(notification);

        }, function (error) {
            console.error(error);
        });
    }

    private static onNotyfication(notification) {
        if (!notification.tap) {
            this.convertToLocal(notification);
            return;
        }

        if (notification.tap) {
            this.makeAction(notification);
            return;
        }

    }

    private static makeAction(notification) {
        if (notification.open_url)
            var ref = (cordova as any).InAppBrowser.open(notification.open_url, "_system");
    }

    private static convertToLocal(notyfication) {
        if (!this.localNotification)
            return;
        let id = Math.round(Math.random() * 1000000);
        this.localNotification.schedule({
            id: id,
            title: "New Message",
            message: "Hi, are you ready? We are waiting.",
            open_url: notyfication.open_url
        });

        this.localNotification.on("click", (notification) => {
            if (notification.id == id) {
                this.makeAction(notification);
            }
        });
    }

}