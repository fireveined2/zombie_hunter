﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397705
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

import { Game } from "./beforeGame/Game"
import { Config } from "./utils/Config"

declare var AndroidFullScreen;

class ZombieHunter {
    private game: Game;

    constructor() {
        let os = (typeof device !== "undefined") ? device.platform : "browser";
        if (os == "Android")
            Config.Version.os = "android";
        if (os == "iOS")
            Config.Version.os = "ios";

        if (os == "WinCE")
            Config.Version.os = "windows";

        if (os == "browser")
            Config.Version.os = "android";

  if(os!="browser")
            (window.screen as any).orientation.lock('landscape');

        document.addEventListener("backbutton", () => this.onBackKeyDown(), false);

        document.addEventListener('pause', () => this.onPause(), false);
        document.addEventListener('resume', () => this.onResume(), false);

        (document.getElementById('game-window').style as any).height = (screen.height > screen.width) ? screen.width : screen.height;
        (document.getElementById('game-window').style as any).width = (screen.width > screen.height) ? screen.height : screen.width;
        $("#game-window").css("position", "absolute");
        $("#game-window").css("top", "0");
        $("#game-window").css("left", "0");

        this.game = new Game();
        document.onclick = () => setTimeout(() => Game.absence.markAsActive(), 100);
        document.ontouchstart = () => setTimeout(() => Game.absence.markAsActive(), 100);
    }


    private onBackKeyDown() {
        if (Game.backButtonCallback)
            Game.backButtonCallback();
    }


    private onPause() {
        Game.pause_base();
    }

    private onResume() {
        Game.resume_base();
    }

}

document.addEventListener('runZH', () => new ZombieHunter(), false);
/*
window.onload = function () {
    new ZombieHunter();
}
*/