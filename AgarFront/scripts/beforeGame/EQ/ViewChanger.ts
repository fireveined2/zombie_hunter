﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"
import { Wall } from "../../utils/Wall"
import { GameEvents } from "../../game/logic/GameEvents"

import * as Models from "../../../../Shared/Shared";

export enum ViewType {
    WEAPON1 = Models.ItemType.WEAPON1 as number,
    WEAPON2=Models.ItemType.WEAPON2,
    CORPUS=Models.ItemType.CORPUS,
    BOOTS=Models.ItemType.BOOTS,
    HEAD=Models.ItemType.HEAD,
    POTION=Models.ItemType.POTION,
    SET=99
}


export class ViewChanger {

    private icons: Phaser.Image[]=[];
    private phaser: Phaser.State;
    private current: ViewType;
    private onSelected: (type: ViewType, prev: ViewType) => void;

    constructor(private group: Phaser.Group, onSelect: (type: ViewType) => void) {
        this.onSelected = onSelect;
        let phaser = this.phaser = group.game.state.getCurrentState();

        let names = ["weapon1", "weapon2", "shirt", "boots", "head", "cure", /*"head"*/];
        let types = [ViewType.WEAPON1,
            ViewType.WEAPON2,
            ViewType.CORPUS,
            ViewType.BOOTS,
            ViewType.HEAD,
            ViewType.POTION /*,
            ViewType.SET*/];

        let width = 57;
        let center = Game.width / 2;
        let start = center - names.length * (width) / 2 - 37;
        let y = Game.height*0.02;
        for (let i = 0; i < names.length; i++) {
            let icon = this.icons[types[i]] = this.phaser.add.image(start + width * i, y, "eq/icons", names[i], this.group);
            icon.inputEnabled = true;
            icon.events.onInputDown.add(() => {
                SFX.get().getSound("buttons/click").play();
                this.select(types[i])
            });
        }

        this.select(types[0]);
    }

    private select(type: ViewType) {
        if (this.current !== undefined) {
            // "dezaktywacja" ikony
            this.icons[this.current].frameName = this.icons[this.current].frameName.split("_")[0];
        }

        let prev = this.current;
        this.current = type;
        this.icons[type].frameName = this.icons[this.current].frameName + "_activ";
        if (this.onSelected)
            this.onSelected(type, prev);

    }
}