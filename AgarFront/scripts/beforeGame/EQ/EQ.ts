﻿import {Game} from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import { HeroAnim } from "../../utils/HeroAnim"
import * as Server from "../../utils/Server"
import { Wall } from "../../utils/Wall"
import { GameEvents } from "../../game/logic/GameEvents"

import * as Models from "../../../../Shared/Shared";
import {ItemsHelpers} from "../../../../Shared/Items";
import { ViewChanger, ViewType } from "./ViewChanger"
import { ItemSelector, ISelectorItem } from "./ItemSelector"
import { ItemDescribe } from "./ItemDescribe"


export class EQ extends Phaser.State {

    private background: Phaser.Image;
    private group: Phaser.Group;
    private wall: Wall;
    private viewChanger: ViewChanger;
    private itemSelector: ItemSelector;
    private itemDescribe: ItemDescribe;
    private hero: HeroAnim;

    init() {
        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;


        this.background = this.add.image(Game.width/2, 0, 'eq/bg', 0, this.group);
        this.background.anchor.x = 0.5;



        this.itemSelector = new ItemSelector(this.group, this.itemSelected.bind(this));
        this.itemDescribe = new ItemDescribe(this.group);


        let wall = this.wall= new Wall(this, this.group, Player.get().data);
        wall.setMenuButton();
        wall.onButtonClick = () => this.returnToMainMenu();


        this.hero = new HeroAnim(this, this.group);
        this.hero.scale.set(0.8);
        this.hero.setItems(Player.get().getAllWearedItems());
        let x = 359 * Game.assetsSize;
        let y = Game.height - 45 * Game.assetsSize - this.hero.height*0.82;
        this.hero.position.set(x, y);

        this.viewChanger = new ViewChanger(this.group, this.changeType.bind(this));
    }

    private changeType(type: ViewType, oldType?: ViewType) {
        if (type != ViewType.SET) {
            let items = ItemsHelpers.getAllOfType(Player.get().data.items, type as any);
            let elements: ISelectorItem[] = [];
            for (let item of items) {
                elements.push({ data: item, icon: "items/" + item.id });
            }
            this.itemSelector.loadItems(elements);
            this.itemSelector.onSelected = this.itemSelected.bind(this);

            let weared = Player.get().getWearedItem(type as any);    
            if (weared)
                this.itemSelector.set(weared);
            else
                this.itemDescribe.setEmpty();

            // cofamy tymczasowe przymiarki
            weared = Player.get().getWearedItem(oldType as any);
            if (weared)
                this.hero.setItem(weared);
        }
    }


    private itemSelected(item: Models.IItem) {
        if (item.type == Models.ItemType.POTION) {
            let desc = ItemsHelpers.getBonusDescribe(item.bonus);;
            let button = "use";
            if (Player.get().data.hp >= 0.996)
                button = null;
            this.itemDescribe.set(item.name, desc, "use");
            this.itemDescribe.onButtonClick = () => this.useItem(item);
        }
        else {
            let title = item.name;
            let button;
            let weared = Player.get().getWearedItem(item.type);
            if (weared && item.id == weared.id)
                title += "\nZAŁOŻONE";
            else {
                button = "puton";
                this.itemDescribe.onButtonClick = () => this.putOn(item);
            }

            let desc = ItemsHelpers.getBonusDescribe(item.bonus);;
            this.itemDescribe.set(title,desc, button);
            this.hero.setItem(item);
        }
    }

    private useItem(item: Models.IItem) {
        Player.get().useItem(item);
        this.wall.updatePlayerData(Player.get().data);
        this.changeType(ViewType.POTION);
        SFX.get().playSingleEffect("drink");
        setTimeout(()=> SFX.get().playOneOfEffect("slon/cure"), 700);
    }


    private putOn(item: Models.IItem) {
        Player.get().putOnItem(item);
        this.itemSelected(item);
    }

    public update() {
        this.hero.updateAnim();
    }

    public shutdown() {
        Player.get().saveEQ();
        this.hero.kill();
    }

    private returnToMainMenu() {
        this.game.state.start('MainMenu', true, false, false);
    }

}