﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"
import { Wall } from "../../utils/Wall"
import { GameEvents } from "../../game/logic/GameEvents"
import * as GUI from "../../utils/gui/gui"
import * as Models from "../../../../Shared/Shared";

export class ItemDescribe {


    private phaser: Phaser.State;

    private title: Phaser.Text;
    private describe: Phaser.Text;

    private button: Phaser.Image;
    public onButtonClick: Function;
  
    constructor(private group: Phaser.Group) {
        let phaser = this.phaser = group.game.state.getCurrentState();

        let window = phaser.add.image(Game.width * 1.10, -5, "eq/elements", "popup_right", this.group);
        window.anchor.x = 1;

        let style = <Phaser.PhaserTextStyle>{ font: "19px CosmicTwo", fill: "white", wordWrap: true, wordWrapWidth: 170, align:"center" }
        this.title = this.phaser.add.text(-window.width * 0.54, window.height * 0.1, "", style);
        this.title.anchor.x = 0.5;
        window.addChild(this.title);

        style = <Phaser.PhaserTextStyle>{ font: "16px Candara", fill: "white", wordWrap: true, wordWrapWidth: 170 }
        this.describe = this.phaser.add.text(-window.width * 0.54, window.height * 0.3, "", style);
        this.describe.anchor.x = 0.5;
        window.addChild(this.describe);

        this.button = this.phaser.add.image(-window.width * 0.54, window.height * 0.8, "buttons/use", 0, this.group);
        this.button.inputEnabled = true;
        this.button.events.onInputDown.add(() => this._onButtonClick());
        this.button.anchor.set(0.5);
        this.button.visible = false;
        window.addChild(this.button);
    }

    private _onButtonClick() {
        SFX.get().getSound("buttons/click").play();
        if (this.onButtonClick)
            this.onButtonClick();
    }

    public setEmpty() {
        this.set("","");
    }
    public set(title: string, describe: string, button?: "use"|"puton") {
        this.title.setText(title);
        this.describe.setText(describe);

 
        if (!button)
            this.button.visible = false;
        else
            this.button.visible = true;
        if (button == "use")
            this.button.loadTexture("buttons/use");
        if (button == "puton")
            this.button.loadTexture("buttons/wear");
    }

    
}