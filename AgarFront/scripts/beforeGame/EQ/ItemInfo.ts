﻿
import {Game} from "../Game"
import * as Models from "../../../../Shared/Shared";
import { ItemsHelpers } from "../../../../Shared/Items";
import * as GUI from "../../utils/gui/gui"
import { SFX } from "../../SFX/SFX"

export class ItemInfo {

    private phaser: Phaser.State;

    private bg: Phaser.Image;

    private name: GUI.Label;
    private type: Phaser.Text
    private icon: Phaser.Image

    private return: Phaser.Image;
    private describe: Phaser.Text;

    private leftArrow: Phaser.Image;
    private rightArrow: Phaser.Image;

    private currentScrollPosition: number;
    public onClose: Function;

    constructor(parent: Phaser.Group, private items: Models.IItem[], startItem?: Models.IItem) {
        SFX.get().playSingleEffect("buttons/click");
        this.phaser = parent.game.state.getCurrentState();
        this.bg = this.phaser.add.image(0, -20, "gui/info_popup_bg", 0, Game.group);
        this.bg.inputEnabled = true;

        let startPos = 0;
        if (startItem)
            for (let i = 0; i < items.length; i++)
                if (items[i].id == startItem.id)
                    startPos = i;

        this.init();
        if (items.length == 0)
            return;

        this.currentScrollPosition = startPos;
        this.setItemInfo(items[startPos]);

        this.initScroll();
    }

    public playSlonComment() {
        let type = this.items[0].type;
        let isAntidotum = this.items[0].bonus.heal>0;
        if (type == Models.ItemType.BOOTS)
            SFX.get().playSlon("slon/rewards/boots", true);

        if (type == Models.ItemType.CORPUS)
            SFX.get().playSlon("slon/rewards/corpus", true);

        if (type == Models.ItemType.WEAPON1)
            SFX.get().playSlon("slon/rewards/weapon", true);

        if (type == Models.ItemType.WEAPON2)
            SFX.get().playSlon("slon/rewards/weapon", true);

        if (isAntidotum)
            SFX.get().playSlon("slon/rewards/antidotum", true);
    }


    private initScroll() {
        this.leftArrow = this.phaser.add.image(145, Game.height * 0.37 + 65, "gui/arrow_left")
        this.leftArrow.anchor.set(0, 0)
        this.leftArrow.inputEnabled = true;
        this.leftArrow.events.onInputDown.add(() => this.scroll(-1));
        this.bg.addChild(this.leftArrow);

        this.rightArrow = this.phaser.add.image(643, Game.height * 0.37 + 65, "gui/arrow_right")
        this.rightArrow.anchor.set(1, 0)
        this.rightArrow.inputEnabled = true;
        this.rightArrow.events.onInputDown.add(() => this.scroll(1));
        this.bg.addChild(this.rightArrow);

        this.updateScroll();
    }

    private scroll(dir: number) {
        this.currentScrollPosition += dir;
        this.setItemInfo(this.items[this.currentScrollPosition]);
        this.updateScroll();
    }

    private updateScroll() {
            this.leftArrow.visible = this.currentScrollPosition > 0;
            this.rightArrow.visible = this.currentScrollPosition < (this.items.length - 1);
    }

    private setItemInfo(item: Models.IItem) {
        let describe: string;
        describe = ItemsHelpers.getBonusDescribe(item.bonus);
        if (item.id == 0)
            describe = `Waluta umożliwiająca udział w Wyzwaniach. Cechuje zdolności Łowcy Zombie i służy do określania umiejętności graczy.`;

        this.name.destroy();
        this.name = new GUI.Label(item.name, Game.width * 0.5, Game.height * 0.125, Game.group);
        this.name.anchor.x = 0.5;
        this.bg.addChild(this.name.element);


        let type = this.getType(item);
        this.type.setText(type);
        this.icon.loadTexture("items/" + item.icon);
        this.describe.setText(describe);
    }


    private init() {
        this.name = new GUI.Label("", Game.width * 0.5, Game.height * 0.125, Game.group);
        this.name.anchor.x = 0.5;
        this.bg.addChild(this.name.element);

        let style = <Phaser.PhaserTextStyle>{ font: "21px CosmicTwo", fill: "black" };

        this.type = this.phaser.add.text(Game.width * 0.5, Game.height * 0.276, "", style);
        this.type.anchor.x = 0.5;
        this.bg.addChild(this.type);

        let frame = this.phaser.add.image(Game.width * 0.5, Game.height * 0.37 + 65, "eq/elements", "frame");
        frame.anchor.set(0.5);
        this.bg.addChild(frame);

        this.icon = this.phaser.add.image(Game.width * 0.5, Game.height * 0.37 + 65, "items/1")
        this.icon.anchor.set(0.5)
        this.bg.addChild(this.icon);

        style = <Phaser.PhaserTextStyle>{ font: "23px CosmicTwo", fill: "black", wordWrap: true, wordWrapWidth: 530, align: "center" }
        this.describe = this.phaser.add.text(Game.width * 0.5, this.icon.bottom + 10, "", style);
        this.describe.anchor.x = 0.5;
        this.bg.addChild(this.describe);

        this.return = this.phaser.add.image(Game.width * 0.95, Game.height * 0.08, "no")
        this.return.anchor.set(1, 0)
        this.return.inputEnabled = true;
        this.return.events.onInputDown.addOnce(() => {
            this.bg.destroy();
            if (this.onClose)
                this.onClose();
        });
        this.bg.addChild(this.return);
    }


    private getType(item: Models.IItem) {
        let type = item.type;

        if (item.bonus.typeDescription)
            return item.bonus.typeDescription;

        if (type == Models.ItemType.WEAPON1)
            return "BROŃ PODSTAWOWA";

        if (type == Models.ItemType.WEAPON2)
            return "BROŃ DODATKOWA";

        if (type == Models.ItemType.HEAD)
            return "GŁOWA";

        if (type == Models.ItemType.CORPUS)
            return "STRÓJ";

        if (type == Models.ItemType.BOOTS)
            return "BUTY";

        if (type == Models.ItemType.POTION)
            return "ANTIDOTUM";

        if (type == Models.ItemType.OTHER) {
            return "NAGRODA";
        }

        return "";
    }
}