﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"
import { Wall } from "../../utils/Wall"
import { GameEvents } from "../../game/logic/GameEvents"
import * as GUI from "../../utils/gui/gui"
import * as Models from "../../../../Shared/Shared";

export interface ISelectorItem {
    icon: string;
    data: any;
}

export class ItemSelector {

    private icons: Phaser.Image[] = [];
    private current: number;
    private frames: Phaser.Image[] =[];
    private phaser: Phaser.State;
    public onSelected: (item: any) => void;
    private items: ISelectorItem[];

    private scrollVal = 0;
    private arrows: GUI.ArrowSelector;

    private frameYMargin: number;
    constructor(private group: Phaser.Group, onSelect: (type: any) => void) {
        this.onSelected = onSelect;
        let phaser = this.phaser = group.game.state.getCurrentState();

        let window = phaser.add.image(-32, -8, "eq/elements", "popup_left", this.group);

        for (let i = 0; i < 3; i++) {
            let frame = this.frames[i] = this.phaser.add.image(window.width * 0.54, 44, "eq/elements", "frame", this.group);
            frame.y = 44 + (frame.height + 30) * i;
            frame.anchor.x = 0.5;
            frame.inputEnabled = true;
            window.addChild(frame);
        }

        this.frameYMargin = (this.frames[0].height + 30);
        
        this.arrows = new GUI.ArrowSelector(this.group, window.right + 25, Game.height - 115, this.scroll.bind(this));
        this.arrows.enableTop(false);
        this.arrows.enableBot(false);
    }


    private scroll(arrow: "bot" | "top") {
        let dir = (arrow == "top" ? -1 : 1);
        for (let icon of this.icons) {
            icon.y += -dir * this.frameYMargin;
            icon.visible = false;
        }

        this.scrollVal += dir;

        for (let i = 0; i < 3; i++)
            this.icons[i + this.scrollVal].visible = true;
        this.highlightFrame(this.current);

        this.arrows.enableTop(this.scrollVal > 0);
        this.arrows.enableBot(this.items.length - this.scrollVal > 3);
    }

    public loadItems(items: ISelectorItem[]) {
        this.items = items;
        for (let icon of this.icons)
            icon.destroy();
        this.icons = []; 

        for (let i = 0; i < 3; i++) {
            let frame = this.frames[i];
            frame.events.onInputDown.removeAll();

            frame.events.onInputDown.add(() => {
                let item = items[i + this.scrollVal];
                if (!item)
                    return;
                SFX.get().getSound("buttons/click").play();
                this.select(item)
            });
        }

        let i = 0;
        for (let item of items) {
            let icon = this.phaser.add.image(this.frames[0].centerX-27, this.frames[0].top + this.frames[0].height / 2 + this.frameYMargin * i - 8, item.icon, 0, this.group);
            this.icons.push(icon);
            icon.anchor.set(0.5);
            if (i > 2)
                icon.visible = false;
            i++

        if (item.data.quantity > 1)
            this.showQuantity(item.data.quantity, icon);
        }

        this.scrollVal = 0;
        this.reset();
    }

    private showQuantity(quantity: number, icon: Phaser.Image) {
        let style = { font: "22px CosmicTwo", fill: "white" };
        let frame = this.frames[0];
        let text = this.phaser.add.text(frame.width * 0.42, frame.height * 0.44, quantity.toString(), style);
        text.anchor.set(1);
        icon.addChild(text);
    }

    public set(item: any) {
        for (let obj of this.items)
            if (obj.data.id == item.id || obj.data == item) {
                this.select(obj);
                return;
            }
        this.select(this.items[0]);
        console.warn("Can't find item: ", item, this.items);
    }

 

    private reset() {
        if (this.current !== undefined) {
            // "dezaktywacja" ramki
            this.highlightFrame(-100);
            this.current = undefined;
        } 

        this.arrows.enableTop(this.scrollVal > 0);
        this.arrows.enableBot(this.items.length - this.scrollVal > 3);

        
        

    }
    private select(item: ISelectorItem) {
        if (!item)
            return;
        this.reset();

        this.current = this.items.indexOf(item);

        this.highlightFrame(this.current);
        if (this.onSelected)
            this.onSelected(item.data);

    }

    private highlightFrame(index: number) {
        for (let frame of this.frames)
            frame.frameName = frame.frameName.split("_")[0];

         index-=this.scrollVal;
         if (index >= 0 && index < 3)
             this.frames[index].frameName = this.frames[index].frameName + "_activ";;
    }
}