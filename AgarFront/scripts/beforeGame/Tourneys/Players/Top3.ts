﻿import { Game } from "../../Game"
import { FX } from "../../../game/FX"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { Settings } from "../../Settings";
import { Player } from "../../../utils/Player"
import { BoardManager } from "../../Boards/BoardManager"
import * as GUI from "../../../utils/gui/gui";
import { GameStarter } from "../../GameStarter";

import * as Models from "../../../../../Shared/Shared";
import { HeroAnim } from "../../../utils/HeroAnim"
import { Wall } from "../../../utils/Wall"
import { Messages } from "../../../utils/Messages"
import { Ranking } from "../../Ranking/Ranking"
import { WebPage } from "../../../utils/WebPage"

import { TextDatabase } from "../../../utils/TextBase"

export class TOP3Dais {


    public group: Phaser.Group;
    private phaser: Phaser.State;

    private hero: HeroAnim[] = [];

    private headers: Phaser.Text[] = [];

    private getDetailsForMe(): Models.ITourneyPlayerDetails {
        let data = <Models.ITourneyPlayerDetails>{};
        data.level = Player.get().data.level;
        data.respect = Player.get().data.respect;

        let items = [];
        let weared = Player.get().getAllWearedItems();
        for (let item of weared)
            items.push(item.set);
        data.wearedItemsIds = items;
        return data;
    }

    private getInfoForMe(place: number, score): Models.IRankingPlayerData {
        let info = <Models.IRankingPlayerData>{};
        info.nick = Player.get().data.username;
        info.score = score;
        info.place = place;
        return info;
    }
    constructor(private parent: Phaser.Group, private data: Models.ITourneyPlayerDetails[], players: Models.IRankingPlayerData[],  myScore: Models.IRankingPlayerData) {

        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        let bg = this.phaser.add.image(0, 0, "menu/bg", 0, this.group);

        if (myScore) {
            let myIndex = myScore.place - 1;
            if (myScore && myScore.place < 4 && data[myScore.place - 1] && players[myScore.place - 1].nick != Player.get().data.username) {
                data[myIndex + 1] = JSON.parse(JSON.stringify(data[myIndex]));
                players[myIndex + 1] = JSON.parse(JSON.stringify(players[myIndex]));
                data[myIndex] = this.getDetailsForMe();
                players[myIndex] = this.getInfoForMe(myScore.place, myScore.score);
            }

            if (!data[myIndex] && myScore.place < 4) {
                data[myIndex] = this.getDetailsForMe();
                players[myIndex] = this.getInfoForMe(myScore.place, myScore.score);
                }
        }


        let xPositions = [0.5, 0.228, 0.74];
        let yPositions = [-30,  4, 25];

        
        for (let i = 0; i < 3; i++) {
            let details = data[i];
            if (!details) 
                continue;

         
            this.hero[i] = new HeroAnim(this.phaser, this.group);
            this.hero[i].scale.set(0.45);


            let y = Game.height - this.hero[i].height + yPositions[i] -26;

            this.hero[i].setItemsFromSetIds(details.wearedItemsIds);
         
            this.hero[i].position.set(Game.width * xPositions[i], y);

            let style = <Phaser.PhaserTextStyle>{ font: "18px CosmicTwo", fill: "white", align: "center" };
            let text = this.headers[i] = this.phaser.add.text(Game.width * xPositions[i], y - 120, players[i].nick + "\n" + players[i].score + " pkt", style, this.group);
            text.anchor.x = 0.5;
            text.setShadow(4, 4, 'rgba(0,0,0,0.5)', 5);

        }

 


        let dais = this.phaser.add.image(Game.width * 0.5, Game.height - 60, "tourney/dais", 0, this.group);
        dais.anchor.set(0.5, 1);
        this.setVisible(false);
    }

    public update() {
        for (let i = 0; i < 3; i++)
            if (this.hero[i])
                this.hero[i].updateAnim();
    }

    public remove() {
        for (let i = 0; i < 3; i++)
            if (this.hero[i])
                this.hero[i].kill();
    }

    public setVisible(visible: boolean) {
        this.group.visible = visible;
        $("#content").css("pointer-events", (!visible) ? "auto" : "none");
        $("#game-window").css("pointer-events", visible ? "auto" : "none");
    }
    
}

