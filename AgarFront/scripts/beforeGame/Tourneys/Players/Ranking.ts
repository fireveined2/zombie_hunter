﻿import { Game } from "../../Game"
import { FX } from "../../../game/FX"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { Settings } from "../../Settings";
import { Player } from "../../../utils/Player"
import { BoardManager } from "../../Boards/BoardManager"
import * as GUI from "../../../utils/gui/gui";
import { GameStarter } from "../../GameStarter";

import * as Models from "../../../../../Shared/Shared";
import { HeroAnim } from "../../../utils/HeroAnim"
import { Wall } from "../../../utils/Wall"
import { Messages } from "../../../utils/Messages"
import { WebPage } from "../../../utils/WebPage"
import { ItemInfo } from "../../EQ/ItemInfo";
import { PlayerPopup } from "../../Profile/PlayerPopup"
import { socketClient } from "../../../utils/server/Socket"
export class Ranking {
    private group: Phaser.Group;

    constructor(players: Models.IRankingPlayerData[], myScore: Models.IRankingPlayerData, prizes: Models.ITourneyPrize[]) {
        $("#content").css("opacity", "0");
        WebPage.loadInBackground("pages/tourneys/ranking.html", () => {
            this.setVisible(false);
            $("#content").css("opacity", "1");
            this.initList(players, myScore, prizes);
        });
    }

    public setVisible(visible: boolean) {
        $("#tourney_fullPageRanking").css("opacity", visible ? "1" : "0");
        $("#tourney_fullPageRanking").css("pointer-events", visible ? "auto" : "none");
        $("#game-window").css("pointer-events", !visible ? "auto" : "none");
    }

    private getRowHtml(row: Models.IRankingPlayerData): string {
        let status = ["offline", "online", "ingame"][row.status];
        return `
		    <tr>
			  <td>${row.place}</td>
			  <td><li id="${status}">${row.nick}</li></td>
			  <td>${row.score}</td>
			  <td><img src="pages/tourneys/assets/gift.png" /></td>
			</tr>`;
    }

    private addToTable(elements: any[]) {
        $("#tourney_rankingTableBody").append(elements);

    }

    public initList(players: Models.IRankingPlayerData[], myScore: Models.IRankingPlayerData, prizes: Models.ITourneyPrize[]) {
        let html = "";
        let elements = [];
        if (myScore) {
            myScore.nick = Player.get().data.username;
            myScore.status = 1;
            let element = $(this.getRowHtml(myScore));
            element.children("td").last().on("click", () => {
                this.showAllPrizesForPlace(myScore.place, prizes);
            });
            elements.push(element);
        }
    
        for (let row of players) {
            if (row.nick == Player.get().data.username)
                continue;
            if (myScore && row.place == myScore.place)
                row.place++;
            let element = $(this.getRowHtml(row));
            element.click(() => {
                (row as any).username = row.nick;
                this.initPlayerInfo(row as any);
            });
            element.children("td").last().on("click", () => {
                this.showAllPrizesForPlace(row.place, prizes );
            });
       
            elements.push(element);
        }
        this.addToTable(elements);

        setTimeout(() => {
            $(function () {
                $("#tourney_rankingTable").tablesorter({
                });
            });
        }, 100);
    }

    private initPlayerInfo(player: Models.PlayerInfo) {
       
        $("#game-window").css("pointer-events", "auto");
        setTimeout(() => $("#game-window").css("pointer-events", "auto"), 300);
        let info = new PlayerPopup(player);
        info.onClose = () => $("#game-window").css("pointer-events", "none");

    }


    private showAllPrizesForPlace(place: number, prizes: Models.ITourneyPrize[]) {
        $("#game-window").css("pointer-events", "auto");
        setTimeout(() => $("#game-window").css("pointer-events", "auto"), 300);

        let prizesForThisPos = [];
        for (let prize of prizes)
            if (prize.rangeStart <= place && prize.rangeEnd >= place) {
                let item: Models.IItem;
                item = JSON.parse(JSON.stringify(prize.prize));
                item.bonus.typeDescription = "NAGRODA DLA MIEJSCA " + place;
                prizesForThisPos.push(item);
            }

        let info = new ItemInfo(Game.group, prizesForThisPos);
        info.onClose = () => $("#game-window").css("pointer-events", "none");
    }

}

