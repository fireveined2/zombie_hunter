﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"
import * as GUI from "../../utils/gui/gui";
import { GameStarter } from "../GameStarter";

import * as Models from "../../../../Shared/Shared";
import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"
import { Challenges } from "../../utils/Challenges"
import { Ranking } from "../Ranking/Ranking"
import { WebPage } from "../../utils/WebPage"



export class TourneyList extends Phaser.State {
    private group: Phaser.Group;

    public create() {
        this.group = this.game.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        WebPage.loadInBackground("pages/tourney_list/list.html", () => {
            $("#fullPageTourneyList").
            //  this.initList(players, myScore, prizes);
            this.initList();
        });

        let wall = new Wall(this, Game.group, Player.get().data);
        wall.setMenuButton();
        wall.onButtonClick = () => this.returnToMainMenu();
    }


    private getRowHtml(row: Models.IChallenge): string {
        let myPlace = "-";
        if (row.tourneyData.myScore)
            myPlace = row.tourneyData.myScore.place.toString();

        return `
		    <tr>
			  <td>${row.name}</td>
			  <td>50zł</td>
			  <td>${row.cost} HCN</td>
                <td>${myPlace}</td>
			</tr>`;
    }

    private addToTable(element: JQuery[]) {
        $("#listTableBody").append(element);

    }

    public initList() {
        let tourneys = Challenges.tourneys;

        let elements = [];
        for (let row of tourneys) {
            let elem = $(this.getRowHtml(row.data));

            elem.click(() => {
                this.openTourneyPage(row);
            });

            elements.push(elem);
        }
        this.addToTable(elements);

        setTimeout(() => {
            $(function () {
                $("#listTable").tablesorter({
                });
            });
        }, 100);
    }

    private openTourneyPage(tourney: Models.IChallengeData) {

        FX.fade(this.game, "out", 400, () => this.game.state.start('Tourneys', true, false, tourney));
    }

    private returnToMainMenu() {     
        FX.fade(this.game, "out", 600, () => {
            WebPage.unload();
            this.game.state.start('MainMenu');       
        });
    }
}

