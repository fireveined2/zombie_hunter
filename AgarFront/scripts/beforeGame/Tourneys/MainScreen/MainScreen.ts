﻿import { Game } from "../../Game"
import { FX } from "../../../game/FX"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { Settings } from "../../Settings";
import { Player } from "../../../utils/Player"
import { BoardManager } from "../../Boards/BoardManager"
import * as GUI from "../../../utils/gui/gui";
import { GameStarter } from "../../GameStarter";

import * as Models from "../../../../../Shared/Shared";
import { HeroAnim } from "../../../utils/HeroAnim"
import { Wall } from "../../../utils/Wall"
import { Messages } from "../../../utils/Messages"
import { Ranking } from "../../Ranking/Ranking"
import { WebPage } from "../../../utils/WebPage"

import { TextDatabase } from "../../../utils/TextBase"

export class MainScreen {


    public group: Phaser.Group;
    private phaser: Phaser.State;
    private tourney: Models.IChallenge;

    public playCallback: Function;
    public trainCallback: Function;
    public resetScoreCallback: Function;

    constructor(private parent: Phaser.Group, private data: Models.IChallengeData) {
        this.tourney = data.data;
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);


        let bg = this.phaser.add.image(Game.width / 2, Game.height / 2, "background1", 0, this.group);
        bg.anchor.set(0.5);
         bg = this.phaser.add.image(0, 0, "screen_bg", 0, this.group);
        let line = this.phaser.add.image(Game.width * 0.5, Game.height * 0.25, "gui/vertical_line", 0, this.group);

        this.initHeader();
        this.initInfoPanel();
        this.initStatsPanel(0.43+0.08);
        this.initDescription();
         this.initPlayPanel();
    }

    public setVisible(visible: boolean) {
        this.group.visible = visible;
        $("#content").css("pointer-events", (!visible) ? "auto" : "none");
        $("game-window").css("pointer-events", visible ? "auto" : "none");
    }

    public initPlayPanel() {
        if (this.tourney.endTime < Date.now())
            return;

        let style = <Phaser.PhaserTextStyle>{ font: "18px CosmicTwo", fill: "white" };

      /*  let play = new GUI.Label("GRAJ ZA " + this.tourney.cost + " HCN", Game.width * 0.75, Game.height * 0.70, this.group, 18, "white");
        play.element.anchor.x = 0.5;
        play.element.inputEnabled = true;
        play.element.events.onInputDown.add(() => this.playCallback());
        */

        /*let training = this.phaser.add.text(Game.width * 0.75, Game.height * 0.78, "TRENUJ ZA " + this.tourney.tourneyData.nextTrainingCost +" HCN", style, this.group);
        training.anchor.set(0.5);
        if (this.tourney.tourneyData.nextTrainingCost === -1)
            training.visible = false;
        training.inputEnabled = true;
        training.events.onInputDown.add(()=>this.trainCallback());
        */

        if (this.tourney.tourneyData.nextTrainingCost === -1)
            return;

        let training = new GUI.Label("TRENUJ ZA " + this.tourney.tourneyData.nextTrainingCost + " HCN", Game.width * 0.75, Game.height * 0.66, this.group, 18, "white");
        training.element.anchor.x = 0.5;
        training.element.inputEnabled = true;
        training.element.events.onInputDown.add(() => this.trainCallback());

        if (this.data.data.tourneyData.myScore) {
            let reset = new GUI.Label("ZRESETUJ WYNIK", Game.width * 0.75, Game.height * 0.76, this.group, 18, "white");
            reset.element.anchor.x = 0.5;
            reset.element.inputEnabled = true;
            reset.element.events.onInputDown.add(() => this.resetScoreCallback());
        }

    }

    private initHeader() {
        let style = <Phaser.PhaserTextStyle>{ font: "27px CosmicTwo", fill: "white" };
        let name = this.phaser.add.text(Game.width * 0.07, Game.height * 0.08, this.tourney.name, style, this.group);
        name.anchor.set(0, 0.5);

         style = <Phaser.PhaserTextStyle>{ font: "17px CosmicTwo", fill: "white" };
        let reg = this.phaser.add.text(Game.width * 0.95, Game.height * 0.09, "REGULAMIN", style, this.group);
        reg.anchor.set(1, 0.5);
        reg.inputEnabled = true;
        reg.events.onInputDown.add(() => {
            var ref = (cordova as any).InAppBrowser.open("https://hophands.pl/gra/zombiehunter/regulamin/Regulamin_Turnieju.pdf", "_system");

        });
    }


    private initInfoPanel() {
        let style = <Phaser.PhaserTextStyle>{ font: "20px CosmicTwo", fill: "white" };

        let mapName = "ARENA";//TextDatabase.getMapName(this.tourney.config.map);
        let map = this.phaser.add.text(Game.width * 0.07, Game.height * 0.15, "Mapa: " + mapName, style, this.group);

   //     let tryb = this.phaser.add.text(Game.width * 0.07, Game.height * 0.24, "Tryb: KLASYCZNY", style, this.group);

        if (this.tourney.endTime ==0) {
            let duration = this.phaser.add.text(Game.width * 0.07, Game.height * 0.23, "Start: CZWARTEK 12:00", style, this.group);
        }
        else {
            let timeLeft = this.calculateDuration(this.tourney.minutesLeft);
            let duration = this.phaser.add.text(Game.width * 0.07, Game.height * 0.23, "Pozostało: " + timeLeft, style, this.group);
        }

        let cost = this.phaser.add.text(Game.width * 0.07, Game.height * 0.31, "Koszt: " + this.tourney.cost + " HCN", style, this.group);
        let free = this.phaser.add.text(Game.width * 0.07, Game.height * (0.31+0.08), "Darmowe wejściówki: " + Player.get().data.freeTourneyEntrances, style, this.group);

       // let cost = this.phaser.add.text(Game.width * 0.07, Game.height * 0.29, "Koszt: " + timeLeft, style, this.group);
    }

    private initStatsPanel(heigtPercent: number) {
        let y = heigtPercent;
        let style = <Phaser.PhaserTextStyle>{ font: "20px CosmicTwo", fill: "white" };

        let slon = this.phaser.add.text(Game.width * 0.07, Game.height * y, "Słoń: " + this.tourney.tourneyData.slonScore+" pkt", style, this.group);
        y += 0.08;

        let avg = this.phaser.add.text(Game.width * 0.07, Game.height * y, "Średnia: " + this.tourney.tourneyData.avgScore + " pkt", style, this.group);
        y += 0.08;

        /*
        let playerScore = this.data.data.tourneyData.myScore;
        if (playerScore) {
            let myScore = this.phaser.add.text(Game.width * 0.07, Game.height * y, "Ty: " + playerScore.score + " pkt", style, this.group);
            y += 0.08;
        }
        */

        let players = this.phaser.add.text(Game.width * 0.07, Game.height * y, "Ilość graczy: " + this.tourney.tourneyData.playersNum, style, this.group);
        y += 0.08;

        let lastPremiumPlace = 0;
        for (let prize of this.tourney.tourneyData.prizes)
            if (prize.rangeEnd > lastPremiumPlace)
                lastPremiumPlace = prize.rangeEnd ;
        let premPlaces = this.phaser.add.text(Game.width * 0.07, Game.height * y, "Premiowane miejsca: " + lastPremiumPlace, style, this.group);
        y += 0.08;
    }

    private initDescription() {
        let style = <Phaser.PhaserTextStyle>{ font: "15px Candara", fill: "white" };
        style.wordWrapWidth = Game.width * 0.45;
        style.wordWrap = true;
        style.align = "center";
        let description = this.phaser.add.text(Game.width * 0.75, Game.height * 0.14, this.tourney.description, style, this.group);
        description.anchor.x = 0.5;
    }

    private calculateDuration(duration: number): string {
        if (duration < 0)
            return "ZAKOŃCZONO";

        if (duration < 60)
            return duration + " minut";
        if (duration < 1440)
            return Math.round(duration / 60) + " godzin";
        return Math.round(duration / 1440) + " dni";
    }
}

