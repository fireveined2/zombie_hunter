﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"
import * as GUI from "../../utils/gui/gui";
import { GameStarter } from "../GameStarter";

import * as Models from "../../../../Shared/Shared";
import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"
import { Messages } from "../../utils/Messages"

import { WebPage } from "../../utils/WebPage"
import { Challenges } from "../../utils/Challenges"
import { MainScreen } from "./MainScreen/MainScreen"

import { Ranking } from "./Players/Ranking"
import { TOP3Dais } from "./Players/Top3"

import { PrizesList } from "./Prizes/PrizesList"
import { TOP3Prizes } from "./Prizes/Top3"

export class Tourney extends Phaser.State {
    private group: Phaser.Group;
    private wall: Wall;

    private mainScreen: MainScreen;

    private ranking: Ranking;
    private top3Players: TOP3Dais;

    private prizesList: PrizesList;
    private top3Prizes: TOP3Prizes;

    private current: { setVisible: (visible) => void };

    private prevTime;
    private tourney: Models.IChallengeData;

    public init(tourney: Models.IChallengeData) {
        this.tourney = tourney;
    }

    public create() {
        this.group = this.game.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.prevTime = Date.now();

        let tourneyData = this.tourney.data.tourneyData;
        this.mainScreen = new MainScreen(this.group, this.tourney);
        this.mainScreen.playCallback = () => { this.startGame() }
        this.mainScreen.trainCallback = () => { this.startGame(true) }
        this.mainScreen.resetScoreCallback = () => this.resetScore();

        this.current = this.mainScreen;
        this.ranking = new Ranking(tourneyData.players, tourneyData.myScore, tourneyData.prizes);

        tourneyData.prizes = tourneyData.prizes.sort((p1, p2) => {
            let v1 = p1.prize.bonus.valuePLN ? p1.prize.bonus.valuePLN : 0;
            let v2 = p2.prize.bonus.valuePLN ? p2.prize.bonus.valuePLN : 0;
            return v2 - v1;
        });
        this.prizesList = new PrizesList(tourneyData.prizes);
        this.top3Players = new TOP3Dais(this.group, tourneyData.top3, tourneyData.players, tourneyData.myScore);
        this.top3Prizes = new TOP3Prizes(this.group, this.tourney.data.tourneyData.prizes);

        let wall = this.wall = new Wall(this, Game.group, Player.get().data, { tourneySetup: true });
        wall.enableHCNDetails();
        wall.setTourneyButton(this.tourney.data.cost, Player.get().data.hcn, () => this.startGame(), this.tourney.data.endTime < Date.now());
        this.backToMainPage();

        FX.fade(this.game, "in", 800, () => {
        }, 400);
    }

    private resetScore() {
        let overlay = FX.createTextOverlay(this.game, "Oczekiwanie na serwer...");
        Player.get().resetScore(this.tourney.data.id, () => {
            overlay.destroy();
            new GUI.Popup("Twój wynik został usunięty", this.game, undefined, () => this.returnToMainMenu());
            this.tourney.data.tourneyData.playersNum--;
            delete this.tourney.data.tourneyData.myScore;
        }, () => {
            overlay.destroy();
            new GUI.Popup("Nie można usunąć wyniku");
        });
    }

    private startGame(training?: boolean) {
        if (!Player.get().logged) {
            new GUI.Popup("Zaloguj się, aby wziąć udział w turnieju!");
            return;
        }

        if (GameStarter.starting)
            return;

        let cost = this.tourney.data.cost;
        if (training)
            cost = this.tourney.data.tourneyData.nextTrainingCost;

        let hcn = Player.get().data.hcn;
        if (cost > hcn && (training || Player.get().data.freeTourneyEntrances<1)) {
            new GUI.Popup("Potrzebujesz więcej HCN! Doładuj konto, aby wziąć udział w turnieju");
            return;
        }

        let starter = new GameStarter(this.group);
        starter.startChallenge({ challengeId: this.tourney.data.id, isChallengeTraining: training });
        starter.onStart = () => WebPage.unload();
        return;
    }

    public backToMainPage() {
        this.wall.setMenuButton("back");
        this.wall.onButtonClick = () => this.returnToMainMenu();
        this.changeScreen(this.mainScreen);

        this.wall.setLeftButton("top3_players", () => this.openRanking("top3"));
        this.wall.setRightButton("prizes", () => this.openPrizes("top3"));
    }

    public setWallButtonBackToMainPage() {
        this.wall.setMenuButton("turnieje");
        this.wall.onButtonClick = () => this.backToMainPage();
    }

    private changeScreen(screen: { setVisible: (visible) => void }) {
        this.current.setVisible(false);
        this.current = screen;
        this.current.setVisible(true);
    }

    public openRanking(type: "list" | "top3") {

        if (!this.tourney.data.tourneyData.myScore) {
            new GUI.Popup("Ranking dostępny tylko dla uczestników turnieju.");
            return;
        }

        this.wall.setRightButton("prizes", () => this.openPrizes("top3"));
        setTimeout(() => {
            if (type == "list")
                this.wall.setLeftButton("top3_players", () => this.openRanking("top3"));
            else
                this.wall.setLeftButton("list", () => this.openRanking("list"));
        }, 20);
        this.setWallButtonBackToMainPage();
        this.changeScreen(type == "list" ? this.ranking : this.top3Players);
    }

    public openPrizes(type: "list" | "top3") {
        this.wall.setLeftButton("top3_players", () => this.openRanking("top3"));
        setTimeout(() => {
            if (type == "list")
                this.wall.setRightButton("prizes", () => this.openPrizes("top3"));
            else
                this.wall.setRightButton("prize", () => this.openPrizes("list"));
        }, 20);

        this.setWallButtonBackToMainPage();
        this.changeScreen(type == "list" ? this.prizesList : this.top3Prizes);
    }

    public update() {
        let now = Date.now();
        let elapsed = now - this.prevTime;
        this.prevTime = now;

        this.top3Players.update();
        this.wall.update();
    }


    public shutdown() {
        WebPage.unload();
        this.top3Players.remove();
    }


    private returnToMainMenu() {
        FX.fade(this.game, "out", 400, () => this.game.state.start('MainMenu'));
    }
}

