﻿import { Game } from "../../Game"
import { FX } from "../../../game/FX"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { Settings } from "../../Settings";
import { Player } from "../../../utils/Player"
import { BoardManager } from "../../Boards/BoardManager"
import * as GUI from "../../../utils/gui/gui";
import { GameStarter } from "../../GameStarter";

import * as Models from "../../../../../Shared/Shared";
import { HeroAnim } from "../../../utils/HeroAnim"
import { Wall } from "../../../utils/Wall"
import { Messages } from "../../../utils/Messages"
import { Ranking } from "../../Ranking/Ranking"
import { WebPage } from "../../../utils/WebPage"
import { ItemInfo } from "../../EQ/ItemInfo";

import { TextDatabase } from "../../../utils/TextBase"

export class TOP3Prizes {


    public group: Phaser.Group;
    private phaser: Phaser.State;


    private gifts: Phaser.Image[] = [];

    constructor(private parent: Phaser.Group, private prizes: Models.ITourneyPrize[]) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        let bg = this.phaser.add.image(0, 0, "menu/bg", 0, this.group);

        let xPositions = [0.5, 0.232   ,0.74, ];
        let yPositions = [-200, -165, -146];

        for (let i = 0; i < 3; i++) {
            let x = Game.width * xPositions[i];
            let y = Game.height + yPositions[i];
            this.gifts[i] = this.phaser.add.image(x, y, "tourney/gift", 0, this.group);
            this.gifts[i].anchor.set(0.5, 1);


            let prizesForThisPos = [];
            for (let prize of prizes)
                if (prize.rangeStart <= (i + 1) && prize.rangeEnd >=(i+1)) {
                    let item: Models.IItem;
                    item = JSON.parse(JSON.stringify(prize.prize));
                    item.bonus.typeDescription = "NAGRODA DLA MIEJSCA " + (i + 1);
                    prizesForThisPos.push(item);
                }

            this.gifts[i].inputEnabled = true;
            this.gifts[i].events.onInputDown.add(() => {
                new ItemInfo(this.group, prizesForThisPos);
            });

            this.initHeader(x, y - this.gifts[i].height - 50, prizesForThisPos);
        }



        let dais = this.phaser.add.image(Game.width * 0.5, Game.height - 60, "tourney/dais", 0, this.group);
        dais.anchor.set(0.5, 1);
        this.setVisible(false);
    }

    private initHeader(x, y, items: Models.IItem[]) {
        let style = { font: "19px CosmicTwo", fill: "white" };
        let value = 0;
        for (let item of items) 
            if (item.bonus.valuePLN)
                value += item.bonus.valuePLN;

        if (value > 0) {
            let text = this.phaser.add.text(x, y, "Wartość: " + value + "PLN", style, this.group);
            text.anchor.x = 0.5;
            text.setShadow(4, 4, 'rgba(0,0,0,0.5)', 5);
        }
    }

    public setVisible(visible: boolean) {
        this.group.visible = visible;
        $("#content").css("pointer-events", (!visible) ? "auto" : "none");
        $("#game-window").css("pointer-events", visible ? "auto" : "none");
    }
    
}

