﻿import { Game } from "../../Game"
import { FX } from "../../../game/FX"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { Settings } from "../../Settings";
import { Player } from "../../../utils/Player"
import { BoardManager } from "../../Boards/BoardManager"
import * as GUI from "../../../utils/gui/gui";
import { GameStarter } from "../../GameStarter";

import * as Models from "../../../../../Shared/Shared";
import { HeroAnim } from "../../../utils/HeroAnim"
import { Wall } from "../../../utils/Wall"
import { Messages } from "../../../utils/Messages"
import { WebPage } from "../../../utils/WebPage"
import { ItemInfo } from "../../EQ/ItemInfo";

export class PrizesList {
    private group: Phaser.Group;

    constructor(prizes: Models.ITourneyPrize[]) {
        $("#content").css("opacity", "0");
        WebPage.loadInBackground("pages/tourneys/nagrody.html", () => {
            $("#content").css("opacity", "1");
            this.setVisible(false);
            this.initList(prizes);

            let bar = document.getElementById("filterGiftsList").getBoundingClientRect();
            let table = document.getElementById("giftListContainer");
            let table_rect = table.getBoundingClientRect();
            let wall = (62 / Game.height);
            table.style.height = (window.innerHeight * (1 - wall) - bar.height) + "px";

           
  
            if (Config.Version.www)
                table.style.height = "460px";
        });

    
    }

    public setVisible(visible: boolean) {
        $("#fullPageGiftLits").css("opacity", visible ? "1" : "0");
        $("#fullPageGiftLits").css("pointer-events", visible ? "auto" : "none");
        $("#game-window").css("pointer-events", (!visible) ? "auto" : "none");
    }

    private getRowHtml(row: Models.ITourneyPrize): string {
        let value = "";
        if (row.prize.bonus.valueHCN !== undefined)
            value = row.prize.bonus.valueHCN + " HCN";
        if (row.prize.bonus.valuePLN !== undefined)
            value = row.prize.bonus.valuePLN + " PLN";

        let rangeString = `${row.rangeStart} - ${row.rangeEnd}`;
        if (row.rangeEnd == row.rangeStart)
            rangeString = row.rangeEnd.toString();
        return `
		    <tr>
			  <td>${row.prize.name}</td>
			  <td>${value}</td>
			  <td>${rangeString} </td>
			</tr>`;
    }

    private addToTable(elements: any[]) {
        $("#giftsTableBody").append(elements);

    }

    public initList(prizes: Models.ITourneyPrize[]) {
        $("#filterGiftsList").css("bottom", (62 / Game.height * 100) + "%");
        if (Config.Version.www)
            $("#filterGiftsList").css("bottom", "9.5%");

        let elements = [];

        for (let row of prizes) {
            let elem = $(this.getRowHtml(row));
            elem.on("click", () => {
                this.initItemInfo(row.prize);
            });
            elements.push(elem);
        }
        this.addToTable(elements);
        setTimeout(() => this.initGiftsJS(), 100);
    }

    private initItemInfo(item: Models.IItem) {
        $("#game-window").css("pointer-events", "auto");
        setTimeout(() => $("#game-window").css("pointer-events", "auto"), 300);
        let info = new ItemInfo(Game.group, [item]);
        info.onClose = () => $("#game-window").css("pointer-events", "none");
    }


    private initGiftsJS() {
        $(function () {
            $(function () {
                $("#giftsTable").tablesorter({

                    sortList: [[1, 1]],
                    widgets: ['filter'],
                    widgetOptions: {
                        filter_reset: '#reset'
                    }
                });
            });


            // filter button
            $('a#showGifts').on('click', function () {
                var filter = [],
                    winningPositionsColumn = 2,
                    choosenPosition = $('#choosenPosition').val();
                if (choosenPosition < 1) {
                    return;
                }
                filter[winningPositionsColumn] = choosenPosition;
                filterGifts(filter);

                $('.buttonVisible').toggle();
                $('.buttonInvisible').toggle();
            });

            //reset button
            $('a#reset').on('click', function () {
                $('input#choosenPosition').val('');
                $('.buttonVisible').toggle();
                $('.buttonInvisible').toggle();
            });

            $('input#choosenPosition').change(function () {
                if ($(this).val() && ($('#reset').css("display") == "block" || $('#reset').css("display") == "inline")) {
                    $('.buttonVisible').toggle();
                    $('.buttonInvisible').toggle();
                } else
                    return;
            });

            function filterGifts(filter) {
                $('table').trigger('search', [filter]);
            }

        });
    }
}

