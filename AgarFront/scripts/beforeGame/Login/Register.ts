﻿import {ICredentials} from "../../utils/Server"
import { Game } from "../Game"
import { Player } from "../../utils/Player"
import * as GUI from "../../utils/gui/gui"
import * as Server from "../../utils/Server"

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export class Register  {

    private disabled = false;
    constructor() {
        $("#submit_register").click(() => this.try());
    }

    private try() {
        if (this.disabled) {
            console.log("Disabled");
            return;
        }

        let nick = $("#register #nick_r").val();
        let email = $("#register #email_r").val();
        let pass = $("#register #password_r").val();
        let newsletter = $("#register #newsletter").is(':checked');
        let rules = $("#register #rules").is(':checked');

        if (!rules) {
            this.setStatus("Musisz zaakceptować regulamin!");
            return;
        }
    
        if (!nick || nick.length < 4) {
            this.setStatus("Nick powinien mieć co najmniej 4 znaki!");
            return;
        }

        if (!pass || pass.length < 2) {
            this.setStatus("Hasło nie może być puste!");
            return;
        }

        if (!email || email.length < 2) {
            this.setStatus("Email nie może być pusty!");
            return;
        }

        if (!validateEmail(email)) {
            this.setStatus("Niepoprawny email");
            return;
        }



        this.disabled = true;
        this.setStatus("Oczekiwanie na serwer...");
        Player.get().register({ email: email, password: pass, usernameFull: nick, gender: 1, newsletter: newsletter, dateOfBirth: "0000-00-00", rules: 1 },
            (token) => this.succes(token, email, pass), (error) => this.fail(error), this.onQueueStatus.bind(this));
    }

    private setStatus(txt: string) {
        $("#status_register").text(txt);
    }

    public onSucces: Function;
    private succes(token, mail, pass) {
        this.onSucces(token, mail, pass);
        this.disabled = false;
    }



    private onQueueStatus(status: Server.QueueMessage, data: any) {
        if (status == Server.QueueMessage.CHANGE_POS)
            this.setStatus("Jesteś " + data + " w kolejce...");

        if (status == Server.QueueMessage.WAIT)
            this.setStatus("Twoje zgłoszenie jest przetwarzane...");
    }

    private fail(error: Server.ServerRequestError) {


     //   if (error == Server.RegisterError.CONNECTION_TIMEOUT)
            this.setStatus("Nie można połączyć się z serwerem!");

            if (error == Server.ServerRequestError.NAME_ALREADY_USED)
            this.setStatus("Konto o takiej nazwie już istnieje");

            if (error == Server.ServerRequestError.EMAIL_ALREADY_USED)
                this.setStatus("Podany adres email jest już powiązany z innym kontem!");

            if (error == Server.ServerRequestError.WRONG_EMAIL)
                this.setStatus("Niepoprawny mail!");
            if (error == Server.ServerRequestError.WRONG_NAME)

                this.setStatus("Niepoprawna nazwa użytkownika!");

          this.disabled = false;

    }
}