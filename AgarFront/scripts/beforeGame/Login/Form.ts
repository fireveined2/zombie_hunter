﻿import {ICredentials} from "../../utils/Server"
import { Game } from "../Game"
import { Player } from "../../utils/Player"
declare var facebookConnectPlugin;

export class LoginForm  {

    public onReturnClick: Function;
    public onLogin: (credentials: ICredentials) => void;
    public onNotValidInputs: () => void;

    private email: HTMLInputElement;
    private password: HTMLInputElement;

    private gameWindow: HTMLElement;


    private loginButton: Phaser.Image;

    public disabled: boolean = false;

    private phaser: Phaser.State;
    private group: Phaser.Group;

    public create(group: Phaser.Group) {
        this.group = group;
        this.phaser = group.game.state.getCurrentState();

        this.gameWindow = document.getElementById("game-window");

        this.createEmailInput();
        this.createPasswordInput();
        this.createLoginButton();
    }

    private createEmailInput() {
        let input = document.getElementById("email") as HTMLInputElement;
        if (Player.get().savedCredentials && Player.get().savedCredentials.email)
            input.value = Player.get().savedCredentials.email;
        this.email = input;
    }

    private createPasswordInput() {
        let input = document.getElementById("password") as HTMLInputElement;
        if (Player.get().savedCredentials && Player.get().savedCredentials.password) {
            input.value = Player.get().savedCredentials.password;
        }
        this.password = input;
    }

    public formDisabledTill: number = 0;

    private createLoginButton() {
        let submit = document.getElementById("submit");
        submit.addEventListener("click", () => {
            this.onLoginClick();
        });
    }

    private emailIsValid(email: string): boolean {
        return true;
    }

    private onLoginClick() {

      //  facebookConnectPlugin.login(["public_profile"], (da) => console.log(da), (da) => console.log(da))

       // let t = true;
        ///if (t)
          //  return;

        if (this.disabled || this.formDisabledTill > Date.now())
            return;

        let password = this.password.value;
        let email = this.email.value;

        let valid = true;
        if (!password || password.length < 1)
            valid = false;

        if (!email || !this.emailIsValid(email))
            valid = false;

        if (!valid) {
            this.onNotValidInputs();
            return;
        }

        this.onLogin(<ICredentials>{ email: email, password: password });
    }

    public remove() {

   }
}