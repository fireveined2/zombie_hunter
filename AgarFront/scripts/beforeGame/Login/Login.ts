﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"

import { Status, StatusType } from "./Status"
import { LoginForm } from "./Form"
import { Register } from "./Register"

import { WebPage } from "../../utils/WebPage"

import { Wall } from "../../utils/Wall"
import { ScreenOrientation } from "../../utils/ScreenOrientation"
import { initLoginScreen } from "../../login"

import { Challenges } from "../../utils/Challenges"

import { socketClient } from "../../utils/server/Socket"
import { Challenge } from "../PvP/Challenge"

export class LoginScreen extends Phaser.State {
    private background: Phaser.Image;
    private group: Phaser.Group;

    private form: LoginForm;
    private status: Status;


    init() {

        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.background = this.add.image(Game.width / 2, Game.height / 2, 'login/bg', 0, this.group);
        this.background.anchor.set(0.5)



        this.status = new Status();

        this.form = new LoginForm();

        let wall = new Wall(this, this.group, null);
        wall.setMenuButton();
        wall.onButtonClick = () => this.returnToMainMenu(false);

        WebPage.load("login/login.html", this.onLoaded.bind(this));

    }

    private onLoaded(element: HTMLElement) {
        $("#rules_button").bind("click", this.openRules);
        $("#rules_button").css("text-decoration", "underline")

        this.form.onReturnClick = () => this.returnToMainMenu();
        this.form.onNotValidInputs = () => this.status.set(StatusType.WRONG_CREDENTIALS);
        this.form.onLogin = this.try.bind(this);
        this.form.create(this.group);
        initLoginScreen();
        let reg = new Register();
        reg.onSucces = (token, mail, pass) => {
            Player.get().saveCredentials(mail, pass);
            this.onSucces(token);
        }

        document.getElementById("return").addEventListener("click", () => {
            this.returnToMainMenu(false);
        });
    }

    private try(credentials: Server.ICredentials) {
        this.form.disabled = true;
        this.status.set(StatusType.WAITING_FOR_RESPONSE);
        Player.get().login(credentials, (token) => {
            Player.get().saveCredentials(credentials.email, credentials.password);
            this.onSucces(token);
        }, this.onError.bind(this), this.onQueueStatus.bind(this));
    }

    private onQueueStatus(status: Server.QueueMessage, data: any) {
        if (status == Server.QueueMessage.CHANGE_POS)
            this.status.setText("Jesteś " + data + " w kolejce...");

        if (status == Server.QueueMessage.WAIT)
            this.status.setText("Twoje zgłoszenie jest przetwarzane...");
    }

    private onSucces(token: string) {


        this.status.set(StatusType.DOWNLOADING_PLAYER_DATA);
        Player.get().downloadPlayerData(() => this.returnToMainMenu(true), this.onDownloadDataError.bind(this));

        socketClient.pvp.challenge.on = (player) => {
            new Challenge().onChallenge(player);
        }

    }


    private onError(error: Server.ServerRequestError) {
        if (error == Server.ServerRequestError.SERVER_TIMEOUT)
            this.status.set(StatusType.SERVER_TIMEOUT);
        else
            if (error == Server.ServerRequestError.GAME_SERVER_TIMEOUT)
                this.status.setText("Nie można połączyć się z serwerem gry! Spróbuj ponownie za 5 sekund");
            else
                if (error == Server.ServerRequestError.BAD_PASSWORD)
                    this.status.setText("Błędne hasło! Spróbuj ponownie za 5 sekund");
                else
                    if (error == Server.ServerRequestError.NAME_TOO_LONG)
                        this.status.setText("Za długa nazwa użytkownika! Spróbuj ponownie za 5 sekund");
        if (error == Server.ServerRequestError.USER_DOES_NOT_EXIST)
            this.status.setText("Nie ma takiego użytkownika! Spróbuj ponownie za 5 sekund");
        else
            this.status.set(StatusType.WRONG_CREDENTIALS);

        this.form.disabled = false;
        this.form.formDisabledTill = Date.now() + 5 * 1000;
    }

    private onDownloadDataError(error: Server.RequestError) {
        if (error == Server.RequestError.CONNECTION_TIMEOUT)
            this.status.set(StatusType.SERVER_TIMEOUT);
        else
            this.status.set(StatusType.CANT_DOWNLOAD_DATA);

        this.form.disabled = false;
    }

    private openRules() {
        WebPage.openInSystemBrowser("https://hophands.pl/regulamin");
    }
    private remove() {
        this.form.remove();
        this.status.remove();
        $("#rules_button").unbind("click", this.openRules);
    }

    private returnToMainMenu(logged: boolean = false) {
        //ScreenOrientation.setLandscape();
        WebPage.unload();
        this.remove();
        if (Challenges.tourneys.length > 0)
            FX.fade(this.game, "out", 800, () => this.game.state.start('Tourneys', true, false, Challenges.tourneys[0]));
        else
            this.game.state.start('MainMenu', true, false, false);
    }


}