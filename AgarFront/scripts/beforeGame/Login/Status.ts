﻿

export enum StatusType {
    NONE,
    WAITING_FOR_RESPONSE,
    DOWNLOADING_PLAYER_DATA,
    SERVER_TIMEOUT,
    WRONG_CREDENTIALS,
    CANT_DOWNLOAD_DATA
}

export class Status {

    private current: StatusType;
    private status: HTMLSpanElement;
    private spinner: Phaser.Image;
    private gameWindow: HTMLElement;

    constructor() {
        this.current = StatusType.NONE;

        this.gameWindow = document.getElementById("game-window");


        this.status = document.getElementById("status");
    }


    private setSpanText(txt: string) {
        if (document.getElementsByTagName("footer")[0])
        document.getElementsByTagName("footer")[0].style.textAlign = "center";
        document.getElementById("status").innerHTML = txt;
    }

    public remove() {
  
}

    public setText(txt) {
        this.setSpanText(txt);
    }

    public set(type: StatusType) {
        if (type == StatusType.WAITING_FOR_RESPONSE) {
            this.setSpanText("Łączenie z serwerem...");
        }

        if (type == StatusType.DOWNLOADING_PLAYER_DATA) {
            this.setSpanText("Połączono, pobieranie danych gracza");
        }

        if (type == StatusType.SERVER_TIMEOUT) {
            this.setSpanText("Serwer nie odpowiada, spróbuj ponownie za 5 sekund");
        }



        if (type == StatusType.WRONG_CREDENTIALS) {
            this.setSpanText("Błędny login lub hasło. Zwróc uwagę na wielkość liter i spróbuj ponownie za 5 sekund!");
        }

        if (type == StatusType.CANT_DOWNLOAD_DATA) {
            this.setSpanText("Nie można pobrać danych użytkownika, spróbuj ponownie!");
        }

        this.current = type;
    }
}