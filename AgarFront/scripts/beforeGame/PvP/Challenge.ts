﻿
import {Game} from "../Game"
import * as Models from "../../../../Shared/Shared";
import { ItemsHelpers } from "../../../../Shared/Items";
import * as GUI from "../../utils/gui/gui"
import { SFX } from "../../SFX/SFX"
import { FX } from "../../game/FX"
import { socketClient } from "../../utils/server/Socket"

export class Challenge {

    private phaser: Phaser.State;
    private overlay: Phaser.Graphics | Phaser.Image;

    private player: Models.PlayerInfo;
    private onClose: Function;

    constructor() {
      

    }

    private remove() {
        this.overlay.destroy();
    }


    private openLobby(data) {
        FX.fade(this.phaser.game, "out", 800, () => this.phaser.game.state.start('PvPLobby', true, false, this.player, data));
    }

    public onChallenge(player: Models.PlayerInfo) {


        let oldPointer = $("#game-window").css("pointer-events");
        $("#game-window").css("pointer-events", "auto");
        setTimeout(() => $("#game-window").css("pointer-events", "auto"), 300);

        this.player = player;
        this.phaser = Game.group.game.state.getCurrentState();
        SFX.get().playSingleEffect("buttons/click");

        this.overlay = this.phaser.add.image(0, -20, "gui/info_popup_bg", 0, Game.group);
        this.overlay.inputEnabled = true;


        let name = new GUI.Label(player.username, Game.width * 0.5, Game.height * 0.125, Game.group);
        name.anchor.x = 0.5;
        this.overlay.addChild(name.element);

        let style = <Phaser.PhaserTextStyle>{ font: "21px CosmicTwo", fill: "black" };
        let level = this.phaser.add.text(Game.width * 0.5, Game.height * 0.276, player.respect +" respektu ", style);
        level.anchor.x = 0.5;
        this.overlay.addChild(level);

        let frame = this.phaser.add.image(Game.width * 0.5, Game.height * 0.37 + 65, "eq/elements", "frame");
        frame.anchor.set(0.5);
        this.overlay.addChild(frame);

        let icon = this.phaser.add.image(Game.width * 0.5, Game.height * 0.37 + 65, "items/" + player.wearedItemsIds[0])
        icon.anchor.set(0.5)
        this.overlay.addChild(icon);

        style = <Phaser.PhaserTextStyle>{ font: "24px CosmicTwo", fill: "black" }
        let challenge = this.phaser.add.text(Game.width * 0.5, Game.height * 0.7, "RZUCIŁ CI WYZWANIE", style);
        challenge.anchor.x = 0.5;
        this.overlay.addChild(challenge);


        let accept = new GUI.Label("AKCEPTUJ", Game.width * 0.2, Game.height * 0.85-20, Game.group, 31);
        accept.element.inputEnabled = true;
        accept.element.events.onInputDown.addOnce(() => this.accept());
        this.overlay.addChild(accept.element);

        let decline = new GUI.Label("ODRZUĆ", Game.width * 0.55, Game.height * 0.85-20, Game.group, 31);
        decline.element.inputEnabled = true;
        decline.element.events.onInputDown.add(() => this.decline());
        this.overlay.addChild(decline.element);

        this.onClose = () => {
            $("#game-window").css("pointer-events", oldPointer);
        }
        socketClient.pvp.enemy.name = player.username;
        socketClient.pvp.challenge.onCancel(() => this.onCancel());
    }


    private accept() {
        socketClient.pvp.challenge.accept();
        this.remove();
        socketClient.pvp.onInitData((data) => this.openLobby(data));
    }


    private decline() {
        socketClient.pvp.challenge.decline();
        this.remove();
        if (this.onClose)
            this.onClose();
    }

    private onCancel() {
        this.remove();
        new GUI.Popup(this.player.username + " anulował wyzwanie").onClose = this.onClose;
    }



    public makeChallenge( player: Models.PlayerInfo) {
        this.player = player;
        this.phaser = Game.group.game.state.getCurrentState();
        let overlay = this.overlay = FX.createTextOverlay(Game.group.game, "Oczekiwanie na " + player.username + "...");
        overlay.events.onInputDown.addOnce(() => {
            this.cancel();
        });

        let style = { font: "24px CosmicTwo", fill: "white" };
        let text = this.phaser.add.text(Game.width / 2, 10, "Kliknij, aby anulować wyzwanie", style);
        overlay.addChild(text);
        text.anchor.x = 0.5;
        text.setShadow(3, 3, 0, 3, true);
        socketClient.pvp.enemy.name = player.username;
        socketClient.pvp.challenge.make(this.player.id, this.onAccepted.bind(this), this.onDeclined.bind(this));
    }
    private onAccepted() {
        this.remove();
        socketClient.pvp.onInitData((data) => this.openLobby(data));

    }


    private onDeclined() {
        this.remove();
        new GUI.Popup(this.player.username +" odrzucił Twoje wyzwanie!");

    }

  

    private cancel() {
        socketClient.pvp.challenge.cancel();
        this.remove();
    }

   
}