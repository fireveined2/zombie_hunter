﻿import { Game } from "../../Game"
import { FX } from "../../../game/FX"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { Settings } from "../../Settings";
import { Player } from "../../../utils/Player"
import * as GUI from "../../../utils/gui/gui";
import { GameStarter } from "../../GameStarter";

import * as Models from "../../../../../Shared/Shared";
import { HeroAnim } from "../../../utils/HeroAnim"
import { Wall } from "../../../utils/Wall"
import { Messages } from "../../../utils/Messages"
import { Ranking } from "../../Ranking/Ranking"
import { WebPage } from "../../../utils/WebPage"
import * as SocketModels from "../../../../../Shared/SocketModels";


export class PvPLobby extends Phaser.State {

 
    private group: Phaser.Group;
    private rank: Phaser.Group;
    private wall: Wall;

    private myHero: HeroAnim;
    private enemyHero: HeroAnim;

    private me: Models.PlayerInfo;
    private enemy: Models.PlayerInfo;
    private data: SocketModels.IPvPInitData;

    public init(enemy: Models.PlayerInfo, data: SocketModels.IPvPInitData) {
        this.enemy = enemy;
        this.data = data;

    }
    public create() {


       

        this.group = this.game.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.add.sprite(Game.width / 2, Game.height / 2, 'pvp/lobby_bg', 0, this.group).anchor.set(0.5);



        this.wall = new Wall(this, this.group, Player.get().data, { empty: true });
        //this.wall.hideHCN();
      //  this.wall.setMenuButton();
        //this.wall.onButtonClick = () => this.returnToMainMenu();

        this.myHero = new HeroAnim(this, this.group);
        this.myHero.setItems(Player.get().getAllWearedItems());
        let x = 200;
        let y = Game.height - 45 * Game.assetsSize - this.myHero.height+98;
        this.myHero.position.set(x, y);
        this.myHero.scale.set(0.6);
        this.makePlayerInfo(200, 40, Player.get().data.username, Player.get().data.level, Player.get().data.respect);

        this.enemyHero = new HeroAnim(this, this.group);
        let items
        this.enemyHero.setItemsFromSetIds(this.enemy.wearedItemsIds);
        x = 600 ;
        y = Game.height - 45 * Game.assetsSize - this.myHero.height+98;
        this.enemyHero.position.set(x, y);
        this.enemyHero.scale.set(0.6);
        this.makePlayerInfo(600, 40, this.enemy.username, this.enemy.level, this.enemy.respect);

        this.startCounting(this.data.lobbyCountingDuration, () => this.startPvP());
    }

    private makePlayerInfo(x: number, y: number, name: string, level: number, respect: number) {
        let style = <Phaser.PhaserTextStyle>{ font: "20px CosmicTwo", fill: "white", align: "center" };
        let text = this.add.text(x, y, name + "\n" + level + " poziom\n" + respect+" respektu", style, this.group);
        text.anchor.x = 0.5;
        text.setShadow(4, 4, 'rgba(0,0,0,0.5)', 5);
    }

    private startCounting(duration: number, callback: Function) {
        let style = <Phaser.PhaserTextStyle>{ font: "21px CosmicTwo", fill: "white", align: "center" };
        let text = this.add.text(400, Game.height-30, " ", style, this.group);
        text.anchor.x = 0.5;
        text.setShadow(4, 4, 'rgba(0,0,0,0.5)', 5);

        let count = () => {
            text.setText("Start za  " + duration + "...");
            duration--;
            if (duration > 0)
                setTimeout(count, 1000);
            else
                callback();
        }
        count();
    }


    private startPvP() {
        new GameStarter(this.group).startPvP({ pvpId: this.data.id });
    }

    private prevTime;
    public update() {
        let now = Date.now();
        let elapsed = now - this.prevTime;
        this.prevTime = now;

        FX.get().update(elapsed / Config.Global.frameTime);
        this.myHero.updateAnim();
        this.enemyHero.updateAnim();
    }

    public shutdown() {
        this.myHero.kill();
        this.enemyHero.kill();
    }
    private returnToMainMenu() {
        FX.fade(this.game, "out", 800, () => this.game.state.start('MainMenu'));
    }
}

