﻿import {Game} from "../.././Game"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { GameStats } from "../../../game/logic/GameStats"
import { Challenges } from "../../../utils/Challenges"
import {ResultsTab, AnimableElement} from "../ResultsWindow";
import * as GUI from "../../../utils/gui/gui";
import * as Models from "../../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../../Shared/Challenges";

export class GameStatsTab implements ResultsTab{

    public alreadyOpened: boolean;
    public name: string;
    private phaser: Phaser.State;
    private group: Phaser.Group;


    private stats: GameStats;
    private list: GUI.CriteriaList;

    private respect: Phaser.Text;
    private result: Phaser.Text;
    private reward: Phaser.Text;
    private results: Models.GameResults;
    private challenge: Models.IChallengeData;
    private criteria: Models.ChallengeCriteria[];
    private offline: boolean;




    constructor(challenge: number, stats: GameStats, offline?: boolean, private isChallengeTraining?: boolean) {
        this.stats = stats;
        this.name = "Ostatnia gra";
        this.alreadyOpened = false;
        this.offline = offline;

        if (challenge) {
            this.challenge = Challenges.get(challenge);
            if (this.challenge.data.type == Models.ChallengeType.KING_OF_THE_HILL || this.challenge.data.type == Models.ChallengeType.TOURNEY) {
                this.stats = JSON.parse(JSON.stringify(stats));
                this.criteria = [];

                if (!isChallengeTraining) {
                    this.criteria.push(<Models.ChallengeCriteria>{ stat: "cutFlesh" });
                    this.criteria.push(<Models.ChallengeCriteria>{ stat: "cur_score" });
                }
                else
                this.criteria.push(<Models.ChallengeCriteria>{ stat: "score" });

            }
            else {
                if (this.challenge.data.type == Models.ChallengeType.SIMPLE)
                    this.stats = JSON.parse(JSON.stringify(stats));
                else
                    this.stats = <GameStats>ChallengeUtils.addStats(JSON.parse(JSON.stringify(stats)), this.challenge.prevStats);
                this.criteria = this.challenge.data.criteria;
            }
        }
        else {
            this.criteria = [];
            this.criteria.push(<Models.ChallengeCriteria>{ stat: "highscore" });
            this.criteria.push(<Models.ChallengeCriteria>{ stat: "cur_score" });
            this.criteria.push(<Models.ChallengeCriteria>{ stat: "cutFlesh" });
            this.criteria.push(<Models.ChallengeCriteria>{ stat: "biggestCut" });
        }



    }



    public init(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        this.list = new GUI.CriteriaList(this.group, Game.width * 0.29, 15, null, null, true);
        this.list.visible = false;
        if (this.isChallengeTraining)
            this.list.addSimple("Gra", "treningowa");
        this.list.create(this.criteria, this.stats);

        let style = { font: "29px CosmicTwo", fill: "white" };
        this.result = this.phaser.add.text(Game.width *0.29, Game.height * 0.43, "", style, this.group);
        this.result.anchor.set(0.5);

        style = { font: "22px CosmicTwo", fill: "white" };
        this.respect = this.phaser.add.text(Game.width * 0.29, Game.height * 0.52, "", style, this.group);
        this.respect.anchor.set(0.5);

 
        this.reward = this.phaser.add.text(Game.width * 0.29, Game.height * 0.60, "", style, this.group);
        this.reward.anchor.set(0.5);

  
    }


    public setResult(results: Models.GameResults) {
        this.results = results;
        if (this.alreadyOpened)
            this.showResults();
    }

    private cantGetResults() {
        this.result.setText("Nie można pobrać rezultatu!");
    }

    private showResults(tries: number = 0) {
        if (this.offline) {
            this.showOfflineResults();
            return;
        }

        if (!this.results) {
                if (tries == 6) {
                    this.cantGetResults();
                    return;
                }
                this.result.setText("Obliczanie rezultatu...");
                setTimeout(() => this.showResults(tries+1), 1000);
                return;
            }

        let koh = this.results.overall.kohResults
        if (koh) {

            if (koh.best)
             

            if (this.isChallengeTraining)
                this.list.addSimple("Zdobyłbyś", koh.place + " miejsce");
            else {
                if (koh.best)
                this.list.addSimple("Najlepszy wynik", koh.best);
                this.list.addSimple("Twoja pozycja", koh.place + " miejsce");
            }

            if (koh.pointsToNextPlace)
            this.list.addSimple("Do miejsca wyżej brakuje", koh.pointsToNextPlace + " pkt");
        }


        if (this.results.overall.stars > 0)
            this.showStarResult(this.results.overall.stars);
        else
            this.showTextResult(this.results.overall.challengeResult);

        this.showRespectAndExp(this.results.overall.respect, this.results.overall.exp);
        this.showReward(this.results.prizes);

        if (!this.results.prizes || this.results.prizes.length == 0) {
            this.respect.y += 20;
            this.result.y += 20;
        }
    }

    private showReward(rewards: Models.IPrize[]) {
        let num = rewards ? rewards.length : 0;
        if(num>0)
        this.reward.setText("Zdobyłeś nagrodę!");
    }

    private showOfflineResults() {
        this.respect.setText("");
    }

    private showRespectAndExp(respect: number, exp: number) {
       if (respect > 0)
            this.respect.setText("+" + respect + " respektu   +"+exp+" doświadczenia");
        else
           this.respect.setText("" + respect + " respektu   +" + exp + " doświadczenia");
    }

    private showTextResult(result: Models.ChallengeResult) {
        switch (result) {
            case Models.ChallengeResult.FAILURE:
                this.result.setText("Porażka");
                break

            case Models.ChallengeResult.SUCCES:
                this.result.setText("Sukces!");
                break;

            case Models.ChallengeResult.PLAY_MORE:
                this.result.setText("Próbuj dalej!");
        }
    }

    private showStarResult(stars: number) {
        this.result.setText("ZYSKAŁEŚ:");
    }

    public show(show: boolean) {
        this.group.visible = show;
    }

    public anim(next?: AnimableElement[], onEnd?: Function) {
        this.show(true);
        let end = () => {
            if (onEnd)
                onEnd();
            this.alreadyOpened = true;

            this.showResults();
        }
        setTimeout(()=>this.list.anim(next, end), 1000);
    }

}