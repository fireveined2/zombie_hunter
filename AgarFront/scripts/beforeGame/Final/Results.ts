﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as GUI from "../../utils/gui/gui"
import { Challenges } from "../../utils/Challenges"
import { ResultsWindow } from "./ResultsWindow";
import { GameStatsTab } from "./Stats/Stats";
import { RewardsTab } from "./Rewards/Rewards";
import * as Server from "../../utils/Server"
import * as Models from "../../../../Shared/Shared";
import { Donate } from "../../utils/Donate"
export class Results {

    private phaser: Phaser.State;
    private group: Phaser.Group;

    private window: ResultsWindow;
    private stats: GameStatsTab;
    private rewards: RewardsTab;
    public onClose: Function;
    public onMoreChallenges: Function;

    constructor(parent: Phaser.Group, private acces: Models.AccesToPlay) {
        this.group = parent;

        if (!acces.challengeId || acces.challengeId < 1)
            this.updateHighscore(acces.config.map, Game.stats.score);



        this.stats = new GameStatsTab(acces.challengeId, Game.stats, acces.offline, Player.get().lastGameRequest.isChallengeTraining);
        this.rewards = new RewardsTab();

        this.window = new ResultsWindow(this.group, [this.stats, this.rewards]);
        this.window.onLeftButtonClick = this._onClose.bind(this);

        if (acces.challengeId) {
            console.log(acces.challengeId);
            let quest = Challenges.get(acces.challengeId).data.quest;
            this.window.onRightButtonClick = () => this._onMoreChallenges(quest);
        }

        if (Player.get().logged)
            this.sendResults();
        else {
            SFX.get().playSlonPlayAgain();

        }

        if (acces.offline)
            this.window.showMiddleMenuButton();
    }

    private updateHighscore(map: number, score: number) {
        let current = Player.get().records.get(map);
        Game.stats["highscore"] = current;
        if (score > current) {
            Player.get().records.set(map, score);
            Game.stats["highscore"] = score;
        }
    }

    private _onClose() {
        if (this.onClose)
            this.onClose();
    }


    private _onMoreChallenges(quest: number) {
        if (this.onMoreChallenges)
            this.onMoreChallenges(quest);
    }


    private sendResults() {
        if (!this.acces.offline)
            Player.get().sendGameData(Game.stats, this.onResultsLoaded.bind(this), this.onError.bind(this));

    }

    private deleteChallengeIfFinished(results: Models.ChallengeResult, challengeId: number) {
        if (results == Models.ChallengeResult.SUCCES)
            Challenges.remove(challengeId);
    }

    private onResultsLoaded(results: Models.GameResults) {
        this.rewards.update(results.prizes);
        this.stats.setResult(results);

        if (this.acces.challengeId)
            this.deleteChallengeIfFinished(results.overall.challengeResult, this.acces.challengeId);

        if (!results.overall.stars) {
            if (results.overall.challengeResult === Models.ChallengeResult.SUCCES)
                this.window.showWinButtons();
            else
                this.window.showLostButtons();
        }
        else 
            this.window.showMiddleMenuButton();

        if (results.prizes[0] && !this.isRewardAntidotum(results.prizes[0]))
            SFX.get().playSlon("slon/rewards/new_item");
        else
            this.playResultSFX(results);

        if (results.prizes && results.prizes.length > 0)
            this.window.showRewardTab(results.prizes[0]);

        console.log(results);
        if (results.overall.canMakeDonate)
        new Donate(this.phaser, this.group);
    }

    private playResultSFX(results: Models.GameResults) {
        if (results.overall.stars) {
            let record = Player.get().records.get(this.acces.config.map);
            if (record == Game.stats.score)
                SFX.get().playSlonNewRecord();
            else
                if (Game.stats.score < record * 0.5)
                    SFX.get().playSlonFuckup();
                else
                    SFX.get().playSlonPlayAgain()

        }

        if (results.overall.challengeResult === Models.ChallengeResult.FAILURE)
            SFX.get().playSlonFuckup();
        if (results.overall.challengeResult === Models.ChallengeResult.SUCCES)
            SFX.get().playSlonNewRecord();
        if (results.overall.challengeResult === Models.ChallengeResult.PLAY_MORE)
            SFX.get().playSlonPlayAgain()
    }

    private isRewardAntidotum(reward: Models.IPrize) {
        return reward.id == 11 || reward.id == 12 || reward.id == 13;
    }

    private onError(error: Server.RequestError) {
        this.rewards.timeout();
        this.window.showMiddleMenuButton();
    }

}