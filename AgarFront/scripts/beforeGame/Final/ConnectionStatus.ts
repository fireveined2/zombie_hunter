﻿import {Game} from ".././Game"
import { SFX, SFXMenu } from "../../SFX/SFX"

export enum StatusType {
    NO_CONNECTION,
    WAITING_FOR_UPDATE,
    UPDATED
}

export class Status {
    private status: Phaser.Text;
    private phaser: Phaser.State;

    constructor(private group: Phaser.Group, state?: StatusType) {
        this.phaser = group.game.state.getCurrentState();
        if (state !== undefined)
        this.set(state);
    }

    public remove() {
        if (!this.status)
            return;

        this.status.destroy();
        delete this.status;
    }

    public set(state: StatusType) {
        this.remove();
        if (state == StatusType.NO_CONNECTION)
            this.status = this.phaser.add.text(120, 120, "Brak połączenia :(", {}, this.group);

        if (state == StatusType.WAITING_FOR_UPDATE)
            this.status = this.phaser.add.text(120, 120, "Oczekiwanie na serwer...", {}, this.group);
    }

}
