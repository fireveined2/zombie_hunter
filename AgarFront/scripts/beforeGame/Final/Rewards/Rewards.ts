﻿import {Game} from "../.././Game"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { GameStats} from "../../../game/logic/GameStats"
import {ResultsTab, AnimableElement} from "../ResultsWindow";
import {Reward} from "./Reward";
import * as Models from "../../../../../Shared/Shared";
import * as Connection from "../ConnectionStatus";

export class RewardsTab implements ResultsTab {
    public alreadyOpened: boolean;
    public name: string;
    private phaser: Phaser.State;
    private group: Phaser.Group;
    private wait;


    private rewards: Reward[] = [];
    private connectionStatus: Connection.Status;

    constructor() {
        this.name = "Nagrody";
        this.alreadyOpened = true;
    }


    public addRow(item: Models.IItem) {
        let row = new Reward(this.group, 40 + 50 * this.rewards.length, item);
        this.rewards.push(row);
    }

    public init(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        this.connectionStatus = new Connection.Status(this.group);
        this.connectionStatus.set(Connection.StatusType.WAITING_FOR_UPDATE);
    }


    public show(show: boolean) {
        this.group.visible = show;
    }



    public update(prizes: Models.IPrize[]) {
        this.connectionStatus.set(Connection.StatusType.UPDATED);

        if (!prizes)
            return;
        for (let prize of prizes)
            this.addRow(prize);
    }

    public timeout() {
        this.connectionStatus.set(Connection.StatusType.NO_CONNECTION);
    }

    public anim() {
        this.show(true);
        this.alreadyOpened = true;
    }

}
