﻿import {Game} from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"

import { GameEvents } from "../../game/logic/GameEvents"

import * as Models from "../../../../Shared/Shared";
import {Results} from "./Results";

import { Validate }  from "../../../../Veryficator/veryficator";

import { HeroAnim } from "../../utils/HeroAnim"
import { GameStarter } from ".././GameStarter"
import { Challenges } from "../../utils/Challenges"
import * as GUI from "../../utils/gui/gui"

export class FinalScore extends Phaser.State {
    private background: Phaser.Image;
    private overlay: Phaser.Graphics;
    private scoreText: Phaser.BitmapText;


    private countingDuration: number = 2300;
    private goToLobbyTimeout: any;
    private group: Phaser.Group;
    


    private defaultBestScore = 2000;
    private playerBestScore = 0;

    static reminderTimeout: any;
    private heroAnim: HeroAnim;

    private results: Results;



    init(score: number, timeLeft: number, acces: Models.AccesToPlay) {
        GameStarter.removeAssetsForMap(this, acces.config.map);
     //   Game.inputCollector.getCompressed();
  


        console.log(acces);
        this.overlay = undefined;
        SFX.setSFX(new SFXMenu(this.game));

        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.background = this.add.image(Game.width, Game.height*0.3, 'background_menu', 0, this.group);
        this.background.anchor.x = 1;
        this.background.anchor.y = 0.3;
        let wall = this.add.image(0, Game.height, "wall", 0, this.group);
        wall.anchor.y = 1;

        this.heroAnim = new HeroAnim(this, this.group);
        this.heroAnim.setItems(Player.get().getAllWearedItems(), true);
        let x = Game.width*0.85 * Game.assetsSize;
        let y = Game.height - 12 * Game.assetsSize - this.heroAnim.height;
        this.heroAnim.position.set(x, y);

       

     //   Player.get().showScoreLabel(this.group);

        FX.get().rain(this.group, 13);

        let overlay = FX.fade(this.game, "in", 1200, null, 800);
        this.update();

        Game.absence.markAsActive();
        let remindCount = 0;
        let remind = () => {
            if (this.game.state.current == "FinalScore" && Game.absence.timeSinceLastActivity() > 20000 + (remindCount * remindCount) * 10000) {
                SFX.get().playSlonPlay();
                clearInterval(FinalScore.reminderTimeout);
                Game.absence.markAsActive();
                FinalScore.reminderTimeout = setInterval(() => remind(), 5 * 1000);
                remindCount++;
            }
        }

        clearInterval(FinalScore.reminderTimeout);
        FinalScore.reminderTimeout = setInterval(() => remind(), 5 * 1000);

        this.manageRecord(score, timeLeft > 5);

        this.results = new Results(this.group, acces);
        if (!acces.challengeId)
            this.results.onClose = () => this.goToMapScreen();
        else 
            this.results.onClose = this.goToMenu.bind(this);


        if (acces.challengeId) {
            let data = Challenges.get(acces.challengeId);
            if (data.data.type == Models.ChallengeType.TOURNEY)
                this.results.onMoreChallenges = this.goToTourneys.bind(this);
            else
                this.results.onMoreChallenges = this.goToChallenges.bind(this);


        }
    }





    private manageRecord(score: number, surrender: boolean) {
        let current = Player.get().getRecord();
    //    setTimeout(() => this.playAfterGameSfx(surrender?0:score, current), 1200);

        if (score > current)
                Player.get().setRecord(score);
    }

    private playAfterGameSfx(score: number, record: number) {
        if (!record) {
            if (score > 2000)
                SFX.get().playSlonNewRecord();
            else if (score < 800)
                SFX.get().playSlonFuckup();
            else SFX.get().playSlonPlayAgain();

        }
        else {
            if (score > record)
                SFX.get().playSlonNewRecord();
            else if (score < record * 0.5)
                SFX.get().playSlonFuckup();
            else SFX.get().playSlonPlayAgain();
        }
    }

  

    private updateHandler;
    private lastTime = Date.now();
    public update() {
        let now = Date.now();
        let elapsed = now - this.lastTime;
        this.lastTime = now;
        FX.get().update(elapsed / Config.Global.frameTime);

        this.heroAnim.updateAnim();
    }


    public shutdown() {
        this.heroAnim.kill();
    }

    private goToMenu(socket: any) {
            if (this.overlay)
            return;
            this.overlay = FX.fade(this.game, "out", 600, () => {
            clearTimeout(this.updateHandler);
            this.game.state.start('MainMenu', true, false);

            setTimeout(() => {
                if (Math.random() > 0.6)
                    GUI.Message.tournamentInfo();
                else
                    GUI.Message.showRandomInfoMessage();
            }, 800);

            }, 800);
    }

    private goToTourneys() {
        if (this.overlay)
            return;

        if (GameStarter.starting)
            return;
        let training = Player.get().lastGameRequest.isChallengeTraining;
        let tourney = Challenges.tourneys[0]
        let cost = tourney.data.cost;
        if (training)
            cost = tourney.data.tourneyData.nextTrainingCost;

        let hcn = Player.get().data.hcn;
        if (cost > hcn && (training || Player.get().data.freeTourneyEntrances < 1)) {
            new GUI.Popup("Potrzebujesz więcej HCN! Doładuj konto, aby wziąć udział w turnieju");
            return;
        }

        let starter = new GameStarter(this.group);
        starter.startChallenge({ challengeId: tourney.data.id, isChallengeTraining: training });
        return;
         /*
        this.overlay = FX.fade(this.game, "out", 600, () => {
            clearTimeout(this.updateHandler);
            this.game.state.start('Tourneys', true, false, Challenges.tourneys[0]);
           setTimeout(() => {
                if (Math.random() > 0.6)
                    GUI.Message.tournamentInfo();
                else
                    GUI.Message.showRandomInfoMessage();
            }, 800);
          
 

    

        }, 800);
          */
    }

    private goToChallenges(quest: number) {
        if (this.overlay)
            return;
        this.overlay = FX.fade(this.game, "out", 600, () => {
            clearTimeout(this.updateHandler);

            setTimeout(() => {
                if (Math.random() > 0.6)
                    GUI.Message.tournamentInfo();
                else
                    GUI.Message.showRandomInfoMessage();
            }, 800);

            if (quest==-1)
                this.game.state.start('ActiveEvents', true, false);
            else
                this.game.state.start('MapScreen', true, false, quest);

        }, 800);
    }


    private goToMapScreen() {
        if (this.overlay)
            return;
        this.overlay = FX.fade(this.game, "out", 600, () => {
            clearTimeout(this.updateHandler);
            this.game.state.start('MapScreen', true, false);
            setTimeout(() => {
                if (Math.random() > 0.6)
                    GUI.Message.tournamentInfo();
                else
                 GUI.Message.showRandomInfoMessage();
            }, 800);

        }, 800);

 
    }
}