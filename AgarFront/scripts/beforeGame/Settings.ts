﻿import { Game } from "./Game"
import { FX } from "../game/FX"
import { SFX } from "../SFX/SFX"
import { Config } from "../utils/Config";
import { SoundAdjust } from "../../../LobbyLogic/clientSide/SoundAdjust";

export interface GameSettings {
    side: "right" | "left";
    effectLevel: number;
    under18: boolean;
}

export class Settings {

    private screen: Phaser.Image;

    private sideTick: Phaser.Image;
    private plus18Tick: Phaser.Image;

    private effectCrosses: Phaser.Image[] = [];

    private selectedSide: "right" | "left";// = Game.settings.side;
    private selectedEffectLevel: number; //= Game.settings.effectLevel;
    private plus18Allowed: boolean; //= Game.settings.effectLevel;

    private save: Phaser.Button;
    
    private click: Phaser.Sound;

    constructor(private phaser: Phaser.Game, group: Phaser.Group) {
         this.selectedSide= Game.settings.side;
         this.selectedEffectLevel = Game.settings.effectLevel;
         this.plus18Allowed = !Game.settings.under18;

         this.screen = phaser.add.image(Game.width*0.353, 0, "settings", 0, group);
         this.screen.scale.set(Game.height / 600);
         this.screen.x = Game.width * 0.5 - this.screen.width / 2;
         this.screen.y = Game.height / 2 - this.screen.height / 2;

         this.click = SFX.get().getSound("buttons/click");

        let sideButton1 = phaser.add.button(442, 205, "settings_button", () => this.putSideTick("right"));
        sideButton1.anchor.set(0.5);
        sideButton1.onDownSound = this.click;
        this.screen.addChild(sideButton1);

        let sideButton2 = phaser.add.button(442, 253, "settings_button", () => this.putSideTick("left"));
        sideButton2.anchor.set(0.5);
        sideButton2.onDownSound = this.click;
        this.screen.addChild(sideButton2);



        this.sideTick = phaser.add.image(0, 0, "yes");
        this.sideTick.anchor.set(0.5);
        this.sideTick.scale.set(0.84);
        this.screen.addChild(this.sideTick);
        this.putSideTick(this.selectedSide);

        let plus18 = phaser.add.button(442, 333, "settings_button", () => this.allow18plus(!this.plus18Allowed));
        plus18.anchor.set(0.5);
        plus18.onDownSound = this.click;

        this.screen.addChild(plus18);

        this.plus18Tick = phaser.add.image(442, 333, "yes");
        this.plus18Tick.anchor.set(0.5);
        this.plus18Tick.scale.set(0.84);
        this.screen.addChild(this.plus18Tick);
        this.allow18plus(this.plus18Allowed);
       

        let xx = [84, 280, 458];
        for (let i = 0; i < 3; i++) {
            this.effectCrosses[i] = phaser.add.image(xx[i], 110, "no");
            this.effectCrosses[i].anchor.set(0.5);
            this.effectCrosses[i].scale.set(0.65);
            this.effectCrosses[i].visible = false;
            this.screen.addChild(this.effectCrosses[i]);
        }
        this.selectEffectLevel(this.selectedEffectLevel);


        this.save = phaser.add.button(this.screen.centerX, 450, "save");
        this.save.anchor.set(0.5);
        this.save.onDownSound = this.click;
        this.screen.addChild(this.save);
    }

    private allow18plus(allowed: boolean) {
        this.plus18Allowed = allowed;
        if (allowed)
            this.plus18Tick.loadTexture("yes");
        else
            this.plus18Tick.loadTexture("no");
    }

    private putSideTick(side: "right" | "left") {
        this.selectedSide = side;
        this.sideTick.x = 442;
        this.sideTick.y = side == "right" ? 205 : 253;
    }

    private selectEffectLevel(level: number) {
        this.selectedEffectLevel = level;
        for (let i = 0; i < 3; i++)
            this.effectCrosses[i].visible = false;
        this.effectCrosses[level].visible = true;
    }

    public remove() {
        if (this.screen) {
            this.screen.events.onInputDown.removeAll();
            this.screen.destroy();
        }
    }

    public getSettings(callback: (settings: GameSettings) => void) {
        this.save.events.onInputDown.addOnce(() => {
            callback(<GameSettings>{
                side: this.selectedSide,
                effectLevel: this.selectedEffectLevel,
                under18: !this.plus18Allowed
            });
            this.remove();
        });

        this.screen.events.onInputDown.add(() => {
            let x = this.phaser.input.activePointer.x;
            console.log(x);
            let y = this.phaser.input.activePointer.y - this.screen.y;
          //  x /= Game.realWidth / 800;
            y /= Game.realHeigt / 600;
            if (new Phaser.Rectangle(0, 70, Game.realWidth * 0.42, 90).contains(x, y)) {
                this.click.play();
                this.selectEffectLevel(0);
            }

            if (new Phaser.Rectangle(Game.realWidth * 0.42, 70, Game.realWidth * 0.18, 90).contains(x, y)) {
                this.click.play();
                this.selectEffectLevel(1);
            }

            if (new Phaser.Rectangle(Game.realWidth * 0.6, 70, Game.realWidth * 0.35, 90).contains(x, y)) {
                this.click.play();
                this.selectEffectLevel(2);
            }

        });
        this.screen.inputEnabled = true;
    }

}