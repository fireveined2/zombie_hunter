﻿
import { WebPage } from "../../utils/WebPage"
import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"
import * as GUI from "../../utils/gui/gui";
import { GameStarter } from "../GameStarter";

import * as Models from "../../../../Shared/Shared";
import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"

import { PlayerPopup } from "../Profile/PlayerPopup"
import { socketClient } from "../../utils/server/Socket"


export class Ranking extends Phaser.State {
    private group: Phaser.Group;
    private rows;
    private rowsCounts = 0;
    public init(rows) {
        this.rows = rows; 
         
    }
    public create() {
        this.group = this.game.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        WebPage.loadInBackground("ranking/ranking.html", () => {

            this.addRows(this.rows);
        });

        let wall = new Wall(this, Game.group, Player.get().data);
        wall.setMenuButton();
        wall.onButtonClick = () => this.returnToMainMenu();
    }


    public addRows(rows: Models.RankingRow[] = this.rows) {
        let elements = [];
        let html = "";
        for (let row of rows) {
            this.rowsCounts++;
            if (!row.place)
                row.place = this.rowsCounts;
            if (!row.records)
                row.records = [0, 0, 0];

            if (row.nick == Player.get().data.username)
                row.status = 1;

            let element = $(this.getRowHtml(row));
            element.click(() => {
                this.initPlayerInfo({
                    id: row.id,
                    level: row.lvl,
                    respect: row.respect,
                    username: row.nick,
                    status: row.status,
                    wearedItemsIds: row.wearedItemsIds
                });

            });

            if (row.nick == Player.get().data.username)
                elements.unshift(element);
            else
                elements.push(element);
        }
        this.addToTable(elements);

        setTimeout(() => {
            $(function () {
                $("#rankingTable").tablesorter({
                  //  sortList: [[1, 1]] 
                });
            });
        }, 100);
    }


    private getRowHtml(row: Models.RankingRow): string {
        let recordMap = 0;
        let record = Math.max(...row.records);
        for (let i = 0; i < 3; i++)
            if (row.records[i] == record)
                recordMap = i;

        let mapImg = ["cmentarz", "szpital", "komisariat"][recordMap];
        let status = ["offline", "online", "ingame"][row.status];
   

        return `
		    <tr>
			  <td>${row.place}</td>
			  <td><li id="${status}"> ${row.nick} </li> </td>
			  <td>${row.lvl}</td>
			  <td>${row.respect}</td>
			  <td><li id="${mapImg}">${record} </li></td >
			</tr>`;
    }

    private initPlayerInfo(player: Models.PlayerInfo) {

        $("#game-window").css("pointer-events", "auto");
        setTimeout(() => $("#game-window").css("pointer-events", "auto"), 300);
        let info = new PlayerPopup(player);
        info.onClose = () => $("#game-window").css("pointer-events", "none");

    }

    private addToTable(elements: any[]) {
        $("#rankingTableBody").append(elements);

    }

    private returnToMainMenu() {
        FX.fade(this.game, "out", 600, () => {
        
            this.game.state.start('MainMenu');
        });
    }

    public shutdown() {
        WebPage.unload();
    }
}



