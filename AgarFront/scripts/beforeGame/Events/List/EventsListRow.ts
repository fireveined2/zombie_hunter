﻿
import { IChallenge } from "../../../utils/Challenges";
import * as Models from "../../../../../Shared/Shared";
import { Game } from "../../Game"
import { ChallengeUtils } from "../../../../../Shared/Challenges";
import * as GUI from "../../../utils/gui/gui";
import { Player } from "../../../utils/Player";
import { SFX } from "../../../SFX/SFX";
import { GameStarter } from "../../GameStarter"
import { ItemInfo } from "../../EQ/ItemInfo"

export class EventsListRow {

    private phaser: Phaser.State;
    private group: Phaser.Group;

    private icon: Phaser.Image;

    private name: Phaser.Text;
    private duration: Phaser.Text;
    private rules: Phaser.Text;
    private respect: GUI.RespectField;
    private progress: Phaser.Text;

    private map: Phaser.Image;

    private go: Phaser.Image;
    private tooLowLevel: Phaser.Text;
    private data: Models.IChallengeData;
    private event: Models.IChallenge;

    public onClick: (event: Models.IChallengeData) => void;

    constructor(parent: Phaser.Group, y: number, event: Models.IChallengeData) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;
        this.data = event;
        this.event = event.data;


        this.icon = this.phaser.add.image(30, y, "items/" + event.data.prize.icon, 0, this.group);
        this.icon.width = 60;
        this.icon.height = 60;
        this.icon.anchor.y = 0.5;
        this.icon.inputEnabled = true;
        this.icon.events.onInputDown.add(() => new ItemInfo(this.group, [event.data.prize]));


        let style = <Phaser.PhaserTextStyle>{ font: "17px Candara", fill: "white" };
        this.name = this.phaser.add.text(100, y, "", style, parent);
        this.name.anchor.y = 0.5;
        this.name.inputEnabled = true;
        this.name.events.onInputDown.add(this._onClick, this);


        this.rules = this.phaser.add.text(363, y, "", style, parent);
        this.rules.anchor.y = 0.5;
        this.rules.anchor.x = 0.5;
        this.rules.inputEnabled = true;
        this.rules.events.onInputDown.add(this._onClick, this);

        this.duration = this.phaser.add.text(482, y, "", style, parent);
        this.duration.anchor.set(0.5)
        this.duration.inputEnabled = true;
        this.duration.events.onInputDown.add(this._onClick, this);

        this.respect = new GUI.RespectField(parent, 629, y, event.data.gain, event.data.cost)
        this.respect.anchor.y = 0.5;

        this.progress = this.phaser.add.text(562, y, "", style, parent);
        this.progress.anchor.y = 0.5;
        this.progress.inputEnabled = true;
        this.progress.events.onInputDown.add(this._onClick, this);

        this.go = this.phaser.add.image(0, y, "take_challenge", 0, this.group);
        this.go.anchor.y = 0.5;
        this.go.x = Game.width - this.go.width * 1.2;
        this.go.inputEnabled = true;
        this.go.events.onInputDown.addOnce(() => this.startChallenge());

        style.fill = "red";
        style.font = "16px Candara";
        style.align = "center";
        this.tooLowLevel = this.phaser.add.text(Game.width - this.go.width - 13, y + 3, "", style, parent);
        this.tooLowLevel.anchor.y = 0.5;

        this.setData(event.data, event.prevStats);
    }

    private startChallenge() {
        if (GameStarter.starting)
            return;

        let starter = new GameStarter(this.group);
        starter.startChallenge({ challengeId: this.event.id });
        return;
    }

    get height(): number {
        return 80;
    }

    get worldBottom(): number {
        return this.icon.worldPosition.y + this.icon.height / 2;
    }

    private showTooLowLevel(required: number) {
        this.go.visible = false;
        this.tooLowLevel.setText("WYMAGANY\nPOZIOM " + required);
    }

    private showNotEnoughRespect(missing: number) {
        this.go.visible = false;
        this.tooLowLevel.setText("BRAKUJE " + missing + "\nRESPEKTU");
    }

    private showStatus(status: "offline" | "notLogged") {
        this.go.visible = false;
        if (status == "notLogged")
            this.tooLowLevel.setText("ZALOGUJ\nSIĘ");
        if (status == "offline")
            this.tooLowLevel.setText("JESTEŚ\nOFFLINE");
    }

    public setData(event: IChallenge, stats: Models.GameStats) {
        this.event = event;

        let name = event.name.toUpperCase();

        this.name.setText(name.substr(0, 20));

        if (event.prizesLeft < 10000)
            this.duration.setText(event.prizesLeft + " NAGRÓD");
        else
            this.duration.setText(this.calculateDuration(event.minutesLeft));

        if (event.quest != -1)
            this.duration.setText("-");
        // this.respect.setText(event.cost.toString());


        let rules = "";
        if (event.type == Models.ChallengeType.SIMPLE)
            rules = "ONE SHOT";
        if (event.type == Models.ChallengeType.CUMULATE)
            rules = "SUMOWANY";
        if (event.type == Models.ChallengeType.SOCIAL)
            rules = "GRUPOWY";
        if (event.type == Models.ChallengeType.KING_OF_THE_HILL)
            rules = "KOH";



        if (!Player.get().status.logged)
            this.showStatus("notLogged");
        else
            if (!Player.get().status.online)
                this.showStatus("offline");
            else
                if (event.minLevel > Player.get().data.level)
                    this.showTooLowLevel(event.minLevel);
                else
                    if (event.cost > Player.get().respect)
                        this.showNotEnoughRespect(event.cost - Player.get().respect);


        this.progress.setText(Math.round(ChallengeUtils.calculateProgess(event.criteria, stats) * 100) + "%");
        this.rules.setText(rules);
    }

    private calculateDuration(duration: number): string {
        if (duration < 60)
            return duration + "m";
        if (duration < 1440)
            return Math.round(duration / 60) + "h";
        return Math.round(duration / 1440) + "d";
    }


    private _onClick() {
        SFX.get().playSingleEffect("buttons/click");
        if (this.onClick)
            this.onClick(this.data);
    }

    public remove() {

    }
}

