﻿import { EventsListRow } from "./EventsListRow"
import { Challenges, IChallenge } from "../../../utils/Challenges";
import { EventsScroller } from "../Scroller/EventScroller"
import { Game } from "../../Game"
import * as GUI from "../../../utils/gui/gui";
import * as Models from "../../../../../Shared/Shared";


export class EventsList { 

    private data: Models.IChallengeData[];
    private challenge: IChallenge;
    private events: EventsListRow[] = [];

    private phaser: Phaser.State;
    private group: Phaser.Group;
    private rowsGroup: Phaser.Group;

    private headers: Phaser.Text[] =[];

    public openDetailsOnClick: boolean = false;
    public onEventSelected: (event: Models.IChallengeData) => void;

    private scrollMask: Phaser.Graphics;
    private line: Phaser.Image;

    constructor(parent: Phaser.Group, challenges: Models.IChallengeData[]) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        this.group.y = 25;
        this.rowsGroup = this.phaser.add.group(this.group);
        this.rowsGroup.y = 65;


        let headers = ["NAGRODA", "NAGRODA", "TYP","POZOSTALO", "POSTĘP", "RESPEKT"]
        let x = [30, 30, 346, 436, 551, 630];
        let linesx = [0, 300, 305, 430, 538, 620];

        for (let i = 1; i < headers.length; i++) {
            let style = { font: "16px Candara", fill: "white" };
            let txt = this.headers[i] = this.phaser.add.text(x[i], 3, headers[i], style, this.group);
            if(i!=0 && i<5)
                this.phaser.add.image(linesx[i + 1]-5, 37, "gui/vertical_line", 0, this.group)
             
        }
        this.line = this.phaser.add.image(70, 25, "gui/horizontal_line", 0, this.group)

     //   this.initScroll();
        this.data = challenges;
        this.create();
    }


    private onScroll() {
        if (this.events.length == 0)
            return; 

        let y = this.phaser.input.activePointer.y;
        let delta = y - this.scrollStartY;
        this.scrollStartY = y;
        if ((this.rowsGroup.position.y + delta < 66 || delta<  0) &&(delta>0 || this.events[this.events.length - 1].worldBottom+delta > Game.height-70)) {
            this.rowsGroup.position.y += delta;
        }
    }

    private scrollStartY = 0;
    private onScrollEnd() {
        this.phaser.input.deleteMoveCallback(this.onScroll, this)
    }

    private initScroll() {
        this.scrollMask = this.phaser.add.graphics(0, 0);
        this.scrollMask.beginFill(0xffffff);
        this.scrollMask.scale = this.group.scale;
        this.scrollMask.drawRect(0, (this.line.bottom + this.group.y + 2) * this.group.scale.y, Game.width + 1400, 700);
        this.rowsGroup.mask = this.scrollMask;


        this.phaser.input.onDown.add(() => {
            this.scrollStartY = this.phaser.input.activePointer.y;;
            this.phaser.input.addMoveCallback(this.onScroll, this);
            this.phaser.input.onUp.addOnce(() => this.onScrollEnd());
        });
    }

    private _onEventSelected(event: Models.IChallengeData) {
        if (this.onEventSelected)
            this.onEventSelected(event);

        if (this.openDetailsOnClick) {
            this.group.visible = false;
            let scroller = new EventsScroller(Game.group, this.data, event);
            scroller.onClose = () => this.group.visible = true;
        }
    }

    private create() {
        let data = this.data;
        for (let i = 0; i < data.length; i++) {
            let event = data[i];
            this.addRow(event);
        }
    }

    private addRow(challenge: Models.IChallengeData) {
        let y = 0;
        if (this.events.length > 0)
            y += this.events.length  * this.events[0].height;
        let row = new EventsListRow(this.rowsGroup, y, challenge);
        row.onClick = this._onEventSelected.bind(this);
        this.events.push(row);
    }


    public remove() {
        for (let row of this.events)
            row.remove();
    }

    public update(data: Models.IChallengeData[]) {
        this.remove();
        this.data = data;
        this.create();
    }

   

}

