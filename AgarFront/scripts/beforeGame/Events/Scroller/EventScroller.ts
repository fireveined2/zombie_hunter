﻿import { Game } from "../../Game"

import { SFX } from "../../../SFX/SFX"
import { Config } from "../../../utils/Config";
import { IChallenge } from "../../../utils/challenges";
import * as GUI from "../../../utils/gui/gui"

import { ActiveEventScreen } from "./EventScreen"
import * as Models from "../../../../../Shared/Shared";


export class EventsScroller {
    private data: Models.IChallengeData[];
    private background: Phaser.Image;
    private group: Phaser.Group;
    private phaser: Phaser.State;

    private eventsGroup: Phaser.Group;
    private events: ActiveEventScreen[] = [];
    private dragX: number;
    public onClose: Function;
    private return: Phaser.Image;

    constructor(parent: Phaser.Group, challenges: Models.IChallengeData[], initial?: Models.IChallengeData) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        this.data = challenges;
        let initialEvent = 0;
        if (initial)
            for (let i = 0; i < challenges.length; i++)
                if (challenges[i].data.id == initial.data.id)
                    initialEvent = i;



        this.background = this.phaser.add.image(Game.width, Game.height * 0.3, 'background_menu', 0, this.group);
        this.background.anchor.x = 1;
        this.background.anchor.y = 0.3;
         
        this.eventsGroup = this.phaser.add.group(this.group);

        this.createScreens();
        this.initDrag();


        this.eventsGroup.position.x = -initialEvent * Game.width;

        this.return = this.phaser.add.image(Game.width * 0.95, Game.height * 0.05, "gui/return", 0, this.group);
        this.return.inputEnabled = true;
        this.return.scale.x = -1;
        this.return.events.onInputDown.addOnce(() => this.remove());
    }

    private createScreens() {
        let data = this.data;
        for (let i = 0; i < data.length; i++) {
            let event = data[i];
            let screen = new ActiveEventScreen(this.eventsGroup, i * Game.width, event);
            this.events.push(screen);
        }

    }


    private initDrag() {



        this.phaser.input.onDown.add(() => {
            this.dragX = this.phaser.input.activePointer.x;;
            this.phaser.input.addMoveCallback(this.onMove, this);
            this.phaser.input.onUp.addOnce(() => this.onDragEnd());
        });
    }

    private onMove() {
        let x = this.phaser.input.activePointer.x;
        let delta = x - this.dragX;
        this.dragX = x; 

        if (this.eventsGroup.position.x + delta < 0 && this.eventsGroup.position.x + delta > -Game.width * (this.events.length - 1))
            this.eventsGroup.position.x += delta; 
    }

    private onDragEnd() {
        this.phaser.input.deleteMoveCallback(this.onMove, this)
        let event = Math.round(this.eventsGroup.position.x / Game.width);
        this.scrollToEvent(event);
    }

    private scrollToEvent(id: number) {
        this.phaser.add.tween(this.eventsGroup.position).to({ x: id * Game.width }, 400).start();;
    }

    private remove() {
        this.phaser.input.onDown.removeAll();
        this.group.removeAll(true);
        if (this.onClose)
            this.onClose();
    }
 
}

