﻿import { Game } from "../../Game"

import { GameStarter } from "../../GameStarter"


import * as GUI from "../../../utils/gui/gui";
import {Player} from "../../../utils/Player";

import * as Models from "../../../../../Shared/Shared";
import { ItemInfo } from "../../EQ/ItemInfo"
import * as TextBase from "../../../utils/TextBase";

export class ActiveEventScreen  {

    private group: Phaser.Group;
    private phaser: Phaser.State;

    private name: Phaser.Text;

    private description: Phaser.Text;
    private prizeIcon: Phaser.Image;
    private prize: Phaser.Text;

    private map: GUI.BorderedText;
    private type: GUI.BorderedText;
    private dur: GUI.BorderedText;

    private cost: Phaser.Text;
    private respect: GUI.RespectField;

    private duration: Phaser.Text;


    private data: Models.IChallengeData;
    private challenge: Models.IChallenge;

    private criteriaList: GUI.CriteriaList | GUI.List;

    private play: Phaser.Image;
    private cantPlayReason: Phaser.Text;

    constructor(parent: Phaser.Group, x: number, data: Models.IChallengeData) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        let bg = this.phaser.add.image(0, 0, "screen_bg", 0, this.group);
        this.group.x = x;
        this.data = data;
        this.challenge = data.data;

        let line = this.phaser.add.image(Game.width *0.6, Game.height * 0.25, "gui/vertical_line", 0, this.group);
        this.initHeader();

        this.initList();

        if (Player.get().status.onlineAndLogged && Player.get().respect >= this.challenge.cost && Player.get().data.level >= this.challenge.minLevel) {
            this.play = this.phaser.add.image(Game.width * 0.8, Game.height * 0.92, "take_challenge", 0, this.group);
            this.play.inputEnabled = true;
            this.play.anchor.set(0.5, 1);
            this.play.events.onInputDown.addOnce(() => this.startChallenge());
        } else {
            let reason = "Zaloguj się, aby wziąć udział w wyzwaniu";
            if (Player.get().status.logged) {

                if (Player.get().respect < this.challenge.cost)
                    reason = "Za mało respektu, aby wziąć udział w wyzwaniu";
                else
                if (Player.get().data.level < this.challenge.minLevel)
                    reason = "Za niski poziom, aby wziąć udział w wyzwaniu";

            }
            let style = <Phaser.PhaserTextStyle>{ font: "27px CosmicTwo", fill: "white", wordWrap: true, wordWrapWidth: 270, align: "center" };
            this.cantPlayReason = this.phaser.add.text(Game.width * 0.8, Game.height * 0.93, reason, style, this.group);
            this.cantPlayReason.anchor.set(0.5, 1);
        }

    }


    private initList() {
        let initKOH = this.challenge.type == Models.ChallengeType.KING_OF_THE_HILL;

        if (!initKOH)
            this.criteriaList = new GUI.CriteriaList(this.group, Game.width * 0.8, Game.height * 0.23, this.challenge.criteria, this.data.prevStats);
        else {
            let rows: GUI.ListRowInitData[] = [];
            let koh = this.challenge.kohData;
            for (let i = 0; i < 3; i++) {
                if (koh.top3[i])
                    rows.push({ header: (i + 1) + " miejsce", content: koh.top3[i].nick + ", " + koh.top3[i].score + " pkt" });
                else
                    rows.push({ header: (i + 1) + " miejsce", content: "Czeka na Ciebie!" });
            }
                         

            if (koh.myScore)
                rows.push({ header: koh.myScore.place + " miejsce", content: Player.get().data.username + ", " + koh.myScore.score + " pkt" });

            this.criteriaList = new GUI.List(this.group, Game.width * 0.8, Game.height * 0.21, rows);

        }
    }


    private startChallenge() {
        if (GameStarter.starting)
            return;

        let starter = new GameStarter(this.group);
        starter.startChallenge({ challengeId: this.challenge.id });
        return;
    }

    private initHeader() {
        let style = <Phaser.PhaserTextStyle>{ font: "27px CosmicTwo", fill: "white" };
        this.name = this.phaser.add.text(Game.width * ((0.55 - 0.08) / 2 + 0.08), Game.height * 0.08, this.challenge.name, style, this.group);
        this.name.anchor.set(0.5);
        this.name.inputEnabled = true;



        this.prizeIcon = this.phaser.add.image(Game.width * 0.08, Game.height * 0.43, "items/" + this.challenge.prize.icon, 0, this.group);
        this.prizeIcon.width = 60;
        this.prizeIcon.height = 60;
        this.prizeIcon.inputEnabled = true;
        this.prizeIcon.events.onInputDown.add(() => new ItemInfo(this.group, [this.challenge.prize]));

       
        style.font = "23px CosmicTwo";
        this.prize = this.phaser.add.text(this.prizeIcon.right + 25, this.prizeIcon.centerY, this.challenge.prize.name, style, this.group);
        this.prize.anchor.y = 0.5;
        this.prize.inputEnabled = true;
        this.prize.events.onInputDown.add(() => new ItemInfo(this.group, [this.challenge.prize]));

        style.font = "16px Candara";
        style.wordWrapWidth = Game.width * 0.55;
        style.wordWrap = true;
        style.align = "center";
        this.description = this.phaser.add.text(Game.width * ((0.55-0.08)/2+0.08), this.name.bottom+4, this.challenge.description, style, this.group);
        this.description.anchor.x = 0.5;

        style.font = "22px CosmicTwo";
       

        let map = "CMENTARZ";
        if (this.challenge.config.map == 2)
            map = "SZPITAL";
        if (this.challenge.config.map == 1)
            map = "KOMISARIAT";
        if (this.challenge.config.map == 3)
            map = "HOPSTOP";

        this.map = new GUI.BorderedText("MAPA: "+map, Game.width * 0.08, Game.height*0.58, this.group)

        let typ = "TYP: ZWYKŁE";
        if (this.challenge.type == Models.ChallengeType.SIMPLE)
            typ = "ONE SHOT";
        if (this.challenge.type == Models.ChallengeType.CUMULATE)
            typ = "SUMOWANY";
        if (this.challenge.type == Models.ChallengeType.SOCIAL)
            typ = "GRUPOWY";
        if (this.challenge.type == Models.ChallengeType.KING_OF_THE_HILL)
            typ = "KING OF THE HILL";


        this.type = new GUI.BorderedText("TYP: "+typ, Game.width * 0.08, this.map.bottom+8, this.group)

        let mark = new GUI.QuestionMark(this.type.element.right + 20, this.type.element.centerY, TextBase.ChallengeDescribe[this.challenge.type], this.group);

        this.duration = this.phaser.add.text(Game.width * 0.08, this.type.bottom+8, "", style, this.group);
        this.updateDuration(this.challenge.minutesLeft, this.challenge.prizesLeft);


        this.cost = this.phaser.add.text(Game.width * 0.08, this.duration.bottom + 8, "Respekt: ", style, this.group);
        this.respect = new GUI.RespectField(this.group, this.cost.right + 10, this.cost.centerY, this.challenge.gain, this.challenge.cost);
        this.respect.anchor.y = 0.5;
    }


    private calculateDuration(duration: number): string {
        if (duration < 60)
            return duration + " minut";
        if (duration < 1440)
            return Math.round(duration / 60) + " godzin";
        return Math.round(duration / 1440) + " dni";
    }

    private updateDuration(minutesLeft: number, prizesLeft: number) {
        if (this.challenge.quest != -1) {
            this.duration.setText("Pozostało: -");
            return;
        }
        let txt = `Pozostało ${this.calculateDuration(minutesLeft)}`;
        if (prizesLeft > 0 && prizesLeft !=10000)
             txt += ` i ${prizesLeft} nagród`
        this.duration.setText(txt);
    }


    public show(show: boolean) {
        this.group.visible = show;
    }
}

