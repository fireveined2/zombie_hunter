﻿/*
import { Game } from "../../Game"
import { Event, Ranking as IRanking } from "../../../utils/events/Events";
import {RankingRow} from "./RankingRow"

export class Ranking  {

    private group: Phaser.Group;
    private phaser: Phaser.State;

    private ranking: IRanking;
    private rows: RankingRow[] = [];

    constructor(parent: Phaser.Group, y: number, ranking: IRanking) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        this.group.y = y;
        this.ranking = ranking;
        this.init();

        
    }


    private init() {
        for (let row of this.ranking.rows) {
            let r = new RankingRow(this.group, this.rows.length * 30, row);
            this.rows.push(r);
        }
    }

}



*/