﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Challenges, IChallenge } from "../../utils/Challenges";
import { EventsList } from "./List/EventsList"
import * as GUI from "../../utils/gui/gui";
import * as Models from "../../../../Shared/Shared";
import { Wall } from "../../utils/Wall"
import { Player } from "../../utils/Player"
   
export class ActiveEventsList extends Phaser.State {

    private background: Phaser.Image;
    private group: Phaser.Group;
  
    private wall: Wall;
    private list: EventsList;
    private return: Phaser.Image;

    init(challenges: Models.IChallengeData[] = Challenges.data, returnToDestination: string = "MainMenu", arg?: any) {
        
        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;
        this.background = this.add.image(0, 0, 'background_menu', 0, this.group);
        let bg = this.add.image(0, 0, "screen_bg", 0, this.group);
        this.background.addChild(bg);

        FX.get().rain(this.group, 12);

        this.list = new EventsList(this.group, challenges);
        this.list.openDetailsOnClick = true;

        this.wall = new Wall(this, this.group, Player.get().data);
        this.wall.setMenuButton();
        this.wall.onButtonClick = () => this.returnTo(returnToDestination, arg);



        let storage = window.localStorage;
        if (storage.getItem("challenges/tip") != "seen") {
            new GUI.Popup("Kliknij nazwę wyzwania, aby poznać szczegóły");
            storage.setItem("challenges/tip", "seen");
        }
    }

    public returnTo(destination, arg) {

        this.game.state.start(destination, true, false, arg); 
    }

}

