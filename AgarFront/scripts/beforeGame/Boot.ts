﻿import { Game } from "./Game";
import { Config } from "../utils/Config";
import { Challenges } from "../utils/Challenges";

export class Boot extends Phaser.State {
    public slasher: Game;
    public preload() {

        this.load.atlas('logo', 'assets/beforeGame/logo.png', 'assets/beforeGame/logo.json');
        this.load.image('hophands', 'assets/beforeGame/hophands.png');

        console.log("Boot preload");
    }
    public create() {
     

        if (Config.Version.mobile) {
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            this.scale.forceLandscape = true;
            this.scale.forceOrientation(true, false);
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        }



        console.log("Boot create");

    
        this.game.state.start('Preloader', true, false);
        Challenges.download();
    }

   public init() {
       this.slasher = this.game as Game;
        this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = true;

        console.log("Boot init");

   }



}