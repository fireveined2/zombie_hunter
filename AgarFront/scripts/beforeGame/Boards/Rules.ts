﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { SoundAdjust } from "../../../../LobbyLogic/clientSide/SoundAdjust";
import { IBoard } from "./BoardManager"



export class Rules implements IBoard {

    public length: number;
    public onClose: Function;

    private images: Phaser.Image[] = [];
    private current: Phaser.Image;
 
    public init(group: Phaser.Group) {
        this.images = [];
        let phaser = group.game;
        for (let i = 1; i <= 5; i++) {
            let img = phaser.add.image(20, 20, "rules" + i, 0, group);
            //    img.anchor.set(0.5, 0.5);
            img.scale.set(Game.height / 600)
            img.x = Game.width * 0.383 - img.width / 2;
            img.y = Game.height / 2 - img.height / 2;
            if (i != 1)
                img.visible = false;

            this.images.push(img);
        }

        this.current = this.images[0];
        this.length = this.images.length;
    }

    public load(number: number) {
        this.current.events.onInputDown.removeAll();
        this.current.inputEnabled = false;
        this.current.visible = false;

        this.images[number].visible = true;
        this.current = this.images[number];
        this.current.inputEnabled = true;
        this.current.events.onInputDown.addOnce(() => this.onClick(this.current));
    }


    private onClick(img: Phaser.Image) {
        let x = img.game.input.activePointer.x;
        if (x > img.x + img.width / 2)
            this.scroll(1);
        else
            this.scroll(-1);
        SFX.get().playClick();
    }

    public scroll(value: number = 1 | -1) {
        let nr = this.images.indexOf(this.current);
        nr += value;
        if (nr < 0)
            nr = 0;
        if (nr > 4)
            this.remove();
        else
            this.load(nr);
    }

    public remove() {
        if (!this.images)
            return;

        for (let img of this.images) 
            img.destroy();     

        delete this.images;
        if (this.onClose)
            this.onClose();
    }

    public isOpened(): boolean {
        return this.images !== undefined;
    }

}

