﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX } from "../../SFX/SFX"
import { Config } from "../../utils/Config";

import {Rules} from "./Rules"
import {Authors} from "./Authors"

export interface IBoard {
    length: number;
    onClose: Function;
    disapearOnMouseUp?: boolean;

    init: (group: Phaser.Group) => void;
    load: (number: number) => void;
    remove: () => void;
    isOpened: () => boolean;

}

export class BoardManager {

    private boards: IBoard[] = [];
    private current: IBoard;

    public onOpen: Function;
    public onClose: Function;

    private group: Phaser.Group;
    constructor(group: Phaser.Group, private phaser: Phaser.Game = group.game) {
        this.group = phaser.add.group(group);

        this.add(new Rules());
        this.add(new Authors());
       
    }

    public add(board: IBoard): number {
        this.boards.push(board);
        return this.boards.length - 1;
    }

    /**
     * zwraca true/false w zależności od tego, czy otwarta karta zamyka się po puszczeniu przycisku
     */
    public open(id: number, onClose?: Function): boolean {
        if (this.current)
            this.current.remove();

        let board = this.boards[id];
        board.init(this.group);
        board.load(0);

        this.current = board;
        this.current.onClose = onClose;

        return this.current.disapearOnMouseUp;
    }

    public closeAll() {
        this.current.remove();
    }

    public isOpened(): boolean {
        return this.current && this.current.isOpened();
    }

}