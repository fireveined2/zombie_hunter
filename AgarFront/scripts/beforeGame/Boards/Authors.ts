﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { SoundAdjust } from "../../../../LobbyLogic/clientSide/SoundAdjust";
import { IBoard } from "./BoardManager"

export class Authors implements IBoard {

    public length: number;
    public onClose: Function;
    public disapearOnMouseUp: boolean = true;

    private images: Phaser.Image[] = [];
    private current: Phaser.Image;
 
    public init(group: Phaser.Group) {
        this.images = [];
        let phaser = group.game;

        let img = phaser.add.image(20, 20, "authors", 0, group);
        img.scale.set(Game.height / 600)
        img.x = Game.width * 0.383 - img.width / 2;
        img.y = Game.height / 2 - img.height / 2;
        this.images.push(img);
        

        this.current = this.images[0];
        this.length = this.images.length;
    }

    public load(number: number) {
        this.images[number].visible = true;
        this.current = this.images[number];
    }


    public remove() {
        if (!this.images)
            return;

        for (let img of this.images) 
            img.destroy();     

        delete this.images;
        if (this.onClose)
            this.onClose();
    }

    public isOpened(): boolean {
        return this.images !== undefined;
    }

}

