﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"

import { Challenges } from "../../utils/Challenges"
import { ResultsWindow } from "./ResultsWindow";
import { GameStatsTab } from "./Stats/Stats";
import { RewardsTab } from "./Rewards/Rewards";
import * as Server from "../../utils/Server"
import * as Models from "../../../../Shared/Shared";
import { socketClient } from "../../utils/server/Socket"
import * as SocketModels from "../../../../Shared/SocketModels";
export class Results {

    private phaser: Phaser.State;
    private group: Phaser.Group;

    private window: ResultsWindow;
    private stats: GameStatsTab;
    private rewards: RewardsTab;
    public onClose: Function;
    public onMoreChallenges: Function;

    constructor(parent: Phaser.Group, private acces: Models.AccesToPlay) {
        this.group = parent;





        this.stats = new GameStatsTab(acces.challengeId, Game.stats, acces.offline, Player.get().lastGameRequest.isChallengeTraining);
        this.rewards = new RewardsTab();

        this.window = new ResultsWindow(this.group, [this.stats, this.rewards]);
        this.window.onLeftButtonClick = this._onClose.bind(this);



        socketClient.pvp.sendData(Player.get().generateGameData(Game.stats), this.onResults.bind(this));
        this.window.showMiddleMenuButton();
    }

    private onResults(results: SocketModels.IPvPResults) {
        console.log(results);
        this.stats.setResult(results);
        let me =results.players[0];
        if (me.name != Player.get().data.username)
            me = results.players[1];
        if (me.id == results.winnerId)
            SFX.get().playSlonNewRecord();
        else
            SFX.get().playSlonFuckup();


        Challenges.update(results.challengesUpdate);
    }

    private _onClose() {
        if (this.onClose)
            this.onClose();
    }


    private _onMoreChallenges(quest: number) {
        if (this.onMoreChallenges)
            this.onMoreChallenges(quest);
    }



    private deleteChallengeIfFinished(results: Models.ChallengeResult, challengeId: number) {
        if (results == Models.ChallengeResult.SUCCES)
            Challenges.remove(challengeId);
    }

   


    private onError(error: Server.RequestError) {
        this.rewards.timeout();
        this.window.showMiddleMenuButton();
    }

}