﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"

import { GameEvents } from "../../game/logic/GameEvents"

import * as Models from "../../../../Shared/Shared";
import { Results } from "./Results";
import { socketClient } from "../../utils/server/Socket"
import { Validate } from "../../../../Veryficator/veryficator";

import { HeroAnim } from "../../utils/HeroAnim"
import { GameStarter } from ".././GameStarter"
import { Challenges } from "../../utils/Challenges"

export class PvPFinalScore extends Phaser.State {
    private background: Phaser.Image;
    private overlay: Phaser.Graphics;
    private scoreText: Phaser.BitmapText;


    private countingDuration: number = 2300;
    private goToLobbyTimeout: any;
    private group: Phaser.Group;



    private defaultBestScore = 2000;
    private playerBestScore = 0;

    static reminderTimeout: any;
    private heroAnim: HeroAnim;

    private results: Results;



    init(score: number, timeLeft: number, acces: Models.AccesToPlay) {
        GameStarter.removeAssetsForMap(this, acces.config.map);
        //   Game.inputCollector.getCompressed();



        console.log(acces);
        this.overlay = undefined;
        SFX.setSFX(new SFXMenu(this.game));

        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.background = this.add.image(Game.width, Game.height * 0.3, 'background_menu', 0, this.group);
        this.background.anchor.x = 1;
        this.background.anchor.y = 0.3;
        let wall = this.add.image(0, Game.height, "wall", 0, this.group);
        wall.anchor.y = 1;

        this.heroAnim = new HeroAnim(this, this.group);
        this.heroAnim.setItems(Player.get().getAllWearedItems(), true);
        let x = Game.width * 0.85 * Game.assetsSize;
        let y = Game.height - 12 * Game.assetsSize - this.heroAnim.height;
        this.heroAnim.position.set(x, y);



        //   Player.get().showScoreLabel(this.group);

        FX.get().rain(this.group, 13);

        let overlay = FX.fade(this.game, "in", 1200, null, 800);
        this.update();

        Game.absence.markAsActive();




        this.results = new Results(this.group, acces);
        this.results.onClose = this.goToMenu.bind(this);
    }





    private updateHandler;
    private lastTime = Date.now();
    public update() {
        let now = Date.now();
        let elapsed = now - this.lastTime;
        this.lastTime = now;
        FX.get().update(elapsed / Config.Global.frameTime);

        this.heroAnim.updateAnim();
    }


    public shutdown() {
        this.heroAnim.kill();
    }

    private goToMenu(socket: any) {
        if (this.overlay)
            return;
        this.overlay = FX.fade(this.game, "out", 600, () => {
            clearTimeout(this.updateHandler);
            this.game.state.start('MainMenu', true, false);
        }, 800);
        socketClient.pvp.close();
    }
}