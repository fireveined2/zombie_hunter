﻿
import {Game} from "../.././Game"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import {AnimableElement} from "../ResultsWindow"
import * as Models from "../../../../../Shared/Shared";

export class Reward {

    private phaser: Phaser.State;
    private row: Phaser.Text;
    private image: Phaser.Image


    constructor(parent: Phaser.Group, y: number, item: Models.IItem) {
        this.phaser = parent.game.state.getCurrentState();

        this.image = this.phaser.add.image(50, y, "items/" + item.id, 0, parent);
        this.row = this.phaser.add.text(120, y, item.name, {}, parent);
    }

}