﻿import {Game} from "../.././Game"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { GameStats} from "../../../game/logic/GameStats"
import {ResultsTab, AnimableElement} from "../ResultsWindow";
import {Reward} from "./Reward";
import * as Models from "../../../../../Shared/Shared";


export class RewardsTab implements ResultsTab {
    public alreadyOpened: boolean;
    public name: string;
    private phaser: Phaser.State;
    private group: Phaser.Group;
    private wait;


    private rewards: Reward[] = [];

    constructor() {
        this.name = "Nagrody";
        this.alreadyOpened = true;
    }


    public addRow(item: Models.IItem) {
        let row = new Reward(this.group, 40 + 50 * this.rewards.length, item);
        this.rewards.push(row);
    }

    public init(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

    }


    public show(show: boolean) {
        this.group.visible = show;
    }



    public update(prizes: Models.IPrize[]) {


        if (!prizes)
            return;
        for (let prize of prizes)
            this.addRow(prize);
    }

    public timeout() {

    }

    public anim() {
        this.show(true);
        this.alreadyOpened = true;
    }

}
