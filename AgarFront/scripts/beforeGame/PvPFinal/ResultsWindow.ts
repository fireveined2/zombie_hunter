﻿import {Game} from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as GUI from "../../utils/gui/gui"
import { ItemInfo } from "../EQ/ItemInfo"
import * as Models from "../../../../Shared/Shared";

export interface AnimableElement {
    anim(next?: AnimableElement[], onEnd?: Function);
}

export interface ResultsTab {
    name: string;
    alreadyOpened: boolean;

    init(group: Phaser.Group);
    show(show: boolean);
    anim();
}

export class ResultsWindow {

    private phaser: Phaser.State;

    private group: Phaser.Group;
    private body: Phaser.Group;

    private buttons: GUI.TabButton[] = [];
    private tabs: ResultsTab[] = [];
    private current: number;

    private bg: Phaser.Image;

    private leftButton: Phaser.Image;
    private rightButton: Phaser.Image;
    private middleButton: Phaser.Image;

    constructor(parent: Phaser.Group, tabs?: ResultsTab[]) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);
        this.group.position.set(20, Game.height * 0.057);
        this.bg = this.phaser.add.image(-Game.width*0.07, -30, "results/window", 0, this.group);

        this.body = this.phaser.add.group(this.group);
        this.body.position.set(0, 65);
        tabs.forEach((tab) => this.addTab(tab));     

        this.leftButton = this.phaser.add.image(0, Game.height * 0.92, "gui/button_menu", 0, this.group);
        this.leftButton.inputEnabled = true;
        this.leftButton.events.onInputDown.add(() => this.onLeft());
        this.leftButton.anchor.set(0, 1);
        this.leftButton.visible = false;
      

        this.rightButton = this.phaser.add.image(this.bg.right - 40, Game.height * 0.92, "gui/button_more_challenges", 0, this.group);
        this.rightButton.inputEnabled = true;
        this.rightButton.events.onInputDown.add(() => this.onRight());
        this.rightButton.anchor.set(1, 1);
        this.rightButton.visible = false;

        this.middleButton = this.phaser.add.image(this.rightButton.x / 2, Game.height * 0.92, "gui/button_try_again", 0, this.group);
        this.middleButton.inputEnabled = true;
        this.middleButton.events.onInputDown.add(() => this.onLeft());
        this.middleButton.anchor.set(0.5, 1);
        this.middleButton.visible = false;
    }

    public showLostButtons() {
        this.leftButton.loadTexture("gui/button_surrender");
        this.leftButton.visible = true;
        this.rightButton.loadTexture("gui/button_try_again");
        this.rightButton.visible = true;
    }

    public showWinButtons() {
        this.leftButton.visible = true;
        this.rightButton.visible = true;
    }

    public showRewardTab(item: Models.IItem) {
        this.buttons[1].visible = true;
        this.buttons[1].onClick = () => {
            new ItemInfo(this.group, [item]).playSlonComment();
        }
        this.buttons[0].element.x = 70;

    }

    public showMiddleMenuButton() {
        this.middleButton.visible = true;
    }


    public onLeftButtonClick: Function;
    public onRightButtonClick: Function;

    private onLeft() {
        if (this.onLeftButtonClick)
            this.onLeftButtonClick();
    }

    private onRight() {
        if (this.onRightButtonClick)
            this.onRightButtonClick();
    }

    private addButton(name: string) {
        let posx = [170, 240, 290];
        let i = this.buttons.length;
        let btn = new GUI.TabButton(name, posx[this.buttons.length], i==0?-9:0, this.group, () => this.open(i));
        this.buttons.push(btn);

        if (name == "Nagrody")
            btn.visible = false;
    }

    public addTab(tab: ResultsTab) {
        tab.init(this.body);
        this.addButton(tab.name);
        this.tabs.push(tab);

        if (this.tabs.length==1)
            this.open(0);
        else
            tab.show(false); 
    }

    public open(index: number) {
        if (this.current !== undefined) {
            let current = this.tabs[this.current];
            current.show(false);
            this.buttons[this.current].active(false);
        }

        this.buttons[index].active(true);
        this.current = index;
        if (this.tabs[index].alreadyOpened)
            this.tabs[index].show(true);
        else
            setTimeout(()=> this.tabs[index].anim(), 700);
    }

}