﻿import { Game } from "../.././Game"
import { SFX, SFXMenu } from "../../../SFX/SFX"
import { GameStats } from "../../../game/logic/GameStats"
import { Challenges } from "../../../utils/Challenges"
import { ResultsTab, AnimableElement } from "../ResultsWindow";
import * as GUI from "../../../utils/gui/gui";
import * as Models from "../../../../../Shared/Shared";
import { ChallengeUtils } from "../../../../../Shared/Challenges";
import * as SocketModels from "../../../../../Shared/SocketModels";
import { Player } from "../../../utils/Player"
import { socketClient } from "../../../utils/server/Socket"
export class GameStatsTab implements ResultsTab {

    public alreadyOpened: boolean;
    public name: string;
    private phaser: Phaser.State;
    private group: Phaser.Group;


    private stats: GameStats;
    private list: GUI.CriteriaList;

    private respect: Phaser.Text;
    private result: Phaser.Text;
    private reward: Phaser.Text;
    private results: SocketModels.IPvPResults;

    private offline: boolean;

    private minePointsRow: Phaser.Text;
    private enemyPointsRow: Phaser.Text;


    constructor(challenge: number, stats: GameStats, offline?: boolean, private isChallengeTraining?: boolean) {
        this.stats = stats;
        this.name = "Ostatnia gra";
        this.alreadyOpened = false;

    }

    private update(data: SocketModels.IPvPUpdate) {
        this.enemyPointsRow.setText(socketClient.pvp.enemy.score +" pkt");
        if (this.stats.score > socketClient.pvp.enemy.score) {
            this.minePointsRow.fill = "green";
            this.enemyPointsRow.fill = "red";
        }
        else {
            this.minePointsRow.fill = "red";
            this.enemyPointsRow.fill = "green";
        }

        let t = socketClient.pvp.enemy.timeLeft > 1 ? socketClient.pvp.enemy.timeLeft.toString() : "";
        if (this.result)
        this.result.setText("Oczekiwanie na " + socketClient.pvp.enemy.name + "... " + t);
    }


    public init(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        this.list = new GUI.CriteriaList(this.group, Game.width * 0.29, 15, null, null, true);
        this.list.visible = true;

        this.list.create([], this.stats);

        let style = { font: "29px CosmicTwo", fill: "white" };
        this.result = this.phaser.add.text(Game.width * 0.29, Game.height * 0.43, "Oczekiwanie...", style, this.group);
        this.result.setText("Oczekiwanie na " + socketClient.pvp.enemy.name + "... " + socketClient.pvp.enemy.timeLeft);
        this.result.anchor.set(0.5);

        style = { font: "22px CosmicTwo", fill: "white" };
        this.respect = this.phaser.add.text(Game.width * 0.29, Game.height * 0.52, "", style, this.group);
        this.respect.anchor.set(0.5);


        this.reward = this.phaser.add.text(Game.width * 0.29, Game.height * 0.60, "", style, this.group);
        this.reward.anchor.set(0.5);

        this.minePointsRow = this.list.addSimple(Player.get().data.username, this.stats.score + " pkt");
        this.enemyPointsRow = this.list.addSimple(socketClient.pvp.enemy.name, socketClient.pvp.enemy.score + " pkt");
        this.update(null)
        socketClient.pvp.onEnemyUpdate = this.update.bind(this);
    }


    public setResult(results: SocketModels.IPvPResults) {
        this.results = results;
        this.showResults();
    }

    private cantGetResults() {
        this.result.setText("Nie można pobrać rezultatu!");
    }

    private pushRespectRow(name: string, respect: number, respectChange: number) {
        if (respectChange > 0)
            this.list.addSimple(name, respect + " respektu (+"+respectChange+")").fill = "green";
        else
            this.list.addSimple(name, respect + " respektu (" + respectChange + ")").fill = "red";
    }

    private showResults(tries: number = 0) {

        let me = this.results.players[0];
        if (me.name != Player.get().data.username)
            me = this.results.players[1];

        let enemy = this.results.players[0];
        if (enemy.name == Player.get().data.username)
            enemy = this.results.players[1];

        socketClient.pvp.enemy.score = enemy.score;
        this.update(null);

        this.pushRespectRow(this.results.players[0].name, this.results.players[0].respect, this.results.players[0].respectChange);
        this.pushRespectRow(this.results.players[1].name, this.results.players[1].respect, this.results.players[1].respectChange);
      
        this.showTextResult(me.id == this.results.winnerId);

        this.showRespectAndExp( me.exp);
 //       this.showReward(this.results.prizes);
/*
        if (!this.results.prizes || this.results.prizes.length == 0) {
            this.respect.y += 20;
            this.result.y += 20;
        }
        */
    }

    private showReward(rewards: Models.IPrize[]) {
        let num = rewards ? rewards.length : 0;
        if (num > 0)
            this.reward.setText("Zdobyłeś nagrodę!");
    }



    private showRespectAndExp(exp: number) {
            this.respect.setText("+"+ exp + " doświadczenia");
    }

    private showTextResult(win: boolean) {
        if (win)
            this.result.setText("Sukces");
        else
            this.result.setText("Porażka");
    }

    private showStarResult(stars: number) {
        this.result.setText("ZYSKAŁEŚ:");
    }

    public show(show: boolean) {
        this.group.visible = show;
    }

    public anim(next?: AnimableElement[], onEnd?: Function) {
        this.show(true);
        this.alreadyOpened = true;
    //    let end = () => {
      //      if (onEnd)
      //          onEnd();
      //      this.alreadyOpened = true;
      //  }
       // setTimeout(() => this.list.anim(next, end), 1000);
    }

}