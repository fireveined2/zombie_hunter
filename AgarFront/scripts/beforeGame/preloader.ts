﻿import { Game } from "./Game"
import { Config } from "../utils/Config";
import { FX } from "../game/FX"
import { SoundBase } from "../SFX/SoundBase"
import { SFX, SFXMenu } from "../SFX/SFX"
import { CuttablesDatabase } from "./CuttablesDatabase"
import { Player } from "../utils/Player";
import * as Server from "../utils/Server/Offline";
import * as GUI from "../utils/gui/gui"
import { AssetLoader } from "./AssetLoader";
import { SleepManager } from "../utils/SleepManager";

export class Preloader extends Phaser.State {

    public preloadBar: Phaser.Sprite;
    public hophandsLogo: Phaser.Image;
    public preloadBackground: Phaser.Sprite;
    private path: string;

    private database: CuttablesDatabase;

    private pendingAssetsCount = 0;

    private loader: AssetLoader;

    private loadFromAMS(name: string, path: string) {
        this.load.image(name, Config.Global.serverAdress+"/load/"+path);

    }

    public preload() {
        console.log("Preloader preload");
        let loader = this.loader = new AssetLoader(this);

        this.load.crossOrigin = true;


        this.database = new CuttablesDatabase();
        let group = this.add.group();
        group.scale.set(1 / Game.assetsScale * 0.7);
        this.preloadBar = this.add.sprite(Game.realWidth / 2, Game.realHeigt * 0.1, 'logo', "logo0009", group);
        this.preloadBar.x = Game.realWidth / 2 / (1 / Game.assetsScale * 0.7) - this.preloadBar.width / 2;
        this.preloadBar.anchor.y = 0;

        this.load.setPreloadSprite(this.preloadBar);

        this.hophandsLogo = this.add.image(Game.realWidth / 2, Game.realHeigt - 20, "hophands");
        this.hophandsLogo.anchor.x = 0.5;
        this.hophandsLogo.anchor.y = 1;
        let size = "s" + Game.assetsSize;
        //  if (!Config.Version.mobile)
        //      size = "www";


        let path = "assets/" + size + "/";
        this.path = path;

        this.loadSfx();

        //lobby
        this.load.image("menu/bg", path + "lobby/bg_menu.png");

        for (let map = 0; map < 4; map++) {
            this.load.image("background" + map, path + "bg" + map + ".jpg");
        }
        this.load.atlasJSONHash("trip_label", path + "maps/trip.png", path + "maps/trip.json")
            .onLoadComplete.addOnce(() => {
                let img = this.add.image(-1000, -1000, "trip_label");
                setTimeout(() => img.destroy(), 200);
            });


        this.load.atlasJSONHash("map2/elements", path + "maps/map2.png", path + "maps/map2.json");

        this.load.atlasJSONHash("map1/elements", path + "maps/map1.png", path + "maps/map1.json");

        this.load.atlasJSONHash("map3/elements", path + "maps/map3.png", path + "maps/map3.json");
        this.load.image("map3/light", path + "maps/map3_light.png");



        this.load.image("tip/trip", path + "tips/trip.png");

        //tips
        for (let i = 0; i < 4; i++)
            this.load.image("tip/map" + i, path + "tips/map" + i + ".png");
        this.load.image("tip/ctp", path + "tips/ctp.png");


        let buttons = ["eq", "settings", "play", "help", "turnieje","turnieje_new", "share", "challenges", "tworcy", "login", "back", "message", "new_message", "logout", "top3_players", "top3_players_black", "list", "list_black", "prize", "prizes", "prizes_black"];
        for (let button of buttons)
            this.load.image("buttons/" + button, path + "/lobby/buttons/button_" + button + ".png");

        this.load.image("background_menu", path + "lobby/background_menu.jpg");
        this.load.image("authors", path + "tworcy.png");


        for (let i = 1; i <= 6; i++)
            this.load.image("button" + i, path + "lobby/buttons/button" + i + ".png");
        this.load.image("buttonBack", path + "lobby/buttons/wroc.png");
        this.load.image("label_points", path + "lobby/buttons/punkty.png");

        this.load.image("window", path + "window.png");

        this.load.image("screen_bg", path + "screen_bg.png");
        this.load.image("take_challenge", path + "challenges/take_challenge.png");



        this.load.atlasJSONHash("map_elements", path + "map_elements.png", path + "map_elements.json");
        this.load.atlasJSONHash("blood", path + "blood.png", path + "blood.json");

        //fonts
        this.load.bitmapFont("medium", path + "fonts/medium.png", path + "fonts/medium.fnt");
        this.load.bitmapFont("timer", path + "fonts/timer.png", path + "fonts/timer.fnt");
        this.load.bitmapFont("big", path + "fonts/big.png", path + "fonts/big.fnt");

        //hud
        if (!Game.disableAnims) {
            this.load.atlasJSONHash("chill_label", path + "hud/chill_label.png", path + "hud/chill.json")
                .onLoadComplete.addOnce(() => {
                    let img = this.add.image(-1000, -1000, "chill_label");
                    setTimeout(() => img.destroy(), 3100);
                });
            this.load.atlasJSONHash("rage_label", path + "rage.png", path + "rage.json")
                .onLoadComplete.addOnce(() => {
                    let img = this.add.image(-1000, -1000, "rage_label");
                    setTimeout(() => img.destroy(), 3110);
                });
            this.load.atlasJSONHash("counting", path + "hud/counting.png", path + "hud/counting.json")
                .onLoadComplete.addOnce(() => {
                    let img = this.add.image(-1000, -1000, "counting");
                    setTimeout(() => img.destroy(), 3100);
                });
        }
        else
            this.load.image("chill_label", path + "hud/chill_labelSimple.png");
        this.load.image("rage_label", path + "rageSimple.png");

        this.load.atlasJSONHash("counting", path + "hud/countingSimple.png", path + "hud/countingSimple.json")
            .onLoadComplete.addOnce(() => {
                let img = this.add.image(-1000, -1000, "counting");
                setTimeout(() => img.destroy(), 3100);
            });
        this.load.image("hud/arrow", path + "hud/arrow.png");

        //WALL
        this.load.image("rank_bg", path + "hud/rank_bg.png");
        this.load.image("wall", path + "lobby/wall.png");
        this.load.image("buttons/ranking", path + "lobby/button_ranking.png");
        this.load.image("buttons/menu", path + "lobby/button_menu.png");
        this.load.image("icons/hp", path + "lobby/icon_zycie.png");
        this.load.image("bars/hp", path + "lobby/hp_bar.png");
        this.load.image("bars/exp", path + "lobby/exp_bar.png");
        this.load.image("bars/hp_fill", path + "lobby/hp_bar_fill.png");
        this.load.image("bars/exp_fill", path + "lobby/exp_bar_fill.png");

        this.load.image("hud/question_mark", path + "hud/question_mark.png");


        this.load.image("overlay", path + "hud/freeze.png");

        this.load.shader('shaders/drugs', 'assets/shaders/drugs.frag');

        //    for (let i = 1; i <= 5; i++)
        //      this.load.image("rules"+i, path + "rules/rules"+i+".png");


        this.database.afterLoadOne = this.loadImageForCuttable.bind(this);
        this.database.loadAll();



        //  this.loadBomb(839, 366, 281, 254);

        this.load.image("mug_frozen", path + "hud/frozen_mug.png");
        this.load.image("yes", path + "hud/yes.png");
        this.load.image("no", path + "hud/no.png");
        this.load.image("gui/label", path + "hud/label.png");
        this.load.image("settings", path + "settings/ustawienia.png");
        this.load.image("settings_button", path + "settings/button.png");
        this.load.image("save", path + "settings/save.png");

        this.load.image("login_button", path + "lobby/buttons/login.png");
        this.load.image("surrender", path + "lobby/buttons/surrender.png");
        this.load.image("bladePattern", path + "bladePattern.png");
        this.load.image("bladePatternChill", path + "bladePattern_chill.png");

        //events
        this.load.image("gui/criterion_line", path + "challenges/criterion_line.png");
        this.load.image("gui/vertical_line", path + "challenges/vertical_line.png");
        this.load.image("gui/horizontal_line", path + "challenges/horizontal_line.png");
        this.load.image("gui/respect", path + "challenges/respect.png");
        this.load.image("gui/return", path + "challenges/return.png");

        this.load.image("gui/arrow_left", path + "hud/left.png");
        this.load.image("gui/arrow_right", path + "hud/right.png");

        //eq
        this.load.image("eq/bg", path + "eq/bg.jpg");
        this.load.atlasJSONHash("eq/icons", path + "eq/icons.png", path + "eq/icons.json")
        this.load.atlasJSONHash("eq/elements", path + "eq/elements.png", path + "eq/elements.json")
        this.load.image("buttons/use", path + "eq/button_use.png");
        this.load.image("buttons/wear", path + "eq/button_wear.png");
        this.load.image("buttons/ok", path + "eq/button_ok.png");
        this.load.image("buttons/cancel", path + "eq/button_cancel.png");

        //map
        this.load.image("map/map", path + "map_screen/map.png");
        this.load.image("map/highlight", path + "map_screen/highlight.png");
        this.load.image("map/play", path + "map_screen/button_play.png");
        this.load.image("map/mark", path + "map_screen/mark.png");
        this.load.image("map/quests", path + "map_screen/button_quests.png");
        this.load.image("map/location", path + "map_screen/location.png");


        //results
        this.load.image("results/window", path + "results/window.png");
        this.load.image("results/btn_events", path + "results/btn_events.png");
        this.load.image("results/btn_rewards", path + "results/btn_rewards.png");
        this.load.image("results/btn_lastGame", path + "results/btn_lastGame.png");
        this.load.image("results/statsIcons", path + "results/stats_icons.png");

        this.load.image("gui/button_surrender", path + "results/button_surrender.png");
        this.load.image("gui/button_try_again", path + "results/button_try.png");
        this.load.image("gui/button_more_challenges", path + "results/button_more.png");
        this.load.image("gui/button_menu", path + "results/button_menu.png");

        //items
        for (let i = 0; i <= 83; i++) {
            let url = path + "items/" + i + ".png";
            loader.image("items/" + i, url);
        }

        let itms = ["70", "71", "72", "73", "75", "76", "77", "78", "79", "80", "81", "83","84", "85", "86", "87", "88", "89"];
        for (let item of itms)
            this.loadFromAMS("items/" + item, item+".png");

        this.loadFromAMS("arena/bg",   "arena_bg.jpg");

        //skills
        for (let i = 1; i <= 3; i++) {
            let url = path + "skills/icon/" + i + ".png";
            loader.image("skills/icon/" + i, url);
        }

        this.load.atlasJSONHash("effects/boom", path + "effects/boom.png", path + "effects/boom.json")
            .onLoadComplete.addOnce(() => {
                let img = this.add.image(-1000, -1000, "skills/boom");
                setTimeout(() => img.destroy(), 4000);
            });
        this.load.atlasJSONHash("effects/big_boom", path + "effects/big_boom.png", path + "effects/big_boom.json")
            .onLoadComplete.addOnce(() => {
                let img = this.add.image(-1000, -1000, "skills/boom");
                setTimeout(() => img.destroy(), 4000);
            });

        this.load.atlasJSONHash("effects/implode", path + "effects/implode.png", path + "effects/implode.json")
            .onLoadComplete.addOnce(() => {
                let img = this.add.image(-1000, -1000, "skills/implode");
                setTimeout(() => img.destroy(), 4000);
            });

        this.load.atlasJSONHash("effects/fire", path + "effects/fire.png", path + "effects/fire.json");


        this.load.image("gui/info_popup_bg", path + "hud/info_popup_bg.png");
        this.load.image("gui/popup", path + "hud/popup.png");

        //hero
        this.load.image("hero/blood", path + "hero/blood.png");

        let parts = ["legs", "rightArm", "head", "head_blink", "boots", "leftArm", "body", "primaryWeapon"];
        let names = ["hero0/", "hero1/", "hero2/", "hero3/", "hero4/", "hero5/", "hero6/", "hero7/", "hero8/","hero9/", "hero10/", "hero11/", "hero12/", "hero13/", "hero14/", "hero15/"];

        for (let name of names) {
            for (let part of parts) {
                let url = path + "hero/" + name + part + ".png";
                loader.image(name + part, url);
                this.load.image(name + "blood/" + part, path + "hero/" + name + "blood/" + part + ".png");
            }


        }
        if (Game.settings.effectLevel != 0)
            this.load.atlasJSONHash("hero0/secondaryWeapon", path + "hero/hero0/secondaryWeapon.png", path + "hero/hero0/secondaryWeapon.json")
                .onLoadComplete.addOnce(() => {
                    let img = this.add.image(-1000, -1000, "hero0/secondaryWeapon");
                    setTimeout(() => img.destroy(), 4000);
                });
        else
            this.load.image("hero0/secondaryWeapon", path + "hero/hero0/secondaryWeaponSimple.png");

        this.load.image("hero2/secondaryWeapon", path + "hero/hero2/secondaryWeapon.png");
        this.load.image("hero1/secondaryWeapon", path + "hero/hero0/secondaryWeaponSimple.png");

        this.load.image("hero3/secondaryWeapon", path + "hero/hero3/secondaryWeapon.png");
        this.load.image("hero4/secondaryWeapon", path + "hero/hero4/secondaryWeapon.png");
        this.load.image("hero11/secondaryWeapon", path + "hero/hero11/secondaryWeapon.png");

        //login
        this.load.image("login/bg", path + "login/bg.jpg");

        //profile
        this.load.image("profile/challenge", path + "profile/challenge.png");

        //pvp
        this.load.image("pvp/lobby_bg", path + "pvp/lobby_bg.jpg");

        //share
        this.load.image("share/records", path + "share_records.png");


        //tourney
        this.load.image("tourney/dais", path + "tourney/dais.png");
        this.load.image("tourney/gift", path + "tourney/gift.png");

        //payments
        this.load.image("payments/button", path + "payments/button.png");
        this.load.image("payments/card_icon", path + "payments/card_icon.png");
        this.load.image("payments/phone_icon", path + "payments/phone_icon.png");
        this.load.image("payments/field", path + "payments/field.png");

        //map1



        //map2

        this.load.image("map1/cut_the_police", path + "maps/cut_the_police.png");


        //www only
        if (Config.Version.www) {
            this.load.spritesheet('cursor', path + 'cursor.png', 32, 32);


        }

        console.log("Preloader preloaded");
    }

    private preloaded: any = {};
    private loadImageForCuttable(map: number, name: string, x: number, y: number, width: number, height: number) {
        let sprite = this.load.spritesheet(name, this.path + map + ".png", width * Game.assetsSize / 2, height * Game.assetsSize / 2, 1, x * Game.assetsSize / 2, y * Game.assetsSize / 2);

        //   if (!this.preload[map])


        //   this.preloaded[map] = true;
    }

    private loadSfx() {
        new SoundBase(this).load();
    }

    public create() {
        let logoSlide = this.add.sound("sfx_menu/logo_slide1");
        new FX(this.game);

        let anim = (frame) => {
            if (frame == 11) {
                this.preloadBar.frameName = "logo0009";


                setTimeout(() => this.startingLobby(), 1450);
                return;
            }
            if (frame == 10)
                this.preloadBar.frameName = "logo00" + frame;
            else
                this.preloadBar.frameName = "logo000" + frame;

            setTimeout(() => anim(frame + 1), 59);
        }

        this.add.tween(this.hophandsLogo).to({ alpha: 1 }, 800).delay(300).start();
        setTimeout(() => {
            logoSlide.play();
            anim(1)
        }, 2500);
    }

    public startingLobby() {
        Player.get().sendDebugMsg("game_loaded");
        var me = this;
        FX.fade(this.game, "out", 800, () => {
            this.game.state.start('MainMenu', true);
            this.preloadBar.destroy();
            this.cache.removeImage("logo");
            setTimeout(() => {
                GUI.Message.showRandomInfoMessage();
            }, 1200);
            //    SleepManager.allowSleep();
            window.onerror = function (msg, url, lineNo, columnNo, error) {
                var string = msg.toLowerCase();
                var substring = "script error";
                if (string.indexOf(substring) == -1)
                    if (string.indexOf("'a'") == -1 && string.indexOf("'worldTransform.a'") == -1)
                        Server.Request.debug("js_error", { msg: msg, line: lineNo, obj: JSON.stringify(error), name: Player.get().data.username });
                return false;
            };

        }, 300);



        //   setTimeout(
        //      () => {  },
        //     2 * 1000 //3s
        //   );
    }
}