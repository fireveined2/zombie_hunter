﻿import { Game } from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"
import { Wall } from "../../utils/Wall"
import { GameEvents } from "../../game/logic/GameEvents"
import { Challenges } from "../../utils/Challenges"
import * as Models from "../../../../Shared/Shared";


export class Map{

    private map: Phaser.Image;
    private phaser: Phaser.State;
    public onSelected: (map: number) => void;


    private highlights: Phaser.Image[] = [];

    private highlight: Phaser.Image;

    constructor(private group: Phaser.Group) {
        let phaser = this.phaser = group.game.state.getCurrentState();

        this.map = phaser.add.image(Game.width * 0.04, Game.height, "map/map", 0, this.group);
        this.map.anchor.y = 1;

        for (let i = 0; i < 4;i++)
            this.enableMap(i);

        this.putQuestsMarks();
    }

    private putQuestsMarks() {
        let x = [103, 203, 280, 405];
        let y = [323, 167, 314, 193];
     
        for (let i = 0; i < x.length; i++) {
            let quests = Challenges.quests[i];
            if (quests && quests.length > 0) {
                let minLevel = 1000;
                for (let quest of quests)
                    minLevel = Math.min(minLevel, quest.data.minLevel);
                if (minLevel > Player.get().data.level)
                    continue;

                let mark = this.phaser.add.image(x[i] + this.map.left, y[i] + this.map.top, "map/mark", 0, this.group);
                mark.alpha = 0.8;
                mark.anchor.set(0);
            }
        }
    }

    private enableMap(index: number, full: boolean = false) {
        let x = [123, 185, 309, 405];
        let y = [323, 157, 309, 183];

        if (!full) {
            this.highlights[index] = this.phaser.add.image(x[index] + this.map.left, y[index] + this.map.top, "map/highlight", 0, this.group);
            this.highlights[index].anchor.set(0.5);
            this.highlights[index].inputEnabled = true;
            this.highlights[index].events.onInputDown.add(() => this._onSelected(index));
            this.highlights[index].alpha = 0.2;
        }
        else {
            if (this.highlight)
                this.highlight.destroy();
            this.highlight = this.phaser.add.image(x[index] + this.map.left, y[index] + this.map.top, "map/highlight", 0, this.group);
            this.highlight.anchor.set(0.5);
            this.highlight.inputEnabled = true;
            this.highlight.events.onInputDown.add(() => this._onSelected(index));
        }
    }

    private _onSelected(nr: number) {
        SFX.get().getSound("buttons/click").play();
        if (this.onSelected)
            this.onSelected(nr);
        this.enableMap(nr, true);

    }


    public update() {
        for (let id in this.highlights) {
            let image = this.highlights[id];
            image.scale.set(Math.sin(Date.now() / 1000) * 0.2 + 0.8);
        }

    }
}