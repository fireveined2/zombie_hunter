﻿import {Game} from ".././Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Challenges } from "../../utils/Challenges"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"
import { Wall } from "../../utils/Wall"
import { GameEvents } from "../../game/logic/GameEvents"
import { GameStarter } from "../GameStarter";

import { Map } from "./Map"

export class MapScreen extends Phaser.State {

    private background: Phaser.Image;
    private group: Phaser.Group;
    private wall: Wall;
    private map: Map;

    private play: Phaser.Image;
    private quests: Phaser.Image;
    private location: Phaser.Text;

    init(setMap?: number) {
        this.group = this.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;


        this.background = this.add.image(Game.width, Game.height * 0.1, 'background_menu', 0, this.group);
        this.background.anchor.x = 1;
        this.background.anchor.y = 0.3;



        this.map = new Map(this.group);

        FX.get().rain(this.group, 11);

        this.map.onSelected = this.onMapSelected.bind(this);
        let wall = new Wall(this, this.group, Player.get().data);
        wall.setMenuButton();
        wall.onButtonClick = this.returnToMainMenu.bind(this);

        if (setMap !== undefined)
            this.onMapSelected(setMap);

        let locationLabel = this.add.image(Game.width - 90, this.background.top + 225, "map/location", 0, this.group);
        locationLabel.anchor.set(0.5);
        let locationName = "WYBIERZ MAPĘ"
        let style = <Phaser.PhaserTextStyle>{ font: "20px CosmicTwo", fill: "white" };
        let location = this.add.text(-3, 10, locationName, style, this.group);
        location.anchor.set(0.5);
        location.angle = 16;
        locationLabel.addChild(location);
        this.location = location;

        this.play = this.add.image(this.background.left +671, this.background.top + 355, "map/play", 0, this.group);
        this.play.inputEnabled = true;
        this.play.visible = false;
        this.play.anchor.set(0.5);


        this.quests = this.add.image(this.background.left + 660, this.background.top +407, "map/quests", 0, this.group);
        this.quests.inputEnabled = true;
        this.quests.visible = false;
        this.quests.anchor.set(0.5);
    }


    private startGame(map: number) {
        let starter = new GameStarter(this.group);
        starter.startTraining(map);
    }

    private onMapSelected(map: number) {
        let loc = ["CMENTARZ", "KOMISARIAT", "SZPITAL", "HOPSTOP"];
        this.location.setText(loc[map]);

        this.quests.visible = false;
        this.play.visible = false;

        this.quests.events.onInputDown.addOnce(() => {
            SFX.get().getSound("buttons/click").play();
            this.showQuests(map)
        });
        this.quests.visible = true;

        if (Player.get().isMapUnlocked(map) || (map == 0 && !Player.get().logged))
        {
            this.play.visible = true;
            this.play.events.onInputDown.removeAll();
            this.play.events.onInputDown.addOnce(() => this.startGame(map));
        }
    }

    public update() {
        this.map.update();
    }

    public shutdown() {

    }

    private returnToMainMenu() {
        this.game.state.start('MainMenu', true, false, false);
    }

    private showQuests(map: number) {
        let quests = Challenges.quests[map];
        if (!quests)
            quests = [];
        this.game.state.start('ActiveEvents', true, false,  quests, "MapScreen", map);
    }
}