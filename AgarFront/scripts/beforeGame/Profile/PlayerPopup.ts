﻿import { Config } from "../../utils/Config"
import {Game} from "../Game"
import * as Models from "../../../../Shared/Shared";
import { ItemsHelpers } from "../../../../Shared/Items";
import * as GUI from "../../utils/gui/gui"
import { SFX } from "../../SFX/SFX"
import * as SocketModels from "../../../../Shared/SocketModels";
import { Player } from "../../utils/Player"
import { Challenge } from "../PvP/Challenge"
export class PlayerPopup {

    private phaser: Phaser.State;
    private bg: Phaser.Image;

    private name: GUI.Label;
    private level: Phaser.Text
    private icon: Phaser.Image

    private return: Phaser.Image;
    private challenge: Phaser.Image;
    private describe: Phaser.Text;

    public onClose: Function;

    constructor( private player: Models.PlayerInfo) {
        SFX.get().playSingleEffect("buttons/click");
        this.phaser = Game.group.game.state.getCurrentState();
        this.bg = this.phaser.add.image(0, -20, "gui/info_popup_bg", 0, Game.group);
        this.bg.inputEnabled = true;
        this.init();
        this.setInfo(player);
      
    }
    
    private setInfo(player: Models.PlayerInfo) {
       
        this.name.destroy();
        this.name = new GUI.Label(player.username, Game.width * 0.5, Game.height * 0.125, Game.group);
        this.name.anchor.x = 0.5;
        this.bg.addChild(this.name.element);



        this.level.setText("Poziom " + this.player.level);
        if (this.player.wearedItemsIds) {
            let head = this.player.wearedItemsIds[0];
            this.icon.loadTexture("items/" + head);
        }
        this.setDescribe();
    }

    private setDescribe() {
        let respect = this.player.respect;
        let status = ["Offline", "Online", "W grze"][this.player.status];

        let txt = respect + " respektu";
        txt += "\n" + status;
        this.describe.setText(txt);
    }

    private init() {
        this.name = new GUI.Label("", Game.width * 0.5, Game.height * 0.125, Game.group);
        this.name.anchor.x = 0.5;
        this.bg.addChild(this.name.element);

        let style = <Phaser.PhaserTextStyle>{ font: "21px CosmicTwo", fill: "black" };

        this.level = this.phaser.add.text(Game.width * 0.5, Game.height * 0.276, "", style);
        this.level.anchor.x = 0.5;
        this.bg.addChild(this.level);

        let frame = this.phaser.add.image(Game.width * 0.5, Game.height * 0.37 + 65, "eq/elements", "frame");
        frame.anchor.set(0.5);
        this.bg.addChild(frame);

        this.icon = this.phaser.add.image(Game.width * 0.5, Game.height * 0.37 + 65, "items/1")
        this.icon.anchor.set(0.5)
        this.bg.addChild(this.icon);

        style = <Phaser.PhaserTextStyle>{ font: "23px CosmicTwo", fill: "black", wordWrap: true, wordWrapWidth: 530, align: "center" }
        this.describe = this.phaser.add.text(Game.width * 0.5, this.icon.bottom + 10, "", style);
        this.describe.anchor.x = 0.5;
        this.bg.addChild(this.describe);

        this.return = this.phaser.add.image(Game.width * 0.95, Game.height * 0.08, "no")
        this.return.anchor.set(1, 0)
        this.return.inputEnabled = true;
        this.return.events.onInputDown.addOnce(() => {
            this.bg.destroy();
            if (this.onClose)
                this.onClose();
        });
        this.bg.addChild(this.return);

        if (this.phaser.cache.checkImageKey("profile/challenge") && parseFloat(Config.Version.ver) > 0.28)
        if (this.player.status == SocketModels.UserStatus.ONLINE && this.player.username != Player.get().data.username && Player.get().data.canMakePvP) {
            this.challenge = this.phaser.add.image(Game.width * 0.5, this.describe.bottom+30, "profile/challenge")
            this.challenge.anchor.set(0.5,0)
            this.challenge.inputEnabled = true;
            let prev = 0;
            this.challenge.events.onInputDown.add(() => {
                let elapsed = Date.now() - prev;
                if (elapsed > 500) {
                    prev = Date.now();
                    new Challenge().makeChallenge(this.player);
                }
            });
            this.bg.addChild(this.challenge);
        }
    }



}