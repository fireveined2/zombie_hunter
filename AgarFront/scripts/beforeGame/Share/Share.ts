﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"
import * as GUI from "../../utils/gui/gui";
import { GameStarter } from "../GameStarter";

import * as Models from "../../../../Shared/Shared";
import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"
import { Messages } from "../../utils/Messages"
import { Ranking } from "../Ranking/Ranking"
import { WebPage } from "../../utils/WebPage"

interface Navigator {
    screenshot: any;
}

export class Share extends Phaser.State {

 
    private group: Phaser.Group;
    private rank: Phaser.Group;
    private wall: Wall;
    private hero: HeroAnim;



    public create() {


       

        this.group = this.game.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.add.sprite(Game.width / 2, Game.height / 2, 'menu/bg', 0, this.group).anchor.set(0.5);



        this.wall = new Wall(this, this.group, Player.get().data);
        this.wall.hideHCN();
        this.wall.setMenuButton();
        this.wall.onButtonClick = () => this.returnToMainMenu();

        this.hero = new HeroAnim(this, this.group);
        this.hero.setItems(Player.get().getAllWearedItems());
        let x = 250 * Game.assetsSize;
        let y = Game.height - 45 * Game.assetsSize - this.hero.height;
        this.hero.position.set(x, y);
       

   
        FX.get().rain(this.group, 13);
        this.rank = Player.get().showNameLabel(this.group);

        this.initRecords();
        this.initScreenshot();

        FX.fade(this.game, "in", 800, () => {
            SFX.get().playOneOfEffect("slon/foto");

        }, 400);

        this.prevTime = Date.now();
        this.update();

  

    }

    private takeScreenshotFunc: Function;
    private initScreenshot() {
        let style = { font: "19px CosmicTwo", fill: "white" };
        let text = this.add.text(Game.width / 2, 10, "Kliknij, aby strzelić selfie", style, this.group);
        text.anchor.x = 0.5;
        text.setShadow(4, 4, 'rgba(0,0,0,0.5)', 5);

        let animText = () => {
            text.alpha = Math.round(Math.abs(Math.sin(Date.now() / 700)));
        }
        let interval = setInterval(animText, 40);
        this.takeScreenshotFunc = () => {
            text.visible = false;
            clearInterval(interval);
          

            setTimeout(() => {
                if (Config.Version.os == "android") {

                    (<any>navigator).screenshot.save(function (error, res) {
                        text.visible = true;
                        text.alpha = 1;
                        if (error) {
                            text.setText("Nie można zapisać zdjęcia");
                            console.error(error);
                        } else {
                            let path = (<string>res.filePath).split("/");

                            text.setText("Zapisano jako " + (path[path.length - 1]));
                            console.log('Zapisano jako: ', res.filePath);
                        }
                    });
                }
                // IOS *****
                else {

                    (<any>navigator).screenshot.save(function (error, res) {
                        text.visible = true;
                        text.alpha = 1;
                        if (error) {
                            text.setText("Nie można zapisać zdjęcia");
                            console.error(error);
                        } else {
                            let path = (<string>res.filePath).split("/");

                            text.setText("Zapisano jako " + (path[path.length - 1]));
                            console.log('Zapisano jako: ', res.filePath);
                        }
                    }, 'jpg', 90);

                }



            }, 1);

        }

        this.input.onDown.addOnce(this.takeScreenshotFunc);
    }

  

    private saveScreenshot() {

    }


    private initRecords() {
        let records = this.add.image(Game.width * 1, Game.height * 0.49, "share/records", 0, this.group);
        records.anchor.set(1, 0.5);

        let style = <Phaser.PhaserTextStyle>{ font: "23px CosmicTwo", fill: "white"};

        let x = records.left;
        let y = records.top;
        let r1 = this.add.text(99+x, 116+y, Player.get().records.get(0).toString(), style, this.group);
        r1.anchor.set(0, 0.5);
     

        r1 = this.add.text(99+x, 169+y, Player.get().records.get(2).toString(), style, this.group);
        r1.anchor.set(0, 0.5);

        r1 = this.add.text(99+x, 228+y, Player.get().records.get(1).toString(), style, this.group);
        r1.anchor.set(0, 0.5);
    }

    private prevTime;
    public update() {
        let now = Date.now();
        let elapsed = now - this.prevTime;
        this.prevTime = now;

        FX.get().update(elapsed / Config.Global.frameTime);
        this.hero.updateAnim();
    }

    public shutdown() {
        this.hero.kill();
    }
    private returnToMainMenu() {
        if (this.takeScreenshotFunc)
        this.input.onDown.remove(this.takeScreenshotFunc);
        FX.fade(this.game, "out", 800, () => this.game.state.start('MainMenu'));
    }
}

