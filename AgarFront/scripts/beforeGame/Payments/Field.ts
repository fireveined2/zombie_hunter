﻿import * as Models from "../../../../Shared/Shared";
import { ZHConfig } from "../../utils/ZHConfig"

export class PaymentField {

    protected phaser: Phaser.State;
    protected group: Phaser.Group;


    private field: Phaser.Image;

    constructor(x: number, y: number, parent: Phaser.Group, payment: Models.HCNBuyUnit) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = parent;

        let field = this.field =  this.phaser.add.image(x, y, "payments/field", 0, this.group);
        field.anchor.set(0.5);


        let style = <Phaser.PhaserTextStyle>{ font: "25px CosmicTwo", fill: "white", align: "center" };
        let freeEntrances = 0;
        if (ZHConfig.happyHours) {
            freeEntrances = payment.freeEntraces;
      

        }
        if (freeEntrances == 0) {
            let hcn_txt = this.phaser.add.text(0, -25, payment.hcn.toString(), style, this.group);
            hcn_txt.anchor.set(0.5);
            field.addChild(hcn_txt);

            hcn_txt = this.phaser.add.text(0, -16 - 5, "\nHCN", style, this.group);
            hcn_txt.anchor.set(0.5);
            field.addChild(hcn_txt);
        }
        else {
            style = <Phaser.PhaserTextStyle>{ font: "16px CosmicTwo", fill: "white", align: "center" };
            let hcn_txt = this.phaser.add.text(0, -37, payment.hcn.toString() +" HCN", style, this.group);
            hcn_txt.anchor.set(0.5);
            field.addChild(hcn_txt);



       
            style = <any>{ font: "16px Candara", fill: "white" };
            let free_entrances = this.phaser.add.text(0, -17, "" + freeEntrances+ " wejściówek", style, this.group);
            free_entrances.anchor.set(0.5);
            field.addChild(free_entrances);

            let mb = "Mystery Box";
            if (payment.hcn > 2000)
                mb = "2x Mystery Box";
            if (payment.hcn > 4000)
                mb = "4x Mystery Box";

            style = <any>{ font: "15px Candara", fill: "white" };
            free_entrances = this.phaser.add.text(0, 3,mb, style, this.group);
            free_entrances.anchor.set(0.5);
            field.addChild(free_entrances);

            let set = "";
            if (payment.hcn > 1000)
                set = "Pełny set Strażaka";
            if (payment.hcn > 2000)
                set = "Pełny set Rycerza";
            if (payment.hcn > 3900)
                set = "Pełny set Króla";

            if (set) {
                style = <any>{ font: "14px Candara", fill: "white" };
                free_entrances = this.phaser.add.text(0, 3+18, set, style, this.group);
                free_entrances.anchor.set(0.5);
                field.addChild(free_entrances);
            }
        }


        style = <any>{ font: "21px CosmicTwo", fill: "black" };
        let cost_txt = this.phaser.add.text(0, 41, payment.pln.toString() + " PLN", style, this.group);
        cost_txt.anchor.set(0.5);
        field.addChild(cost_txt);

        if (payment.bonus) {
            style.fill = "rgb(0,255,255)"
            style.font = "17px CosmicTwo";
            let bonus = this.phaser.add.text(3, -58, "+" + payment.bonus.toString() + "%", style, this.group);
            bonus.anchor.set(0.5);
            field.addChild(bonus);
        }
    }

    get bottom() {
        return this.field.bottom;
    }


    get anchor() {
        return this.field.anchor;
    }

    get element() {
        return this.field;
    }

    public destroy() {
        this.field.destroy();
    }


}