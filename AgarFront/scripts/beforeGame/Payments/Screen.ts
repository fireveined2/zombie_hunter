﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"
import * as GUI from "../../utils/gui/gui";
import { GameStarter } from "../GameStarter";

import * as Models from "../../../../Shared/Shared";
import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"
import { Messages } from "../../utils/Messages"


import { PaymentField } from "./Field"

export class Screen {


    public group: Phaser.Group;
    private card_group: Phaser.Group;
    private sms_group: Phaser.Group;

    private phaser: Phaser.State;
    private phone_icon: Phaser.Image;
    private card_icon: Phaser.Image;

    constructor(private parent: Phaser.Group, private onButtonClick: Function) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);



        let bg = this.phaser.add.image(0, 0, "menu/bg", 0, this.group);
        bg = this.phaser.add.image(0, 0, "screen_bg", 0, this.group);
        bg.inputEnabled = true;

        this.card_group = this.phaser.add.group(this.group);
        this.sms_group = this.phaser.add.group(this.group);

        this.initFields();
        this.initButton();
        this.initIcons();
    }

  
    private initButton() {
        let button = this.phaser.add.image(Game.width * 0.5, Game.height * 0.75, "payments/button", 0, this.group);
        button.anchor.set(0.5);
        button.inputEnabled = true;
        button.events.onInputDown.add(this.onButtonClick);
    }

    private initFields() {
        let sms: Models.HCNBuyUnit[] = [{ pln: 6, hcn: 30, freeEntraces: 9}, { pln: 10, hcn: 50, freeEntraces: 17 }, { pln: 20, hcn: 100, freeEntraces: 36 }];
        let card: Models.HCNBuyUnit[] = [{ pln: 10, hcn: 100, freeEntraces: 13 }, { pln: 25, hcn: 270, bonus: 5, freeEntraces: 35 }, { pln: 50, hcn: 545, bonus: 10, freeEntraces: 75 },
            { pln: 100, hcn: 1110, bonus: 10, freeEntraces: 160 }, { pln: 200, hcn: 2260, bonus: 10, freeEntraces: 350 }, { pln: 400, hcn: 4600, bonus: 15,  freeEntraces: 900 }];

        for (let i = 0; i < 3; i++) {
            let field = new PaymentField(200 + 200 * i, Game.height * 0.4, this.sms_group, sms[i]);
        }

        for (let i = 0; i < 6; i++) {
            let x = 200 + 200 * (i % 3);
            let y = Game.height * (0.22 + 0.3 *Math.floor(i/3)); 
            let field = new PaymentField(x, y, this.card_group, card[i]);
        }

    }

    private initIcons() {
        let card_icon = this.card_icon = this.phaser.add.image(Game.width * 0.05, Game.height * 0.75, "payments/card_icon", 0, this.group);
        card_icon.anchor.y = 0.5;
        card_icon.inputEnabled = true;
        card_icon.events.onInputDown.add(() => this.activateGroup("card"));

        let phone_icon = this.phone_icon = this.phaser.add.image(Game.width * 0.05 + card_icon.width+7, Game.height * 0.75, "payments/phone_icon", 0, this.group);
        phone_icon.inputEnabled = true;
        phone_icon.events.onInputDown.add(() => this.activateGroup("sms"));
        phone_icon.anchor.y = 0.5;

        this.activateGroup("card");
    }


    private activateGroup(group: "card" | "sms") {
        if (group == "card") {
            this.card_group.visible = true;
            this.sms_group.visible = false;
            this.card_icon.alpha = 1;
            this.phone_icon.alpha = 0.3;
        }

        if (group == "sms") {
            this.card_group.visible = false;
            this.sms_group.visible = true;
            this.card_icon.alpha = 0.3;
            this.phone_icon.alpha = 1;
        }
    }


}

