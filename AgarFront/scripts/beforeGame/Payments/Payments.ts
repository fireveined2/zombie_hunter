﻿import {Game} from "../Game"
import { FX } from "../../game/FX"
import { Config } from "../../utils/Config"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Player } from "../../utils/Player"
import * as Server from "../../utils/Server"
import { WebPage } from "../../utils/WebPage"

import { Wall } from "../../utils/Wall"
import { ScreenOrientation } from "../../utils/ScreenOrientation"
import { initLoginScreen } from "../../login"

import { Screen } from "./Screen"
import * as GUI from "../../utils/gui/gui";

export class Payments{

     private phaser: Phaser.State;
    private browser: any;
    private group: Phaser.Group;
    private wall: Wall;

    private screen: Screen;
    private overlay: Phaser.Graphics;

    private successfulOpen: boolean = false;
    private checkStatusTimeout: any;

    public onCloseCallback: Function;

    constructor(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);


        this.screen = new Screen(this.group, this.openPage.bind(this));

        this.wall = new Wall(this.phaser, this.group, Player.get().data);
        this.wall.setMenuButton();
        this.wall.onButtonClick = () => this.return();
    }


    private return() {
        if (this.onCloseCallback)
            this.onCloseCallback();
        this.group.destroy();
    }

    public openPage() {
        SFX.get().pause(true);
        let web: any = cordova;

        let options = ",hidden=yes";
        if (Config.Version.os == "android")
            options += ",hardwareback=yes";
      //  if (Config.Version.os == "ios")
        //    options += ",closebuttoncaption=Zamknij";


        this.browser = web.InAppBrowser.open('https://hophands.pl', '_blank', 'location=no' + options);
        this.browser.addEventListener('loadstop', (e) => { this.onLoaded(); console.log(e) });
        this.browser.addEventListener('loaderror', (error) => this.error(error));
        this.browser.addEventListener('exit', (err) => this.onClose(err));
        //  setTimeout(() => this.onLoaded(), 3000);

        this.createOverlay();
    
    }


    private createOverlay() {
        this.overlay = FX.createTextOverlay(Game.group.game, "Otwieranie płatności...");

        this.checkStatusTimeout = setTimeout(() => {
            this.onFailedOpenPayments();
        }, 7000);
    }


    private loadNumber = 0;
    private onLoaded() {
        clearTimeout(this.checkStatusTimeout);
        this.checkStatusTimeout = setTimeout(() => { this.onFailedOpenPayments(); }, 6000);

        if (this.loadNumber == 0) {
            let credintials = Player.get().savedCredentials;
            let code = `
                var userIsLogged = document.body.innerHTML.indexOf("Wyloguj") != -1;
                if(!userIsLogged){
                     (document.getElementsByName("_username").item(0)).value = '${credintials.email}';
                     (document.getElementsByName("_password").item(0)).value ='${credintials.password}';
                      (document.getElementById("login-submit-button")).click();
                }
                else
                    window.location.href = "https://hophands.pl/profile/finances";`;

            this.browser.executeScript({ code: code }, (result) => { console.log(result) });
        }

        if (this.loadNumber == 1) {


            let code = `
                function __returnValue(val){
                    return val;
                }
                var userIsLogged = document.body.innerHTML.indexOf("Wyloguj") != -1;
                if(!userIsLogged)
                    __returnValue("fail");
                if(window.location.href!='https://hophands.pl/profile/finances')
                    window.location.href = "https://hophands.pl/profile/finances";
                else
                    __returnValue("succes");`;

            this.browser.executeScript({ code: code }, (result) => {
                console.log(result);
                let status = result[0];
                if (status=="succes")
                    this.onSuccesfulOpenPayments();
                else
                if (status == "fail")
                    this.onFailedOpenPayments();
            });

           
        }

        if (this.loadNumber == 2) {
            this.onSuccesfulOpenPayments();
        }
        this.loadNumber++;
    }

    private onSuccesfulOpenPayments() {
        this.browser.show();
        (window.screen as any).orientation.lock('portrait');
        this.successfulOpen = true;
        clearTimeout(this.checkStatusTimeout);
    }

    private onFailedOpenPayments() {
        this.browser.close();
       // new GUI.Popup("Nie można otworzyć panelu płatności. Spróbuj ponownie!");

        GUI.Message.simple("Błąd płatności!", `Niestety w tym momencie otwarcie panelu płatności w aplikacji jest niemożliwe. <br><br>
Jeśli widzisz ten komunikat, odwiedź stronę internetową https://hophands.pl i zaloguj się przy użyciu swoich danych do logowania do Zombie Huntera, a następnie tam dokonaj płatności.<br><br>
Przepraszamy za utrudnienia!`, null, true);
    }

    private onClose(err?) {
        console.log("Closed");
        if(err)
        console.log(err);
        clearTimeout(this.checkStatusTimeout);
        this.successfulOpen = false;
        this.overlay.destroy();
        SFX.get().pause(false);

        (window.screen as any).orientation.lock('landscape');
        this.phaser.scale.refresh();
        (document.getElementById('game-window').style as any).height = (screen.height > screen.width) ? screen.width : screen.height;
        (document.getElementById('game-window').style as any).width = (screen.width > screen.height) ? screen.height : screen.width;
        this.loadNumber = 0;


    }

    private error(error) {
        this.onFailedOpenPayments();
        console.error(error);
    }

}