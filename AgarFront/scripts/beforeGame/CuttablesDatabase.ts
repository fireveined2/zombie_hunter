﻿import { Config } from "../utils/Config";
import { Phaser } from "../utils/phaser"

interface ICuttableInfo {
    frames: Phaser.Rectangle[];
}

interface ICuttableBaseInfo {
    size: number;
    type: "bomb" | "flesh" | "special";
}

export class CuttablesDatabase {

    static instance: CuttablesDatabase;

    constructor() {
        CuttablesDatabase.instance = this;
    }

    public sizes: Phaser.Rectangle[];
    public frames: Phaser.Rectangle[][];
    public points: number[] = [];

    private data: { [name: string]: ICuttableInfo } = {};
    private baseData: { [name: string]: ICuttableBaseInfo } = {};
    public afterLoadOne: (map: string, name: string, x: number, y: number, width: number, height: number) => void;
    private instant: boolean = false;

    private add(name: string, x: number, y: number, w: number, h: number) {
        this.data[name] = <ICuttableInfo>{};

        let frames = [];
        frames.push(new Phaser.Rectangle(x, y, w, h));
        frames.push(new Phaser.Rectangle(x, y, w / 2, h));
        frames.push(new Phaser.Rectangle(x + w / 2,y, w / 2, h));
        frames.push(new Phaser.Rectangle(x, y, w, h / 2));
        frames.push(new Phaser.Rectangle(x, y + h / 2, w, h / 2));
        this.data[name].frames = frames;
    }


  

    public loadOne(name: string, x: number, y: number, width: number, height: number, size: number = 1) {
        this.add(name, x / 2 * this.multipler, y / 2 * this.multipler, width / 2 * this.multipler, height / 2 * this.multipler);

        let map = this.currentMap;
        if (this.afterLoadOne)
            this.afterLoadOne(map, name, x * this.multipler, y * this.multipler, width * this.multipler, height * this.multipler);
            if (size < 4) {
                this.loadOne(name + "!1", x, y, width / 2, height, size + 1)
                this.loadOne(name + "!2", x + width / 2, y, width / 2, height, size + 1)
                this.loadOne(name + "!3", x, y, width, height / 2, size + 1)
                this.loadOne(name + "!4", x, y + height / 2, width, height / 2, size + 1)
            }

    }


    private curFleshNum = 1;
    private curBombNum = 1;
    private curBonusNum = 1;
    private currentMap = "map0";
    private multipler = 1;

    private loadFlesh(x: number, y: number, width: number, height: number, points: number = 0) {
        let num = this.curFleshNum;
        let name = this.currentMap + "/flesh"+num;
        this.loadOne(name, x, y, width, height);
      
        Config.Cuttable.points[name] = points;
        Config.Cuttable.fleshTypes[this.currentMap] = this.curFleshNum;
        this.curFleshNum++;
    }

    private loadBomb(x: number, y: number, width: number, height: number) {
        let name = this.currentMap + "/bomb" + this.curBombNum;
        this.loadOne(name, x, y, width, height);
        Config.Cuttable.bombTypes[this.currentMap]  = this.curBombNum;
        this.curBombNum++;
    }

    private loadBonus(x: number, y: number, width: number, height: number) {
        let name = this.currentMap + "/bonus" + this.curBonusNum;
        this.loadOne(name, x, y, width, height);
        Config.Cuttable.points[name] = 10;
        Config.Cuttable.bonusTypes[this.currentMap]  = this.curBonusNum;
        this.curBonusNum++;
    }

    private changeMap(map: string) {
            this.curFleshNum = 1;
            this.curBombNum = 1;
            this.curBonusNum = 1;
            this.currentMap = map;
    }

    private loadTextData(data: string) {

        let array = data.split(",");
        for (let i = 0; i < array.length; i += 6) {

            let name = array[i].replace(/\s/g, '');
            let x = parseInt(array[i + 1]);
            let y = parseInt(array[i + 2]);
            let width = parseInt(array[i + 3]);
            let height = parseInt(array[i + 4]);
            let points = parseInt(array[i + 5]);

            name = this.currentMap + "/"+name;
            this.loadOne(name, x, y, width, height);

            Config.Cuttable.points[name] = points;
            if (name.indexOf("flesh") != -1) {
                Config.Cuttable.fleshTypes[this.currentMap] = this.curFleshNum;
                this.curFleshNum++;
            }
            if (name.indexOf("bomb") != -1) {
                Config.Cuttable.bombTypes[this.currentMap] = this.curBombNum;
                this.curBombNum++;
            }
        }
    
    }

    public loadAll(onLoaded?: Function, instant: boolean = false) {
        this.instant = instant;

        this.currentMap = "map0";
        this.loadFlesh(180 * 2, 511 * 2, 177 * 2, 158 * 2);
        this.loadFlesh(0, 293, 210, 379, 15);
        this.loadFlesh(0, 674, 335, 380, 10);
        this.loadFlesh(839, 0, 359, 364, 10);
        this.loadFlesh(0, 0, 308, 291, 10);
        this.loadFlesh(839, 622, 317, 219, 15);
        this.loadFlesh(578, 674, 244, 337, 10);
        this.loadFlesh(212, 293, 89, 218, 20);
        this.loadFlesh(839, 843, 128, 188, 20);
        this.loadFlesh(310, 0, 246, 164, 20);
        this.loadFlesh(558, 584, 207, 88, 20);
        this.loadFlesh(310, 380, 159, 215, 15);
        this.loadFlesh(558, 294, 240, 288, 15);
        this.loadFlesh(337, 674, 239, 338, 10);

        this.loadBonus(969, 843, 229 + 60, 182 + 52);

        this.loadBomb(558, 0, 279, 292);

        ////////////
        this.multipler = 2 * 0.66;
        this.changeMap("map1");
        this.loadTextData(`bomb1,0,564,332,287,10,
bonus1,189,366,201,182,10,
bonus2,393,0,193,221,10,
flesh1,193,0,198,177,15,
flesh2,393,223,190,131,15,
flesh3,833,0,159,219,15,
flesh4,893,853,62,153,20,
flesh5,334,748,135,57,20,
flesh6,833,443,159,215,10,
flesh7,334,564,229,182,10,
flesh8,359,853,156,187,15,
flesh9,833,221,155,220,15,
flesh10,833,660,136,138,20,
flesh11,588,0,243,265,10,
flesh12,588,638,211,121,15,
flesh13,393,356,149,161,20,
flesh14,588,267,221,203,15,
flesh15,179,853,178,193,15,
flesh16,193,179,107,170,20,
flesh17,588,761,206,72,15,
flesh18,0,853,177,204, 15,
flesh19,517,853,189,184,15,
flesh20,0,178,155,186,15,
human1,588,472,232,164,10,
human2,0,366,187,196,10,
human3,0,0,191,176,10,
human4,708,853,183,184,10`);


        this.multipler = 2 * 0.65;
        this.changeMap("map2");
        this.loadTextData(`
bomb1,614,0,192,257,0,
bonus1,0,0,201,182,10,
bonus2,202,0,251,159,10,
flesh1,807,617,207,185,10,
flesh2,807,485,190,131, 15,
flesh3,454,378,159,219, 15,
flesh4,377,404,62,153, 20,
flesh5,202,277,135,57, 20,
flesh6,217,404,159,215, 15,
flesh7,221,623,229,182, 10,
flesh8,454,190,156,187, 15,
flesh9,0,183,155,220, 15,
flesh10,618,623,136,138, 20,
flesh11,0,404,216,218, 10,
flesh12,0,623,220,206, 10,
flesh13,807,0,210,306, 10,
flesh14,614,258,192,175, 15,
flesh15,202,160,231,116, 15,
flesh16,451,623,166,147, 20,
flesh17,454,0,159,189, 15,
flesh18,614,434,182,172, 15,
flesh19,807,307,162,177, 15`);


        this.multipler = 2;
        this.changeMap("map3");
        this.loadTextData(`
bomb1,0,327,122,69,0,
bomb2,246,327,122,69,0,
bomb3,123,327,122,69,0,
bonus1,618,531,118,107,5,
bonus2,203,397,85,133,5,
bonus3,117,397,85,133,5,
bonus4,309,141,85,133,5,
flesh1,115,175,121,108,15,
flesh2,618,129,145,137,10,
flesh3,0,175,114,151,15,
flesh4,398,138,94,185,10,
flesh5,618,0,153,128,15,
flesh6,494,274,115,139,15,
flesh7,618,267,122,115,20,
flesh8,494,136,104,137,20,
flesh9,309,0,88,140,15,
flesh10,114,0,87,172,15,
flesh11,289,397,183,125,10,
flesh12,347,534,215,83,10,
flesh13,233,534,113,134,15,
flesh14,202,0,106,168,15,
flesh15,0,534,126,154,15,
flesh16,494,0,123,135,20,
flesh17,494,414,102,116,10,
flesh18,0,0,113,174,15,
flesh19,127,534,105,134,15,
flesh20,0,397,116,136,15,
flesh21,398,0,95,137,20,
flesh22,618,383,93,147,15`);

        if (onLoaded) 
            setTimeout(onLoaded, 2000);
    }


    public get(name: string): ICuttableInfo {
        if (this.data[name])
            return this.data[name];
        else {
           
            console.error("Can't find cuttable info: " + name);
            return this.data["map0/flesh1"];
        }
    }
}