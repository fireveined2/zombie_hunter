﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"
import * as GUI from "../../utils/gui/gui";
import { GameStarter } from "../GameStarter";

import * as Models from "../../../../Shared/Shared";
import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"
import { Buttons } from "./Buttons"
import { Messages } from "../../utils/Messages"
import { Ranking } from "../Ranking/Ranking"
import { WebPage } from "../../utils/WebPage"
import { Challenges } from "../../utils/Challenges"
import { socketClient } from "../../utils/server/Socket"
declare var init_rules;
export class MainMenu extends Phaser.State {

 
    private group: Phaser.Group;
    private rank: Phaser.Group;

    private buttons: Buttons;

    private wall: Wall;
    private screens: Phaser.Image[] = [];
    private clickSfx: Phaser.Sound;
    private settings: Settings;



    private updateHandler;
    private prevTime = Date.now();

    private heroUpdateFnc: Function;
    private heroUpdateTime;

    private fadeIn: boolean = true;

    private boards: BoardManager;
    private hero: HeroAnim;

   


    private initScreens() {
        this.screens[3] = this.add.image(10, 10, "tourneys", 0, this.group);
        this.screens[3].visible = false;
        this.screens[3].scale.set(Game.height / 600)
        this.screens[3].x = Game.width * 0.383 - this.screens[3].width / 2;
        this.screens[3].y = Game.height / 2 - this.screens[3].height / 2;


        this.boards = new BoardManager(this.group);
    }



    private createMenuElements() {

        this.wall = new Wall(this, this.group, Player.get().data);

   
        this.hero = new HeroAnim(this, this.group);
        this.hero.setItems(Player.get().getAllWearedItems());
        let x = 400* Game.assetsSize;
        let y = Game.height - 45 * Game.assetsSize - this.hero.height;
        this.hero.position.set(x, y);

        this.rank = Player.get().showRankLabel(this.group);
        let inbox = new Messages(this, this.group);
    }

    public init(fadeIn: boolean = true) {
        this.fadeIn = fadeIn;
    }

    public create() {
        socketClient.game.end();

        if (!SFX.get())
            SFX.setSFX(new SFXMenu(this.game));

        this.group = this.game.add.group();
        this.group.scale.setTo(1 / Game.assetsScale);
        Game.group = this.group;

        this.add.sprite(Game.width / 2, Game.height / 2, 'menu/bg', 0, this.group).anchor.set(0.5);


   

        this.createMenuElements();
   

        FX.get().rain(this.group, 13);
        this.buttons = new Buttons(this.group);


        this.initScreens();
        this.bindButtons();

        if (this.fadeIn)
            FX.fade(this.game, "in", 800, null, 400);

        this.prevTime = Date.now();
        this.update();
    }

    private bindButtonToBoard(events: Phaser.Events, board) {
        events.onInputDown.add(() => {
            if (this.isLocked())
                return;

            this.showElements(false);
           let closeOnMouseUp = this.boards.open(board, () => {
                this.showElements(true);
            });

           if (closeOnMouseUp)
               events.onInputUp.addOnce(() => this.boards.closeAll());
        }, this);

    
    }

    private bindButtons() {

        this.buttons.getSignal("play").add(this.startGame, this);
        this.buttons.getSignal("challenges").add(this.openEvents, this);
        this.buttons.getSignal("eq").add(this.openEQ, this);

        this.buttons.getSignal("share").add(() => this.openShareScreen());


        this.buttons.getSignal("turnieje").add(() => this.openTourneys());


        this.buttons.getSignal("help").add(() => {
            WebPage.load("zasady.html", () => init_rules(), false, true);
            
        });

        this.buttons.getSignal("tworcy").add(() => {
            this.showElements(false);
            let authors = this.add.image(Game.width / 2, Game.height / 2, "authors", 0, this.group);
            authors.anchor.set(0.5);
            authors.inputEnabled = true;
            authors.events.onInputDown.addOnce(() => {
                authors.destroy();
                this.showElements(true);
            });
        });

        this.wall.onButtonClick = () => {
            this.openRanking();
        }

        if (!Player.get().logged)
            this.buttons.getSignal("login").add(this.openLoginScreen, this);
        else
            this.buttons.getSignal("login").add(this.logout, this);

       // let down
    //    this.bindButtonToBoard(this.buttons.events("eq"), );
    //    this.bindButtonToBoard(2, 0);
   //     this.bindButtonToBoard(4, 1);



        this.buttons.getSignal("settings").add(() => {
            if (this.isLocked())
                return;
            this.showElements(false);
            this.settings = new Settings(this.game, this.group);
            this.settings.getSettings((settings) => {
                Game.settings = settings;
                Player.get().saveSettings(settings);
                this.showElements(true);
                delete this.settings;
            });
        }, this);
    }


    private rankingTimer: any = -1;

    private openRanking() {
        if (!Player.get().logged) {
            new GUI.Popup("Zaloguj się, aby zobaczyć ranking");
            return;
        }

        if (!Player.get().status.online) {
            new GUI.Popup("Brak połączenia z serwerem");
            return;
        }

        if (this.rankingTimer != -1)
            return;

        this.rankingTimer = setTimeout(() => {
            this.rankingTimer = -1;
        }, 5000);

        Player.get().getRanking((rows: Models.RankingRow[]) => {

                this.rankingTimer = 0;
                this.game.state.start('Ranking', true, false, rows)
           
        } );
       
    }


    private isLocked(): boolean {
        return this.settings != undefined || this.boards.isOpened();
    }

    private showElements(show: boolean) {
        this.hero.visible = show;
        this.wall.visible = show;
        if (this.rank)
        this.rank.visible = show;
        this.buttons.group.visible = show;
    }


    private logout() {
        Player.get().logout();
        this.game.state.start('MainMenu'); 
    }

    public update() {
        let now = Date.now();

        let elapsed = now - this.prevTime;
        this.prevTime = now;

        FX.get().update(elapsed / Config.Global.frameTime);


        if (this.hero)
            this.hero.updateAnim();
    }

  
    private buttonsOver: boolean[] = [];


    private remove() {
        clearTimeout(this.updateHandler);
        if (this.settings)
            this.settings.remove();
        this.hero.kill();
    }


    private openLoginScreen() {
        this.remove();
        this.game.state.start('LoginScreen'); 

    }

    private openMyProfile() {
        if (!Player.get().logged) {
            return;
        }

    //    new Profile(this.group);
    }

    private openEvents() {
        this.remove();
        this.game.state.start('ActiveEvents');
    }

    private openTourneys() {
        /*
        this.remove();
        FX.fade(this.game, "out", 600, () => this.game.state.start('TourneyList', true, false));
        */
        if (!Player.get().logged) {
            new GUI.Popup("Zaloguj się, aby dołączyć do turnieju")
            return;
        }

        let available = false;
        if (!Challenges.tourneys[0]) {
            new GUI.Popup("Kolejne turnieje już wkrótce!")
            return;
        }
        this.remove();
        FX.fade(this.game, "out", 800, () => this.game.state.start('Tourneys', true, false, Challenges.tourneys[0]));

    }

    private openEQ() {
        if (!Player.get().logged) {
            GUI.Message.youHaveToBeLogged();
            return;
        }

        this.remove();
        this.game.state.start('EQ');
    }

    private openShareScreen() {
        if (!Player.get().logged) {
            new GUI.Popup("Zaloguj się, aby pochwalić się osiągnięciami w grze!")
            return;
        }
        FX.fade(this.game, "out", 800, () => this.game.state.start('Share'));
    }
    public startGame() {
        if (Config.Version.mobile || Config.Global.skipLobby) {
            if (GameStarter.starting)
                return;


            this.remove();
            this.game.state.start('MapScreen')
            return;
        }
    }

   
}

