﻿import { Game } from "../Game"
import { FX } from "../../game/FX"
import { SFX, SFXMenu } from "../../SFX/SFX"
import { Config } from "../../utils/Config";
import { Settings } from "../Settings";
import { Player } from "../../utils/Player"
import { BoardManager } from "../Boards/BoardManager"

import { GameStarter } from "../GameStarter";


import { HeroAnim } from "../../utils/HeroAnim"
import { Wall } from "../../utils/Wall"
export class Buttons {

    private phaser: Phaser.State;
    public group: Phaser.Group;

    private buttons: { [name: string]: Phaser.Image } = {};

    private loginButton: Phaser.Image;

    constructor(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.group = this.phaser.add.group(parent);

        let click = SFX.get().getSound("buttons/click");

        let buttons = ["help", "settings", "tworcy", "share", "play", "challenges", "turnieje", "eq"];
        let x = [238, 155, 152, 221, 570, 625, 631, 585];
        let y = [70, 174, 297, 420, 71, 185, 303, 429].map((val) => val * Game.height / 600);

        let i = 0;
        for (let button of buttons) {
            let img = button;
            if (button == "turnieje" && this.phaser.cache.checkImageKey("buttons/turnieje_new")) {
                img = "turnieje_new";
            }
            this.buttons[button] = this.phaser.add.image(x[i], y[i], "buttons/" + img,  0, this.group);
            this.buttons[button].anchor.set(0.5);
            this.buttons[button].inputEnabled = true;
            this.buttons[button].events.onInputDown.add(() => click.play());
            i++;
        }

        let button = "login";
        if (Player.get().logged)
            button = "logout";
            this.loginButton = this.phaser.add.image(Game.width + 7, Game.height * 0.02, "buttons/"+button, 0, this.group);
            this.loginButton.anchor.set(1, 0);
            this.loginButton.inputEnabled = true;
            this.loginButton.events.onInputDown.add(() => click.play());
  
    }


    public getSignal(button: "eq" | "settings" | "play" | "help" | "turnieje" | "share" | "challenges" | "tworcy" | "login") {
        console.log(button);
        if (button == "login")
            return this.loginButton.events.onInputDown;
        else
            return this.buttons[button].events.onInputDown;
    }

    public events(button: "eq" | "settings" | "play" | "help" | "turnieje" | "share" | "challenges" | "tworcy" | "login") {
        if (button == "login")
            return this.loginButton.events;
        else
            return this.buttons[button].events;
    }
}

