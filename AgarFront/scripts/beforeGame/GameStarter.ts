﻿import { Game } from "./Game"
import { FX } from "../game/FX"
import { SFX } from "../SFX/SFX"
import { Config } from "../utils/Config";
import * as GUI from "../utils/gui/gui"
import * as Server from "../utils/server"
import { Player } from "../utils/Player";
import { Challenges } from "../utils/Challenges";
import { SoundBase } from "../SFX/SoundBase"
import { Tips } from "../utils/Tips"

export class GameStarter {

    private phaser: Phaser.State;
    private overlay: Phaser.Graphics;
    private accesToPlay: Server.AccesToPlay;

    private ready: boolean = false;
    static starting: boolean = false;
    public onStart: Function;
    private pvp: boolean = false;
    constructor(parent: Phaser.Group) {
        this.phaser = parent.game.state.getCurrentState();
        this.overlay = FX.createOverlay(this.phaser);

        this.accesToPlay = null;
        this.ready = false;
        this.sound = [];
    }


    public startChallenge(request: Server.IGameAccesRequest) {
        if (Player.get().data.banned)
            return;

        Player.get().startChallenge(request, this.onAccesToPlay.bind(this), this.onError.bind(this));
        this.prepare(Challenges.get(request.challengeId).data.config.map);
    }

    public startPvP(request: Server.IGameAccesRequest) {
      //  if (Player.get().data.banned)
        //    return;
        this.pvp = true;
        Player.get().startPvP(request, this.onAccesToPlay.bind(this), this.onError.bind(this));
        this.prepare(0);
    }


    private onError(error: Server.ChallengeError) {
        if (this.map != -1) {
            this.phaser.load.reset(undefined, true);
            GameStarter.removeAssetsForMap(this.phaser, this.map);
        }
        GameStarter.starting = false;
        this.overlay.destroy();
        new GUI.Popup("Nie można połączyć się z serwerem!");
    }

    public startTraining(map: number) {
        Player.get().startTraining(map, this.onAccesToPlay.bind(this), this.onError.bind(this));
        this.prepare(map);
    }
    private map: number = -1;
    private prepare(map: number): Phaser.Signal {
        this.map = map;
        this.mapLoaded = false;
      setTimeout(()=> this.loadAssetsForMap(map), 1200);

        GameStarter.starting = true;
        let tween = this.phaser.add.tween(this.overlay).to({ alpha: 1 }, 1800).start();
        SFX.get().deactivate(1400);
        SFX.get().playSingleEffect("menu/coffin");
        tween.onComplete.addOnce(() => {
            setTimeout(() => this.onReady(), 1900);
        });
        return tween.onComplete;
    }

    private mapLoaded: boolean = false;
    private sound: string[] = [];
    private loadAssetsForMap(map: number) {
        let path = "assets/s1/";
        SoundBase.instance.phaser = this.phaser;
        let soundtracksVolume = 0.68;

      //  this.phaser.load.image("background"+map, path + "bg"+map+".jpg");

        if (map == 2) {

        //   this.sound.push("sfx_map2/soundtracks/normal1");
        //    this.sound.push("sfx_map2/soundtracks/trip1");
        //  this.sound.push("sfx_map2/soundtracks/rage1");
//
        //    SoundBase.instance.add("map2/soundtracks/rage", null, soundtracksVolume + 0.2);
       //    SoundBase.instance.add("map2/soundtracks/trip", null, soundtracksVolume);

 
       //     SoundBase.instance.add("map2/soundtracks/normal", null, soundtracksVolume);
        }

        if (map == 1) {
          
         //   SoundBase.instance.add("map1/soundtracks/rage", null, soundtracksVolume + 0.2);
        //    SoundBase.instance.add("map1/soundtracks/ctp", null, soundtracksVolume);

         //   this.sound.push("sfx_map1/soundtracks/normal1");
         //   this.sound.push("sfx_map1/soundtracks/rage1");
       //     this.sound.push("sfx_map1/soundtracks/ctp1");
         //   SoundBase.instance.add("map1/soundtracks/normal", null, soundtracksVolume);
        }

        if (map == 0) {
      //      this.sound.push("sfx_soundtracks/normal1");
        //    this.sound.push("sfx_soundtracks/rage1");

        //    SoundBase.instance.add("soundtracks/rage", null, soundtracksVolume + 0.2);
        //    SoundBase.instance.add("soundtracks/normal", null, soundtracksVolume);
        }




       // this.phaser.load.start();
       // this.phaser.load.onLoadComplete.addOnce(() => {    
            this.mapLoaded = true;
      //  });
  
    }


    static removeAssetsForMap(phaser: Phaser.State, map: number) {
        if (phaser)
            return;

        if (map == 2) {
            SoundBase.removeSound("map2/soundtracks/normal");
            SoundBase.removeSound("map2/soundtracks/rage");
            SoundBase.removeSound("map2/soundtracks/trip");
        //    phaser.cache.removeImage("map2/elements");
        //    phaser.cache.removeImage("trip_label");
        }

        if (map == 1) {
            SoundBase.removeSound("map1/soundtracks/normal");
            SoundBase.removeSound("map1/soundtracks/rage");
            SoundBase.removeSound("map1/soundtracks/ctp");
           // phaser.cache.removeImage("map1/elements");
          //  phaser.cache.removeImage("trip_label");
        }


        if (map == 0) {
            SoundBase.removeSound("soundtracks/normal");
            SoundBase.removeSound("soundtracks/rage");
           // phaser.cache.removeImage("map2/elements");
          //  phaser.cache.removeImage("trip_label");
        }
       // phaser.cache.removeImage("background" + map);
    }

    private onReady() {
        this.ready = true;

        if (this.accesToPlay)
            this.startGame();
    }

    private onAccesToPlay(acces: Server.AccesToPlay) {
        this.accesToPlay = acces;

        if (this.ready)
            this.startGame();
    }

    private startGame(tries = 0) {
        if (tries > 60)
            return;

        if (!this.mapLoaded) {
            setTimeout(() => this.startGame(tries++), 100);
            return;
        }

        for (let sound of this.sound) 
            if (!this.phaser.cache.isSoundDecoded(sound)){
                setTimeout(() => this.startGame(tries++), 100);
                return;
            }


        let start = () => {
            GameStarter.starting = false;
            this.phaser.game.state.start('MainGame', true, false, this.accesToPlay, this.pvp);
        }
        if (this.onStart)
            this.onStart();
        if (this.pvp)
            setTimeout(start, 100);
        else
            Tips.showRandom(start);
    }



}

