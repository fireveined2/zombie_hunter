﻿import { Game } from "./Game"
import { Config } from "../utils/Config";
import { FX } from "../game/FX"
import { SoundBase } from "../SFX/SoundBase"
import { SFX, SFXMenu } from "../SFX/SFX"
import { CuttablesDatabase } from "./CuttablesDatabase"
import { Player } from "../utils/Player";
import * as Server from "../utils/Server/Offline";

import { FileUtils } from "../loader/FileUtils";


export class AssetLoader {


    constructor(private state: Phaser.State) {

        if (this.utilsExists() && !FileUtils.get())
            FileUtils.create();

    }

    private utilsExists() {
        return FileUtils.get;
    }

    public image(name: string, path: string) {
        if (this.utilsExists()) 
            path = FileUtils.get().getURL(path);
        this.state.load.image(name, FileUtils.get().getURL(path));

    }

}