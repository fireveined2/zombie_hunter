﻿import { Preloader } from "./Preloader";
import { Boot } from "./Boot";
import { LoginScreen } from "./Login/Login";
import { ActiveEventsList } from "./Events/ActiveEvents";

import { MainMenu } from "./MainMenu/MainMenu";
import { MainGame } from "../game/MainGame";
import { FX } from "../game/FX";
import { MapScreen } from "./MapScreen/MapScreen";
import { FinalScore } from "./Final/FinalScore";
import { EQ } from "./EQ/EQ";
import { Config } from "../utils/Config";
import { GameStats } from "../game/logic/GameStats";
import { GameSettings } from "./Settings";
import { SFX } from "../SFX/SFX";
import { UserAbsence } from "../utils/UserAbsence";
var request = require('superagent');
import * as Server from "../utils/Server";
import { Collector } from "../game/InputCollector";
import { Player } from "../utils/Player";
import { Share } from "./Share/Share";
import { Payments } from "./Payments/Payments";

import { TourneyList } from "./Tourneys/Tourneys";
import { Tourney } from "./Tourneys/Tourney";
import { SleepManager } from "../utils/SleepManager";
import { AssetLoader } from "./AssetLoader";
import { Ranking } from "./Ranking/Ranking";
import { PvPLobby } from "./PvP/Lobby/Lobby";
import { PvPFinalScore } from "./PvPFinal/FinalScore";

export class Game extends Phaser.Game {

    static instance: Game;
    static aspectRatio: number;
    static assetsScale: number;
    static assetsSize: number;
    static width: number;
    static height: number;
    static realWidth: number;
    static realHeigt: number;

    static group: Phaser.Group;

    static pause: Function;
    static resume: Function;
    static absence: UserAbsence;
    static paused: boolean = false;
    static stats: GameStats = new GameStats();
    static inputCollector: Collector;

    static backButtonCallback;

    static settings: GameSettings = {
        effectLevel: Config.Version.www?2:0,
        side: "right",
        under18: true
    }
    ;
    static disableAnims: boolean;

    constructor() {
        SleepManager.keepAwake();
        new AssetLoader(null);

        Game.absence = new UserAbsence();
        Game.inputCollector = new Collector();

        let targetWidth = Config.Display.targetWidth;
        let targetHeight = Config.Display.targetHeight;

        let width = window.innerWidth;
        let height = window.innerHeight;
        if (Config.Version.www)
            width = 1000, height = 600;

        let wx = width * ( window.devicePixelRatio);
        let hx = height* (window.devicePixelRatio  );
        let w = wx > hx ? wx : hx;
        let h = wx > hx ? hx : wx;

        if (!Config.Version.mobile)
            w = targetWidth, h = targetHeight;

        let config = <Phaser.IGameConfig>{
            transparent: true,
            width: w,
            height: h,
            renderer: Phaser.WEBGL,
            parent: 'game-window',
            forceSetTimeOut: Config.Version.www
        };

        super(config);
        console.log("GAME NEW");

        Game.realWidth = w;
        Game.realHeigt = h;
        Game.aspectRatio = targetWidth / w;
        Game.assetsSize = Math.round(1 / Game.aspectRatio);
        Game.assetsSize = Math.max(Game.assetsSize, 1);
        Game.assetsSize = Math.min(Game.assetsSize, 2);
        Game.assetsSize = 1;
        Game.assetsScale = Game.aspectRatio * Game.assetsSize;

        Game.width = w * Game.assetsScale;
        Game.height = Math.round( h * Game.assetsScale);

        console.log("Size: " + w + "x" + h);
        console.log("Game: " + Game.width + "x" + Game.height);
        console.log("DPR: " + devicePixelRatio);
        console.log("Scale: " + Game.assetsScale);
        console.log("Ratio: " + Game.aspectRatio);
        console.log("Assets Size: " + Game.assetsSize);

        this.state.add('Boot', Boot, false);
        this.state.add('Preloader', Preloader, false);
        this.state.add('MainMenu', MainMenu, false);
        this.state.add('MainGame', MainGame, false);
        this.state.add('FinalScore', FinalScore, false);
        this.state.add('LoginScreen', LoginScreen, false);
        this.state.add('ActiveEvents', ActiveEventsList, false);
        this.state.add('MapScreen', MapScreen, false);
        this.state.add('EQ', EQ, false);
        this.state.add('Share', Share, false);
       //his.state.add('Payments', Payments, false);
        this.state.add('Tourneys', Tourney, false);
        this.state.add('TourneyList', TourneyList, false);
        this.state.add('Ranking', Ranking, false);
        this.state.add('PvPLobby', PvPLobby, false);
        this.state.add('PvPFinalScore', PvPFinalScore, false);

        Game.instance = this;

        Game.settings = Player.get().loadSettings();
        Game.disableAnims = Game.settings.effectLevel == 0;
   // Server.Request.login({ email: "artnovium@gmail.com", password: "qwerty" }, (a) => console.log(a), (b) => console.log(b));
        this.state.start('Boot');

        // Listen for resize changes
        setTimeout(() => {
            window.addEventListener("resize", () => {
               // alert(JSON.stringify(this));
                this.scale.refresh();
            }, false);

            window.addEventListener("orientationchange", () => {
                this.scale.refresh();
            }, false);

        }, 50);

        if ((window as any).FirebasePlugin)
        (window as any).FirebasePlugin.grantPermission();
      //  this.input..add(() => Game.lastClickTime = Date.now());
    }

    public resize(factor: number) {
        let width = window.innerWidth;
        let height = window.innerHeight;
        if (Config.Version.www)
            width = 1000, height = 600;

        let wx = width* (factor == 1 ? 1 : window.devicePixelRatio);
        let hx = height * (factor == 1 ? 1 : window.devicePixelRatio);
        let w = wx > hx ? wx : hx;
        let h = wx > hx ? hx : wx;

        this.renderer.resize(w, h);
        this.scale.setGameSize(w, h);
        this.scale.refresh();
        this.canvas.width = w;
        this.canvas.height = h;


        Game.realWidth = w;
        Game.realHeigt = h;
        Game.aspectRatio = Config.Display.targetWidth / w;
        Game.assetsSize = Math.round(1 / Game.aspectRatio);
        Game.assetsSize = Math.max(Game.assetsSize, 1);
        Game.assetsSize = Math.min(Game.assetsSize, 2);
        Game.assetsSize = 1;
        Game.assetsScale = Game.aspectRatio * Game.assetsSize;

        Game.width = w * Game.assetsScale;
        Game.height = h * Game.assetsScale;

        console.log("Size: " + w + "x" + h);
        console.log("Game: " + Game.width + "x" + Game.height);
        console.log("DPR: " + devicePixelRatio);
        console.log("Scale: " + Game.assetsScale);
        console.log("Ratio: " + Game.aspectRatio);
        console.log("Assets Size: " + Game.assetsSize);
    }


    static pause_base() {
        if (!Game.group || !Game.group.game)
            return;

        Game.paused = true;
        Game.group.game.gamePaused(undefined);

        if (Game.pause)
            Game.pause();

        if (SFX.get())
            SFX.get().pause(true);
    }
    static resume_base() {
        if (!Game.group || !Game.group.game)
            return;
        let game = Game.group.game;
        setTimeout(() => {
            game.lockRender = false;
            FX.showPauseOverlay(game, resume);
        }, 0);

        let resume = () => {
            game.gameResumed(undefined)
            Game.paused = false;
        
            if (Game.resume)
                Game.resume();

            if (SFX.get())
                SFX.get().pause(false);
       };
    }
}
