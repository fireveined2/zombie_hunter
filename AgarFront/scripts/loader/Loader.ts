﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397705
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

import { Game } from "../beforeGame/Game"
import { Config } from "../utils/Config"
import { Update } from "./Update"
import { AssetsDownloader } from "./AssetsDownloader"
import { LoaderScreen } from "./Screen"
import { FileUtils } from "./FileUtils"
import * as Server from "../utils/Server/Offline"

declare var AndroidFullScreen;
declare var ZombieHunter;
declare var Screen: any;

export class Loader {

    static version = "0.1.1";

    private screen: LoaderScreen;
    private shouldLoadDefaultAppJs = false;

    constructor() {
        FileUtils.create();

        window.onerror = function (msg, url, lineNo, columnNo, error) {
            var string = msg.toLowerCase();
            var substring = "script error";
            if (string.indexOf(substring) ==-1) 
                Server.Request.debug("js_error", { msg: msg, line: lineNo, obj: error });
            return false;
        };

        $("#loader").css("visibility", "visible");
        let os = (typeof device !== "undefined")?device.platform: "browser";
        if (os == "Android")
            Config.Version.os = "android";
        if (os == "iOS")
            Config.Version.os = "ios";

        if (os == "WinCE")
            Config.Version.os = "windows";

        if (os == "browser")
            Config.Version.os = "android";



        this.screen = new LoaderScreen();
        this.screen.setStatus("seeking");

        if (os!="browser")
            (window.screen as any).orientation.lock('portrait');
        if (typeof AndroidFullScreen !== 'undefined')
            AndroidFullScreen.immersiveMode(() => console.log("Immersive Mode: succes"), (error) => console.warn("Immersive Mode: ", error));


        // document.addEventListener("backbutton", onBackKeyDown, false);

        (document.getElementById('game-window').style as any).height = (screen.height > screen.width) ? screen.width : screen.height;
        (document.getElementById('game-window').style as any).width = (screen.width > screen.height) ? screen.height : screen.width;

        this.initDefaultAppJsHack();

        let run = (path) => {
            if (this.shouldLoadDefaultAppJs)
             path = "scripts/app.js";
            console.log("Running game from " + path);
            this.screen.setStatus("done");
            $.getScript(path, (script, status, obj) => {
                console.log(status, obj);         
            });
        };
        let alreeadyRun = false;


        $.ajaxSetup({ timeout: 13000 });
        var emulated = (window as any).tinyHippos != undefined || os == "browser";
        if (!emulated) {
            new Update((url, error, version) => {
            
                $.ajaxSetup({ timeout: 10000 });
                run(url);
  
             if (!error || error == "no_error")
                  this.startDownloadingAssets();
             else
                    this.enableGame();

                if (error == "cant_download")
                    this.screen.setStatus("error_while_downloading");
                if (error == "cant_save")
                    this.screen.setStatus("error_while_saving");
            });
        
        }
        else {
            run("scripts/app.js");
            this.screen.setStatus("done");
            this.enableGame();
        }

       

     
    }


    private initDefaultAppJsHack() {
        let clicks = 0;
        let timeout;
        $(".header").click(() => {
            if (this.shouldLoadDefaultAppJs)
                return;
            console.log(clicks);
            clearTimeout(timeout);
            clicks++;
            timeout = setTimeout(() => clicks = 0, 1000);
            if (clicks == 10) {
                console.log("Loading default app.js");
                this.shouldLoadDefaultAppJs = true;
                $(".content").hide();
                setTimeout(() => $(".content").show(), 400);
            }
        });

    }

    private enableGame() {
 
        this.screen.onPlayButtonClick = () => {
            this.screen.onPlayButtonClick = () => { };
            var event = new Event('runZH');

            $("#loader").fadeOut(800, () => {
                document.dispatchEvent(event);
                $("#loader").detach();
           
            });  
        };
    }

    private startDownloadingAssets() {
        this.screen.setStatus("preparing");

        let downloader = new AssetsDownloader();
        downloader.onListDownloaded = () => this.screen.setStatus("downloading", 0);

        downloader.onEnd = () => {
            console.log("Downloading ended. Enabling gmae.");
            this.enableGame();
            this.screen.setStatus("done");
        };


        downloader.onOneDownloaded = (asset, data, next, progress) => {
            FileUtils.get().save(asset.path, data, () => { });
                this.screen.setStatus("downloading", progress);
            }
        }
}

document.addEventListener('runLoader', () => new Loader(), false);