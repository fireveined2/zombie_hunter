﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397705
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.


import { LoaderURL } from "./LoaderURL"

import { Notifications } from "./Notifications"




export class Starter {

    private isBrowser = false;
    constructor() {
        this.isBrowser = location.href.indexOf("browser") != -1;
        if (!this.isBrowser)
            document.addEventListener('deviceready', ()=>this.deviceReady(), false);
       else
            this.deviceReady();

        (window as any).starterVersion = "1.5";
        (window as any).baseGameVersion = "0.28";
    }


    private deviceReady() {
        this.isBrowser = location.href.indexOf("browser") != -1;
        if (!this.isBrowser)
        Notifications.listen();
  
        $("#loader").css("visibility", "hidden");

        if ((window.screen as any).orientation)
            (window.screen as any).orientation.lock('portrait');

        var emulated = (window as any).tinyHippos != undefined || this.isBrowser;
       // emulated = true;
        let run = (path) => {
       //    path = "scripts/loader.js";
            $.getScript(path, (script, status, obj) => {
                console.log("Loader attached from path " + path);
                var event = new Event('runLoader');
            //    Notifications.disabled = true;
                document.dispatchEvent(event);
            });
        }

        if (!emulated)
            new LoaderURL((path) => {
                console.log(path);
                run(path);
                });
        else {
            if (this.isBrowser) {
                $.getScript("scripts/app.js" , (script, status, obj) => {
                    var event = new Event('runZH');
                    document.dispatchEvent(event);
                    });
       
            }
            else 
                run("scripts/loader.js");
        }
    }
}

window.onload = () => {
    new Starter();

}
