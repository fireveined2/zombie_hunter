﻿
declare var LocalFileSystem;



export class LoaderURL {
    private curVersion: number;

    constructor(private onReady: (url: string) => void) {
        this.get(onReady);
    }


    private get(succes: (url: string) => void) {
 
        let self = this;
        //if (succes)
       //     self.returnDefaultScript(succes);
     //   else
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
            console.log('file system open: ' + fs.name);
            fs.root.getFile("loader3.js", { create: false, exclusive: false }, function (fileEntry) {
                console.log("Resolved loader.js url ");
                succes(fileEntry.toURL());
            }, (e) => { console.error(e); self.returnDefaultScript(succes) });

        }, (e) => { console.error(e); self.returnDefaultScript(succes) });
    }

    private returnDefaultScript(succes: (path: string) => void) {
        (<any>window).resolveLocalFileSystemURL(cordova.file.applicationDirectory + "www/scripts/loader.js",
            function (file) {
                succes(file.toURL());
            }, function (err) {
                console.log(err);
            }
        );
    }

}