﻿
export class Notifications {

    private static localNotification;
    static disabled: boolean = false;
    static listen() {
        if (this.localNotification === undefined) {
            this.localNotification = ((<any>cordova).plugins && (<any>cordova).plugins.notification) ? (<any>cordova).plugins.notification.local : null;
        }
        if (!(window as any).FirebasePlugin)
            return;

        (window as any).FirebasePlugin.onNotificationOpen((notification) => {
            if (this.disabled)
                return;

            console.log(notification);
            this.onNotyfication(notification);

        }, function (error) {
            console.error(error);
        });
    }

    private static onNotyfication(notification) {
        if (!notification.tap) {
            this.convertToLocal(notification);
            return;
        }

        if (notification.tap) {
            this.makeAction(notification);
            return;
        }

    }

    private static makeAction(notification) {
        if (notification.open_url)
            var ref = (cordova as any).InAppBrowser.open(notification.open_url, "_system");
    }

    private static convertToLocal(notification) {
        if (!this.localNotification)
            return;

        let id = Math.round(Math.random() * 1000000);
        this.localNotification.schedule({
            id: id,
            title: notification.title,
            message: notification.body,
            open_url: notification.open_url,
            icon: "res://notification"
        });

        this.localNotification.on("click", (clickedNotification) => {
            console.log(clickedNotification);
            if (clickedNotification.id == id) {
                this.makeAction(notification);
            }
        });
    }
}

