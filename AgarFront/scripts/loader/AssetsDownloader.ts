﻿import { Config } from "../utils/Config"
import {FileUtils } from "./FileUtils"

declare var LocalFileSystem;

function writeFile(fileEntry, dataObj, callback, error) {
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onwriteend = function () {
            console.log("Successful file write...");
            callback();
        };

        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
            error(e);
        };


        if (!dataObj) {
            console.error("Empty js file!");
            return;
        }

        fileWriter.write(dataObj);
    });
}




interface Asset {
    name: string;
    path: string;
    size: number;
    md5: string;
}


export class AssetsDownloader {

    private assets: Asset[] = [];
    private totalSize: number;
    private totalDownloaded: number = 0;

    private files: FileUtils;

    constructor() {
        this.getList();
    }

    public onOneDownloaded: (asset: Asset, data: any, next: Asset, progress: number) => void;
    public onListDownloaded: (assets: Asset[], totalSize: number) => void;
    public onEnd: (error?: number) => void;

    private getList() {
        $.get(Config.Global.serverAdress + "/update/assets", (data) => {
            this.assets = data;
            this.totalSize = 0;
            for (let asset of this.assets)
                this.totalSize += asset.size;

            console.log(JSON.parse(JSON.stringify(this.assets)));
            this.onListDownloaded(this.assets, this.totalSize);
            this.downloadNext()
        }, "json")
            .fail((error) => {
                this.onEnd(error);

            });
    }

    private downloadNext(): Asset {
        if (this.assets.length == 0) {
            setTimeout(() => this.onEnd(), 0);
            return null;
        }

        let next = this.assets.shift();

        let match = FileUtils.get().md5Match(next.path, next.md5);
        if (match) {
            console.log("[UPDATER] MD5 match for " + next.path+". Skipping.");
            setTimeout(() => this.downloadNext(), 0);
        }
        else
            setTimeout(() => this.downloadOne(next), 0);
        

        return next;
    }

    private downloadOne(asset: Asset) {
        console.log("[UPDATER] Start downloading " + asset.path);

        var req = new XMLHttpRequest();
        req.open('GET', Config.Global.serverAdress + "/update/assets/" + asset.name, true); 
        req.responseType = 'arraybuffer';
        req.onreadystatechange =  (aEvt) => {
            if (req.readyState == 4) {
                if (req.status == 200)
                    this.process(req.response, asset);
                else {
                    this.onEnd();
                    console.error("Cant download " + asset.name,);
                }
            }
        };
        req.send();
    }

    private process(data, asset: Asset) {
        this.totalDownloaded += asset.size;
        let next = this.downloadNext();
        let progress = this.totalDownloaded / this.totalSize;
        this.onOneDownloaded(asset, data, next, progress);
        console.log("[UPDATER] Downloaded " + asset.path + ", size = " + asset.size + ", progress = " + Math.round(progress * 100) + "%");
    }
}