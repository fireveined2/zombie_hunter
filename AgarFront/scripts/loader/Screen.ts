﻿
export class LoaderScreen {


    constructor() {
        $("#play").click(() => { if (this.onPlayButtonClick) this.onPlayButtonClick() });
    }


    public setStatus(status: "seeking" | "preparing" | "downloading" | "done" | "error" | "error_while_downloading" | "error_while_saving", progress?: any) {
        let set = (status) => $("#status").text(status);

        if (status == "seeking")
            set("Wyszukiwanie aktualizacji...");

        if (status == "preparing")
            set("Przygotowywanie aktualizacji...");

        if (status == "downloading")
            set("Pobieranie aktualizacji... " + Math.round(progress*100)+"%");

        if (status == "done")
            set("Zaktualizowano!");

        if (status == "error")
            set("Nie można pobrać aktualizacji!");

        if (status == "error_while_downloading")
            set("Nie można pobrać aktualizacji!");

        if (status == "error_while_saving")
            set("Nie można zapisać aktualizacji!");
    }

    public onPlayButtonClick: Function;
}

