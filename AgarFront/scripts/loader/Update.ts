﻿import { Config } from "../utils/Config"
import { md5 } from "./md5";
declare var LocalFileSystem;
import * as Server from "../utils/Server/Offline"
function writeFile(fileEntry, dataObj, callback, error) {
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onwriteend = function () {
            console.log("Successful file write...");
            callback();
        };

        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
            error(e);
        };


        if (!dataObj) {
            console.error("Empty js file!");
            return;
        }

        fileWriter.write(dataObj);
    });
}

function runScript(data: string) {
    $(function () {
        $('<script>')
            .attr('type', 'text/javascript')
            .text(data)
            .appendTo('head');
    })
}

export class Update {
    private loaderVer: number;
    private gameVer: number;

    public gameVersion: string;

    constructor(private onReady: (url: string, error?: "cant_save" | "no_error" | "cant_download", newVersion?: string) => void) {
        let gameVer = window.localStorage.getItem("game2.js/md5");
        let loaderVer = window.localStorage.getItem("loader2.js/md5");

        if (!gameVer)
            gameVer = "0";  
         
        if (!loaderVer)
            loaderVer = "0";   

        this.loaderVer = (loaderVer) as any;
        this.gameVer = (gameVer) as any;

        this.fetch();
    }


    private fetch() {
        let self = this;

        let online = false;
        if (navigator.network && navigator.network.connection.type != Connection.NONE)
            online = true;
        if (!navigator.network)
            online = true;
        //this.onError();
        if (online)
            $.get(Config.Global.serverAdress + "/update", { gameVersion: this.gameVer, loaderVersion: this.loaderVer, starterVersion: (window as any).starterVersion }, this.onResponse.bind(this), "json")
                .fail(this.onError.bind(this));
        else
            this.onError(null);
    }

    private processLoaderUpdate(update) {
        if (update == "OK") {
            console.log("Loader version is OK");
            return;
        }

        this.saveFile(update.code, "loader3.js", () => {
            window.localStorage.setItem("current_loader_version", update.version);
        }, (error) => { Server.Request.debug("loader_error", error);console.error(error)});
    }

    private saveFile(data: string, name: string, succes, error) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
            console.log('file system open: ' + fs.name);
            fs.root.getFile(name, { create: true, exclusive: false }, function (fileEntry) {
                writeFile(fileEntry, new Blob([data], { type: 'text/plain' }), () => {
                    let hash = md5(data);
                    window.localStorage.setItem(name+"/md5", hash);
                    succes(fileEntry.toURL());
                    console.log("Downloaded and saved new version. MD5 = " + hash);
                }, error);
            },error);
        }, error);
    }

    private onResponse(response: any) {
        let gameUpdate = response.game;
        let loaderUpdate = response.loader;

        this.processLoaderUpdate(loaderUpdate);

        if (gameUpdate == "OK") {
            console.log("Game version is ok");
            this.getScriptURL(this.onReady);
            return;
        }


        let self = this;
        this.saveFile(gameUpdate.code, "game2.js", (url) => {
            window.localStorage.setItem("current_game_version", gameUpdate.version);
            this.onReady(url);
        }, (error) => {
            console.error(error);
            Server.Request.debug("loader_error", error);
            this.getScriptURL((url) => this.onReady(url, "cant_save"));
            });
    }
    
    private onError(error) {
        if (error) {
            console.error(error);
            Server.Request.debug("loader_error", error);
        }
        
        this.getScriptURL((url) => this.onReady(url, "cant_download"));
    }

    private getDownloadedScriptVersion(fileEntry, callback: (version: number) => void, error: Function) {
        fileEntry.file(function (file) {
            var reader = new FileReader();

            reader.onloadend = () => {
                let data = reader.result;
                let pos = data.indexOf("Version.ver");
                pos = data.indexOf('"', pos + 1);
                let end = data.indexOf('"', pos + 1);
                let ver = parseFloat(data.substr(pos + 1, end - pos - 1));
                callback(ver);
            };

            reader.readAsText(file);

        }, (e) => {console.error("Cant load file", e);error()
    });
}
    private getScriptURL(succes: (url: string) => void) {
        let self = this;
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
            console.log('file system open: ' + fs.name);
            fs.root.getFile("game2.js", { create: false, exclusive: false }, function (fileEntry) {
                console.log("Resolved game2.js url " + fileEntry.toURL());
                self.getDownloadedScriptVersion(fileEntry, (version) => {
                    let ver = (window as any).baseGameVersion;
                if (!ver)
                    ver = 0;
                let baseVersion = parseFloat(ver);
                console.log(version + " vs " + baseVersion);
                if (baseVersion > version)
                    self.returnDefaultScript(succes);
                else
                    succes(fileEntry.toURL());
                }, ()=>self.returnDefaultScript(succes));
            
            }, (e) => { console.error(e); self.returnDefaultScript(succes); Server.Request.debug("loader_error", e); });

        }, (e) => { console.error(e); self.returnDefaultScript(succes); Server.Request.debug("loader_error", e); });
     }

    private returnDefaultScript(succes: (path: string) => void) {
        (<any>window).resolveLocalFileSystemURL(cordova.file.applicationDirectory + "www/scripts/app.js",
            function (file) {
                succes(file.toURL());
            }, function (err) {
                Server.Request.debug("loader_error", err);
                console.log(err);
            }
        );
    }

}