﻿import { Config } from "../utils/Config"
import { md5 } from "./md5";

declare var LocalFileSystem;

function writeFile(fileEntry, dataObj, callback, error) {
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onwriteend = function () {
            console.log("Successful file write...");
            callback(); 
        };

        fileWriter.onerror = function (e) {
            console.log("Failed file write: ", e);
            error(e);
        };


        if (!dataObj) {
            console.error("Empty js file!");
            return;
        }

        fileWriter.write(dataObj);
    });
}



interface INativeStorage{
    setItem(name: string, value: string, succes: Function, error: Function);
    getItem(name: string, succes: (value: string) => void, error: Function);
}

declare var NativeStorage: INativeStorage;

interface IFile {
    name: string;
    path: string;
    md5: string;
}
export class FileUtils {

    private storage: INativeStorage;
    private list: IFile[] = [];
    private static instance: FileUtils;

    static get() {
        return this.instance;
    }

    static create() {
        this.instance = new FileUtils();
    }

    constructor() {
        if (typeof NativeStorage !== "undefined") {
            this.storage = NativeStorage;;

            this.storage.getItem("fileSystem/filelist", (data) => {
                this.list = JSON.parse(data);
            }, (error) => this.list = []);
        }

    }

    public getList() {
        return this.list;
    }

    private addToList(file: IFile) {
        for (let i = 0; i < this.list.length; i++)
            if (this.list[i].name == file.name) {
                this.list.splice(i, 1);
                i--;
            }
        this.list.push(file);
        this.saveList();
    }

    private saveList() {
        this.storage.setItem("fileSystem/filelist", JSON.stringify(this.list), () => { console.log("List saved"); }, (error) => console.error("Cant save list!", error));
    }

    public save(path: string, data: any, whenDone: Function) {
        this.saveFile(path, data, (url) => {
            this.addToList(<IFile>{ name: path, path: url, md5: md5(data) });
        }, () => { });
    }


    private getFile(path: string) {
        for (let file of this.list)
            if (file.name == path)
                return file;

        return null;
    }

    public exists(path: string) {
        return !!this.getFile(path);
    }

    public md5Match(path: string, md5: string) {
        let file = this.getFile(path);
        if (file)
            return file.md5==md5;
        else
            return false;
    }

    public getURL(path: string) {
        let file = this.getFile(path);
        if (file)
            return file.path;
        else
            return path;
    }

    private createAllFolders(fileSystem: FileSystem, path: string, done: Function, error: Function) {
     /*   let folders = path.split("/");
        let num = folders.length - 1;
        let current = 0;
        let currentPath = "";
        let functions: Function[] = [];
        for (let i = 0; i < num; i++) {
            functions[i] = (current, path) => {
                if (current >= num)
                    return;
                this.createFolder(fileSystem, path + folders[current], () => {
                    functions
                }, error);
            };

            this.createFolder(fileSystem, currentPath + folders[i], () => {

            });
        }
        */
    }

    private saveFile(path: string, data: any, succes: Function, onError?: Function) {
        path = path.replace(/\//g, "-");
        console.log("Saving " + path);
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {



            fs.root.getFile(path, { create: true, exclusive: false }, function (fileEntry) {
                let type = "application/octet-stream";
               if (path.indexOf(".js")!=-1)
                    type = "text/plain";

               if (path.indexOf(".png") != -1)
                   type = "image/png";

               writeFile(fileEntry, new Blob([data], { type: type }), () => {
                
                    succes(fileEntry.toURL());
                    console.log("Saved " + path + " as " + fileEntry.toURL() + ", type: " + type);
                    console.log(data);
                }, (e) => { console.error(e); onError(e); });
            }, (e) => { console.error(e); onError(e); });
        }, (e) => { console.error(e); onError(e); });
    }

    private createFolder(fileSystem: FileSystem, name: string, callback: Function, error: Function) {
        fileSystem.root.getDirectory(name, { create: true }, callback as any, error as any);
    }
}