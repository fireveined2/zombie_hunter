﻿import { IAuthByTokenModel } from "../../../Shared/Models";
//import { Events } from "../../../Shared/Events";
import { GlobalConfig } from "../../../Shared/Config";

export class Authorization {
    private static AuthorizationTimeout: number = 200000; // 200s
    private static authorized: boolean = false;
    private static token = "";

    public static auth(socket: SocketIOClient.Socket, callbackAuthorized: () => void, callbackNotAuthorized: () => void) {


      /*  let token = this.getTokenFromCookie();
        let authorizationHandle = setTimeout(() => {
            callbackNotAuthorized();
        }, Authorization.AuthorizationTimeout);

        if (token == null) return;

        socket.on("connect",
            () => {
                let model: IAuthByTokenModel = {
                    token: token
                };
                socket.emit(Events.Authorization, model);
                socket.on(Events.Authorized,
                    () => {
                        clearTimeout(authorizationHandle);
                        this.authorized = true;
                        callbackAuthorized();
                    });

                socket.on(Events.NotAuthorized,
                    () => {
                        clearTimeout(authorizationHandle);
                        this.authorized = false;
                        callbackNotAuthorized();
                    });

            });
        */
    }

    public static getTokenFromCookie() {

        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3QiLCJjcmVhdGVfdGltZSI6MTQ4MjMwMzY5MCwiZXhwIjoxNDgyOTA4NDkwLCJ1c2VybmFtZSI6ImFkbWludXNlciIsInVzZXJfaWQiOjExLCJ0b3VyX2lkIjozMSwiZ2FtZV9oYXNoIjoid2IxMDAifQ==.00594eb3127422e700bd7048df64e6051c0e9a934f03492089bb6c256b6f7e54";

        /*
        let match = document.cookie.match(new RegExp(GlobalConfig.cookie + "=([^;]+)"));
            if (match && match[1] && match[1].split(".").length === 3) return match[1];
            else return null;
        */

    }
    public static get isAuthorized() {
        return this.authorized;
    }

}