﻿import { Authorization } from "../auth/Authorization";

import io = require("socket.io-client");


export class AM {
    public static amSocket: SocketIOClient.Socket;
    private static amLocation: string = "";
    public static isAuthorized: boolean = false;

    public static connect = (amLocation: string): void => {
        if (AM.amSocket !== undefined && AM.amSocket.connected !== undefined && AM.amSocket.connected === true) return;
        AM.amLocation = amLocation;
        var socket = AM.amSocket = io.connect(amLocation, {
            'reconnection': true,
            'reconnectionDelay': 1000,
            'reconnectionDelayMax': 5000,
            'reconnectionAttempts': 5
        });
    }

    public static authorize = (callbackAuthorized: () => void, callbackNotAuthorized: () => void): void => {
        Authorization.auth(AM.amSocket, callbackAuthorized, callbackNotAuthorized);
    }
}