﻿import * as Models from "./Shared"

export namespace ChallengeUtils {
    export function matchCriterion(criterion: Models.ChallengeCriteria, value: number): boolean {
        let expected = criterion.value;
        let actual = value;

        switch (criterion.operator) {
            case Models.ChallengeCriteriaOperator.LESS:
                if (!(actual < expected))
                    return false;
                break;

            case Models.ChallengeCriteriaOperator.MORE:
                if (!(actual >= expected))
                    return false;
                break;

            case Models.ChallengeCriteriaOperator.EQUAL:
                if (!(actual == expected))
                    return false;
                break;
        }
        return true;
    }


    export function addStats(...stats: Models.GameStats[]): Models.GameStats {
        if (!stats || stats.length == 0)
            throw Error("Empty stats array!");
        stats[0].numberOfTries = 0;

        for (let i = 1; i < stats.length; i++) {

            for (let stat in stats[i])
                if (stats[i].hasOwnProperty(stat)) {
                    if (!stats[0][stat])
                        stats[0][stat] = 0;
                    stats[0][stat] += stats[i][stat];
                }

        }
        if (!stats[0].numberOfTries)
            stats[0].numberOfTries = 0;
        stats[0].numberOfTries++;

        return stats[0];
    }





    export function calculateProgess(criteria: Models.ChallengeCriteria[], stats: Models.GameStats): number {
        let progres = [];
        if (!stats)
            return 0;
        for (let criterion of criteria) {
            let expected = criterion.value;
            let actual = stats[criterion.stat];

            switch (criterion.operator) {
                case Models.ChallengeCriteriaOperator.MORE:
                    let prog = actual / expected;
                    progres.push(prog);
                    break;

                case Models.ChallengeCriteriaOperator.LESS:

                    break;
            }
        }

        let result = 0;
        let onePart = 1 / progres.length;
        for (let prog of progres) {
            result += Math.min(prog, 1) * onePart;
        }
        return result;
    }


}