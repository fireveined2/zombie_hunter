﻿import * as Models from "./Items"
export * from "./Items"
import * as SocketModels from "./SocketModels"
export interface PlayerData {
    username: string;
    respect: number;
    exp: number;
    level: number;
    hp: number;
    items: Models.IItem[];
    wearedItemsids: number[];

    hcn?: number;
    messages?: IUserMessage[];
    banned: boolean;

    configUpdate?: ZHConfig;
    canMakePvP: boolean;
    freeTourneyEntrances: number;

    grantedTourneyEntrances: number;
    pendingRewards: IPendingReward[];
}

export enum IRewardType {
    DEFAULT,
    MYSTERY_BOX
}
export interface IPendingReward {
    name: string;
    icon: number;
    type: IRewardType;
}

export interface PlayerInfo {
    id: number;
    username: string;
    respect: number;
    level: number;
    wearedItemsIds: number[];
    status?: SocketModels.UserStatus;

}

export interface RankingRow {
    id: number;
    place?: number;

    nick: string;
    lvl: number;
    respect: number;
    records: number[];
    wearedItemsIds?: number[];

    status?: SocketModels.UserStatus;
}


export interface AccesToPlay {
    config: GameConfig;
    challengeId?: number;
    offline?: boolean
}

export interface GameStats {
    score: number;

    cutPonies: number;
    missedPonies: number;

    cutFlesh: number;
    missedFlesh: number;

    trips: number;
    ctps: number;
    cutInCtps: number;
    rages: number;
    chills: number;
    pigs: number;
    duration: number;

    cutsCount: number;
    biggestCut: number;
    longestCut: number;
    highestScoreForCut: number;
    cutElements: {};
    bladeRoad: number;

    numberOfTries?: number; 

    //only for server purposes
    numberOfTrainings?: number;
}

export interface GameData {
    seed?: number;
    stats: GameStats;
    challenge: number;
    trainingMap?: number;
    input: ArrayBuffer;
    screenWidth: number;
    screenHeight: number;
    pvpId?: number;
    debugPointsInfo?: number[];
}


export interface Game {
    challengeId: number;

    stats?: GameStats;
    input?: any;
}

export interface IPvPResults {
    win: boolean;
    enemyScore: number;
    
}

export interface IKOHResults {
    place: number;
    best: number;
    pointsToNextPlace: number;
}

export enum ChallengeResult {
    SUCCES,
    FAILURE,
    PLAY_MORE // przy nie wygranych wyzwaniach kumulowanych
}
export interface OverallResults {
    challengeResult?: ChallengeResult;
    stars?: number;

    respect?: number;
    exp: number;

    kohResults?: IKOHResults;
    canMakeDonate?: boolean;
    wasChallengeTraining?: boolean;
}

export interface IGameHandleFlags {
    removeGameData: boolean;
    isChallengeTraining: boolean;
    saveOnlyIfBetter: boolean;
}

export interface ICalculateResultData{
    overall: OverallResults;
    flags: IGameHandleFlags;
}
export interface GameResults {
    challengesUpdate: IChallengeData[];
    playerUpdate: PlayerData;

    prizes?: IPrize[];
    overall?: OverallResults;
}


export enum ChallengeCriteriaOperator {
    MORE,
    LESS,
    EQUAL
}
export interface ChallengeCriteria {
    stat: string;
    operator?: ChallengeCriteriaOperator;
    value?: number;
}




export interface IUser {
    username: string;
    respect: number;
}




export interface IPrize extends Models.IItem {
}


export enum ChallengeType {
    SIMPLE,
    CUMULATE,
    SOCIAL,
    KING_OF_THE_HILL,
    TOURNEY,
    FINISHED_TOURNEY
}


export interface IRankingPlayerData {
    nick: string;
    score: number;
    place: number;
    status?: SocketModels.UserStatus;

    id?: number;
    wearedItemsIds?: number[];
    level?: number;
    respect?: number;
}

export interface IKingOfTheHillPlayerData extends IRankingPlayerData {

}


export interface IKingOfTheHillData {
    top3: IKingOfTheHillPlayerData[];
    myScore: IKingOfTheHillPlayerData;
}

export interface ITourneyPrize{
    rangeStart: number;
    rangeEnd: number;
    prize?: IPrize;
    prizeId: number;
}



export interface ITourneyPlayerDetails {
    wearedItemsIds: number[];
    level: number;
    respect: number;
}

export interface ITourneyData {
    prizes: ITourneyPrize[];
    players?: IRankingPlayerData[];
    top3?: ITourneyPlayerDetails[];
    myScore?: IRankingPlayerData;

    slonScore: number;
    avgScore: number;
    playersNum: number;
    nextTrainingCost: number;
}


export interface IChallenge {
    id: number;
    name: string;
    description: string;

    criteria: ChallengeCriteria[];

    cost: number;
    gain: number;

    prize: IPrize;

    type: ChallengeType;

    prizesLeft: number;
    endTime: number;
    minutesLeft: number;
    minLevel: number;
    config: GameConfig;

    quest: number;

    kohData?: IKingOfTheHillData;
    tourneyData?: ITourneyData;

}

export interface IChallengeData {
    data: IChallenge;
    prevStats: GameStats;
    ended: boolean;
}

export enum ChallengeError {
    SERVER_TIMEOUT,
    NO_ENOUGH_RESPECT,
    CHALLENGE_ENDED
}

export enum ServerRequestError {
    SERVER_TIMEOUT,
    BAD_CREDENTIALS,
    NAME_ALREADY_USED,
    EMAIL_ALREADY_USED,
    WRONG_EMAIL,
    WRONG_NAME,
    GAME_SERVER_TIMEOUT,
    USER_DOES_NOT_EXIST,
    BAD_PASSWORD,
    NAME_TOO_LONG
}

export interface HHUserData {
    user_id: number;
    hit_coins: number;
    username_full: string;

    token?: string;
}

export class QueueMessage {

    static CHANGE_POS = "a";
    static WAIT = "b";
}

export interface GameConfig  {
    seed: number;
    duration: number;
    map: number;

    bonus: Models.IGameBonus;
}


export interface IUserMessage {
    id: number;
    message: string;
    title: string;
    openImmediately: boolean;
}


export enum Map {
    CEMENTARY = 0,
    POLICE = 1,
    HOSPITAL = 2,
    STATION = 3

}

export interface IGameAccesRequest {
    challengeId?: number;
    isChallengeTraining?: boolean;

    trainingMap?: number;

    pvpId?: number;
    
}


export interface HCNBuyUnit {
    hcn: number;
    pln: number;
    bonus?: number;
    freeEntraces: number;
}

export interface ZHConfig {
    minVersion: string;
    expLevels: number[];
    streamEnabled: boolean;
    happyHours: boolean;

    hcnSMS?: HCNBuyUnit[];
    hcnPLN?: HCNBuyUnit[];
}

export enum GameDataStatus {
    NORMAL,
    SUCCES,
    TRAINING
}