﻿export interface IHelloMsg {
    seed: number;
    duration: number;
}


export interface IInputSample {
    x: number;
    y: number;
    frame: number;
}

export interface IReplay {
    seed: number;
    duration: number;
    score?: number;
    inputs: IInputSample[];
}


export type InputBuffer = { input: IInputSample, prev: IInputSample }[];