﻿/**
 * Names of events that runs eg. on connection.
 */
class GameEvents {
    public InputSample = "a";
    public Result = "b";
}

export class Events {

    static Game: GameEvents = new GameEvents;

    public static GameStarts: string = "e";
    public static ScoreUpdate: string = "f";
    public static Hello: string = "g";
    public static Authorization: string = "h";
    public static Authorized: string = "i";
    public static NotAuthorized: string = "j";
    public static ConnectionRefused: string = "k";
    public static AdvancedToNextRound: string = "l";
    public static NotAdvancedToNextRound: string = "m";
    public static Winner: string = "n";
    public static TimeToStart: string = "o";
    public static GameServerAuthRequest: string = "gsar";
    

    public static IncorrectTourneyID: string = "incorrectTurney";
    public static IncorrectUserID: string = "incorrectUserID";

    public static Training: string = "t";
    public static SecsTillNextRound: string = "u";

    public static GameEnded: string = "o";
    public static AuthRequest: string = "authReq";
    public static GameServerSocket: string = "gss";
    public static UserAuth: string = "userAuth";
    public static Start: string = "aa";
    public static GameServerFinishedTournament: string = "ab";
    public static Sleep: string = "ac";
    public static GoToLobby: string = "af"

    public static ChatMessage: string = "chatMessage";
    public static RankingDatabaseData: string = "rdbd";
    public static RoomRanking: string = "rr";
    public static PlayerName: string = "pn";

    public static isAlive: string = "ia";
    public static PlayersOnline: string = "po";
    public static Kill: string = "kl";
    public static LockGameplay: string = "lg"
    public static DisallowSigningIn: string = "dsi";

    public static StageEnded: string = "sse";
    public static StageFinalEnded: string = "sfe";
    public static RoomPlayersCurrentPoints: string = "rpcp"; 
    public static WrongToken: string = "ij";
    public static TokenExpired: string = "ik";
}