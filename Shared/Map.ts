﻿export interface IElementData {
    key: string;
    x: number;
    y: number;
    width: number;
    height: number;
    size: number;
}

export interface IGeneratorElementData extends IElementData{
    probability: number;
}

export interface IGeneratorData {
    elements: string;

    percentProbability?: number;

    baseInterval: number;
    extraThrowInterval: number;
    extraThrowSize: number;
}

export interface IMap {



}