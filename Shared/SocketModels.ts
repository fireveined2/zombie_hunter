﻿export interface IAuthData {
    token: string;
}

export enum UserStatus {
    OFFLINE,
    ONLINE,
    INGAME

}

export interface IInvitation {
    sender: any;

}

export enum InvitationAnswer {
    YES,
    NO,
    TIMEOUT
}

export interface IPvPInitData {
    id: number;
    lobbyCountingDuration: number;
}

export interface IPvPPlayerResults {
    id: number;
    name: string;
    score: number;
    respect: number;
    respectChange: number;
    exp: number;
}
export interface IPvPResults {
    players: IPvPPlayerResults[];
    winnerId: number;
    challengesUpdate: any[];

}

export interface IPvPUpdate {
    score: number;
    timeLeft: number;
}

export class Events {
    static Auth = "au";
    static StartedGame = "sg";
    static EndedGame = "eg";

    static InvitationCancel = "inc";
    static Invite = "in";
    static InvitationResponse = "inr";

    static PvPInitData = "pvpi";
    static PvPUpdate = "pvpud";

    static GameData = "grs";
    static PvPResults = "pvpr";

    static ConfirmRequest = "conr";
    static ConfirmAnswer = "cona";

    static JoinQueue = "jq";

    static GetProfile
}