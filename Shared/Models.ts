﻿import { GlobalConfig } from "./Config";
import * as GameModels from "./GameModels";

declare var require:any;

var BitView = require('bit-buffer').BitView;
var BitStream = require('bit-buffer').BitStream;


export enum TournamentMode {
    STEPWISE,
    NORMAL,
    DEADMATCH,
    ROZDAJO,
    PAYANDIMPROVE,
    DUEL,
    DIFFERENT
}

export enum GameState {
    NotDownloaded,
    NotInitiated,
    Initiated, // posiada już listę graczy
    Preparing, // < TimeToPrepareServers = przygotowuje serwery
    Prepared, // < finalCountDown + gotowe serwery
    Running,
    WaitingForNextStage,
    Finished,
    CannotStart
}
export enum PlayAccessState {
    NotAllowed,
    Allowed,
    Lost,
    Winner
}
export interface PlayAccess {
    access: PlayAccessState,
    toStart: number,
    gameState: GameState
}



export interface RankingPlayer {
    user_id: string;
    points: number;
    username: string;
}
export interface Ranking {
    players: RankingPlayer[]
}


export interface IAuthModel {
    token: string;
}



export interface IHelloModel {
    username: string,
   // config: IGameConfig;
}

export interface IGameState {
  //  fields: IField[][];
    score: number;
    seed: number;
}


//Gameserver - AM - Client relations
export interface GameServerSocket {
    address: string;
}
export interface IAuthByTokenModel {
    token: string;
}
export interface IAuthUserToGameServer {
    user_id: number,
    token: string;
}
export interface IUserInfo {
    id: number;
    username: string;
    token: string;
}
export interface IPlayerScore {
    user_id: number,
    username: string,
    points: number
}
export interface IWinnerPlayers {
    winners: IPlayerScore[];
    tournament_id: string;
}
export interface IStage {
    rooms: Room[];
    tournament_id: string;
    stageNummber: number;
}
export interface Stage {
    stagePlayers: IPlayerScore[];
    tournament_id: string;
    stageNumber: number;
}
export interface Room {
    players: IPlayerScore[];
    tournament_id: string;
}
export interface IGameServerAddress {
    address: string;
}
export interface TournamentID {
    tournament_id: string;
}
export interface IPlayerChoice {
    id: number;
    choice: [number, number];
}

export interface ISecsLeft {
    left: number;
}

export interface RoomScores {
    players: IPlayerScore[];
    tournament_id: string;
    room_id: number
}

export interface IUpdatePoints {
    username: string;
    points: number;
}


export interface ChatMessages {
    user_id: number,
    username: string,
    message: string
}






export class BinaryConverter {

    private static ReplayHeaderSize = 4+ 2 + 2;



    public static packReplay(replay: GameModels.IReplay): Uint8Array {
        let size = BinaryConverter.ReplayHeaderSize;
        size += 6 * replay.inputs.length;
               
        const stream: Buffer = BinaryConverter.alocateBuffer(size);
        let offset = 0;

        offset = stream.writeUInt16LE(replay.score, offset);
        offset = stream.writeUInt32LE(replay.seed, offset);
        offset = stream.writeUInt16LE(replay.duration, offset);

        for (let item of replay.inputs) {
            offset = stream.writeUInt16LE(item.frame, offset);
            offset = stream.writeUInt16LE(item.x, offset);
            offset = stream.writeUInt16LE(item.y, offset);
        }

        return stream;
    }


    public static unpackReplay(data: ArrayBuffer): GameModels.IReplay{
        let count = Math.floor((data.byteLength - BinaryConverter.ReplayHeaderSize) / 6);
        let inputs: GameModels.IInputSample[] = [];
        let replay  = <GameModels.IReplay>{};
        let stream = BinaryConverter.createBuffer(data);
        let offset = 0;

        replay.score = stream.readUInt16LE(offset);
        offset += 2;
        replay.seed = stream.readUInt32LE(offset);
        offset += 4;
        replay.duration = stream.readUInt16LE(offset);
        offset += 2;

        for (let i = 0; i < count; i++) {
            let input = <GameModels.IInputSample>{};
            input.frame = stream.readUInt16LE(offset);
            offset += 2;
            input.x = stream.readUInt16LE(offset);
            offset += 2;
            input.y = stream.readUInt16LE(offset);
            offset += 2;
            inputs.push(input);
        }

        replay.inputs = inputs;
        return replay;
    }

    public static packInputBuffer(buffer: GameModels.InputBuffer): Uint8Array{
        let first = buffer[0];
        let nextFrame = false;

        if (first.prev)
            nextFrame = first.input.frame == (first.prev.frame + 1);

        let array = new ArrayBuffer(3 + buffer.length * 3);
        let bs = new BitStream(array);

        bs.writeBits(+nextFrame, 1);
        if(!nextFrame)
            bs.writeBits(first.input.frame, 11);

        for (let item of buffer) {
            let x, y, dx = false, dy = false, sameFrame = false, nextFrame = false;
            let input = item.input;
            let prev = item.prev;

            if (prev) {
                x = Math.round(input.x - prev.x);
                y = Math.round(input.y - prev.y);
                dx = Math.abs(x) < 128;
                dy = Math.abs(y) < 128;
                sameFrame = input.frame == prev.frame;
                nextFrame = input.frame == (prev.frame + 1);
            }

            bs.writeBits(+dx, 1);
            bs.writeBits(+dy, 1);

            if (dx) {
                bs.writeBits(+(x > 0), 1);
                bs.writeBits(Math.round(Math.abs(x)/2), 6);
            }
            else
                bs.writeBits(Math.round(input.x/2), 10);


            if (dy) {
                bs.writeBits(+(y > 0), 1);
                bs.writeBits(Math.round(Math.abs(y) / 2), 6);
            }
            else
                bs.writeBits(Math.round(input.y / 2), 10);

        }
       
       // BinaryConverter.unpackInputBuffer(bs.view.buffer.slice(0, bs.byteIndex), first.prev);
        
        return bs.view.buffer.slice(0, bs.byteIndex);
    }

    static unpackInputBuffer(data: ArrayBuffer, firstPrev: GameModels.IInputSample): GameModels.IInputSample[]  {
        let bs = new BitStream(data);
        let inputBuffer: GameModels.IInputSample[] = [];

        let frame = 0;
        if (firstPrev)
            frame = firstPrev.frame;

        let nextFrame = bs.readBits(1, false);
        if (!nextFrame)
            frame = bs.readBits(11, false);
        else
            frame++;

        let dx, dy;
        let prev: GameModels.IInputSample = firstPrev;

        while (data.byteLength - bs.byteIndex > 1) {
            let input = <GameModels.IInputSample>{};
            input.frame = frame;

            dx = bs.readBits(1, false);;
            dy = bs.readBits(1, false);;

            if (dx) {
                let sign = bs.readBits(1, false);;
                input.x = bs.readBits(6, false) * 2;;
                if (!sign)
                    input.x *= -1;
                input.x += prev.x;
            }
            else 
                input.x = bs.readBits(10, false) * 2;;
            


            if (dy) {
                let sign = bs.readBits(1, false);;
                input.y = bs.readBits(6, false)*2;
                if (!sign)
                    input.y *= -1;
                input.y += prev.y;
            }
            else 
                input.y = bs.readBits(10, false) * 2;;


                inputBuffer.push(input);
                prev = input;
        }

        return inputBuffer;
    }
        

    // public static packInput(input: GameModels.IInputSample, prev: GameModels.IInputSample): {pack: number, length: number} {
    public static packInput(input: GameModels.IInputSample, prev: GameModels.IInputSample): Uint8Array {
        let x, y, dx=false, dy=false, sameFrame =false, nextFrame = false;
        if (prev) {
            x = Math.round(input.x - prev.x);
            y = Math.round(input.y - prev.y);
            dx = Math.abs(x) < 128;
            dy = Math.abs(y) < 128;
            sameFrame = input.frame == prev.frame;
            nextFrame = input.frame == (prev.frame + 1);
        }
      
     
        let pack = 0;
        let length = 0;

       let array = new ArrayBuffer(6);
       let bitStream = new BitStream(array);

       bitStream.writeBits(+sameFrame, 1);
       pack <<= 1;
       pack |= +sameFrame;
       length += 1;


       if (!sameFrame) {
           bitStream.writeBits(+nextFrame, 1);
           pack <<= 1;
           pack |= +nextFrame;
           length += 1;

           if (!nextFrame) {
               bitStream.writeBits(input.frame, 11);
               pack <<= 11;
               pack |= input.frame;
               length += 11;
           }
       }

       bitStream.writeBits(+dx, 1);
       pack <<= 1;
       pack |= +dx;
       length += 1;


       bitStream.writeBits(+dy, 1);
       pack <<= 1;
       pack |= +dy;
       length += 1;

  
        if (dx) {
            pack <<= 7;
            pack |= Math.abs(x);
            pack <<= 1;
            pack |= +(x > 0);
            length += 8;
            bitStream.writeBits(+(x > 0), 1);
            bitStream.writeBits(Math.abs(x), 7);
          
        }
        else {
            bitStream.writeBits(input.x, 11);
            pack <<= 11;
            pack |= input.x;
            length += 11;
        }



        if (dy) {
            pack <<= 7;
            pack |= Math.abs(y);
            pack <<= 1;
            bitStream.writeBits(+(y > 0), 1);
            bitStream.writeBits(Math.abs(y), 7);
          
            pack |= +(y > 0);
            length += 8;

        }
        else {
            bitStream.writeBits(input.y, 11);
            pack <<= 11;
            pack |= input.y;
            length += 11;
        }



  

      //  bitStream.
        let bytes = Math.ceil(length/8);
        const byteArray = this.alocateBuffer(bytes);
        byteArray.writeIntBE(pack, 0, bytes);
      //  byteArray.buffer.
      // console.log('----');
        console.log(input);
        //console.log(BinaryConverter.unpackInput(bitStream.view.buffer.slice(0, bytes), prev));
        return bitStream.view.buffer.slice(0, bytes);
    }


    public static unpackInput(data: ArrayBuffer, prev: GameModels.IInputSample): GameModels.IInputSample {
        const buffer: Buffer = BinaryConverter.createBuffer(data);
        let pack = buffer.readIntBE(0, buffer.length);
        let input = <GameModels.IInputSample>{};


        let nextFrame = 0;
        let sameFrame = 0;
        let dx = 0;
        let dy = 0;

        let bitStream = new BitStream(data);

        sameFrame = pack & 1;
        pack >>= 1;

        sameFrame = bitStream.readBits(1, false);

        if (!sameFrame) {
            nextFrame = pack & 1;
            pack >>= 1;
            nextFrame = bitStream.readBits(1, false);
            if (!nextFrame) {
                input.frame = pack & 2047;
                pack >>= 11;

                input.frame  = bitStream.readBits(11, false);;
            }
        }

        if (sameFrame)
            input.frame = prev.frame;
 
        if (nextFrame)
            input.frame = prev.frame + 1;

        //position
        dx = pack & 1;
        pack >>= 1;
        dy = pack & 1;
        pack >>= 1;

        dx = bitStream.readBits(1, false);;
        dy = bitStream.readBits(1, false);;

        if (dx) {
            let sign = pack & 1;
            pack >>= 1;
            sign = bitStream.readBits(1, false);;

            input.x = pack & 127;
            input.x = bitStream.readBits(7, false);;
            pack >>= 7;
            if (!sign)
                input.x *= -1;
            input.x += prev.x;
        }
        else {
            input.x = pack & 2047;
            input.x = bitStream.readBits(11, false);;
            pack >>= 11;
        }


        if (dy) {
            let sign = pack & 1;
            sign = bitStream.readBits(1, false);;
            pack >>= 1;
            input.y = pack & 127;
            pack >>= 7;
            input.y = bitStream.readBits(7, false);;
            if (!sign)
                input.y *= -1;
       
            input.y += prev.y;
        }
        else {
            input.y = pack & 2047;
            input.y = bitStream.readBits(11, false);;
            console.log(input.y);
            pack >>= 11;
        }

    



        return input;
    }



    private static setNBit(num: number, pos: number, val?: boolean): number {
        if (!val) val = true;
        return num | (val ? 1 : 0) << pos;
    }
    private static getNBit(num: number, pos: number): boolean {
        return (num & (1 << pos)) != 0;
    }

    private static createBuffer(buff: ArrayBuffer): Buffer {
        return new Buffer(buff); //Buffer.from(buff);
    }
    private static alocateBuffer(size: number): Buffer {
        return new Buffer(size);// Buffer.allocUnsafe(size);
    }
}