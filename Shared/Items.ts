﻿
export type IItemBonusType = "heal" | "unlockMap" | "pointPerCut" | "dropChance";


export enum ItemType {
    HEAD,
    CORPUS,
    BOOTS,
    WEAPON1,
    WEAPON2,
    POTION,

    OTHER,
    HCN,
    MYSTERY_BOX
}

export enum ItemSet {
    BASIC,
    POLICE,
    HOSPITAL
}

export interface IItem {
    id: number;
    name: string;
    type: ItemType;

    icon?: number;
    set: ItemSet;

    bonus: IItemBonus;
    quantity?: number;

    dropChance?: number;
}

export interface IEQChange {
    usedItemsIds: number[];
    wearedItemsIds: number[];
}

export interface IItemBonus extends IGameBonus {
    heal: number;
    unlockMap: number;

    valuePLN?: number;
    valueHCN?: number;
    typeDescription?: string;
    icon?: number;
}

export interface IGameBonus {
    duration: number;
    pointsPerCut: number;
    pointsPercent: number;
    pointsPercentInPrisonBreak: number;
    zombiePercent: number;

    rageDuration: number;
    chillDuration: number;

    chillPercent: number;
    pointsForPiggy: number;

    pointsPercentInTrip: number;
   // cureDropPercent
    rageLoadPercent: number;

    ctpPercent: number;
    tripPercent: number;
    tripDuration: number;
    pointsPercentInRage: number;

    negativElementPercent: number;


    basePossibleCuts: number;

    dontIgnoreItemBonus?: boolean;
}

export namespace ItemsHelpers {
    export function getAllOfType(all: IItem[], type: ItemType) {
        let items: IItem[] = [];
        for (let item of all)
            if (item.type == type) {
                item.quantity = 1;

                let stacked = false;
                for (let selected of items)
                    if (selected.id == item.id)
                        selected.quantity++ , stacked = true;
                if (!stacked)
                    items.push(item);

            }



        return items;
    }



    export function getAllBonusses(items: IItem[]) {
        let bonus = <IItemBonus>{};

        for (let item of items) {
            for (let name in item.bonus) {
                    if (!bonus[name])
                        bonus[name] = 0;
                    bonus[name] += item.bonus[name];
                }
        }
        return bonus;
    }


    export function addBonuses(bonus: IGameBonus[]) {
        let result = <IGameBonus>{};
        for (let item of bonus) {
            for (let name in item) {
                if (!result[name])
                    result[name] = 0;
                result[name] += item[name];
            }
        }
        return result;
    }



    var maps = ["Cmentarz", "Komisariat", "Szpital", "HopStop"];
    function getSignAndStat(value: number) {
        let sign = "+";
        if (value < 0)
            sign = "";
        return sign + value;
    }

    export function getBonusDescribe(bonus: IItemBonus): string {
        let desc = "";
            for (let name in bonus) {
                if (name == "heal")
                    desc += "Leczy " + Math.round(bonus[name] * 100) + "% zarazy\n"

                if (name == "description")
                    desc += bonus[name];

                if (name == "unlockMap")
                    desc += "Odblokowuje mapę: " + maps[bonus[name]]+"\n"

                if (name == "duration")
                    desc += getSignAndStat(bonus[name]) + "s długości rozgrywki\n";

                if (name == "chillDuration")
                    desc += getSignAndStat(bonus[name])  + "s długości Chillu\n"

                if (name == "rageDuration")
                    desc += getSignAndStat(bonus[name])  + "s długości Rage Mode\n"

                if (name == "pointsPercentInPrisonBreak")
                    desc += getSignAndStat(bonus[name]*100) + "% punktów podczas CTP\n"

                if (name == "pointsPerCut")
                    desc += getSignAndStat(bonus[name]) + "pkt za trafienie\n";

                if (name === "pointsForPiggy")
                    desc += getSignAndStat(bonus[name]) + "pkt za świnkę\n";

                if (name == "pointsPercent")
                    desc += getSignAndStat(bonus[name]*100) + "% punktów\n"

                if (name == "zombiePercent")
                    desc += getSignAndStat(bonus[name]*100) + "% zombie\n"

                if (name == "comboPercent")
                    desc += getSignAndStat(bonus[name]*100) + "% pkt za combo\n"

                if (name == "negativElementPercent")
                    desc += getSignAndStat(bonus[name]*100) + "% elementów z negatywnym efektem\n"

                if (name == "chillPercent")
                    desc += getSignAndStat(bonus[name] * 100) + "% szansy na CHILL\n"

                if (name == "pointsPercentInTrip")
                    desc += getSignAndStat(bonus[name] * 100) + "% punktów podczas tripu\n"

                if (name == "rageLoadPercent")
                    desc += getSignAndStat(bonus[name] * 100)+ "% szybsze ładowanie RageMode\n"

                if (name == "tripPercent")
                    desc += getSignAndStat(bonus[name] * 100) + "% fiolek z tripem\n"

                if (name == "ctpPercent")
                    desc += getSignAndStat(bonus[name] * 100) + "% kluczy w komisariacie\n"

                if (name == "pointsPercentInRage")
                    desc += getSignAndStat(bonus[name] * 100) + "% punktów w Rage Mode\n"

                if (name == "tripDuration")
                    desc += getSignAndStat(bonus[name]) + "s długości tripa\n"


                //stacja
                if (name == "canistersPercent")
                    desc += getSignAndStat(bonus[name] * 100) + "% butli na HopStop\n"

                if (name == "napalmPoints")
                    desc += getSignAndStat(bonus[name]) + " pkt za podpalenie Zombie\n"

                if (name == "napalmRangePercent")
                    desc += getSignAndStat(bonus[name]*100) + "% zasięgu ognia\n"

                if (name == "explosionPointsPercent")
                    desc += getSignAndStat(bonus[name] *100) + "% punktów za eksplozję\n"

                if (name == "implodeDuration")
                    desc += getSignAndStat(bonus[name]) + "s trwania implozji\n"

                if (name == "hotdogs")
                    desc += getSignAndStat(bonus[name]*100) + "% hotdogów z czosnkowym\n"


                //po grze
                if (name == "dropPercent")
                    desc += getSignAndStat(bonus[name] * 100) + "% szansy na zdobycie przedmiotu\n"

                if (name == "expPercent")
                    desc += getSignAndStat(bonus[name] * 100) + "% doświadczenia\n"


                

                if (name == "basePossibleCuts")
                    desc += "Możesz ciąć zombie "+bonus[name]+" razy"
            }

            return desc;
    }


}