﻿export class RND {
    public seed: number;
    public counter: number;
    static rnd: RND;
    static get(): RND {
        return RND.rnd;
    }

    constructor(seed: number) {
        this.seed = seed;
        this.counter = 0;
    }

    private next(min: number, max: number): number {
        max = max || 0;
        min = min || 0;

        this.seed = (this.seed * 9301 + 49297) % 233281;
        var rnd = this.seed / 233281;
        return min + rnd * (max - min);
    }

    public between(min: number, max: number): number {
        return Math.floor(this.next(min, max));
    }

    public double(): number {
        return this.next(0, 1);
    }

    public pick(collection: any[]): any {
        return collection[this.between(0, collection.length)];
    }
}