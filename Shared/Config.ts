﻿import {   } from "./Models";


export class GlobalConfig {
    

    public static GamePort: number = 133;
    public static RoundDurationSeconds: number = 3 * 60; //w sekundach
    public static ShowScoreAfterGameDuration: number = 9; //w sekundach


    public static Debug: boolean = (typeof document !== "undefined") ? (document.location.host == "localhost" ? true : false) : false;
    public static gameTimelimit: number = 3 * 60; //1min

    public static AMport: string = "1336";
    public static AMadress: string = "http://localhost" + ":" + GlobalConfig.AMport;
    public static host: string = "http://dev-132.dev-hophands.pl";
    public static looserURI: string = GlobalConfig.host + "/tournament/show/defeat";
    public static winnerURI: string = GlobalConfig.host + "/tournament/show/win";

    public static cookie: string = "gameToken";
}

export class GameConfig {
    public static gameModeMaxPlayersPerRoom = {
        normal: 5,
        rozdajo: 1,
        payandimprove: 1,
        duel: 2,
        stepwise: 5,
        deadmatch: 5
    };
}


export class JewelConfig {
    public static JewelsCount: number = 7;
    public static JewelEmpty: number = 7;
}
export class VolumeConfig {
    public static Level = [0, 0.35, 1];
}



