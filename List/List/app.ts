﻿class Donation {
    public message: string;
    public user: string;
    public hcn: number;
    public rotationsLeft: number;
}


class DonationList {
    private element: JQuery;

    private donations: Donation[] = [];
    constructor() {
        this.element = $("#list");
        console.log(this.element);

    }

    public push(donation: Donation) {
        donation.rotationsLeft = 1;
        this.donations.push(donation);
        let elem = $(this.getHtml(donation));

        if (donation.hcn > 0)
            elem.addClass("hcn0");

        if (donation.hcn > 20)
            elem.addClass("hcn20");

        if (donation.hcn > 40)
            elem.addClass("hcn40");

        this.element.prepend(elem);
    }

    private getHtml(donation: Donation) {
        return `<li><b>[${donation.user}] [${donation.hcn} HCN] </b> ${donation.message}</li>`;
    }
}

var list = new DonationList();
function update() {
 

    $.get("https://ams.hophands.pl:9241/donate", { token: "pass" })
        .done(function (data: any[]) {
            for (let donat of data) {
                console.log(donat);
               // for (let a = 0; a < 100;a++)
                list.push(donat);

            }
            console.log(data);
        });
}

setInterval(update, 5000);
