﻿import "reflect-metadata";



declare function require(name: string);
//var Phaser = require("./phaser")
import * as fs from "fs";
import * as https from "https"
import * as http from "http"
import * as express from "express"
import * as compress from "compression"
import { Server } from "./scripts/server/Server"
import winston = require("winston")

winston.level = 'debug';
winston.add(winston.transports.File, {
    filename: 'log.log', formatter: function (options) {
        return new Date().toLocaleTimeString()+ ' ' + options.level.toUpperCase() + ' ' + (options.message ? options.message : '') +
            (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
    }, json: false });

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, { formatter: function (options) {
        // Return string will be passed to logger.
        return new Date().toLocaleTimeString() + ' ' + options.level.toUpperCase() + ' ' + (options.message ? options.message : '') +
            (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
    }, json: false
});


try {
    var privateKey = fs.readFileSync('/etc/letsencrypt/live/privkey1.pem').toString();
    var certificate = fs.readFileSync('/etc/letsencrypt/live/fullchain1.pem').toString();
}
catch (err) {
    winston.warn("There is no SSL certyficate.");
}

declare function require(name: string);
let app = express();


if (privateKey) {
    var credentials = { key: privateKey, cert: certificate };
    var server = https.createServer(credentials, app).listen(9241);
    var server2 = https.createServer(credentials, app).listen(9251);
}
else {
    var server = http.createServer(app).listen(9241);
    var server2 = http.createServer(app).listen(9251);
}

import * as socketIO from "socket.io"
let io = socketIO(server);
let io_lobby = socketIO(server2);

let test = false;
new Server(app, io, io_lobby);