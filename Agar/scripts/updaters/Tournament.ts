﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";
import { DB } from "../databases/Database"
import { GameData } from "../entities/GameData"

import { Profiler } from "../utils/Profiler"
import { SocketServer } from "../socket/Server"
export class TournamentUpdater {

    private allPlayers: GameData[]  = [];
    private data: Models.ITourneyData;
    private handler: any;
    private updates = 0;
    public dead: boolean = false;
    constructor(private id: number, interval: number = 10) {
        this.handler = setInterval(() => this.update(), interval * 1000);
        this.update();
        this.updates = 0;
    }

    public getData(): Models.ITourneyData {
        if (this.data)
            return this.data;
        else
            return <Models.ITourneyData>{};
    }

    public getPlayerData(userId: number): { score: number, place: number, nick: string } {
        for (let i = 0; i < this.allPlayers.length; i++) 
            if (this.allPlayers[i].user == userId)
                return {
                    score: this.allPlayers[i].overallScore,
                    place: i + 1,
                    nick: " "
                }
        
        return null;
    }

    private async update() {

        let challenge = await DB.challenges.get(this.id);

        if (challenge.endTime < Date.now() && challenge.type != Models.ChallengeType.TOURNEY) {
            clearInterval(this.handler);

            this.updates = 0;
            this.dead = true;
            return;
        }
        let profilerId = Profiler.get().start("TournamentUpdater:update");

        this.updates++;
        let cached = this.updates != 20;
        if (!cached) {
            this.updates = 0;
            console.log("Full update");
        }
        let games = this.allPlayers = await DB.gamesData.getAllFromChallenge(this.id, cached);
       // let playerNames = await DB.users.getManyUserNames(games);
        let players =  await DB.users.getManyUsers(games);
        let tourney = this.data = challenge.tourneyData;
        tourney.avgScore = 0;
        tourney.playersNum = games.length;
        tourney.players = [];

        for (let i = 0; i < Math.min(games.length, 400);i++) {
            let game = games[i];

            tourney.avgScore += game.overallScore / tourney.playersNum;
            tourney.players.push({
                place: i + 1,
                nick: players[i].name,
                id: players[i].id,
                respect: players[i].respect,
                level: players[i].level,
                wearedItemsIds: JSON.parse(players[i].usedItemsIds),
                score: game.overallScore,
                status: SocketServer.instance.getUserStatus(game.user)
            });
        }
        tourney.avgScore = Math.round(tourney.avgScore);


        tourney.top3 = [];
        for (let i = 0; i < Math.min(games.length, 3); i++) {
            let user = await DB.users.get(games[i].user);
            let details = <Models.ITourneyPlayerDetails>{};
            details.level = user.level;
            details.respect = user.respect;

            let items = [];
            let weared = user.getAllWearedItems();
            for (let item of weared)
                items.push(item.set);
            details.wearedItemsIds = items;
             
            tourney.top3.push(details);
        }

        let slonGame = await DB.gamesData.get(1, this.id);
        tourney.slonScore = slonGame ? slonGame.overallScore : 0;
        if (!tourney.slonScore)
            tourney.slonScore = 0;

        Profiler.get().end(profilerId);
    }

}