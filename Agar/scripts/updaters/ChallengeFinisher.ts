﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";
import { DB } from "../databases/Database"
import { Challenge } from "../entities/Challenge"
import { Item } from "../entities/Item"
import { User } from "../entities/User"
import winston = require("winston")
import * as Processors from "../processors/Processors"

interface IPrizeToGive {
    prizes: Models.IPrize[];
    place: number;
    user: User;
}

export class ChallengeFinisher {

    private handler: any;
    constructor(interval: number = 60*2 ) {
        this.handler = setInterval(async () => await this.update(), interval * 1000);

    }

    private async getFinsishedChallenges() {
        return DB.challenges.getFinishedKOHsAndTournaments();
    }

    private async update() {
        let challenges = await this.getFinsishedChallenges();

        if (challenges)
        for (let challenge of challenges) {
            if (challenge.type == Models.ChallengeType.KING_OF_THE_HILL)
                await this.finishKOH(challenge); 

            if (challenge.type == Models.ChallengeType.TOURNEY)
               await this.finishTournament(challenge); 

            winston.info("Challenge " + challenge.name + " finished!");

           this.setAsFinished(challenge);
        }
    }


    private async finishKOH(challenge: Challenge) {
        let players = await DB.gamesData.getAllFromChallenge(challenge.id);
        let prize = challenge.prize;
        let prizesToGive: IPrizeToGive[] = [];
        for (let i = 0; i < Math.min(players.length, 3); i++)
            prizesToGive.push({ prizes: [prize], user: await DB.users.get(players[i].user, true), place:i+1 });
        
        await this.assignPrizes(prizesToGive);
    }

    private async finishTournament(challenge: Challenge) {
        let players = await DB.gamesData.getAllFromChallenge(challenge.id);
        let prizes = challenge.tourneyData.prizes;
        let prizesToGive: IPrizeToGive[] = [];

        let prizesForThatUser: Models.IPrize[];
        for (let i = 0; i < players.length; i++) {
            prizesForThatUser = [];
            let place = i + 1;
          
            for (let prize of prizes) 
                if (prize.rangeStart <= place && prize.rangeEnd >= place)
                    prizesForThatUser.push(  prize.prize );

            prizesToGive.push({ prizes: prizesForThatUser, user: await DB.users.get(players[i].user, true), place:place });
        }

        await this.assignPrizes(prizesToGive);
    }


    private async assignPrizes(prizes: IPrizeToGive[]) {
        let usersToUpdate: User[] = [];
        for (let prizeData of prizes) {
            let prizes = prizeData.prizes;
            let user = prizeData.user;
            usersToUpdate.push(user);

            for (let prize of prizes) {
                switch (prize.type) {
                    case Models.ItemType.OTHER:
                       await  this.assignCustomPrize(prize, user);
                        break;

                    case Models.ItemType.MYSTERY_BOX:
                        await this.assignMysteryBox(prize, user);
                        break;

                    case Models.ItemType.HCN:
                        await this.assignHCN(prize, user);
                        break;

                    default:
                        await this.assignItem(prize as Models.IItem, user);
                }
            }
        }

        DB.users.saveMany(usersToUpdate).catch(() => {
            winston.error("Can't assign prizes!");
        });;
    }

    private async assignItem(prize: Models.IItem, user: User) {
        user.addItemsIds([prize]);
        user.addMessage((await DB.messages.add(this.createItemMsg(prize))).id);
    }

    private async assignMysteryBox(prize: Models.IItem, user: User) {
        let randomItem = await Processors.Rewards.dropRandomItem();
        await this.assignItem(randomItem, user);
    }

    private async assignCustomPrize(prize: Models.IPrize, user: User) {
        user.addMessage((await DB.messages.add(this.createCustomMsg(prize))).id);
    }


    private createHCNMsg(item: Models.IItem) {
        return `<style> 
                h3, h2 {
                text-align: center;
                margin: 10px;
                }
                img {
                    display: block;
                    margin: auto;
                    width: 18%;
                }
                </style>
                <h2> Zdobyłeś ${item.bonus.valueHCN} HCN! </h2>

                <h3>Za udział w turnieju zostałeś nagrodzony<br>
                <b>${item.bonus.valueHCN} HitCoinami</b></h3><br>
                <img src='assets/s1/items/${item.icon}.png'>
                <br>
                <h3> Zdobyte HitCoiny pojawią się niedługo na Twoim koncie! </h3>
                `;
    }

    private createCustomMsg(item: Models.IItem) {
        return `<style> 
                h3, h2 {
                text-align: center;
                margin: 10px;
                }
                img {
                    display: block;
                    margin: auto;
                    width: 18%;
                }
                </style>
                <h2> Nagroda! </h2>

                <h3>Za udział w turnieju zdobyłęś:<br>
                <b>${item.name}</b></h3><br>
                <img src='assets/s1/items/${item.icon}.png'>
                <br>
                <h3> Aby otrzymać nagrodę, napisz swój nick w prywatnej wiadomości do HopHands na Facebooku</h3>
                `;
    }


    private createItemMsg(item: Models.IItem) {
        return `<style> 
                h3, h2 {
                text-align: center;
                margin: 10px;
                }
                img {
                    display: block;
                    margin: auto;
                    width: 18%;
                }
                </style>
                <h2> Nowy przedmiot! </h2>

                <h3>Za udział w turnieju zostałeś nagrodzony nowym przedmiotem:<br>
                <b>${item.name}</b></h3><br>
                <img src='assets/s1/items/${item.icon}.png'>
                <br>
                <h3> Przedmiot znajdziesz w swoim ekwipunku! </h3>
                `;
    }


    private async assignHCN(prize: Models.IPrize, user: User) {
        let hcn = prize.bonus.valueHCN;
        if (!hcn)
            hcn = 0;
        user.addMessage((await DB.messages.add(this.createHCNMsg(prize))).id);
        winston.info(hcn + " HCN assigned to user " + user.name + " (" + user.hophands_id + ")");
    }

    private setAsFinished(challenge: Challenge) {
        challenge.endTime = 0;
        DB.challenges.save(challenge);
    }   
}