﻿import * as socketIO from "socket.io"
import { DB } from "../databases/Database"
import request = require("request")
import winston = require("winston")
import { User } from "./User"
import * as SocketModels from "../../../Shared/SocketModels"
import { Token } from "../routes/Token"
import * as Models from "../../../Shared/Shared"
import { PvPResults } from "./PvPResults"

export class PvP {
    private users: User[] = [];
    public acces: Models.AccesToPlay;

    private data: Models.GameData[] = [];
    private results: PvPResults;

    constructor(private player1: User, private player2: User, public id: number) {

        let data = <SocketModels.IPvPInitData>{
            id: id,
            lobbyCountingDuration: 10
        };

        this.generateAcces();
        player1.sendPvPInitData(data);
        player2.sendPvPInitData(data);

        player1.onResults((data) => this.onResults(data, 0));
        player2.onResults((data) => this.onResults(data, 1));

        player1.onUpdate((data) => player2.sendUpdate(data));
        player2.onUpdate((data) => player1.sendUpdate(data));

        player1.onDisconnected = (score) => this.playerDisconnected(0, score);
        player2.onDisconnected = (score) => this.playerDisconnected(1, score);
      //  player1.askForConfirmation(3);
    }

    private playerDisconnected(player: number, score: number) {
        this.onResults(<any>{
            stats: <any>{
                score: score,
                cutFlesh: 1
            }
        }, player);

        winston.warn("[PVP]"+[this.player1, this.player2][player].socketUser.entity.name+ " disconnected!");
    }

    private onResults(data, player: number) {
        if (this.results) {
            [this.player1, this.player2][player].sendResults(this.results.results);
            return;
        }

        this.data[player] = data;

        if (this.data[0] && this.data[1])
            this.calculateOverallResult();
    }

    private async calculateOverallResult() {
        let results = this.results = new PvPResults(this.data, [this.player1, this.player2], this.acces);

        let res1 = JSON.parse(JSON.stringify(results.results));
        let res2 = JSON.parse(JSON.stringify(results.results));
        // let ver = parseFloat(token.version);
        res1.challengesUpdate = await DB.challenges.getChallengesUpdateData(this.player1.socketUser.entity.id, DB.challenges.getChallengeSendOptionsForVersion(0.28), false);
        res2.challengesUpdate = await DB.challenges.getChallengesUpdateData(this.player2.socketUser.entity.id, DB.challenges.getChallengeSendOptionsForVersion(0.28), false);

        this.player1.sendResults(results.results);
        this.player2.sendResults(results.results);

        winston.info(`[PvP] ${this.player1.socketUser.entity.name}:  ${this.data[0].stats.score}, ${this.player2.socketUser.entity.name}:  ${this.data[1].stats.score}`);
    }

    private generateAcces() {
        this.acces = <Models.AccesToPlay>{
            config: <Models.GameConfig>{
                seed: Date.now() % 100000 + 100,
                map: Math.floor(Math.random() * 4),
                duration: 90,
                bonus: {}
            },
            challengeId: 0,
            offline: false
        }
    }

    public hasUser(id: number) {
        return this.player1.socketUser.entity.id == id || this.player2.socketUser.entity.id;
    }

 
}
