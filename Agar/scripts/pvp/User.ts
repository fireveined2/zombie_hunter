﻿import * as socketIO from "socket.io"
import { DB } from "../databases/Database"
import request = require("request")
import winston = require("winston")
import { User as SocketUser} from "../socket/User"
import * as SocketModels from "../../../Shared/SocketModels"
import { Token } from "../routes/Token"
import * as Models from "../../../Shared/Shared"

export class User {
    public onInvitationAnswer: (answer: SocketModels.InvitationAnswer) => void;

    constructor(public socketUser: SocketUser) {
        this.disconnectTimeout = setTimeout(() => { if (this.onDisconnected) this.onDisconnected(0) }, 40*1000);

    }

    public onDisconnected: (score: number) => void;
    private disconnectTimeout: any;


    public sendInvitationResponse(response: SocketModels.InvitationAnswer) {
        this.socketUser.emit(SocketModels.Events.InvitationResponse, response);
    }

    public sendInvitation(data: SocketModels.IInvitation, callback: (answer: SocketModels.InvitationAnswer) => void) {
        this.socketUser.emit(SocketModels.Events.Invite, data);
        this.socketUser.socket.once(SocketModels.Events.InvitationResponse, callback);
    }

    public cancelInvitation() {
        this.socketUser.emit(SocketModels.Events.InvitationCancel);
        this.socketUser.socket.removeAllListeners(SocketModels.Events.InvitationResponse);
    }

    public sendPvPInitData(data: SocketModels.IPvPInitData) {
        this.socketUser.emit(SocketModels.Events.PvPInitData, data);
    }

    public askForConfirmation(timeout: number, timeoutCallback: Function) {
        let timeoutHandler = setTimeout(timeoutCallback, timeout * 1000);

        let id = Date.now() % 10000;
        this.socketUser.emit(SocketModels.Events.ConfirmRequest, id);
        this.socketUser.socket.once(SocketModels.Events.ConfirmAnswer, (userId) => {
            if (userId == id)
                clearTimeout(timeoutHandler);
        });

    }
    public sendUpdate(data: SocketModels.IPvPUpdate) {
        this.socketUser.emit(SocketModels.Events.PvPUpdate, data);
    }

    public onUpdate(callback: (data: SocketModels.IPvPUpdate) => void) {  
        this.socketUser.socket.removeAllListeners(SocketModels.Events.PvPUpdate);
        this.socketUser.socket.on(SocketModels.Events.PvPUpdate, (data: SocketModels.IPvPUpdate) => {
            clearTimeout(this.disconnectTimeout);
            this.disconnectTimeout = setTimeout(() => { if (this.onDisconnected) this.onDisconnected(data.score) }, 10 * 1000);
            callback(data)
        });
  
    }


    public sendResults(data: SocketModels.IPvPResults) {
        this.socketUser.emit(SocketModels.Events.PvPResults, data);
    }

    public onResults(callback: (data: Models.GameData) => void) {
        clearTimeout(this.disconnectTimeout);
        this.socketUser.socket.once(SocketModels.Events.GameData, callback);
    }
}
