﻿import * as socketIO from "socket.io"
import { DB } from "../databases/Database"
import request = require("request")
import winston = require("winston")
import { User } from "./User"
import * as SocketModels from "../../../Shared/SocketModels"
import { Token } from "../routes/Token"
import { User as SocketUser } from "../socket/User"
import { Invitation } from "./Invitation"
import { PvP } from "./PvP"
export class PvPServer {

    private pendingInvitations: Invitation[] = [];
    private pvps: PvP[] = [];
    constructor() {

 


        
    }


    public onPvPRequest(senderSocket: SocketUser, addresseeSocket: SocketUser) {
        let invitation = new Invitation(senderSocket, addresseeSocket, this.onPvPArranged.bind(this));
    }


    private onPvPArranged(player1: User, player2: User) {
        let id = this.pvps.length+1;
        let pvp = new PvP(player1, player2, id);
        this.pvps.push(pvp);
    }


    public getPvPAccesToPlay(userId: number, pvpId: number) {
        for (let i = 0; i < this.pvps.length; i++)
            if (this.pvps[i].id == pvpId && this.pvps[i].hasUser(userId)) 
                return this.pvps[i].acces;

            

        return null;
    }

    public onJoinQueue(user: SocketUser) {

    }
}
