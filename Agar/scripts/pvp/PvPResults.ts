﻿import * as socketIO from "socket.io"
import { DB } from "../databases/Database"
import request = require("request")
import winston = require("winston")
import { User } from "./User"
import * as SocketModels from "../../../Shared/SocketModels"
import { Token } from "../routes/Token"
import * as Models from "../../../Shared/Shared"

export class PvPResults {

    public results: SocketModels.IPvPResults;

    constructor(private data: Models.GameData[], private players: User[], private acces: Models.AccesToPlay) {
        let winner = this.getWinner();
        let looser = this.getLooser();

       
           

        let p1 = players[0].socketUser.entity;
        let res1 = p1.respect;
        let p2= players[1].socketUser.entity;
        let res2 = p2.respect;

        winner.socketUser.entity.respect += 50;
        looser.socketUser.entity.respect -= 50;

        if (looser.socketUser.entity.respect < 0)
            looser.socketUser.entity.respect = 0;

        players[0].socketUser.entity.exp += data[0].stats.cutFlesh
        players[1].socketUser.entity.exp += data[0].stats.cutFlesh

        DB.users.saveMany([players[0].socketUser.entity, players[1].socketUser.entity]);

        let result1 = <SocketModels.IPvPPlayerResults>{
            id: players[0].socketUser.entity.id,
            name: players[0].socketUser.entity.name,
            score: data[0].stats.score,
            respect: players[0].socketUser.entity.respect,
            respectChange: players[0].socketUser.entity.respect - res1,
            exp: data[0].stats.cutFlesh
        }

        let result2= <SocketModels.IPvPPlayerResults>{
            id: players[1].socketUser.entity.id,
            name: players[1].socketUser.entity.name,
            score: data[1].stats.score,
            respect: players[1].socketUser.entity.respect,
            respectChange: players[1].socketUser.entity.respect - res2,
            exp: data[1].stats.cutFlesh
        }

        let results = <SocketModels.IPvPResults>{
            players: [result1, result2],
            winnerId: this.getWinner().socketUser.entity.id
        }
        this.results = results;
    }

    

    public getWinner() {
        if (this.data[0].stats.score > this.data[1].stats.score)
            return this.players[0]
        else
            return this.players[1];
    }

    public getLooser() {
        if (this.data[0].stats.score > this.data[1].stats.score)
            return this.players[1]
        else
            return this.players[0];
    }

}
