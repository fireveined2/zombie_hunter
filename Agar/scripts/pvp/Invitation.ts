﻿import * as socketIO from "socket.io"
import { DB } from "../databases/Database"
import request = require("request")
import winston = require("winston")
import { User as UserSocket } from "../socket/User"
import * as SocketModels from "../../../Shared/SocketModels"
import { Token } from "../routes/Token"
import { User } from "./User"

export class Invitation {

    private sender: User;
    private addressee: User;
    public pending: boolean = true;
    

    constructor(public senderSocket: UserSocket, public addresseeSocket: UserSocket, private onAccepted: (player1, player2) => void) {
        let sender = this.sender = new User(senderSocket);
        //  console.log(addresseeSocket);
        if (!addresseeSocket || addresseeSocket.getStatus() != SocketModels.UserStatus.ONLINE) {
            sender.sendInvitationResponse(SocketModels.InvitationAnswer.NO);
            this.pending = false;
            return;
        }
        if (!senderSocket.entity || !addresseeSocket.entity){

            sender.sendInvitationResponse(SocketModels.InvitationAnswer.NO);
            this.pending = false;
            return;
        }
        winston.info(senderSocket.entity.name + " challenged " + addresseeSocket.entity.name);
        this.addressee = new User(addresseeSocket);
        this.addressee.sendInvitation({
            sender: senderSocket.entity.generateInfo()
        }, this.onResponse.bind(this));

        senderSocket.socket.removeAllListeners(SocketModels.Events.InvitationCancel);
        senderSocket.socket.once(SocketModels.Events.InvitationCancel, ()=>this.cancel());
    }

    private onResponse(response: SocketModels.InvitationAnswer) {
        this.sender.sendInvitationResponse(response);

        if (response == SocketModels.InvitationAnswer.YES) {
            winston.info(this.addresseeSocket.entity.name + " accepted");
            this.onAccepted(this.sender, this.addressee);
        }

        if (response == SocketModels.InvitationAnswer.NO)
            winston.info(this.addresseeSocket.entity.name + " declined");

    }

    private cancel() {
        this.addressee.cancelInvitation();
    }


}
