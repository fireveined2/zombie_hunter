﻿
import * as Models from "../../../Shared/Shared"
import * as SocketModels from "../../../Shared/SocketModels"
import winston = require("winston")
import { User as UserEntity } from "../entities/User"
export class User {

    public onAuthCallback: (data: SocketModels.IAuthData) => UserEntity;
    public entity: UserEntity;
    private isInGame: boolean = false;

    public onPvPRequest: (addresseeId: number) => void;
    public onJoinPvPQueue: () => void;

    constructor(public socket: SocketIO.Socket) {
        socket.on(SocketModels.Events.Auth, async (data) => {
            winston.info("Auth "+data.token);
            setTimeout(async () => {await this.onAuth(data)}, 1000);
        });


        socket.on(SocketModels.Events.StartedGame, (data) => {
            this.isInGame = true;
        });

        socket.on(SocketModels.Events.EndedGame, (data) => {
            this.isInGame = false;
        });


        socket.on(SocketModels.Events.Invite, (id) => {
            this.onPvPRequest(id);
        });

        socket.on(SocketModels.Events.JoinQueue, () => {
            this.onJoinPvPQueue();
        });
    }

    private async onAuth(data: SocketModels.IAuthData) {
        this.entity = await this.onAuthCallback(data);
        if (this.entity)
            winston.info("[SOCKET] " + this.entity.name + " auth");
        else
            winston.warn("[SOCKET] Can't auth user!");
    }

    public getGameId() {
        if (this.entity)
            return this.entity.id;
        return 0;
    }

    public getStatus() {
        if (this.isInGame)
            return SocketModels.UserStatus.INGAME;

        return  SocketModels.UserStatus.ONLINE;
    }

    public emit(event: string, data?: any) {
        this.socket.emit(event, data);
    }

    public getSocketId() {
        return this.socket.id;
    }

}


