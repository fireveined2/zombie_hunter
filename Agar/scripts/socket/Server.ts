﻿import * as socketIO from "socket.io"
import { DB } from "../databases/Database"
import request = require("request")
import winston = require("winston")
import { User } from "./User"
import * as SocketModels from "../../../Shared/SocketModels"
import { Token } from "../routes/Token"


export class SocketServer {
    private users: { [id: number]: User } = {} ;

    static instance: SocketServer;

    constructor(private socket: SocketIO.Server) {
        SocketServer.instance = this;

        this.socket.on("connect", (socket) => {
           this.onUserConnected(socket);
        });
        
    }

    public onPvPRequest: (sender: User, addresee: User) => void;
    public onJoinPvPQueue: (user: User) => void;

    private async getUserFromAuthData(data: SocketModels.IAuthData) {
        return DB.users.get(Token.getIDFromToken(data.token), true);
    }

    private removeUser(user: User) {
        if (user.entity)
        winston.info("[SOCKET] " + user.entity.name + " disconnected, online players: " + this.getOnlineUsersCount());
        delete this.users[user.getSocketId()];
    }

    private addUser(user: User) {
        this.users[user.getSocketId()] = user;
    }

    public getUserById(id: number) {
        for (let socketid in this.users)
            if (this.users.hasOwnProperty(socketid))
                if (this.users[socketid].getGameId() == id)
                    return this.users[socketid];

        return null;
    }

    public getUserStatus(id: number) {
        let user = this.getUserById(id);
        if (user)
            return user.getStatus();
        return SocketModels.UserStatus.OFFLINE;
    }

    private getOnlineUsersCount() {
        let online = 0;
        for (let socketid in this.users)
            if (this.users.hasOwnProperty(socketid))
                online++;

        return online;
    }

    private onUserConnected(socket: SocketIO.Socket) {
        winston.info("[SOCKET] Somebody connected, online players: " + this.getOnlineUsersCount());
        let user = new User(socket);
        user.onAuthCallback = this.getUserFromAuthData.bind(this);
        user.onJoinPvPQueue = () => this.onJoinPvPQueue(user);
        user.onPvPRequest = (id) => this.onPvPRequest(user, this.getUserById(id));
        socket.on("disconnect", () => this.onUserDisconnected(user));
        this.addUser(user);
    }

    private onUserDisconnected(user: User) {

        this.removeUser(user);
    }
}
