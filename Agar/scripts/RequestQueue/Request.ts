﻿export enum RequestType {
    LOGIN,
    REGISTER
}

export interface IRequestData {
    email: string;
    password: string;

    username?: string;
    usernameFull?: string;
}


export class Req {

    type: RequestType;
    data: IRequestData;
    version: string;
    os: string;
    osVersion: string;


    constructor(data: any) {
        let req = data.data;
        req.username = req.usernameFull;
        if (req.username)
            this.type = RequestType.REGISTER;
        else
            this.type = RequestType.LOGIN; 

        if(req.email)
            req.email = req.email.trim();
        if(req.username)
            req.username = req.username.trim();

        this.data = req;

        this.osVersion = data.osVersion;
        this.version = data.version;
        this.os = data.os;

        //console.log(this);
    }
}