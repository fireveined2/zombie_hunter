﻿import { Req } from "./Request"
import * as Models from "../../../Shared/Shared"

export class User {
    private request: Req;
    private pos: number;
    public disconnected: boolean = false;
    public onRequest: Function;

    constructor(private socket: SocketIO.Socket) {
        socket.on("req", (data) => {
            this.request = new Req(data);
            this.onRequest(this.request);
        });
    }

    public setAndSendPos(pos: number) {
        this.pos = pos;
        this.socket.emit(Models.QueueMessage.CHANGE_POS, pos);
    }

    public getRequest() {
        return this.request;
    }


    public wait() {
        this.disconnected = true;
        this.socket.emit(Models.QueueMessage.WAIT);
    }

    public sendToken(token: string) {
        this.socket.emit("token", token);
    }

    public onError(error: Models.ServerRequestError) {
        this.socket.emit("error", error);
    }





}




export class DummyUser {
    private request: Req;
    private pos: number;
    public disconnected: boolean = false;
    public onRequest: Function;

    constructor() {
        let data = {
            os: "android", version: "0.13", data: {
                password: Math.random().toString(),
                email: "test" + Math.random().toString() + "@vp.pl",
                usernameFull: undefined
            }
        };
        if (Math.random() > 0.5)
            data.data.usernameFull = "test_user" + Math.round(Math.random() * 9999)

        this.request = new Req(data);
        setTimeout(() => this.onRequest(this.request), 500);
    }

    public setAndSendPos(pos: number) {
        this.pos = pos;
      //  this.socket.emit(Models.QueueMessage.CHANGE_POS, pos);
    }

    public getRequest() {
        return this.request;
    }


    public wait() {
        this.disconnected = true;
       // this.socket.emit(Models.QueueMessage.WAIT);
    }

    public sendToken(token: string) {
   //     this.socket.emit("token", token);
    }

    public onError(error: Models.ServerRequestError) {
   //     this.socket.emit("error", error);
    }

}