﻿import * as socketIO from "socket.io"
import { User, DummyUser } from "./User"
import { Req, IRequestData, RequestType } from "./Request"
import request = require("request")
import winston = require("winston")
import * as Models from "../../../Shared/Shared"



export class RequestQueue {
    private users: User[] = [];

    private busy: boolean = false;
    public onDataDownloaded:  (data: Models.HHUserData) => void;

    private delay = 1.5;
    private prev = Date.now();
    private prevDuration = 0;

    constructor(private socket: SocketIO.Server) {

        this.socket.on("connect", (socket) => {
            let user = new User(socket);
            socket.on("disconnect", () => this.onUserDisconnected(user));
            this.addUser(user);
           
             
        });


      //  setInterval(() => {
      //    //  if(this.user)
      //  }, 5000);
       
    }


    private addUser(user: User) {

        user.onRequest = (req: Req) => {
            for (let userInQueue of this.users)
                if (userInQueue.getRequest().data.email == req.data.email)
                    return;

            let elapsed = Date.now() - this.prev;
            this.users.push(user);
            user.setAndSendPos(this.users.length);
            winston.debug(req.data.email + " connected to queue (" + req.type + "). Current length: " + this.users.length + ", delay " + Math.round(this.delay * 1000) + "ms, " + req.version + ", " + req.os + " " + req.osVersion);

            this.prev = Date.now();
            if (this.users.length == 1 && !this.busy)
                this.handleNextUser();
        }
    }

    private onUserDisconnected(user: User) {
        if (user.disconnected)
            return;

        winston.debug("User disconnected from queue");
        this.users.splice(this.users.indexOf(user), 1);
        this.updatePositions();
    }

    private handleNextUser() {
        if (this.users.length == 0)
            return;
        let user = this.users.shift();

        this.sendRequest(user.getRequest(), user.sendToken.bind(user),(err)=> user.onError(err));
        this.updatePositions();

        this.busy = true;
        user.wait();
        
        setTimeout(() => {
            this.busy = false;
            if (this.users.length > 0)
                this.handleNextUser();
        }, this.delay * 1000);
        
    }

    private updatePositions() {
        for (let i = 0; i < this.users.length; i++)
            this.users[i].setAndSendPos((i+1));
    }

    private sendRequest(req: Req, tokenCallback: (token: string) => void, errorCallback: (error) => void) {
        if (req.type == RequestType.LOGIN)
            this.sendLoginReq(req, tokenCallback, errorCallback);
        if (req.type == RequestType.REGISTER)
            this.sendRegisterReq(req, tokenCallback, errorCallback);
    }


    private sendLoginReq(req: Req, tokenCallback: (token: string) => void, errorCallback: (err) => void) {
        let data = req.data;
        let time = Date.now();
        let timeout = setTimeout(() => winston.error("REQUEST ERROR " + req.data.email), 30*1000);
        try {
            request.post("https://hophands.pl/mobile/user/sign-in", { body: JSON.stringify(data), timeout: 25 * 1000 }, async (error, response: request.RequestResponse, body) => {
                let elapsed = Date.now() - time;
                this.handleDelay(elapsed);
                clearTimeout(timeout);

                if (error) {
                    this.delay += 2;
                    if (error.code === 'ETIMEDOUT') {
                        errorCallback(Models.ServerRequestError.SERVER_TIMEOUT);
                        winston.warn("Can't login " + data.email + ", SERVER TIMEOUT!");
                     
                        return;
                    }

                    errorCallback(Models.ServerRequestError.SERVER_TIMEOUT);
                    winston.warn("Can't login " + data.email + ", error!", error.code);
                    return;

                }
                if (body) {
                    let token = await this.handleData(body, req.version, req.os);
                    if (token)
                        tokenCallback(token);
                    else {
                        winston.warn("Can't login " + data.email + ", bad credentials");
                        errorCallback(Models.ServerRequestError.BAD_CREDENTIALS);
                   
                    }
                }
                else {

                    winston.warn("Can't login " + data.email + ", empty body");
                    errorCallback(Models.ServerRequestError.SERVER_TIMEOUT);
                }
            });
        }
        catch (e) {
            winston.error(e);
        }
    }

    private async handleData(body: any, version, os) {
        if (!os || !version) {
            winston.warn("Empty os or version");
            return null;
        }
         
        let user;
        try {
            user = JSON.parse(body);
        }
        catch (e) {
            this.delay += 8;
            winston.error("Can't parse login response!", e);
            return null;
        }

        if (user && user.token !== undefined) {
            os = os[0];
            let baseToken = user.token;
            user.token = version + os + "|" + user.token;
            await this.onDataDownloaded(user);




            return baseToken;
        }
        else {
            winston.debug(body);
            return null;
        }
    }


    private handleDelay(elapsed: number) {
        this.prevDuration = elapsed;
        if (elapsed > 15 * 1000)
            this.delay += 0.2;
        else
            if (elapsed > 7 * 1000)
                this.delay += 0.1;
        if (elapsed > 3.5 * 1000)
            this.delay += 0.0;
        else {
            if (this.delay > 5)
                this.delay -= 2;
            else
                if (this.delay > 1.5)
                    this.delay -= 0.5;
                else {
                    if (this.delay > 1.2)
                        this.delay -= 0.02;
                }
            }
    }
     private sendRegisterReq(req: Req, tokenCallback: (token: string) => void, errorCallback: (error) => void) {
         let data = req.data;
         let time = Date.now();
         request.post("https://hophands.pl/mobile/user/sign-up", { body: JSON.stringify(data), timeout: 25 * 1000 }, async (error, response: request.RequestResponse, body) => {

             let elapsed = Date.now() - time;
             this.handleDelay(elapsed);


             if (error) {

                 this.delay += 2;
                 if (error.code === 'ETIMEDOUT') {
                     errorCallback(Models.ServerRequestError.SERVER_TIMEOUT);
                     winston.warn("Can't register " + data.email + ", SERVER TIMEOUT!");

                     return;
                 }
                 errorCallback(Models.ServerRequestError.SERVER_TIMEOUT);
                 winston.warn("Can't register " + data.email + ", error!", error.code);


                 return;
             }
             if (body) {
                 let token = await this.handleData(body, req.version, req.os);
                 if (token)
                     tokenCallback(token);
                 else {
                     let reason = Models.ServerRequestError.SERVER_TIMEOUT;
                     if (body.indexOf("username") != -1) {
                         if (body.indexOf("wprowa") != -1)
                             reason = (Models.ServerRequestError.WRONG_NAME);
                         else
                             reason = (Models.ServerRequestError.NAME_ALREADY_USED);

                     }
                     else
                         if (body.indexOf("email") != -1)
                             reason = (Models.ServerRequestError.EMAIL_ALREADY_USED);
                         else
                             reason = (Models.ServerRequestError.SERVER_TIMEOUT);

                     errorCallback(reason);
                     winston.warn("Can't register " + data.email + ". Reason: " + Models.ServerRequestError[reason]);
                 }

             }
             else {
                 winston.warn("Can't register " + data.email + ", empty body!");
                 errorCallback(Models.ServerRequestError.SERVER_TIMEOUT);
             }

        });
    }
}