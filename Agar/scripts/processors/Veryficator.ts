﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"
import winston = require("winston")
import { Veryficator } from "../server/GameVeryficator"
import { GameConfig } from "../entities/GameConfig"
import { User } from "../entities/User"
import { GameToValidate } from "../entities/GameToValidate"
var fs = require('fs');
import { ItemsHelpers } from "../../../Shared/Items"
export class VeryficationProcessor{

    private static veryficator: Veryficator = new Veryficator();

    static async addGameToVeryficationQueue(game: Models.GameData, userId: number) {
        let user = await DB.users.get(userId);
        let acces = <Models.AccesToPlay>{
            challengeId: game.challenge,
            config: null
        };
        if (game.pvpId) {
            acces.config = DB.pvp.getPvPAccesToPlay(userId, game.pvpId).config;
        }
        else
        if (game.challenge) {
            let challenge = await DB.challenges.get(game.challenge, true);

            acces.config = JSON.parse(JSON.stringify(challenge.config));
            if ( acces.config.bonus.dontIgnoreItemBonus) 
                acces.config.bonus = ItemsHelpers.addBonuses([ acces.config.bonus, ItemsHelpers.getAllBonusses(user.getAllWearedItems())]);
            if (acces.config.seed == 0)
                acces.config.seed = game.seed;

            if (game.challenge == 70 && game.stats.score>20000) {
                let logName = "tournament_input_" + user.name + "_" + game.stats.score + ".txt"
                fs.writeFile(logName, game.input, function (err) {
                    if (err) {
                        return winston.error("Can't save input ", err);
                    }
                });
                winston.info("Saving tournament input as " + logName);
            }
        }
        else {
            acces.config = GameConfig.getTrainingConfig(game.trainingMap, user.getAllWearedItems());
            acces.config.seed = user.nextTrainingSeed;
        }

        if (acces.config.map === undefined) {
            winston.debug("Game: ", { challenge: game.challenge }, acces);
            throw new Error("[VERYFICATOR] Undefined map!");
        }

        let data = <Models.GameData>{
            input: game.input,
            challenge: game.challenge,
            screenHeight: game.screenHeight,
            screenWidth: game.screenWidth,
            stats: JSON.parse(JSON.stringify(game.stats)),
            debugPointsInfo: game.debugPointsInfo
        }

       

        this.veryficator.push(data, acces, (game, acces, actual) => this.onNotValid(user, game, acces, actual));   
    }

    static onNotValid(user: User, data: Models.GameData, acces: Models.AccesToPlay, actual: Models.GameStats) {
     //   user.banned = 1;
     //   DB.users.save(user);
        try {
  
            let repo = DB.instance.repoFor(GameToValidate);
            let game = repo.create();
            game.gameStats = JSON.stringify(actual);
            game.playerStats = JSON.stringify(data.stats);
            game.config = JSON.stringify(acces.config);
            game.input = " ";
            game.user = user.id;
            game.width = data.screenWidth;
            game.height = data.screenHeight;
            repo.persist(game).catch((error) => {
                winston.error("Can't save game for manual validation! ", error);
            });;

            let logName = "input_" + user.name + "_" + data.stats.score + ".txt"
            fs.writeFile(logName, data.input, function (err) {
                if (err) {
                    return winston.error("Can't save input ", err);
                }
            });
            winston.info("Saving input as " + logName);
        }
        catch (error) {
            winston.error("[onNotValid] Error: ", error, data, actual, acces);
        }
    }
}


var ab2str_arraymanipulation = function (buf) {
    var bufView = new Uint8Array(buf);
    var unis = [];
    for (var i = 0; i < bufView.length; i++) {
        unis.push(bufView[i]);
    }
    return String.fromCharCode.apply(null, unis);
}
