﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"
import winston = require("winston")
import { CriteriaMatchResult } from "./Criteria"
import { HHClient } from "../server/HHClient"
import { Profiler } from "../utils/Profiler"

export class GameResultsProcesssor {


    static async getGameResults(stats: Models.GameStats, userId: number, challengeId: number): Promise<Models.ICalculateResultData> {
  
        if (challengeId == 0 || !challengeId || challengeId == undefined || (challengeId as any)=="0")
            return await GameResultsProcesssor.getResultsForTraining(stats);
        else
            return await GameResultsProcesssor.getResultsForChallenge(stats, userId, challengeId);
    }

    private static async getResultsForTraining(stats: Models.GameStats): Promise<Models.ICalculateResultData> {

        let overall = <Models.OverallResults>{
            stars: 3,
            respect: Math.round(stats.score / 1000),
            exp: Math.floor(stats.cutFlesh)
        }

        let results = <Models.ICalculateResultData>{
            overall: overall,
            flags: {}
        }
        return Promise.resolve(results);
    }


    private static async getResultsForChallenge(stats: Models.GameStats, userId: number, challengeId: number): Promise<Models.ICalculateResultData> {
        

        let results = <Models.OverallResults>{};
        let flags = <Models.IGameHandleFlags>{};

        let user = await DB.users.get(userId);
        let challenge = await DB.challenges.get(challengeId, true);

        let profilerId = Profiler.get().start("getResultsForChallenge" + challenge.type);

        let criteria = challenge.criteria;

         results.exp = stats.cutFlesh;
        if (challenge.type == Models.ChallengeType.CUMULATE) {
            // skumuluj wszystkie rozgrywki
            let prevStats = <Models.GameStats>{};
            let previus = await DB.gamesData.get(userId, challengeId);
            if (previus) {
                prevStats = Processors.GameStats.decompress(previus.data);
               // winston.debug(prevStats.numberOfTries + " past tries found for user " + user);
            }
            stats = Processors.GameStats.add(stats, prevStats);
            console.log(stats.numberOfTries);

            let result = Processors.ChallengeCriteria.match(stats, criteria);
          

            if (result == CriteriaMatchResult.SUCCES) {
                results.challengeResult = Models.ChallengeResult.SUCCES;
                results.respect = challenge.gain;
            }

            if (result == CriteriaMatchResult.PLAY_AGAIN) {
                results.challengeResult = Models.ChallengeResult.PLAY_MORE;
                results.respect = previus ? 0 : -challenge.cost;
            }

            if (result == CriteriaMatchResult.FAILURE) {
                results.challengeResult = Models.ChallengeResult.FAILURE;
                results.respect = previus ? 0 : -challenge.cost;
                flags.removeGameData = true;
            }

        }
        else if (challenge.type == Models.ChallengeType.SIMPLE) {
            let result = Processors.ChallengeCriteria.match(stats, criteria);
            results.challengeResult = Models.ChallengeResult.SUCCES;
            results.respect = challenge.gain
            if (result != CriteriaMatchResult.SUCCES) {
                results.challengeResult = Models.ChallengeResult.FAILURE;
                results.respect = -challenge.cost;
            }
        }
        else if (challenge.type == Models.ChallengeType.KING_OF_THE_HILL) {
            let allGames = await DB.gamesData.getAllFromChallenge(challenge.id);
            let koh = <Models.IKOHResults>{ place: allGames.length + 1, best: 0 };
            results.kohResults = koh;
            for (let i = 0; i < allGames.length; i++) {
                if (allGames[i].overallScore < stats.score ) {
                    koh.place = i + 1;
                    break;
                }
            }
   
            if (koh.place != 1) {
                if (allGames[koh.place - 2].user == userId)
                    koh.place--;
                koh.pointsToNextPlace = allGames[koh.place - 2].overallScore - stats.score;

            }
            results.exp = stats.cutFlesh;
            results.challengeResult = Models.ChallengeResult.PLAY_MORE;
            results.respect = -challenge.cost;           
        }
        else if (challenge.type == Models.ChallengeType.TOURNEY) {
            let isTraining = user.isCurrentChallengeTraining();
           
            if (isTraining) {

                flags.isChallengeTraining = true;
                results.wasChallengeTraining = true;
            }
            else {
                flags.saveOnlyIfBetter = true;
                user.tourneyGames++;
            }
            let allGames = await DB.gamesData.getAllFromChallenge(challenge.id);
            let koh = <Models.IKOHResults>{ place: allGames.length + 1, best: stats.score };
            results.kohResults = koh;
            for (let i = 0; i < allGames.length; i++) {
                if (allGames[i].overallScore < stats.score || allGames[i].user == userId) {
                    if (allGames[i].user == userId && allGames[i].overallScore > koh.best)
                        koh.best = allGames[i].overallScore;
                    koh.place = i + 1;
                    break;
                }
            }


            if (koh.place != 1)
                koh.pointsToNextPlace = allGames[koh.place - 2].overallScore - stats.score;


            results.exp = stats.cutFlesh;
            results.challengeResult = Models.ChallengeResult.PLAY_MORE;
            results.respect = Math.round(stats.score / 1000);

            if (!isTraining && !user.chatBan && DB.zhconfig.getConfigForPlayers().streamEnabled && user.tourneyGames%2==1)
            results.canMakeDonate = true;   
        }

        if (results.respect>0)
        results.respect *= Math.min(user.hp + 0.2, 1);
        results.respect = Math.round(results.respect);

        Profiler.get().end(profilerId);
        return Promise.resolve({ flags: flags, overall: results });
    }





}

