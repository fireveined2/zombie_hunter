﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"
import winston = require("winston")


export class GameDataProcessor {

    static async update(data: Models.GameData, user: number, result: Models.ChallengeResult, options: Models.IGameHandleFlags) {
        let challenge = await DB.challenges.get(data.challenge);

        let prev = await DB.gamesData.get(user, data.challenge);
        if (options.removeGameData) {
            if (prev)
                DB.gamesData.remove(prev).catch((error) => {
                    winston.error("Cant remove game data!", { user: user }, error.stack);
                });
            return;
        }


        if (!prev) {
            prev = await DB.gamesData.repo.create();
            prev.challenge = data.challenge;
            prev.user = user;
            prev.overallScore = -1;
   
            if (options.isChallengeTraining) {
                prev.status = Models.GameDataStatus.TRAINING;
                prev.data = JSON.stringify({numberOfTrainings: 1 });
            }
        }


        if (options.saveOnlyIfBetter && data.stats.score < prev.overallScore)
            return;




        if (!options.isChallengeTraining) {
            if (result == Models.ChallengeResult.SUCCES)
                prev.status = Models.GameDataStatus.SUCCES;
            else
                prev.status = Models.GameDataStatus.NORMAL;
        prev.overallScore = data.stats.score;
            if (prev.stats)
            data.stats.numberOfTrainings = prev.stats.numberOfTrainings;
            prev.data = Processors.GameStats.compress(data.stats);
        }
        else {
            if (prev.stats) {
                prev.stats.numberOfTrainings++;
                prev.data = Processors.GameStats.compress(prev.stats);
            }
        }


        DB.gamesData.save(prev).catch((error) => {
            winston.error("Cant update game data!", { user: user }, error.stack);
        });
    }

}


