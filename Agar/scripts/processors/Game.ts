﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"

import { Profiler } from "../utils/Profiler"
import winston = require("winston")

export class GameProcessor {


    static async handleGame(game: Models.GameData, user: number, uptodatedVersion: boolean): Promise<Models.GameResults> {
        try {
            return await this._handleGame(game, user, uptodatedVersion);
        }
        catch (error) {
            winston.error("Can't parse user game!", error.stack,  game.stats, game.challenge, user );
        }
    }

    private static async _handlePvP(game: Models.GameData, userId: number) {
        let profilerId = Profiler.get().start("_handlePvP");
        let user = await DB.users.get(userId);

        Processors.Veryficator.addGameToVeryficationQueue(game, userId);
       // let data = await Processors.GameResults.getGameResults(game.stats, user, game.challenge);


    }
    
    private static async _handleGame(game: Models.GameData, user: number, uptodatedVersion: boolean): Promise<Models.GameResults> {
        let profilerId = Profiler.get().start("_handleGame");

        let rewards: Models.IPrize[] = [];
        let overall = <Models.OverallResults>{};
        let results = <Models.GameResults>{};

        let userData = await DB.users.get(user);
      //  winston.debug("Start parsing challenge " + game.challenge);

        // wrzuca grę do kolejki weryfikacji
        if (uptodatedVersion)
            Processors.Veryficator.addGameToVeryficationQueue(game, user);
        else {
            winston.warn(userData.name + " plays outdated version!");
        }
        
        // zwraca tablicę wydropionych w czasie gry nagród, niezależnie od ostatecznego rezultatu gry
        let drop = await Processors.Rewards.dropRewards(game.stats, user); //TO-DO usunąć asynchroniczność, bo się da
        rewards.push(...drop);

        /*
        zwraca ogólną ocenę gry:
        [
            - sukces/porażka w przypadku wyzwania zwykóego
            - sukces/playMore w przypadku wyzwania kumulowanego
            - oceną gwiazdkową w przypadku treningu
        ]
        oraz
        - ilość zdobytego/straconego respektu
        */
     
        let data = await Processors.GameResults.getGameResults(game.stats, user, game.challenge);
        overall = data.overall;
        let flags: Models.IGameHandleFlags = data.flags;

        // wyzwanie wygrane - przyznaj graczowi nagrodę
        if (overall.challengeResult === Models.ChallengeResult.SUCCES) {
            let prize = await Processors.Rewards.getChallengeRewards(game.challenge);

            ///TYLKO DLA BETY
            if (prize.length > 0)
                rewards = [];

            rewards.push(...prize);
        }
     
        //log
        let result = ", result: LOST";
        if (overall.challengeResult === Models.ChallengeResult.SUCCES)
            result = ", result: WIN";

        if (overall.challengeResult === Models.ChallengeResult.PLAY_MORE)
            result = ", result: TRY AGAIN";

        let h1 = `Challenge: ${game.challenge}`;
        if (flags.isChallengeTraining)
            h1 += " (T)";

        if (!game.challenge) {
            h1 = `Map: ${game.trainingMap}`;
            result = "";
        }
        let rewardString = "";
        if (rewards.length > 0)
            rewardString = ", reward: " + rewards[0].name;
        winston.info("[" + userData.name + `]  ${h1} ${result}, respect: ${overall.respect} / ${userData.respect + overall.respect}, exp: ${overall.exp} / ${userData.exp + overall.exp}, score: ${game.stats.score}` + rewardString);

        results.overall = overall;
        results.prizes = rewards;


      // jeśli gra jest wyzwaniem, uaktualnij zapis gry w bazie: najlepszy wynik dla wyzwania zwykłego i ogólny dla kumulowanego
        if (game.challenge != 0) {
            await Processors.GameData.update(game, user, results.overall.challengeResult, flags);
        }

        // uaktualnia respekt, nagrody, exp
        await Processors.User.updateAfterGame(game, results, user);

  

        // jeśli gra jest wyzwaniem, uaktualniamy dane w bazie: ilość pozostałych nagród i ew. ilość udanych prób
        if (game.challenge != 0)
            await Processors.Challenge.updateAfterGame(overall, game.challenge, user, game.stats)  // uaktualnia usera: ilość gier, ilość nagród


        Profiler.get().end(profilerId);
        return results;
    }
}



