﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"


export class RewardsProcessor {

    static async dropRandomItem(): Promise<Models.IItem> {
        let items = await DB.items.getAll();
        let itemsWages = [];
        for (let item of items)
            for (let i = 0; i < item.dropChance; i++)
                itemsWages.push(item);

        let reward = itemsWages[Math.floor(Math.random() * itemsWages.length)]
        return reward;
    }



    static async dropRewards(stats: Models.GameStats, user: number): Promise<Models.IPrize[]> {
        let reward: Models.IPrize;

        let dropChance = 0.01;
        if (Math.random() < dropChance) {
            return [await this.dropRandomItem()];
        }


        let drop: number[] = DB.zhconfig.get("cureDrop");
        let antidotum = 0;
        for (let i = 0; i < stats.rages; i++) {
            let rnd = Math.random();
            if (rnd < drop[0]) {
                antidotum = 3;
                break;
            }

            if (rnd < drop[1]) {
                antidotum = 2;
                break;
            }

            if (rnd < drop[2]) {
                antidotum = 1;
                break;
            }
        }
        if (antidotum)
            return [await DB.items.getCure(antidotum)];


        return Promise.resolve([]);
    }


    static async getChallengeRewards(challengeId: number): Promise<Models.IPrize[]> {

        let challenge = await DB.challenges.get(challengeId, true);
        if (!challenge.prize)
            return [];
        return [await DB.items.get(challenge.prizeId)];
    }
}

