﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"
import winston = require("winston")

export class ChallengeProcessor {

    static async updateAfterGame(results: Models.OverallResults, challengeId: number, userId: number, stats: Models.GameStats) {
        let challenge = await DB.challenges.get(challengeId);

        if (results.challengeResult == Models.ChallengeResult.SUCCES) {
            challenge.succesfulTries++;

            if (challenge.prize && challenge.prizesLeft != DB.challenges.enity.PrizesInfinityMark) {
               // winston.debug("Removing one prize from challenge " + challengeId);
                challenge.prizesLeft--;
            }
        }


    //    if (stats.numberOfTries<2)
            challenge.tries++;

 
        DB.challenges.save(challenge).catch((error) => {
            winston.error("Cant update challenge!", { challengeId: challengeId }, error.stack);
        });
    }


}

