﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"
import * as Processors from "./Processors"
import winston = require("winston")
import { User } from "../entities/User"

export class UserProcessor {

    static async updateAfterGame(game: Models.GameData, results: Models.GameResults, userId: number) {
        let user = await DB.users.get(userId, false);

        user.playedGamesNum++;
        user.lastGameTime = Date.now();
        user.hp -= DB.zhconfig.get("diseaseGrowthPerGame");
        if (user.hp < 0.01)
            user.hp = 0.01;

        user.respect += results.overall.respect;
        user.exp += results.overall.exp;

        let neededExp = DB.zhconfig.get("expLevels")[user.level];
        if (user.exp >= neededExp) {
            if (await DB.challenges.userHasDoneAllQuests(userId, user.level))
                user = this.levelUp(user);
            else {
                let subExp = user.exp - neededExp;
                user.exp -= subExp;
                results.overall.exp -= subExp;
                winston.debug(user.name + " has enough exp but hasn't done all requires quests!");
            }
        }

        if (!game.challenge) {
            if (user.getRecords()[game.trainingMap] < game.stats.score) {
                user.setRecordOnMap(game.trainingMap, game.stats.score);
            }

            if (user.getDailyRecords()[game.trainingMap] < game.stats.score) {
                user.setDailyRecordOnMap(game.trainingMap, game.stats.score);
            }
        }

        user.removeAccesForChallenge();

        let prizes = results.prizes;
        for (let prize of prizes)
            user.addItemsIds([await DB.items.get(prize.id)]);

        DB.users.save(user).catch((error) => {
            winston.error("Cant update user!",  user , error.stack);
        });
    }


    static levelUp(user:User) {
        let needed = DB.zhconfig.get("expLevels")[user.level];
        if (user.exp < needed) {
            winston.error("LevelUp invoked but user has not enough exp", user.name);
            return;
        }
        winston.debug("User " + user.name + " has now level " + (user.level+1));
        user.exp -= needed;
        user.level++;
       // user.respect = 0;
        let msg = DB.messages.getLvlUpMessageID(user.level);
        if (msg)
            user.addMessage(msg);

        return user;
    }

    static async updateEQ(eq: Models.IEQChange, userId: number) {
        let user = await DB.users.get(userId, false);
        

        for (let item of eq.usedItemsIds) 
            if (!user.useItem(item))
                return false;
        
       
        for (let item in eq.wearedItemsIds)
            if (!user.hasItem(eq.wearedItemsIds[item]))
                return false;

        user.usedItemsIds = JSON.stringify(eq.wearedItemsIds);
        DB.users.save(user).catch((error) => {
            winston.error("Cant update user eq!", user, eq, error.stack);
        });

        winston.debug("User " + user.name + " changed EQ", eq);
        return true;
    }
}

