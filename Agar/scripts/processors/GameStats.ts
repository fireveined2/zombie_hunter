﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";
import { DB } from "../databases/Database"
import { User } from "../entities/User"
import { GameConfig } from "../entities/GameConfig"
import { ChallengeUtils } from "../../../Shared/Challenges"
import winston = require("winston")

export class GameStatsProcessor {

    static compress(stats: Models.GameStats): string {
        return JSON.stringify(stats);
    }

    static decompress(data: string): Models.GameStats {
        let parsed;
        try {
            parsed = JSON.parse(data);
        }
        catch (error) {
            winston.error("Can't parse game stats!", error, data);
            parsed = {};
        }
        return parsed;
    }

    static add(...stats: Models.GameStats[]): Models.GameStats {
        return ChallengeUtils.addStats(...stats);
    }

   // static average(...stats: Models.GameStats[], games: number): Models.GameStats {
     //   return ChallengeUtils.addStats(...stats);
   // }
}