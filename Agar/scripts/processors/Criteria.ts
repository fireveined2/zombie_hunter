﻿import * as Models from "../../../Shared/Shared"
import { DB } from "../databases/Database"

import winston = require("winston")

export enum CriteriaMatchResult {
    FAILURE,
    PLAY_AGAIN,
    SUCCES

}

export class ChallengeCriteriaProcessor {

    static compress(criteria: Models.ChallengeCriteria[]): string {
        return JSON.stringify(criteria);
    }

    static decompress(data: string): Models.ChallengeCriteria[] {
        return JSON.parse(data);
    }

    static match(stats: Models.GameStats, criteria: Models.ChallengeCriteria[]): CriteriaMatchResult {
        let play_again = false;
        for (let criterion of criteria) {
            let expected = criterion.value;
            let actual = stats[criterion.stat];

       
            switch (criterion.operator) {
                case Models.ChallengeCriteriaOperator.LESS:
                    if (!(actual < expected)) {
                            return CriteriaMatchResult.FAILURE;
                    }
                    break;

                case Models.ChallengeCriteriaOperator.MORE:
                    if (!(actual >= expected))
                        play_again = true;
                    break;

                case Models.ChallengeCriteriaOperator.EQUAL:
                    if (!(actual == expected)) {
                        if (actual > expected)
                            return CriteriaMatchResult.FAILURE;
                        else
                            play_again = true;
                    }
                        
                    break

                default:
                    winston.warn("Unknown operator in challenge criterion!");
            }

        }
        if (play_again)
            return CriteriaMatchResult.PLAY_AGAIN;
        else
            return CriteriaMatchResult.SUCCES;
    }
}