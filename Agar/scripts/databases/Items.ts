﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";

import { DB } from "./Database"
import { Item } from "../entities/Item"
import { GameConfig } from "../entities/GameConfig"

export class Items {

    private cache: Item[] = [];

    private static instance;

    static get(): Items {
        if (!Items.instance)
            throw Error("Items does not exist");
        return this.instance;
    }

    constructor(private DB: DB) {
        if (Items.instance)
            throw Error("Items already exists");

        Items.instance = this;


    }

    get repo() {
        return this.DB.repoFor(Item);
    } 

    get enity(): typeof Item {
        return Item;
    }

    public save(entity: Item) {
        this.cache[entity.id] = entity;
        return this.repo.persist(entity);
    }


    public get(id: number): Promise<Models.IItem>{
        if (this.cache[id])
            return Promise.resolve(this.cache[id]);

        return this.repo.findOneById(id).then((item) => {
            if (item)
                this.cache[id] = item;
            return item;
        });
    }

    public getCure(size: number): Promise<Models.IItem> {
        return this.get(10 + size);
    }

    public getAll(): Promise<Models.IItem[]> {
        return this.repo.find();
    }

    private initDefault() {


    }

}