﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";
import { DB } from "./Database"
import { User } from "../entities/User"
import { GameConfig } from "../entities/GameConfig"
import winston = require("winston")
import { Token } from "../routes/Token"
import { ItemsHelpers } from "../../../Shared/Items"
import { Challenge } from "../entities/Challenge"
import { HHClient } from "../server/HHClient"
import * as Processors from "../processors/Processors"
// /users/id
export class Users {



    private cache: User[] = []
    private static instance;


    constructor(private DB: DB) {
        if (Users.instance)
            throw Error("Users already exists");
        Users.instance = this;

        setTimeout(async () => {
            let box = 0;
            if (Date.now() < 1498069779819 + 1000 * 3600 * 3 && box>0) {
                await this.getAll();
             
                for (let user of this.cache) {
                    if (!user)
                        continue;
                    box++;
                    let item = await Processors.Rewards.dropRandomItem();
                    user.addItemsIds([item]);
                    user.pendingRewards.push({ type: Models.IRewardType.MYSTERY_BOX, name: item.name, icon: item.icon });

                }
            }
            winston.info("Mystery Box granted for all users: "+box);
        }, 1000*60);
    }

    static get(): Users {
        if (!Users.instance)
            throw Error("Users does not exist");

        return this.instance;
    }

    get repo() {
        return this.DB.repoFor(User);
    }

    public save(entity: User, onlyCache?: boolean) {
        this.cache[entity.id] = entity;
        if (onlyCache)
            return;
        return this.repo.persist(entity).catch((error) => winston.error("Can't save user data!", entity, error));
    }

    public saveMany(entities: User[]) {
        for (let entity of entities)
        this.cache[entity.id] = entity;
        return this.repo.persist(entities).catch((error) => winston.error("Can't save users data!", error));
    }


    public get(id: number, cached: boolean = true): Promise<User> {
        if (cached && this.cache[id])
            return Promise.resolve(this.cache[id]);


        return this.repo.findOneById(id).then(async (user) => {
            if (user) {
                await this.assignItemsToUser(user);
                this.saveToCache(user);
            }

            return user;
        });
    }

    private saveToCache(user: User) {
        let pending = [];
                   if (this.cache[user.id] && this.cache[user.id].pendingRewards)
            pending = JSON.parse(JSON.stringify(this.cache[user.id].pendingRewards));
        this.cache[user.id] = user;
        this.cache[user.id].pendingRewards = pending;
    }

    public getAll(lastGameLimitMs: number = Date.now()) {
        return this.repo.createQueryBuilder("user")
            .where("user.lastGameTime > " + (Date.now() - lastGameLimitMs))
            .getMany()
            .then((users) => {
            if (users)
                for (let user of users) {
                    this.assignItemsToUser(user);
                    this.saveToCache(user);
                }
            return users;
        });
    }


    private creatingUsers: boolean[] = [];
    public async getUserFromHophandsId(hh_id: number): Promise<User> {
        if (this.creatingUsers[hh_id]) {
            return new Promise<User>((resolve) => {
                setTimeout(() => resolve(this.getUserFromHophandsId(hh_id)), 1000);

            });
        }

        return this.repo.createQueryBuilder("user")
            .where("user.hophands_id = " + hh_id)
            .getOne()
            .then(async (user) => {
                if (user) {
                    await this.assignItemsToUser(user);
                    this.saveToCache(user);
                    return user;
                }

                return null;
            });
    }

    public async createUser(hh_id: number, username: string): Promise<User> {
        let user = this.repo.create();
        user.name = username;
        user.hophands_id = hh_id;
        this.creatingUsers[hh_id] = true;
        return this.repo.persist(user)
            .then((user) => {
                winston.info("User created ", user.name, user.id);
                this.saveToCache(user);
                this.creatingUsers[hh_id] = false;
                return user;
            })
            .catch((error) => {
                this.creatingUsers[hh_id] = false;
                winston.error("Can't save new user", username, error);
                return null;
            });
    }

    public async generateUserData(userId: number) {
        let user = await this.get(userId, true);

        user.prevHCNCount = Token.getHCN(user.hophands_id);
        this.save(user);
        let data = <Models.PlayerData>{
            username: user.name,
            respect: user.respect,
            items: user.items,
            level: user.level,
            exp: user.exp,
            wearedItemsids: JSON.parse(user.usedItemsIds),
            hp: user.hp,
            hcn: Token.getHCN(user.hophands_id),
            messages: await DB.messages.getMany(user.getPendingMessages()),
            banned: user.banned != 0,
            configUpdate: DB.zhconfig.getConfigForPlayers(),
            canMakePvP: (!!user.canMakePvP) as boolean,
            freeTourneyEntrances: user.freeTourneyEntrances,
            grantedTourneyEntrances: user.grantedTourneyEntrances,
            pendingRewards: user.pendingRewards
           
        };
        user.pendingRewards = []
        return data;
    }

    public async assignItemsToUser(user: User) {
        user.items = [];
        let itemsIds = user.getItemsIds();
        for (let id of itemsIds)
            user.items.push(await DB.items.get(id));

    }

    public async grantTrainingAccesFor(id: number, map: number): Promise<Models.AccesToPlay> {
        let user = await this.get(id, true);
        if (user.banned) {
            winston.warn(user.name + " is banned but tried to play training!");
            return null;
        }

        let config = GameConfig.getTrainingConfig(map, user.getAllWearedItems());
        user.nextTrainingSeed = config.seed;
        this.repo.persist(user);

        return this.createAccesFromConfig(config);
    }



    public async grantAccesForChallenge(userId: number, request: Models.IGameAccesRequest): Promise<Models.AccesToPlay> {
        let challengeId = request.challengeId;
        let challenge = DB.challenges.getActiveChallenge(challengeId);
        if (!challenge)
            return null;

        let user = await this.get(userId, true)

        if (user.banned) {
            winston.warn(user.name + " is banned but tried to play challenge " + challengeId);
            return null;
        }

        if (challenge.type == Models.ChallengeType.TOURNEY)
            if (!await this.grantAccesForTournament(user, challenge, request.isChallengeTraining)) {
                winston.warn("Cant grant acces to " + user.name + " for tournament " + challenge.name);
                return null;
            }

        let config: Models.GameConfig = JSON.parse(JSON.stringify(challenge.config));
        if (config.bonus.dontIgnoreItemBonus)
            config.bonus = ItemsHelpers.addBonuses([config.bonus, ItemsHelpers.getAllBonusses(user.getAllWearedItems())]);

        if (config.seed == 0)
            config.seed = Math.round(Math.random() * 45434) + 44123;
        user.grantAccesToChallenge(challengeId, request.isChallengeTraining);
        return this.createAccesFromConfig(config, challengeId);
    }



    private async grantAccesForTournament(user: User, challenge: Challenge, isTraining: boolean): Promise<boolean> {
        let cost = challenge.cost;
        if (cost == 0)
            return true;
        if (isTraining) {
            let prevTrainings = 0;
            let game = await DB.gamesData.get(user.id, challenge.id);
            if (game) {
                let prevTrainings = game.stats.numberOfTrainings;
                if (!prevTrainings) prevTrainings = 0;
                if (prevTrainings > 5) return false;
            }
            cost = prevTrainings + 1;
            if (DB.zhconfig.get("freeTraining"))
                cost = 0;
            winston.debug(user.name + " -" + cost + " HCN");
            if (Token.getHCN(user.hophands_id) >= cost) {
                HHClient.addHCN(user.hophands_id, -cost);
                Token.addHCN(user.hophands_id, -cost);
            }
            return Token.getHCN(user.hophands_id) >= cost;
        }

        let hasAcces = await HHClient.buyAccesFor(user.hophands_id, 118);
        if (hasAcces) {
            HHClient.removeAccesFrom(user.hophands_id, 118);
            Token.addHCN(user.hophands_id, -10);
        }
        else 
            if (user.freeTourneyEntrances > 0) {
                winston.info(user.name + " user free entrance");
                user.freeTourneyEntrances--;
                DB.users.save(user);
                return true;
            }

        return hasAcces;
    }
    public async getManyUsers(ids: { user: number }[]) {
        let result: User[] = [];
        let notCached: number[] = [];

        for (let obj of ids) {
            if (this.cache[obj.user])
                result.push(this.cache[obj.user]);
            else
                result.push(await this.get(obj.user));
        }

        return result;
    }


    public  getManyUserNames(ids: { user: number }[]) {
        let result: string[] = [];
        let notCached: number[] = [];

        for (let obj of ids) {
            if (this.cache[obj.user])
                result.push(this.cache[obj.user].name);
            else
                result.push(" ");
        }

        return result;
    }

    private createAccesFromConfig(config: Models.GameConfig, challengeId?: number): Promise<Models.AccesToPlay> {
        let acces = <Models.AccesToPlay>{
            config: config,
            challengeId: challengeId
        }
        return Promise.resolve(acces);
    }
    get enity(): typeof User {
        return User;
    }
}