﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";
import winston = require("winston")
import { DB } from "./Database"
import { ZombieHunterConfig } from "../entities/ZombieHunterConfig"

export class ZHConfig {

    private config: { [name: string]: any };

    private static instance;

    static get(): ZHConfig {
        if (!ZHConfig.instance)
            throw Error("ZHConfig does not exist");
        return this.instance;
    }

    constructor(private DB: DB) {
        if (ZHConfig.instance)
            throw Error("ZHConfig already exists");

        ZHConfig.instance = this;
        setInterval(() => this.refresh(), 1000*60 * 3);
    }

    get repo() {
        return this.DB.repoFor(ZombieHunterConfig);
    } 

    get enity(): typeof ZombieHunterConfig {
        return ZombieHunterConfig;
    }

    public async refresh() {
        let configs = await this.repo.find()
            .catch((error) => {
                winston.error("Cant download config!", error);
            });

        if (!this.config) {
            winston.info("Config loaded");
            console.log(configs);
            this.config = {};
        }
     
        try {
            if (configs)
                for (let config of configs) {
                    //if (this.config[config.name] && this.config[config.name] !=JSON.parse( config.value)  )
                      //  winston.info("Config refreshed: [" + config.name + "] = " + config.value);

                    if (config.value[0] == "[")
                        config.value = JSON.parse(config.value);
                    else
                        config.value = parseFloat(config.value) as any;

                    this.config[config.name] = config.value;
                }
            let now = Date.now();
            let start = new Date("2017-06-13 16:00").getTime();
            let end = new Date("2017-06-13 22:00").getTime();

            if (now > start && now < end) 
                this.config["happyHours"] = 1;  
            else if (start > now)
                 winston.info(((start - now) / 1000 / 60) + " minutes to start happy hours");
        }
        catch (e) {
            winston.error("Error loading config:" , e);
        }
    }

    public getConfigForPlayers(): Models.ZHConfig {
        return <Models.ZHConfig>{
            expLevels: this.get("expLevels"),
            minVersion: this.get("minVersion"),
            streamEnabled: !!parseInt(this.get("streamEnabled")),
            happyHours: this.get("happyHours")
        }
    }
    public get(name: string): any{
        if (this.config[name] === undefined) {
            winston.error("Cant find config: " + name);
            return 0;
        }
        return this.config[name];
    }

 
}