﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";

import { DB } from "./Database"
import { Donate } from "../entities/Donate"
import { GameConfig } from "../entities/GameConfig"
import winston = require("winston")
export class Donates {


    private static instance;

    static get(): Donates {
        if (!Donates.instance)
            throw Error("Donates does not exist");
        return this.instance;
    }

    constructor(private DB: DB) {
        if (Donates.instance)
            throw Error("Donates already exists");

        Donates.instance = this;
    }

    get repo() {
        return this.DB.repoFor(Donate);
    } 

    get enity(): typeof Donate {
        return Donate;
    }

    public add(msg: string, hcn: number, user: string) {
        let donate = this.repo.create();
        donate.message = msg;
        donate.status = 1;
        donate.user = user;
        donate.time = Date.now();
        donate.hcn = hcn;
        this.save(donate);
        winston.info("[DONATE " + hcn + "HCN] [" + user + "] " + msg);
    }
    public save(entity: Donate) {

        return this.repo.persist(entity);
    }


    

    public getAll(): Promise<Donate[]> {
        return this.repo.createQueryBuilder("donate")
            .where("donate.status = 1")
            .getMany().then((data) => {
                this.repo.query("UPDATE donate set status = 0");
                return data;
            });
    }

}