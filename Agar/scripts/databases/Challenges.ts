﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";

import { DB } from "./Database"
import { Challenge } from "../entities/Challenge"
import { GameConfig } from "../entities/GameConfig"
import { TournamentUpdater } from "../updaters/Tournament"
import { GameData } from "../entities/GameData"
import { Profiler } from "../utils/Profiler"

interface GetChallengesOptions {
    dontSendKOH: boolean;
    dontSendChallengesWithNumberOftries: boolean;
    dontSensTourneys: boolean;
    dontSendFinishedTourneys: boolean;
    alwaysSendFullUpdate: boolean;
    dontSendHopStop: boolean;
}


export class Challenges {

    private cache: Challenge[] = [];
    private configs: GameConfig[] = [];

    private tourneyUpdaters: TournamentUpdater[] = [];

    private static instance;

    static get(): Challenges {
        if (!Challenges.instance)
            throw Error("Challenges does not exist");
        return this.instance;
    }

    constructor(private DB: DB) {
        if (Challenges.instance)
            throw Error("Challenges already exists");

        Challenges.instance = this;
    }

    get repo() {
        return this.DB.repoFor(Challenge);
    }

    get enity(): typeof Challenge {
        return Challenge;
    }

    public save(entity: Challenge) {
        this.cache[entity.id] = entity;
        return this.repo.persist(entity);
    }

    private async getActiveKOHsAndTournaments() {
        let profilerId = Profiler.get().start("getActiveKOHsAndTournaments");
        return this.DB.repoFor(Challenge).createQueryBuilder("challenge")
            .where("(( challenge.endTime > " + Date.now() + "  AND challenge.type = " + Models.ChallengeType.KING_OF_THE_HILL + ")OR challenge.type = " + Models.ChallengeType.TOURNEY + ")")
            .getMany()
            .then(async (challenges) => {
                if (challenges)
                    for (let challenge of challenges) {
                        await this.initChallenge(challenge);
                    }
                Profiler.get().end(profilerId);
                return challenges;
            });
    }

    private async _getAllActive(): Promise<Challenge[]> {
        let profilerId = Profiler.get().start("_getAllActive");
        return this.DB.repoFor(Challenge).createQueryBuilder("challenge")
            .where("( challenge.endTime > " + Date.now() + " OR challenge.type = " + Models.ChallengeType.TOURNEY + ")")
            .andWhere("challenge.prizesLeft > 0")
            .getMany()
            .then(async (challenges) => {
                if (challenges)
                    for (let challenge of challenges) {
                        await this.initChallenge(challenge);
                    }
                Profiler.get().end(profilerId);
                return challenges;
            });
    }

    public async getFinishedKOHsAndTournaments() {
        return this.DB.repoFor(Challenge).createQueryBuilder("challenge")
            .where("challenge.endTime < " + (Date.now() - 1000 * 60 * 5) + " AND challenge.endTime > 0 AND (challenge.type = 3 OR challenge.type=4) ")
            .getMany()
            .then(async (challenges) => {
                if (challenges)
                    for (let challenge of challenges)
                        await this.initChallenge(challenge);
                return challenges;
            })
    }

    private async initChallenge(challenge: Challenge) {
        challenge.config = await this.getConfig(challenge.configId);
        this.cache[challenge.id] = challenge;

        if (challenge.prizeId)
            challenge.prize = await DB.items.get(challenge.prizeId);

        if (challenge.type == Models.ChallengeType.TOURNEY) {
            challenge.tourneyData = <Models.ITourneyData>{};
            let prizes = challenge.getTourneyPrizesIds();
            challenge.tourneyData.prizes = [];
            for (let prize of prizes) {
                challenge.tourneyData.prizes.push({
                    prizeId: prize.prizeId,
                    prize: await DB.items.get(prize.prizeId),
                    rangeEnd: prize.rangeEnd,
                    rangeStart: prize.rangeStart
                });
            }

            if (!this.tourneyUpdaters[challenge.id] || this.tourneyUpdaters[challenge.id].dead)
                this.tourneyUpdaters[challenge.id] = new TournamentUpdater(challenge.id);
        }


    }

    public getConfig(id: number): Promise<Models.GameConfig> {
        if (this.configs[id])
            return Promise.resolve(this.configs[id]);

        return this.DB.repoFor(GameConfig).findOneById(id).then((config) => {
            if (config)
                this.configs[id] = config;
            return config;
        });
    }


    public getActiveChallenge(id: number): Challenge {
        if (!this.cache[id])
            return null;
        if (this.cache[id].isActive())
            return this.cache[id];
        else {
            this.cache[id] = null;
            return null;
        }
    }

    public async get(id: number, cached: boolean = false): Promise<Challenge> {
        if (this.cache[id] && cached)
            return Promise.resolve<Challenge>(this.cache[id]);

        return this.repo.findOneById(id).then(async (challenge) => {
            if (challenge)
                await this.initChallenge(challenge);
            return challenge;
        });
    }

    private async initKOHData(challenge: Models.IChallenge, userId?: number) {
        let profilerId = Profiler.get().start("initKOHData");

        let ranking = await DB.gamesData.getAllFromChallenge(challenge.id);
        let koh = challenge.kohData = <Models.IKingOfTheHillData>{};
        koh.top3 = [];
        for (let i = 0; i < 3; i++) {
            if (ranking[i]) {
                koh.top3[i] = {
                    place: i + 1,
                    nick: (await DB.users.get(ranking[i].user)).name,
                    score: ranking[i].overallScore
                }
            }
        }

        if (userId)
            for (let i = 3; i < ranking.length; i++) {
                if (ranking[i].user == userId) {
                    koh.myScore = {
                        place: i + 1,
                        nick: "",
                        score: ranking[i].overallScore
                    };
                    break;
                }
            }

        Profiler.get().end(profilerId);
    }

    private getNextTrainingCostForGame(game: GameData) {
        let trainings = 0;
        if (game) {
            trainings = game.stats.numberOfTrainings;
            if (!trainings)
                trainings = 0;

            if (trainings > 5)
                return -1;
        }
        if (DB.zhconfig.get("freeTraining"))
            return 0;

        return trainings + 1;
    }

    private async getOneChallengeUpdateData(userId: number, challenge: Challenge, options: GetChallengesOptions) {
        if (!challenge) {
            return;
        }

        if (options.dontSendHopStop && challenge.config.map == Models.Map.STATION)
            return null;

        let chdata = <Models.IChallengeData>{
            data: challenge,
            prevStats: undefined,
            ended: false
        };

        if (challenge.type == Models.ChallengeType.TOURNEY) {
            if (options.dontSensTourneys)
                return null;

            if (options.dontSendFinishedTourneys && challenge.endTime < Date.now())
                return null;

            challenge.tourneyData = this.tourneyUpdaters[challenge.id].getData();
            if (userId) {
                challenge.tourneyData.myScore = this.tourneyUpdaters[challenge.id].getPlayerData(userId);
                challenge.tourneyData.nextTrainingCost = this.getNextTrainingCostForGame(await DB.gamesData.get(userId, challenge.id));
            }
        }
        else
            //KOH
            if (challenge.type == Models.ChallengeType.KING_OF_THE_HILL) {
                if (!options.dontSendKOH)
                    await this.initKOHData(challenge, userId);
                else
                    return null;
            }
            else
                //POPRZEDNIE GRY GRACZA
                if (userId) {
                    let game = await DB.gamesData.get(userId, challenge.id);
                    if (game) {
                        if (game.status == Models.GameDataStatus.SUCCES)
                            return null;

                        if (game.status != Models.GameDataStatus.TRAINING)
                            chdata.prevStats = game.stats;
                    }
                }


        if (options.dontSendChallengesWithNumberOftries)
            for (let crit of challenge.criteria)
                if (crit.stat == "numberOfTries")
                    return null;

        return chdata;
    }



    public async getChallengesUpdateData(userId: number, options = <GetChallengesOptions>{}, fullUpdate: boolean = false, prevPlayedChallenge: number = 0) {
        if (options.alwaysSendFullUpdate)
            fullUpdate = true;

        let profilerId = Profiler.get().start("getChallengesUpdateData" + (fullUpdate ? " - full" : ""));


        let challenges: Challenge[];
        let data: Models.IChallengeData[] = [];
        if (fullUpdate)
            challenges = await this._getAllActive();
        else {
            challenges = await this.getActiveKOHsAndTournaments();
            if (prevPlayedChallenge)
                challenges.push(await this.get(prevPlayedChallenge, true));
        }
        for (let challenge of challenges) {
            let challengeData = await this.getOneChallengeUpdateData(userId, challenge, options);
            if (challengeData)
                data.push(challengeData);
        }

        Profiler.get().end(profilerId);
        return data;
    }

    public getChallengeSendOptionsForVersion(version: number): GetChallengesOptions {
        return {
            dontSendKOH: version < 0.20,
            dontSendChallengesWithNumberOftries: version < 0.21,
            dontSensTourneys: version < 0.28,
            dontSendFinishedTourneys: version < 0.23,
            alwaysSendFullUpdate: version < 0.25,
            dontSendHopStop: version < 0.26
        }
    }

    public async getAllQuestsForLvl(lvl: number) {
        return this.DB.repoFor(Challenge).createQueryBuilder("challenge")
            .where("challenge.minLevel <= " + lvl)
            .andWhere("challenge.quest != -1")
            .getMany()
            .then(async (challenges) => {
                if (challenges)
                    for (let challenge of challenges) {
                        await this.initChallenge(challenge);
                    }

                return challenges;
            });
    }

    public async userHasDoneAllQuests(user: number, lvl: number) {
        let challenges = await this.getAllQuestsForLvl(lvl);
        for (let challenge of challenges) {
            let game = await DB.gamesData.get(user, challenge.id);
            if (!game || game.status != Models.GameDataStatus.SUCCES)
                return false;
        }
        return true;
    }

}