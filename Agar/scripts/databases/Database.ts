﻿
import { createConnection, Connection, Repository, Entity } from "typeorm"
import { GameConfig } from "../entities/GameConfig"
import { User } from "../entities/User"
import { Challenge } from "../entities/Challenge"
import { Item  } from "../entities/Item"
import { GameData as GameDataEtinity } from "../entities/GameData"
import { Message } from "../entities/Message" 
import { GameToValidate } from "../entities/GameToValidate" 
import { ZombieHunterConfig } from "../entities/ZombieHunterConfig"
import { Donate } from "../entities/Donate"

import { Users } from "./Users"
import { Challenges } from "./Challenges"
import { GameData } from "./GameData"
import { Items } from "./Items"
import { Messages } from "./Messages"
import { ZHConfig } from "./ZHConfig"
import { PvPServer } from "../pvp/Server"
import { Donates } from "./Donates"

import { Users as TestUsers } from "./tests/Users"
import { Challenges as TestChallenges } from "./tests/Challenges"
import { GameData as TestGameData } from "./tests/GameData"


var host = "localhost";
var port = 3306;
var username = "root";
var password = "zmalqp10";
var database = "zombie_hunter";

export class DB {

    private connection: Connection;
    static instance: DB;


    constructor(callback: Function, hophands: boolean = false) {
        DB.instance = this;

        this.create(callback, hophands);

        DB.challenges = new Challenges(this);
        DB.users = new Users(this);
        DB.gamesData = new GameData(this);
        DB.items = new Items(this);
        DB.messages = new Messages(this);
        DB.zhconfig = new ZHConfig(this);
        DB.donates = new Donates(this);
    }

    private create(callback: Function, hophands: boolean) {
        createConnection({
            driver: {
                type: "mysql",
                host: host,
                port: port,
                username: username,
                password: hophands?password:"" ,
                database: database
            },
            entities: [
                User, Challenge, GameConfig, Item, GameDataEtinity, Message, GameToValidate, ZombieHunterConfig, Donate
            ],
            autoSchemaSync: true,
        }).then(connection => {
            this.connection = connection;
            DB.zhconfig.refresh();
            console.log("Database connected");
            if (callback)
                callback();
        }).catch(error => console.log("Can't connect to database ", error));

    }


    public repoFor<T>(entity: new () => T): Repository<T> {
        return this.connection.getRepository(entity);
    }



    static users: Users;
    static challenges: Challenges;
    static gamesData: GameData;
    static items: Items;
    static messages: Messages;
    static zhconfig: ZHConfig;
    static pvp: PvPServer;
    static donates: Donates;
}