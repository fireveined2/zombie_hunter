﻿import * as Models from "../../../../Shared/Shared"
import * as express from "express";

import { DB } from "../Database"
import { Challenge } from "../../entities/Challenge"
export { Challenge as ChallengeEntity } from "../../entities/Challenge"


export class Challenges {

    private cache: Challenge[] = [];
    private static instance;

    static get(): Challenges {
        if (!Challenges.instance)
            throw Error("Challenges does not exist");
        return this.instance;
    }

    constructor(private DB: DB) {
        if (Challenges.instance)
            throw Error("Challenges already exists");

        Challenges.instance = this;
    }

    get repo() {
        return this.DB.repoFor(Challenge);
    }

    get enity(): typeof Challenge {
        return Challenge;
    }

    public add(entity: Challenge) {
        this.cache[entity.id] = entity;
    }

    public save(entity: Challenge) {
        this.add(entity);
        return Promise.resolve();
      //  return this.repo.persist(entity);
    }

    public getAllActive(): Promise<Challenge[]> {
        return Promise.resolve(this.cache);
    }

    public getActiveChallenge(id: number): Challenge {
        return this.cache[id];
    }

    public async get(id: number, cached: boolean = false): Promise<Challenge> {
        return Promise.resolve(this.cache[id]);
    }

    public async getReward(id: number): Promise<Models.IPrize> {
        let challenge = await this.get(id, true);
        return Promise.resolve(challenge.prize);
    }

}