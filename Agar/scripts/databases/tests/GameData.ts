﻿import * as Models from "../../../../Shared/Shared"
import * as express from "express";

import { DB } from "../Database"
import { GameData as GameDataEtinity } from "../../entities/GameData"
export { GameData as GameDataEtinity } from "../../entities/GameData"
import { GameConfig } from "../../entities/GameConfig"

// /users/id
export class GameData {



    private cache: GameDataEtinity[] = []


    private static instance;

    constructor(private DB: DB) {
        if (GameData.instance)
            throw Error("GameData already exists");
        GameData.instance = this;
    }

    static get(): GameData {
        if (!GameData.instance)
            throw Error("GameData does not exist");

        return this.instance;
    }

    get repo() {
        return this.DB.repoFor(GameDataEtinity);
    }

    public add(entity: GameDataEtinity) {
          this.cache[entity.id] = entity;
    }

    public save(entity: GameDataEtinity) {
        this.add(entity);
        return Promise.resolve();
      //  return this.repo.persist(entity);
    }

    public get(user: number, challenge: number): Promise<GameDataEtinity> {
        for (let i of this.cache)
            if (i.user == user && i.challenge == challenge)
                return Promise.resolve(i);
    }
}