﻿import * as Models from "../../../../Shared/Shared"
import * as express from "express";
import { DB } from "../Database"
import { User } from "../../entities/User"
import { GameConfig } from "../../entities/GameConfig"
export { User as UserEntity } from "../../entities/User"

// /users/id
export class Users {



    private cache: User[] = []
    private static instance;

    constructor(private DB: DB) {
        if (Users.instance)
            throw Error("Users already exists");
        Users.instance = this;


    
    }

    public addUser(user: User) {
        this.cache[user.id] = user;
    }

    static get(): Users {
        if (!Users.instance)
            throw Error("Users does not exist");

        return this.instance;
    }

    get repo() {
        return this.DB.repoFor(User);
    }

    public save(entity: User) {
        this.addUser(entity);
        return Promise.resolve();
    //    return this.repo.persist(entity);
    }

    public get(id: number, cached: boolean = false): Promise<User> {
            return Promise.resolve(this.cache[id]);
    }

    public async grantTrainingAccesFor(id: number): Promise<Models.AccesToPlay> {
        let user = await this.get(id, true);
        let config = null;//GameConfig.getTrainingConfig(1);
        user.nextTrainingSeed = config.seed;
        this.save(user);

        return this.createAccesFromConfig(config);
    }

    public async grantAccesForChallenge(userId: number, challengeId: number): Promise<Models.AccesToPlay> {
        let challenge = DB.challenges.getActiveChallenge(challengeId);
        if (!challenge)
            return null;

        let user = await this.get(userId, true)
      //  user.grantAccesToChallenge(challengeId, 1000);
        return this.createAccesFromConfig(challenge.config, challengeId);
    }

    private createAccesFromConfig(config: Models.GameConfig, challengeId?: number): Promise<Models.AccesToPlay> {
        let acces = <Models.AccesToPlay>{
            config: config,
            challengeId: challengeId
        }
        return Promise.resolve(acces);
    }

}