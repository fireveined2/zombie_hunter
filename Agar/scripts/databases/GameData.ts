﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";

import { DB } from "./Database"
import { GameData as GameDataEtinity } from "../entities/GameData"
import { GameConfig } from "../entities/GameConfig"

// /users/id
export class GameData {



    private cache: GameDataEtinity[][] = [];
    private challengesDatas: GameDataEtinity[][] = [];

    private static instance;

    constructor(private DB: DB) {
        if (GameData.instance)
            throw Error("GameData already exists");
        GameData.instance = this;
    }

    static get(): GameData {
        if (!GameData.instance)
            throw Error("GameData does not exist");

        return this.instance;
    }

    get repo() {
        return this.DB.repoFor(GameDataEtinity);
    }

    public remove(entity: GameDataEtinity) {
        delete this.cache[entity.user][entity.challenge];


        let challengeGames = this.challengesDatas[entity.challenge];
        if (challengeGames)
            for (let i = 0; i < challengeGames.length; i++)
                if (challengeGames[i].user == entity.user)
                    challengeGames.splice(i, 1);

   

        return this.repo.remove(entity);
    }


    public save(entity: GameDataEtinity) {
        entity.decompress();
        if (!this.cache[entity.user])
            this.cache[entity.user] = [];
        this.cache[entity.user][entity.challenge] = entity;

        if (entity.status == Models.GameDataStatus.NORMAL) {
            let challenges = this.challengesDatas[entity.challenge];
            if (challenges) {
                let addedToChallengeData = false;
                for (let i = 0; i < challenges.length; i++) {
                    if (challenges[i].user == entity.user) {
                        challenges[i] = entity;
                        addedToChallengeData = true;
                    }
                }
                if (!addedToChallengeData)
                    challenges.push(entity);

                challenges.sort((a, b) => b.overallScore - a.overallScore);
            }
        }

        return this.repo.persist(entity);
    }

    public get(user: number, challenge: number): Promise<GameDataEtinity> {
        if (this.cache[user]) {
            if (this.cache[user][challenge])
                return Promise.resolve(this.cache[user][challenge]);
        }

        return this.repo.findOne({ user: user, challenge: challenge }).then((data) => {
            if (data) {
                if (!this.cache[data.user])
                    this.cache[data.user] = [];
                this.cache[data.user][data.challenge] = data;
            }
            return data;
        });
    }


    public async getAllFromChallenge(challenge: number, cached = true): Promise<GameDataEtinity[]> {
        if (this.challengesDatas[challenge] && cached)
            return this.challengesDatas[challenge];

        return this.repo.find({ challenge: challenge, success: Models.GameDataStatus.NORMAL }).then((data) => {
            if (data) {
                this.challengesDatas[challenge] = data;
                for (let game of data) {
                    if (!this.cache[game.user])
                        this.cache[game.user] = [];
                    this.cache[game.user][game.challenge] = game;
                } 
            }
            else
                this.challengesDatas[challenge] = [];
            this.challengesDatas[challenge].sort((a, b) => b.overallScore - a.overallScore);
            return this.challengesDatas[challenge];
        })

    }

}