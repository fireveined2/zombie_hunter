﻿import * as Models from "../../../Shared/Shared"
import * as express from "express";

import { DB } from "./Database"
import { Message } from "../entities/Message"

export class Messages {

    private cache: Message[] = [];

    private static instance;

    static get(): Messages {
        if (!Messages.instance)
            throw Error("Messages does not exist");
        return this.instance;
    }

    constructor(private DB: DB) {
        if (Messages.instance)
            throw Error("Items already exists");

        Messages.instance = this;
    }

    get repo() {
        return this.DB.repoFor(Message);
    } 

    get enity(): typeof Message {
        return Message;
    }

    public save(entity: Message) {
        this.cache[entity.id] = entity;
        return this.repo.persist(entity);
    }

    public async add(txt: string) {
      
        let msg = this.repo.create();
        msg.message = txt;
        return this.save(msg);
    }

    public get(id: number): Promise<Models.IUserMessage>{
        if (this.cache[id] && Math.random()>0.5)
            return Promise.resolve(this.cache[id]);

        return this.repo.findOneById(id).then((item) => {
            if (item)
                this.cache[id] = item;
            return item;
        });
    }

    public getLvlUpMessageID(newlvl: number): number {
        if (newlvl == 2 || newlvl == 3)
            return newlvl + 2;
        else
            return 0;


    }

    public async getMany(ids: number[]): Promise<Models.IUserMessage[]>{
        let msgs: Models.IUserMessage[]=[];
        for (let id of ids) {
            let msg = await this.get(id);
            if (msg)
                msgs.push(msg);
        }
        return msgs;
    }
}