﻿import { IRoute } from "./Routes"
import * as express from "express";
import * as Processors from "../processors/Processors"
import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"
import { Token } from "./Token"
import { md5 } from "../utils/md5"

import { Debug } from "./Debug"
var pathLib = require('path');
import * as fs from "fs";
import * as path from "path";
// /users/id

import winston = require("winston")

interface AssetMeta {
    name: string;
    path: string;
    size: number;
    md5: string;
}



export class Updater implements IRoute{

    private gameCode: string;
    private gameVersion: string;

    private loaderCode: string;
    private loaderVersion: string;


    private assetsDatabase: AssetMeta[] = [];
    private assetsData: any[] = [];

    private cancelUpdates = false;


    constructor() {

        this.checkForNewJS();
        setInterval(() => this.checkForNewJS(), 120 * 1000);


        this.addAsset("ninja_head_icon", "assets/s1/items/31.png");
        this.addAsset("ninja_head", "assets/s1/hero/hero9/head.png");
        this.addAsset("ninja_head_blink", "assets/s1/hero/hero9/head_blink.png");
        this.addAsset("icon", "assets/s1/items/37.png");

    }

    private checkForNewJS() {
        fs.readFile("updates/app.js", "utf8", this.onGameLoaded.bind(this));
        fs.readFile("updates/loader.js", "utf8", this.onLoaderLoaded.bind(this));

    }

    private addAsset(name: string, path: string) {
        let encoding = "binary";
        if (path.indexOf(".js") != -1)
            encoding = "utf8";

        let file = fs.readFile("updates/" + path, encoding,(error, data) => {
            if (error) {
                winston.error("Cant download " + name, error);
                return;
            }
            let asset: AssetMeta = { name: name, path: path, size: Math.round(data.length / 1024), md5: md5(data) };
            this.assetsDatabase.push(asset);
            this.assetsData.push(data);
            winston.info("[UPDATER] " + name + " loaded. Size = " + asset.size + "kB, md5 = " + asset.md5);
        });
    }

    private onGameLoaded(error: NodeJS.ErrnoException, data: string) {
        if (error) {
            winston.error("Error loading game update: ", error);
            this.cancelUpdates = true;
            return;
        }
        data = data.replace("http://192.168.1.100:9241", "https://ams.hophands.pl:9241");
        let ver = this.getVersion(data, "Version.ver");
        if (this.gameVersion == ver)
            return;

        this.gameVersion = ver;
        this.gameCode = data;
        if (!this.gameVersion) {
            winston.error("Can't detect game version!");
            this.cancelUpdates = true;
            return;
        }

        winston.info("[UPDATER] Game loaded. Version " + this.gameVersion);
    }

    private onLoaderLoaded(error: NodeJS.ErrnoException, data: string) {
        if (error) {
            winston.error("Error loading loader update: ", error);
            this.cancelUpdates = true;
            return;
        }
        data = data.replace("http://192.168.1.100:9241", "https://ams.hophands.pl:9241");
        let ver = this.getVersion(data, "Loader.version");
        if (this.loaderVersion == ver)
            return;

        this.loaderVersion = ver;

        this.loaderCode = data;

   
        if (!this.loaderVersion) {
            winston.error("Can't detect laoder version!");
            this.cancelUpdates = true;
            return;
        }

        winston.info("[UPDATER] Loader loaded. Version " + this.loaderVersion);
    }


    private getVersion(data: string, versionString: string) {
        if (data)
            return md5(data);

        let pos = data.indexOf(versionString);
        pos = data.indexOf('"', pos + 1);
        let end = data.indexOf('"', pos+1);

        if (pos == -1 || end == -1)
            return null;

        return data.substr(pos + 1, end - pos - 1);
    }
 

    public setup(app: express.Application) {

        app.get("/update", this.updateJS.bind(this));
        app.get("/update/assets", this.getAssetsList.bind(this));
        app.get("/update/assets/:name", this.getAsset.bind(this));

        app.use('/load', express.static('updates/assets'))
    }





    private async updateJS(req: express.Request, res: express.Response) {
        let userGameVersion = req.query.gameVersion;
        let userLoaderVersion = req.query.loaderVersion;

        if (this.cancelUpdates)
            return;

        if (!this.gameCode || !this.loaderCode) {
            setTimeout(() => this.updateJS(req, res), 500);
            return;
        }

        let gameUpdate = "OK";
        let loaderUpdate = "OK";
        if (this.loaderVersion != userLoaderVersion) {
            winston.info("Updating " + req.ip + " loader from " + userLoaderVersion + " to " + this.loaderVersion);
            loaderUpdate = { version: this.loaderVersion, code: this.loaderCode } as any;
        }
      
        if (this.gameVersion != userGameVersion) {
            winston.info("Updating " + req.ip + " game from " + userGameVersion + " to " + this.gameVersion);
            gameUpdate = { version: this.gameVersion, code: this.gameCode } as any;
        }

       

        let update = { game: gameUpdate, loader: loaderUpdate };
        res.send(update);
    }


    private async getAssetsList(req: express.Request, res: express.Response) {
        let userGameVersion = req.query.gameVersion;
        res.send(this.assetsDatabase);
    }

    private async getAsset(req: express.Request, res: express.Response) {
        let name = req.params.name;
        for (let i = 0; i < this.assetsDatabase.length; i++)
            if (this.assetsDatabase[i].name == name) {
                winston.info(name + " sent to " + req.ip);
    
                res.type("png");
                res.end(this.assetsData[i], "binary");

                return;
            }
   
        res.sendStatus(404);
        winston.warn(name + " not found");
    }

 }