﻿import { IRoute } from "./Routes"
import * as express from "express";
import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"
import { Token } from "./Token"
import winston = require("winston")

interface IPData {
    ip: string;
    time: number;

}

export class Debug {

    private ipsWhichOpenedGame: IPData[] = [];

    private loadTimeTotal = 0;
    private loadsCount = 0;
    private crashes = 0;

    private offlineGames = 0;

    constructor() {
        setInterval(() => this.log(), 1000 * 3600 * 1);
    }


    public someonePlayedOffline() {
        winston.debug("Someone played offline!");
        this.offlineGames++;
    }
    private log() {
        let avg = Math.round(this.loadTimeTotal / this.loadsCount / 100) / 10;
        winston.info("[DEBUG] Average loading time: " + avg + "s, game was opened " + this.loadsCount + " times, probablly " + this.crashes + " crashes, " + this.offlineGames+" offline games");
    }

   private static instance: Debug;

   static get() {
       if (!Debug.instance)
           Debug.instance = new Debug();

       return Debug.instance
   }

   public someoneOpenedGame(ip: string) {
       this.ipsWhichOpenedGame.push({ ip: ip, time: Date.now() });
       setTimeout(() => this.checkIp(ip), 150*1000);
       
   }

   private checkIp(ip: string) {
       for (let i = 0; i < this.ipsWhichOpenedGame.length; i++) {
           let ipData = this.ipsWhichOpenedGame[i];
           if (ipData.ip == ip ) {
               let elapsed = Date.now() - ipData.time;
               if (elapsed > 1000 * 120) {
                   this.crashes++;
                   winston.warn(ip +" probablly crashed");
                   this.ipsWhichOpenedGame.splice(i, 1);
               }
           }
       }
   }

   public someoneLoadedGame(ip: string) {
       for (let i = 0; i < this.ipsWhichOpenedGame.length; i++) {
           let ipData = this.ipsWhichOpenedGame[i];
           if (ipData.ip == ip) {
               let elapsed = Date.now() - ipData.time;
               this.loadsCount++;
               this.loadTimeTotal += elapsed;

               this.ipsWhichOpenedGame.splice(i,1);
           }
       }
   }

        
}