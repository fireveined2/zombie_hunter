﻿import { IRoute } from "./Routes"
import * as express from "express";
import * as Processors from "../processors/Processors"
import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"
import { Token } from "./Token"

// /users/id

import winston = require("winston")

export class Editor implements IRoute{

    constructor() {
 
    }

    public setup(app: express.Application) {
        app.get("/items", this.items.bind(this));




    }


    private async items(req: express.Request, res: express.Response) {
        if (req.query.token != "dfalkvd382") {
            winston.error("Editor API wrong id ", req.query.token);
            return;
        }
        winston.info("Editor API: /items");
        res.send(await DB.items.getAll());
    }


}