﻿import { IRoute } from "./Routes"
import * as express from "express";
import * as Processors from "../processors/Processors"
import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"
import { Token } from "./Token"
import { User } from "../entities/User"
import { Debug } from "./Debug"
import { Profiler } from "../utils/Profiler"
import { SocketServer } from "../socket/Server"
// /users/id

import winston = require("winston")

export class Ranking{

    private ranking: Models.RankingRow[];
    private dailyRanking: Models.RankingRow[];
    private rankingReseted: boolean = false;

    constructor(private getUserFunc: Function, private updateInterval: number = 25) {
        this.updateRanking();
        setInterval(() => this.updateRanking(), this.updateInterval * 1000);
    }

    private async updateRanking() {

        if (new Date().toString().indexOf("23:59:") != -1 && !this.rankingReseted) {
            this.rankingReseted = true;
            DB.users.repo.query("UPDATE user SET dailyRecordsString = '0 0 0 0 '");
            setTimeout(() => this.rankingReseted =false, 3600 * 1000)
        }
        let profilerId = Profiler.get().start("updateRanking");

        let users: User[] = await this.getUserFunc();


        this.ranking = [];
        this.dailyRanking = [];
        for (let user of users) {
            let dailyRecords = user.getDailyRecords() as any;
            let best = parseInt(dailyRecords[0]);
            for (let a = 0; a < 4; a++)
                if (parseInt(dailyRecords[a]) > best)
                    best = parseInt(dailyRecords[a]) ;

            this.ranking.push({
                nick: user.name,
                respect: user.respect,
                lvl: user.level,
                records: user.getRecords(),
                id: user.id,
            });


            this.dailyRanking.push(<any>{
                id: user.id,
                nick: user.name,
                respect: user.respect,
                lvl: user.level,
                records: dailyRecords,
                best: best,
                status: SocketServer.instance.getUserStatus(user.id),
                wearedItemsIds: JSON.parse(user.usedItemsIds)
            });
        }

        this.ranking.sort((a, b) => b.respect - a.respect);
        this.dailyRanking.sort((a: any, b: any) => b.best - a.best);
        Profiler.get().end(profilerId);
    }

    public async getOverallRankingForUser(user: User) {
        if (!this.ranking)
            await this.updateRanking();
        return this.getRankingForUser(user, this.ranking);
    }

    public async getDailyRankingForUser(user: User) {
        if (!this.dailyRanking)
            await this.updateRanking();
        return this.getRankingForUser(user, this.dailyRanking);
    }

    private async getRankingForUser(user: User, ranking: Models.RankingRow[]) {
        let profilerId = Profiler.get().start("getRankingForUser");

        let result: Models.RankingRow[] = [];
        let userPos = -1;
        for (let i = 0; i < ranking.length; i++) {
            if (!ranking[i])
                continue;
            if (i < 100) {
                result.push(ranking[i]);
                ranking[i].place = i + 1;
            }
            else
                if (userPos != -1) {
                    Profiler.get().end(profilerId);
                    return result;
                }
            if (ranking[i].nick == user.name) {
                if (i >= 100) {
                    result.push(ranking[i]);
                    ranking[i].place = i;
                    Profiler.get().end(profilerId);
                    return result;
                }
                userPos = i;
            }
        }

        if(userPos==-1)
        result.push({
            nick: user.name,
            lvl: user.level,
            place: ranking.length,
            respect: user.respect,
            records: user.getDailyRecords(),
            id: user.id,
            status: SocketServer.instance.getUserStatus(user.id)
            
        });

        Profiler.get().end(profilerId);
        return result;
    }


}