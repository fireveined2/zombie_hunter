﻿
import request = require("request")
import winston = require("winston")

import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"

interface UserData {
    user_id: number;
    hit_coins: number;
    username_full: string;
}

interface IOnlineUser {
    id: number;
    lastActiveTime: number;
}

export class Token {
    private static TokenData: { [hh_id: number]: UserData } = {};
    private static TokenToHHIdMap: { [token: string]: number } = {};
    public user: UserData;
    public version: string = "";
    public os: "ios" | "android" | "wp" | "unknown" = "unknown";

    public minVersion: string = "0.1";
    public uptodateVersion: boolean = false;


    static getHCN(hh_id: number) {
        let hcn = this.TokenData[hh_id].hit_coins;
        if (!hcn)
            hcn = 0;
        return hcn;
    }

    static addHCN(hh_id: number, hcn: number) {
         this.TokenData[hh_id].hit_coins += hcn;
    }
    static getIDFromToken(token: string) {
        let hh_id = this.TokenToHHIdMap[token];
        if (!hh_id)
            return null;

        return this.TokenData[hh_id].user_id; 
    }

    private static lastActiveTime: {
        [id: number]: number
    } = {};

    private static handleActivePlayersInterval: any;
    constructor(public token: string) {
        if (!token || !token.length)
            token = "0|";

        let pos = token.indexOf("|");

            let os = token[pos - 1];
            if (os == "i")
                this.os = "ios";
            if (os == "a")
                this.os = "android";
            if (os == "w")
                this.os = "wp";

            if (this.os == "unknown")
                this.version = token.substr(0, pos);
            else
                this.version = token.substr(0, pos-1);
        

        this.token = token.substr(pos+1, 999);

        if (parseFloat(this.version) >= parseFloat(this.minVersion))
            this.uptodateVersion = true;

        if (Token.handleActivePlayersInterval === undefined) {
            Token.handleActivePlayersInterval = setInterval(() => Token.checkOnlinePlayers(), 5*60*1000);
        }
    }

    private static checkOnlinePlayers() {
        let last5Minutes = 0;
        let lastHour = 0;
        for (let key in Token.lastActiveTime) {
            let time = Token.lastActiveTime[key];
            let elapsed = Date.now() - time;
            if (elapsed < 5 * 60 * 1000)
                last5Minutes++;

            if (elapsed < 60 * 60 * 1000)
                lastHour++;
        }
        winston.info("[ONLINE PLAYERS] Last 5 minutes: " + last5Minutes + ", last hout: " + lastHour);
    }

    private static downloadedTokens: string[] = [];

    private static isCurentlyDownloaded(token: string) {
        return this.downloadedTokens.indexOf(token) != -1;

    }

    static async putLoadedData(data: Models.HHUserData) {
        let token = new Token(data.token);
        let hh_id = data.user_id;

        winston.debug("reg:", data);
        try {
            let user = await DB.users.getUserFromHophandsId(hh_id);

            if (!user)
                data.user_id = (await DB.users.createUser(hh_id, data.username_full)).id;
            else {
                data.user_id = user.id;
                user.version = token.version;
                DB.users.save(user);
            }
         
        }
        catch (error) {
            winston.error("Can't create user", data, error);
            return null;
        }


        this.TokenData[hh_id] = JSON.parse(JSON.stringify(data));

        this.TokenToHHIdMap[token.token] = hh_id;

        


        winston.debug(data.username_full + " logged in through queue, client version: " + token.version + ", OS: " + token.os);
    }

    public async  parse(): Promise<UserData> {
        if (!this.isValid())
            return null;

        let data = <UserData>{};
        let hh_id = Token.TokenToHHIdMap[this.token];

        if (!hh_id) {

            // informacje o graczy są właśnie pobierane, spróbuj za chwilę
            if (Token.isCurentlyDownloaded(this.token)) 
                return new Promise < UserData>((resolve) => setTimeout(() => resolve(), 200)).then(() => { return this.parse() });
            
            data = await this.downloadData();
            if (!data) {
                Token.downloadedTokens.splice(Token.downloadedTokens.indexOf(this.token), 1);
                return null;
            }
            hh_id = data.user_id;
            Token.TokenToHHIdMap[this.token] = hh_id;
        }

        let cached = Token.TokenData[hh_id];
        if (!cached) {
            cached = <UserData>{};
 
            try {
                let user = await DB.users.getUserFromHophandsId(hh_id);
                if (!user)
                    cached.user_id = (await DB.users.createUser(hh_id, data.username_full)).id;
                else {
                    cached.user_id = user.id;

                }
            }
            catch (error) {
                winston.error("Can't create user", data, error);
                Token.downloadedTokens.splice(Token.downloadedTokens.indexOf(this.token), 1);
                return null;
            }

            if (!cached.user_id) {
                Token.downloadedTokens.splice(Token.downloadedTokens.indexOf(this.token), 1);
                return null;
            }

       
            Token.TokenData[hh_id] = cached;
        }
        if (data.username_full) {
            cached.hit_coins = data.hit_coins;
            cached.username_full = data.username_full;
            if (cached.username_full)
                winston.debug(cached.username_full + " logged in, client version: " + this.version + ", OS: " + this.os);
        }
    

        Token.lastActiveTime[cached.user_id] = Date.now();
    
        this.user = cached;
        Token.downloadedTokens.splice(Token.downloadedTokens.indexOf(this.token), 1);
        return cached;
    }


    private async downloadData(): Promise<UserData> {
   
        Token.downloadedTokens.push(this.token);
        return new Promise<UserData>((resolve, reject) => {

            request.get("https://hophands.pl/mobile/user/data", (error, response, body) => {
                if (error) {
                    winston.warn("Can't parse token: ", this.token, error);
                    resolve(null);
                    return;
                }
                if (body) {
                    try {
                        resolve(JSON.parse(body));
                    }
                    catch (error) {
                        winston.error("Can't parse data from server: ", this.token, body);
                        resolve(null);
                    }
                }
                else
                    resolve(null);
            })
                .setHeader("Authorization", this.token);
        });
    }

    public isValid(): boolean {
        return this.token && this.token.length > 0;
    }

    public toString() {
        return this.token;
    }
}