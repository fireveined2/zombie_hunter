﻿export { Users } from "./Users"
export { Challenges } from "./Challenges"
export { Editor } from "./EditorAPI"
export { Updater } from "./Updater"

export interface IRoute {
    setup(app: Express.Application);
}