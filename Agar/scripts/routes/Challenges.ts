﻿import { IRoute } from "./Routes"
import * as express from "express";
import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"
import { Token } from "./Token"
import winston = require("winston")

import { Debug } from "./Debug"
import { ChallengeFinisher } from "../updaters/ChallengeFinisher"

export class Challenges implements IRoute {

    constructor() {
        
    }


    public setup(app: express.Application) {
        app.get("/challenges", this.challenges.bind(this));
        new ChallengeFinisher();
    }

    private async challenges(req: express.Request, res: express.Response) {
        let token = new Token(req.query.token);
        let user = 0;
        if (await token.parse()) 
            user = token.user.user_id;

        try {
            let ver = parseFloat(token.version);
            let data = await DB.challenges.getChallengesUpdateData(user, DB.challenges.getChallengeSendOptionsForVersion(ver), true);
            res.send(data);
            if (!user) {
                winston.debug("Challenges sent to " + req.ip);
                Debug.get().someoneOpenedGame(req.ip);
            }
        }
        catch (error) {
            winston.error("/challenges: Error \n" + error.stack);
        }
        
    }
}