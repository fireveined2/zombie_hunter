﻿import { IRoute } from "./Routes"
import * as express from "express";
import * as Processors from "../processors/Processors"
import { DB } from "../databases/Database"
import * as Models from "../../../Shared/Shared"
import { Token } from "./Token"
import { User } from "../entities/User"
import { Debug } from "./Debug"
import { Ranking } from "./Ranking"
// /users/id

import winston = require("winston")
import { Profiler } from "../utils/Profiler"

export class Users implements IRoute {

    private ranking: Ranking;
    constructor() {
        this.ranking = new Ranking(() => DB.users.getAll(1000 * 3600 * 24), 25);
    }

    public getPvPAcces: (userId: number, pvpId: number) => void;

    public setup(app: express.Application) {
        DB.users.getAll();


        app.get("/users/:id", this.users.bind(this));
        app.get("/ranking", this.getRanking.bind(this));

        app.get("/game/:challenge/:map?", this.getAccesToPlay.bind(this));
        app.post("/game/:challenge/:map?", this.handleUserGame.bind(this));


        app.get("/pvp/:id", this.getAccesToPlay.bind(this));
        //   app.post("/pvp/:id", this.handleUserPvPGame.bind(this));

        app.post("/eq", this.changeUserEQ.bind(this));

        app.post("/message", this.readMessage.bind(this));

        app.post("/debug", this.debug.bind(this));

        app.post("/donate", this.donate.bind(this));
        app.get("/donate", this.getDonates.bind(this));

        app.post("/reset/:challenge", this.resetScore.bind(this));
    }

    private async debug(req: express.Request, res: express.Response) {
        let type = req.body.type;
        res.send("O.K");
        if (type == "offline_game")
            Debug.get().someonePlayedOffline();

        if (type == "game_loaded")
            Debug.get().someoneLoadedGame(req.ip);

        if (type == "loader_error") {
            winston.warn("Loader error: ", req.body.params);
        }
        if (type == "js_error") {
            winston.warn("Js error: ", req.body.params);
        }

    }

    private async getRanking(req: express.Request, res: express.Response) {
        let token = new Token(req.query.token);
        if (!await token.parse()) {
            winston.warn("[getRanking] Wrong token!", token);
            res.status(404).send("Wrong token!");
            return;
        }

        let user = (await DB.users.get(token.user.user_id));
        /* let ranking = {
             overall: await this.ranking.getOverallRankingForUser(userName),
             daily: await this.ranking.getDailyRankingForUser(userName),
         };
         */
        let ranking = await this.ranking.getDailyRankingForUser(user);

        res.send(ranking);
    }

    private async resetScore(req: express.Request, res: express.Response) {
        let token = new Token(req.body.token);
        if (!await token.parse()) {
            res.status(404).send("Wrong token!");
            return;
        }

        let challenge = req.body.challenge;
        let user = await DB.users.get(token.user.user_id, true);
        let game = await DB.gamesData.get(token.user.user_id, challenge);
        if(game)
            DB.gamesData.remove(game);
        winston.info(user.name + " deleted his score from challenge " + challenge);
        // let data = await this._users(id);
        //   data.hcn = token.user.hit_coins;

        res.send(true);
    }

    public _users(id: number): Promise<Models.PlayerData> {
        return DB.users.generateUserData(id);
    }

    private async users(req: express.Request, res: express.Response) {
        let token = new Token(req.query.token);
        if (!await token.parse()) {
            res.status(404).send("Wrong token!");
            return;
        }

        let id = req.params["id"];
        if (id == "me") {
            id = token.user.user_id;


        }
        let user = await DB.users.get(id, false);

        if (user.prevHCNCount != token.user.hit_coins) {
            user.prevHCNCount = token.user.hit_coins;
            let diff = token.user.hit_coins - user.prevHCNCount;
            if (diff > 0 && user.prevHCNCount != -1) {
                await this.grantHCNBonus(user, diff);
            }

        
        }


        let data = await this._users(id);
        //   data.hcn = token.user.hit_coins;

        if (!data) {
            res.status(404).send("Can't find user!");
            return;
        }
        res.send(data);
    }

    private async grantHCNBonus(user: User, hcn: number) {
        let giveMysteryBox = async () => {
            let item = await Processors.Rewards.dropRandomItem();
            user.addItemsIds([item]);
            user.pendingRewards.push({ type: Models.IRewardType.MYSTERY_BOX, name: item.name, icon: item.icon });
        }
        let giveItem = (item: Models.IItem) => {
            user.addItemsIds([item]);
            user.pendingRewards.push({ type: Models.IRewardType.DEFAULT, name: item.name, icon: item.icon });
        }

        let entrances = 0;
        if (hcn > 27 && hcn < 35) {
            entrances += 9
        }

        if (hcn >= 35 && hcn < 55) {
            entrances += 17;
        }

        if (hcn >= 55 && hcn < 120) {
            entrances += 36;
            await giveMysteryBox();
        }

        if (hcn >= 120 && hcn < 320) {
            entrances += 35;
            await giveMysteryBox();
        }

        if (hcn >= 320 && hcn < 645) {
            entrances += 75;
            await giveMysteryBox();
        }

        if (hcn >= 645 && hcn < 1200) {
            entrances += 160;
            await giveMysteryBox();
            giveItem(await DB.items.get(59));
            giveItem(await DB.items.get(60));
            giveItem(await DB.items.get(61));
            giveItem(await DB.items.get(62));
        }

        if (hcn >= 1200 && hcn < 2500) {
            entrances += 350;
            await giveMysteryBox();
            await giveMysteryBox();
            giveItem(await DB.items.get(56));
            giveItem(await DB.items.get(57));
            giveItem(await DB.items.get(58));
        }

        if (hcn >= 2500) {
            entrances += 900;
            for (let i = 0; i < 4; i++)
                await giveMysteryBox();
            giveItem(await DB.items.get(49));
            giveItem(await DB.items.get(50));
            giveItem(await DB.items.get(51));
            giveItem(await DB.items.get(52));
        }


        user.grantFreeTourneyEntrances(entrances);
        winston.info(user.name + " gained " + entrances + " entraces for " + hcn + " HCN");
        DB.users.save(user);
    }

    private async getDonates(req: express.Request, res: express.Response) {

        if (req.query.token != "pass") {
            res.status(404).send("Can't get donates!");
        }

        let data = await DB.donates.getAll();
        //   data.hcn = token.user.hit_coins;

        if (!data) {
            res.status(404).send("Can't find user!");
            return;
        }
        res.send(data);
    }
    private async donate(req: express.Request, res: express.Response) {
        let token = new Token(req.body.token);
        if (!await token.parse()) {
            res.status(404).send("Wrong token!");
            return;
        }

        let message = req.body.msg;
        let hcn = req.body.hcn;



        let user = await DB.users.get(token.user.user_id, true);
        DB.donates.add(message, hcn, user.name);
        // let data = await this._users(id);
        //   data.hcn = token.user.hit_coins;

        res.send(true);
    }

    public async _getAccesToPlay(request: Models.IGameAccesRequest, user: number): Promise<Models.AccesToPlay> {
        let acces;

        if (request.pvpId !== undefined)
            acces = this.getPvPAcces(user, request.pvpId)
        else if (request.challengeId == 0)
            acces = await DB.users.grantTrainingAccesFor(user, request.trainingMap)
        else
            acces = await DB.users.grantAccesForChallenge(user, request)

        return acces;
    }

    private async getAccesToPlay(req: express.Request, res: express.Response) {
        let token = new Token(req.query.token);
        if (!await token.parse()) {
            res.status(404).send("Wrong token!");
            return;
        }

        let request: Models.IGameAccesRequest = req.query.request;

        if (!request || (request.challengeId === undefined && request.pvpId === undefined)) {
            request = <Models.IGameAccesRequest>{};
            let map = req.params["map"];
            let challenge = req.params["challenge"];

            request.trainingMap = map;
            request.challengeId = challenge;

        }

        let acces: Models.AccesToPlay;
        let user = token.user.user_id;
        let userData = await DB.users.get(user, true);

        acces = await this._getAccesToPlay(request, user);

        if (acces) {
            res.send(acces);
            if (request.pvpId)
                winston.debug(userData.name + " has acces to PvP " + request.pvpId);
            else
                winston.debug(userData.name + " has acces to challenge " + request.challengeId);
        }
        else {
            if (request.pvpId)
                winston.warn(userData.name + " HAS NO acces to PvP " + request.pvpId);
            else
                winston.warn(userData.name + " HAS NO acces to challenge " + request.challengeId);
            res.status(404).send("Can't find challenge!");
        }
    }





    public async _handleUserGame(userId: number, game: Models.GameData, uptodatedVersion: boolean): Promise<Models.GameResults> {
        let user = await DB.users.get(userId, true);

        /* if (!user.hadAccesFor(game)) {
             winston.warn("No acces for game!", { user: user.id, challenge: game.challenge });
             return null;
         }
         
         */
        let result = await Processors.Game.handleGame(game, user.id, uptodatedVersion);
        return result;
    }


    private async handleUserGame(req: express.Request, res: express.Response) {
        let start = Date.now();
        let token = new Token(req.body.token);
        if (!await token.parse()) {
            winston.warn("[handleUserGame] Wrong token!", token);
            res.status(404).send("Wrong token!");
            return;
        }
        let profilerId = Profiler.get().start("handleUserGame");

        let user = token.user.user_id;
        let game: Models.GameData = req.body.gameData;

        let results = await this._handleUserGame(user, game, token.uptodateVersion);


        if (results) {
            results.playerUpdate = await DB.users.generateUserData(user);
            let ver = parseFloat(token.version);
            results.challengesUpdate = await DB.challenges.getChallengesUpdateData(user, DB.challenges.getChallengeSendOptionsForVersion(ver), false, game.challenge);

            res.send(results);
            Profiler.get().end(profilerId);
        }
    }



    private async readMessage(req: express.Request, res: express.Response) {
        let token = new Token(req.body.token);
        if (!await token.parse()) {
            winston.warn("[readMessage] Wrong token!", token);
            res.status(404).send("Wrong token!");
            return;
        }

        let user_id = token.user.user_id;
        let message: number = req.body.message;
        let user = await DB.users.get(user_id);
        user.readMessage(message);
        DB.users.save(user);
        winston.debug(user.name + " read message " + message);
    }


    private async changeUserEQ(req: express.Request, res: express.Response) {
        let token = new Token(req.body.token);
        if (!await token.parse()) {
            winston.warn("[changeUserEQ] Invalid token!", token);
            res.status(404).send("Wrong token!");
            return;
        }

        let user = token.user.user_id;
        let eq: Models.IEQChange = req.body.change;
        let result = true;

        if (eq && eq.usedItemsIds && eq.wearedItemsIds) {
            try {
                let result = await Processors.User.updateEQ(eq, user);
            }
            catch (error) {
                result = false;
                winston.error(error, eq, user);
            }
        }
        else
            result = false;

        if (result)
            res.send(true);
        else {
            winston.warn("Can't update EQ", eq, user);
            res.send({ error: "Can't update eq" });
        }

    }
}