﻿import winston = require("winston")

interface IProfilerEntry {
    name: string;
    start: number;

}

interface IFunctionCallsStats {
    calls: number;
    totalDuration: number;
}
export class Profiler {
    private curId = 0;

    private pending: { [id: number]: IProfilerEntry } = {} as any;
    private history: { [name: string]: number[] } = {} as any;

    private static instance: Profiler;

    constructor() {

        setInterval(() => this.log(), 60*1000*60);
    }
    static get() {
        if (!this.instance)
            this.instance = new Profiler();
        return this.instance;
    }

    public start(name: string) {
        let id = this.curId;
        this.pending[id] = { start: Date.now(), name: name };
        this.curId++;
        return id;
    }

    public end(id: number) {
        let entry = this.pending[id];
        if (!entry) {
            winston.warn("[PROFILER] Missing entry id = " + id);;
            return;
        }
        let duration = Date.now() - entry.start;

        if (!this.history[entry.name])
            this.history[entry.name] = [];

        this.history[entry.name].push(duration);
    }


    private logOne(name: string, calls: number[]) {
        let avg = 0;
        let mediana = 0;
        let max = 0;
        let top10PercentAvg = 0;
        calls = calls.sort((a,b)=>a-b);
        for (let call of calls)
            avg += call / calls.length;
        avg = Math.round(avg);

        let start = Math.round(calls.length * 0.9);;
        for (let i =start; i < calls.length;i++)
            top10PercentAvg += calls[i];
        if (start != calls.length)
            top10PercentAvg /= calls.length - start;
        top10PercentAvg = Math.round(top10PercentAvg);

        mediana = calls[Math.round(calls.length / 2)];
        max = Math.max(...calls);

        winston.info("[PROFILER] " + name + ", " + calls.length + " calls");
        winston.info(`      Average: ${avg} ms`);
        winston.info(`      Median: ${mediana} ms`);
        winston.info(`      Max: ${max} ms`);
        winston.info(`      Worst 10% average: ${top10PercentAvg} ms`);
    }

    private log() {
        for (let name in this.history) 
            this.logOne(name, this.history[name]);

        this.reset();
    }

    private reset() {
        this.history = {} as any;
        this.curId = 0;
    }
}