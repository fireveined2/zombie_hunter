﻿import * as express from "express";
import * as Routes from "../routes/Routes"
import { DB } from "../databases/Database"
import bodyParser = require("body-parser")
import { Tests } from "../tests/Tests"


import { RequestQueue } from "../RequestQueue/Queue"
import { Token } from "../routes/Token"
import { SocketServer } from "../socket/Server"
import { PvPServer } from "../pvp/Server"
var hophands = true;


export class Server {

    private app: express.Express;
    private database: DB;
    private routes: Routes.IRoute[] = [];
    

    constructor(app: express.Express, socket, private socket2) {
        this.database = new DB(() => this.setupRoutes(), hophands);
        this.app = app;
        this.setupCors();
        app.use(bodyParser.json({ limit: '1mb' }));

        let queue = new RequestQueue(socket);
        queue.onDataDownloaded = Token.putLoadedData.bind(Token);

 
    }

    private setupRoutes() {
    
        let socketServer = new SocketServer(this.socket2);
        let pvpServer = new PvPServer();
        DB.pvp = pvpServer;
        socketServer.onPvPRequest = pvpServer.onPvPRequest.bind(pvpServer);
        socketServer.onJoinPvPQueue = pvpServer.onJoinQueue.bind(pvpServer);

        let users = new Routes.Users();
        users.getPvPAcces = pvpServer.getPvPAccesToPlay.bind(pvpServer);
        this.routes.push(users);
        this.routes.push(new Routes.Challenges());
        this.routes.push(new Routes.Editor());
        this.routes.push(new Routes.Updater());

        for (let route of this.routes)
            route.setup(this.app);

    }

    private setupCors() {
        this.app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });
    }

}