﻿import * as express from "express";
import * as Routes from "../routes/Routes"
import { DB } from "../databases/Database"
import bodyParser = require("body-parser")
import { Tests } from "../tests/Tests"


import { RequestQueue } from "../RequestQueue/Queue"
import { Token } from "../routes/Token"
const crypto = require('crypto');
var request = require("request");
import winston = require("winston")

export class HHClient {



    static async buyAccesFor(user_id: number, tournament_id: number): Promise<boolean> {
        let token = encodeWEBaccessToken(parseInt(tournament_id as any), parseInt(user_id as any));

        return new Promise<boolean>((resolve) => {
            request({
                uri: "https://hophands.pl/api/game-server/tournament/user/status",
                method: "POST",
                json: true,
                body: {
                    token: token,
                    status: 10
                }
            }, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    winston.info(`{ cupon } User ${user_id} has bougth coupon.`);

                    return resolve(true);
                }
                else if (!error && response.statusCode === 202) {
                    winston.info(`{ cupon } User ${user_id} has already bought coupon.`);
                    return resolve(true);
                }
                else {
                    winston.warn(`{ coupon } Error while buying new cupon by user ${user_id}.`);
                    winston.warn(JSON.stringify(response, null, 8));
                    winston.warn(JSON.stringify(body, null, 8));
                    winston.warn(JSON.stringify(error, null, 8));
                    return resolve(false);

                }
            }, function (error, response, body) {
                winston.warn(`{ coupon } Error while buying new cupon by user ${user_id}.`);
                if (response && response.statusCode === 400) {
                    winston.warn(`{ cupon } no money`);
                }
                winston.warn(JSON.stringify(response, null, 8));
                winston.warn(JSON.stringify(body, null, 8));
                return resolve(false);
            });
        });
    }

    static removeAccesFrom(user_id: number, tournament_id: number) {
        let token = encodeWEBaccessToken(tournament_id, user_id);

        request({
            uri: "https://hophands.pl/api/game-server/tournament/user/status",
            method: "POST",
            json: true,
            body: {
                token: token,
                status: 30
            }
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
               // winston.warn(`{ cupon } User ${user_id} cupon to tournament ${tournament_id} changed status to 30`);
            }
            else {
                winston.error(`Error while preventing user access to tournament.`);
                winston.error(JSON.stringify(response, null, 8));
                winston.error(JSON.stringify(body, null, 8));
            }
        });

    }



    static addHCN(user_id: number, hcn: number) {
        let token = encodUserAccessToken(user_id);
      //  console.log("adding");

        let addFlag = 1;
        if (hcn < 0)
            addFlag = 2;

        let type = 2;

        request({
            uri: "https://hophands.pl/api/game-server/user/change-hcn",
            method: "POST",
            json: true,
            body: {
                token: token,
                hcn: Math.abs(hcn),
                add: addFlag,
                type: type
            }
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                winston.info(hcn + " HCN for user " + user_id);
            }
            else {
                winston.warn(`Cant give ${hcn} HCN to user ` + user_id);
                winston.warn(JSON.stringify(response, null, 8));
                winston.warn(JSON.stringify(body, null, 8));
            }
        });

    }

}


//HHClient.addHCN(111, 2);

function base64encode(string: string): string {
    return new Buffer(string).toString('base64');
}

function encodUserAccessToken(user_id: number): string {
    let headerArr = {
        typ: "JWT",
        alg: "SH256"
    },
        header = JSON.stringify(headerArr),
        headerEnc = base64encode(header);

    let payloadArr = {
        iss: 'AM',
        exp: new Date().getTime(),
        user_id: user_id
    },
        payload = JSON.stringify(payloadArr),
        payloadEnc = base64encode(payload);
  //  console.log(payload);
    let headerAndPayload = headerEnc + "." + payloadEnc;

    let secret = crypto.createHmac('sha256', "alamakota")
       .update(headerAndPayload)
        .digest('hex');
    let token = headerAndPayload + "." + secret;
  //  console.log(token);
    return token;
}


function encodeWEBaccessToken(tournament_id: number, user_id: number): string {
    let headerArr = {
        typ: "JWT",
        alg: "SH256"
    },
        header = JSON.stringify(headerArr),
        headerEnc = base64encode(header);

    let payloadArr = {
        iss: 'AM',
        exp: new Date().getTime(),
        tour_id: tournament_id,
        game_hash: "96624",
        user_id: user_id
    },
        payload = JSON.stringify(payloadArr),
        payloadEnc = base64encode(payload);
   // console.log(payload);
    let headerAndPayload = headerEnc + "." + payloadEnc;

    let secret = crypto.createHmac('sha256', "alamakota" + tournament_id)
        .update(headerAndPayload)
        .digest('hex');
    let token = headerAndPayload + "." + secret;
   // console.log(token);
    return token;
}