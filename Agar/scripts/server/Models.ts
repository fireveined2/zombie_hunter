﻿export interface IRoute {
    setup(app: Express.Application);
}