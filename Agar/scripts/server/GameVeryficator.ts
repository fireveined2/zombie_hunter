﻿import * as Processors from "../processors/Processors"
import { Validate } from "../../../Veryficator/veryficator"
import * as Models from "../../../Shared/Shared"
import winston = require("winston")
var Worker = require("tiny-worker");

export interface IGameToVerification {
    id: number;
    game: Models.GameData;
    acces: Models.AccesToPlay;
}


interface VeryficatorWorker {
    worker: Worker;
    busy: boolean;
}


export class Veryficator {
    private queue: IGameToVerification[] = [];


    private totalVeryficationTime: number = 0;
    private totalGames  =0;
    private totalGamesTime: number =0;
    private curId = 0;

    private scores: number[] = [0,0,0];
    private gamesPlayed: number[] = [0, 0, 0];
    private stats: Models.GameStats[] = [];


    private validFunctions: { [id: number]: Function } = {}


    private workers: VeryficatorWorker[] = [];

    constructor() {
        setInterval(() => this.logAverage(), 4 * 3600 * 1000);
        // settIMEOUT(() => this.logAverage(), 0.1 * 3600 * 1000);

        this.createNewWorker();
        this.createNewWorker();
        this.createNewWorker();
        this.createNewWorker();
    }

    private createNewWorker() {
        let worker = new Worker(function () {
            console.info("Veryficator process created");

            var Validate;
            self.onmessage = function (ev) {

                if (!Validate) {
                    try {
                        Validate = require(ev.data + "/../../../Veryficator/veryficator");
                        postMessage("Validate lib loaded", null);
                    }
                    catch (e) {
                        console.info(e);
                        postMessage(e.stack, null);
                    }
                    return;
                }

                try {
                    let item = <IGameToVerification>JSON.parse(ev.data);
                    Validate.Validate(item.acces, item.game.input, item.game.screenWidth, item.game.screenHeight,
                        (stats, duration, debugPoints, input) => {
                            postMessage([item, stats, duration, debugPoints, input], null);
                        }, (item as any).collectInput, (item as any).speed);
                }
                catch (error) {
                    console.log(error.stack);
                    postMessage("Veryfication error: " + error.stack, null);
                }

            };
        })
        setTimeout(() => worker.postMessage(__dirname), 0);
        worker.onmessage = (msg) => winston.info("Veryficator: " + msg.data);


        let process = <VeryficatorWorker>{
            worker: worker,
            busy: false
        }
        this.workers.push(process);
    }

    private getFreeWorker(): VeryficatorWorker {
        for (let worker of this.workers)
            if (!worker.busy)
                return worker;

        return null;
    }

    public push(game: Models.GameData, acces: Models.AccesToPlay, onNotValid: (game: Models.GameData, acces: Models.AccesToPlay, actual: Models.GameStats) => void) {
        if (this.queue.length > 4) {
            winston.warn("Veryfication aborted!");
            return;
        }

        let item = <IGameToVerification>{
            game: game,
            acces: acces,
            id: this.curId++
        }
        this.queue.push(item);
        this.validFunctions[item.id] = onNotValid;


       this.startVeryfication();
    }


    private speedForNextVeryfication = undefined;
    private collectInputForNextVeryficatin = false;

    public startVeryfication() {
        if (this.queue.length == 0)
            return;

        if (this.queue.length > 5)
            winston.warn("[VERYFICATOR] Queue length = " + this.queue.length + "!");

        let worker = this.getFreeWorker();
        if (!worker)
            return;

        worker.busy = true;
        let item = this.queue.shift();

        let timeout = setTimeout(() => {
           /*
            this.currentlyValidating = false;
            winston.error("Can't validate game! Timeout!", item);

            if (this.queue.length > 0)
                this.startVeryfication();
            */
        }, 10000);

        if (this.speedForNextVeryfication)
            (item as any).speed = this.speedForNextVeryfication;
        this.speedForNextVeryfication = undefined; 

        if (this.collectInputForNextVeryficatin)
            (item as any).collectInput = true;
        this.collectInputForNextVeryficatin = false; 

        worker.worker.postMessage(JSON.stringify(item));
        worker.worker.onmessage = (msg) => {
            worker.busy = false;
            clearTimeout(timeout);

            let data = msg.data;
            if (typeof data === "string")
                winston.error(data);
            else {
                this.onGameEnded(data[0], data[1], data[2], data[3], this.workers.indexOf(worker), data[4]);
            }


            if (this.queue.length > 0)
                this.startVeryfication();
        }
    }

    private num = 0;

    private onGameEnded(item: IGameToVerification, actual: Models.GameStats, duration: number, debugPoints: number[], workerNr: number, input?: any) {
        let expected = item.game.stats;
        if (expected.score == actual.score) {
            winston.debug("Gra zweryfikowana, worker nr " + (workerNr + 1) + ", czas: " + duration + " ms, pkt: " + item.game.stats.score);
         //   winston.debug("Debug: ", item.game.stats, actual, item.acces.config);

            this.totalGamesTime += item.game.stats.duration*1000;
            this.totalVeryficationTime += duration;
            this.totalGames++;

            let map = item.acces.config.map;
            this.scores[map] += actual.score;
            this.gamesPlayed[map]++;

            if (!this.stats[map])
                this.stats[map] = <any>{};
            this.stats[map] = Processors.GameStats.add(this.stats[map], actual);
            delete this.validFunctions[item.id];

  
        }
        else {
        

            winston.error("Game is not valid! Worker " + workerNr, item.game.stats, actual, item.acces.config, duration);
            if (debugPoints && debugPoints.length && item.game.debugPointsInfo && item.game.debugPointsInfo.length)
                for (let i = 0; i < debugPoints.length; i++) {
                    if (debugPoints[i] != item.game.debugPointsInfo[i]) {
                        winston.info("Points dont match since " + i + " measure. ", debugPoints[i], item.game.debugPointsInfo[i]);
                        break;
                    }
                    if (i == debugPoints.length - 1)
                        winston.info("Point measures match!");
                }
            else
                winston.warn("NO DEBUG DATA!");



            if (this.validFunctions[item.id])
            this.validFunctions[item.id](item.game, item.acces, actual);
            delete this.validFunctions[item.id];

/*
            if (this.num < 7) {
                this.num += 3;
                item.game.stats = actual;
                item.game.input = input;
                this.push(item.game, item.acces, () => { });
            }
            else
                this.num = 0;
            */
        }

    }    


    private logAverage() {
        if (this.totalGames == 0)
            return;

        let avg = Math.round(this.totalVeryficationTime / this.totalGames);
        let ones = Math.round(this.totalGamesTime / this.totalVeryficationTime  *10);
        winston.info("Average veryfication time: " + avg + "ms. Veryfication duration for 10 seconds of game: " + ones);

        for (let i = 0; i < this.scores.length; i++) {
            if (!this.scores[i])
                continue;
            let avg = this.scores[i] / this.gamesPlayed[i];

            let avgStats = {};
            for (let stat in this.stats[i])
                avgStats[stat] = this.stats[i][stat]/this.gamesPlayed[i];

            winston.info("Stats of map " + i + ": ", { score: this.scores[i], played: this.gamesPlayed[i], average_score: avg }, avgStats);
        }
    }
}