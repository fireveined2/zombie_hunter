﻿import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from "typeorm";
import * as Models from "../../../Shared/Shared"
import { ItemsHelpers } from "../../../Shared/Items"

@Entity()
export class GameConfig implements Models.GameConfig {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("int")
    seed: number;

    @Column("int")
    duration: number;

    @Column("int")
    map: number;

    @Column("string", { length: 300, default: "{}" })
    bonusString: string;
    bonus: Models.IGameBonus;

    @AfterLoad()
    private parse() {
        this.bonus = JSON.parse(this.bonusString);
    }

    static getTrainingConfig(map: number, items: Models.IItem[]): Models.GameConfig {
        return <Models.GameConfig>{
            seed: Date.now()%9999999,
            duration: 90,
            map: map,
            bonus: ItemsHelpers.getAllBonusses(items)
        }
    }
}