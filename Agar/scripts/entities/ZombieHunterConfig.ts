﻿import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from "typeorm";
import * as Models from "../../../Shared/Shared"
import { ItemsHelpers } from "../../../Shared/Items"



@Entity()
export class ZombieHunterConfig{
    @PrimaryGeneratedColumn()
    id: number;

    @Column("string", { length:30, default:"stat" })
    name: number;

    @Column("string", { length: 130, default: "0" })
    value: string;


    
}