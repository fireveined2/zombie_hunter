﻿import { Entity, Column, PrimaryGeneratedColumn, OneToOne, AfterLoad } from "typeorm";
import { Challenge } from "./Challenge"
import * as Models from "../../../Shared/Shared"
import * as Processors from "../processors/Processors"

@Entity()
export class GameData {
    @PrimaryGeneratedColumn()
    id: number;


    @Column("int")
    user: number;

    @Column("int")
    challenge: number;

    @Column("string", { length: 1900 })
    data: string;
    stats: Models.GameStats;

    @Column("smallint")
     success: Models.GameDataStatus;

    @Column("int")
    overallScore: number;

    @AfterLoad()
    public  decompress() {
        this.stats = Processors.GameStats.decompress(this.data);
    }

    get status() {
        return this.success
    }
    set status(status: Models.GameDataStatus) {
        this.success = status;
    }
}