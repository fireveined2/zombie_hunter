﻿import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, AfterLoad, RelationId } from "typeorm";
import * as Models from "../../../Shared/Shared"
import { GameConfig } from "./GameConfig"
import * as Processors from "../processors/Processors"


@Entity()
export class Challenge implements Models.IChallenge {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("string", {
        length: 40,
        default: "Nazwa"
    })
    name: string;

    @Column("string", { default: "Opis", length: 500 })
    description: string;

    @Column("string", { length: 10, default:"5/10" })
    respect: string;
    cost: number;
    gain: number;

    @Column("int", { default: 0 })
    prizeId: number;
    prize: Models.IPrize;

    @Column("smallint", { default: 0})
    type: Models.ChallengeType;

    @Column("int", { default: Challenge.PrizesInfinityMark })
    prizesLeft: number;

    @Column("bigint", { default: Date.now()+1000*3600*24 })
    endTime: number;

    @Column("bigint", { default: 1 })
    minLevel: number;


    @Column("int", { default: 1 })
    configId: number;
    config: Models.GameConfig;
     
    @Column("string", { length: 1550 })
    criteriaCompressed: string;
    criteria: Models.ChallengeCriteria[];

    @Column("int")
    succesfulTries: number;

    @Column("int")
    tries: number;

    @Column("smallint")
    quest: number;

    tourneyData?: Models.ITourneyData;

    minutesLeft: number;

    @AfterLoad()
    public parseDuration() {
        this.minutesLeft = Math.round((this.endTime - Date.now()) / 1000 / 60);
        this.criteria = Processors.ChallengeCriteria.decompress(this.criteriaCompressed);

        let res = this.respect.split("/");
        this.cost = parseInt(res[0]);
        this.gain = parseInt(res[1]);
    }

    public getTourneyPrizesIds(): Models.ITourneyPrize[] {
        return this.criteria as any;
    } 
     
    static PrizesInfinityMark = 10000;
    public isActive() {
        return Date.now() < this.endTime && this.prizesLeft > 0;
    }
}