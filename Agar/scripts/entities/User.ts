﻿import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { Challenge } from "./Challenge"
import * as Models from "../../../Shared/Shared"

interface ChallengeAcces {
    challenge: number;
    isTraining: boolean;
}

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("int")
    hophands_id: number;


    @Column("string", {
        length: 30,
        default: "Gracz"
    })
    name: string;

    @Column("int", { default: 10 })
    respect: number;

    @Column("int", { default: 0 })
    exp: number;

    @Column("int", { default: 1 })
    level: number;

    @Column("int", { default: 0 })
    nextTrainingSeed: number;

    @Column("string", { length: 60, default: "" })
    pendingMessages: string;

    @Column("float", { default: 0.9 })
    hp: number;

    @Column("string", {
        default: '{"0":1,"1":2,"2":3,"3":4,"4":5}',
        length: 60
    })
    usedItemsIds: string;

    @Column("string", {
        default: ' 1 2 3 4 5 11 11',
        length: 1750
    })
    itemsIds: string;
    items: Models.IItem[];

    @Column("int", { default: 0 })
    banned: number;

    @Column("string", {
        default: '0 0 0 0 0 0',
        length: 50
    })
    recordsString: string;

    @Column("string", {
        default: '0 0 0 0 0 0',
        length: 50
    })
    dailyRecordsString: string;

    //stats
    @Column("int", { default: 0 })
    playedGamesNum: number;

    @Column("bigint", { default: 0 })
    lastGameTime: number;

    @Column("string", {
        default: '0',
        length: 6
    })
    version: string;

    @Column("smallint", { default: 0 })
    canMakePvP: number;

    @Column("smallint", { default: 0 })
    chatBan: boolean;

    @Column("smallint", { default: 0 })
    freeTourneyEntrances: number;

    @Column("int", { default: -1 })
    prevHCNCount: number;

    tourneyGames: number = 0;

    grantedTourneyEntrances: number = 0;
    pendingRewards: Models.IPendingReward[]=[];

    public generateInfo() {
        return <Models.PlayerInfo>{
            id: this.id,
            level: this.level,
            respect: this.respect,
            username: this.name,
            wearedItemsIds: JSON.parse(this.usedItemsIds)
        }
    }
    private challengeAcces: ChallengeAcces;

    public grantFreeTourneyEntrances(num: number) {
        this.grantedTourneyEntrances = num;
        this.freeTourneyEntrances += num;
    }

    public getRecords(): number[] {
       return this.recordsString.split(" ") as any;
    }

    public setRecordOnMap(map: number, record: number) {
        let records = this.recordsString.split(" ");
        records[map] = record.toString();
        this.recordsString = records.join(" ");
    }

    public getDailyRecords(): number[] {
        return this.dailyRecordsString.split(" ") as any;
    }

    public setDailyRecordOnMap(map: number, record: number) {
        let records = this.dailyRecordsString.split(" ");
        records[map] = record.toString();
        this.dailyRecordsString = records.join(" ");
    }


    public getWearedItem(type: Models.ItemType) {
        let weared = JSON.parse(this.usedItemsIds);
        for (let item of this.items)
            if (item.id == weared[type])
                return item;
    }

    public getAllWearedItems()  {
        let type = Models.ItemType;
        let types = [type.HEAD, type.CORPUS, type.BOOTS, type.WEAPON1, type.WEAPON2];
        let items: Models.IItem[] = [];
        for (let typ of types) {
            let item = this.getWearedItem(typ);
            if (item)
                items.push(item);
        }
        return items;
    } 


    public readMessage(id: number) {
        this.pendingMessages = this.pendingMessages.replace(id.toString() + " ", '');
    }

    public getPendingMessages(): number[] {
        let items = this.pendingMessages.split(" ") as any[];
        if (items[0] == 0)
            items.splice(0, 1);
        return items;
    }
    public addMessage(id: number) {
        this.pendingMessages += id + " ";
    }

    public grantAccesToChallenge(id: number, isTraining: boolean) {
        this.challengeAcces = <ChallengeAcces>{
            challenge: id,
            isTraining: isTraining
        };
    }
    public isCurrentChallengeTraining() {
        if (!this.challengeAcces)
            return false;

        return this.challengeAcces.isTraining;
    }

    public getItemsIds(): number[] {
        let items = this.itemsIds.split(" ") as any[];
        if (items[0] == 0)
            items.splice(0,1);
        return items;
    }

    public saveItemsIds(ids: number[]) {
        this.itemsIds = ids.join(" ");
    }
    public addItemsIds(items: Models.IItem[]) {
        for (let item of items) {
            this.itemsIds += " " + item.id;
            this.items.push(item);
        }
    }

    private removeItem(id: number) {
        this.itemsIds =  this.itemsIds.replace(" "+id.toString(), '');
    }

    public removeAccesForChallenge() {
        delete this.challengeAcces;
    }

    public hadAccesFor(game: Models.GameData) {
        return (game.challenge == 0) ||
            (this.challengeAcces && this.challengeAcces.challenge == game.challenge);
    }

    public hasItem(id: number): Models.IItem {
        for (let it of this.items) 
            if (it.id == id)
                return it;
        return null;
    }

    public useItem(id: number) {
        let item = this.hasItem(id);
        if (!item)
            return false;
      
        for (let stat in item.bonus)
            if (stat == "heal") {
                this.hp += item.bonus[stat];
                if (this.hp > 1)
                    this.hp = 1;
            }
        this.removeItem(id);
        let index = this.items.indexOf(item);
        this.items.splice(index, 1);
        return true;
    }


}


