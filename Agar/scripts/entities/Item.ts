﻿import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from "typeorm";
import * as Models from "../../../Shared/Shared"



@Entity()
export class Item implements Models.IItem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("string", {
        length: 30,
        default: ""
    })
    name: string;

    @Column("smallint", { default: 0 })
    type: Models.ItemType;

    @Column("string", { length: 150, default:"{}" })
    bonusesString: string;
    bonus: Models.IItemBonus;

    @Column("number", { default: -1 })
    set: Models.ItemSet;

    @Column("int", { default: 0})
    dropChance: number;

    @AfterLoad()
    private parse() {
        this.bonus = JSON.parse(this.bonusesString);

        this.icon = this.id;
        if (this.bonus.icon !== undefined)
            this.icon = this.bonus.icon;
    }


    icon: number;
}