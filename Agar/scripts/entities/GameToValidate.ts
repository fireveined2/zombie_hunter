﻿import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from "typeorm";
import * as Models from "../../../Shared/Shared"



@Entity()
export class GameToValidate {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("number", { default: 0 })
    user: number;

    @Column("text")
    config: string;

    @Column("text")
    input: string;

    @Column("text")
    playerStats: string;

    @Column("text")
    gameStats: string;

    @Column("number", { default: 0 })
    width: number;

    @Column("number", { default: 0 })
    height: number;


    @AfterLoad()
    private parse() {
       
    }
}