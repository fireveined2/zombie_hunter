﻿import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from "typeorm";
import * as Models from "../../../Shared/Shared"



@Entity()
export class Message implements Models.IUserMessage {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("string", {
        length: 100,
        default: ""
    })
    title: string;

    @Column("text")
    message: string;

    @Column("boolean", { default: 0})
    openImmediately: boolean;
}