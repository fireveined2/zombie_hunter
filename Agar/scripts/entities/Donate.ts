﻿import { Entity, Column, PrimaryGeneratedColumn, AfterLoad } from "typeorm";
import * as Models from "../../../Shared/Shared"



@Entity()
export class Donate {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("string", {
        length: 256,
        default: ""
    })
    message: string;

    @Column("string", { default: "", length: 50, })
    user: string;

   
    @Column("number", { default: 0 })
    hcn: number;

    @Column("number", { default: 0 })
    time: number;

    @Column("int", { default: 1 })
    status: number;
}