﻿import * as Models from "../../../Shared/Shared"

import { DB } from "../databases/Database"
import { Challenges, ChallengeEntity } from "../databases/tests/Challenges"
import { Users, UserEntity } from "../databases/tests/Users"
import { GameData, GameDataEtinity } from "../databases/tests/GameData"
import * as Processors from "../processors/Processors"

import { assert } from "chai"
import "mocha"

import "./Challenges"
import "./Parsers"

export interface TestClass {
    prepare();
    run();
}
export class Tests {

    static tests: TestClass[] = []
    static async run() {
     
        for (let test of this.tests) {
            try {
                await test.prepare();
                await test.run();
            }
        catch (error) {
            console.log(error.stack);
            }
        }
        run();
    }
}

/*


let test = () => {


    let users = DB.users as Users;
    let challenges = DB.challenges as Challenges;
    let games = DB.gamesData as GameData;


    let user = new UserEntity();
    user.id = 1;
    user.name = "User1";
    user.respect = 10;
    users.addUser(user);

    describe("Simple challenge results", async () => {

        let challenge = challenges.repo.create();
        challenge.cost = 5;
        challenge.endTime = Date.now() + 100000;
        challenge.id = 1;
        challenge.name = "Event 1";
        challenge.type = Models.ChallengeType.SIMPLE;
        challenge.succesfulTries = 0;
        challenge.prizesLeft = 3;
        challenge.criteria = JSON.stringify([<Models.ChallengeCriteria>{
            stat: "score",
            operator: Models.ChallengeCriteriaOperator.MORE,
            value: 100
        }]);
        challenge.config = <any>{ duration: 10 };
        challenge.parseDuration();

        let data = <Models.GameData>{
            stats: <Models.GameStats>{
                score: 200
            },
            challenge: 1,
            input: {}
        }
        // let results = await Processors.Game.handleGame(data, user.id);

        it("should be SUCCES if player match the criteria", async () => {
            let results = await Processors.Game.handleGame(data, user.id);
            let expected = Models.ChallengeResult.SUCCES;
            let actual = results.overall.challengeResult;
            assert.equal(actual, expected);
        });


    });


    describe("opis", () => {

        it("shoudl work", () => {

            assert.equal(2, 3);
        })
    });

    console.log("Run!");
    run();
}

let db = new  DB(test, true);

*/