﻿import * as Models from "../../../Shared/Shared"

import { DB } from "../databases/Database"
import { Challenges, ChallengeEntity } from "../databases/tests/Challenges"
import { Users, UserEntity } from "../databases/tests/Users"
import { GameData, GameDataEtinity } from "../databases/tests/GameData"
import * as Processors from "../processors/Processors"

import { assert } from "chai"
import "mocha"
import { TestClass, Tests } from "./Tests"

export class Test implements TestClass {


    public async prepare() {


    }

    public async run() {
        describe("Game stats", async () => {
            let stats = <Models.GameStats>{
                score: 100,
                biggestCut: 131,
                numberOfTries: 83,
                chills: 3,
             //   cutElements: [3,4],
                cutFlesh: 145,
                cutPonies: 8234,
                duration: 24,
                cutsCount: 821,
                highestScoreForCut: 99,
                bladeRoad: 432,
                longestCut: 99,
                missedFlesh: 918,
                missedPonies: 123,
                pigs: 111,
                rages: 222
            }

            let stats2 = <Models.GameStats>{
                score: 5,
            }
            let stats3 = <Models.GameStats>{
                pigs: 1,
                rages: 3,
                score: 2
            }

            let validCriteria = [<Models.ChallengeCriteria>{ stat: "score", operator: 0, value: 90 },
                <Models.ChallengeCriteria>{ stat: "cutFlesh", operator: 0, value: 1 },
                <Models.ChallengeCriteria>{ stat: "duration", operator: 0, value: 0 },
                <Models.ChallengeCriteria>{ stat: "pigs", operator: 1, value: 122 },
                <Models.ChallengeCriteria>{ stat: "chills", operator:2, value: 3 }
                ];

            let invalidCriteria = [<Models.ChallengeCriteria>{ stat: "score", operator: 0, value: 90 },
            <Models.ChallengeCriteria>{ stat: "cutFlesh", operator: 0, value: 1 },
            <Models.ChallengeCriteria>{ stat: "duration", operator: 2, value: 0 },
            <Models.ChallengeCriteria>{ stat: "pigs", operator: 1, value: 122 },
            <Models.ChallengeCriteria>{ stat: "chills", operator: 2, value: 3 }
            ];
            let invalidCriteria2 = [<Models.ChallengeCriteria>{
                stat: "longestCut", operator: 0, value: 100
            }];

            let invalidCriteria3 = [<Models.ChallengeCriteria>{
                stat: "pigs", operator: 1, value: 100
            }];


            it("shoudl be the same after compress/decompress", () => {
                let expected = stats;
                let actual = Processors.GameStats.decompress(Processors.GameStats.compress(stats));
                assert.deepEqual(actual, expected);
            });

            it("shoudl match proper criterias", () => {
                let t1 = Processors.ChallengeCriteria.match(stats, validCriteria);
                let t2 = Processors.ChallengeCriteria.match(stats, invalidCriteria);
                let t3 = Processors.ChallengeCriteria.match(stats, invalidCriteria2);
                let t4 = Processors.ChallengeCriteria.match(stats, invalidCriteria3);

                let expected = [true, false, false, false];
                let actual = [t1, t2, t3, t4];
                assert.deepEqual(actual, expected);
            });

            it("shoudl be corectly added", () => {
                let expected = <Models.GameStats>{
                    pigs: 1,
                    rages: 3,
                    score: 7,
                    numberOfTries: 2
                };

                let actual = Processors.GameStats.add(stats3, stats2);
                assert.deepEqual(actual, expected);
            });

        });



        describe("Challenge criterias", async () => {
            let criteria = [<Models.ChallengeCriteria>{ stat: "score", operator: 0, value: 90 },
            <Models.ChallengeCriteria>{ stat: "cutFlesh", operator: 0, value: 1 },
            <Models.ChallengeCriteria>{ stat: "duration", operator: 0, value: 0 },
            <Models.ChallengeCriteria>{ stat: "pigs", operator: 1, value: 122 },
            <Models.ChallengeCriteria>{ stat: "chills", operator: 2, value: 3 }
            ];

            it("shoudl be the same after compress/decompress", () => {
                let expected = criteria;
                let actual = Processors.ChallengeCriteria.decompress(Processors.ChallengeCriteria.compress(criteria));
                assert.deepEqual(actual, expected);
            });

        });
    }
}
setTimeout(()=>Tests.tests.push(new Test()), 0);




/*


let test = () => {


    let users = DB.users as Users;
    let challenges = DB.challenges as Challenges;
    let games = DB.gamesData as GameData;


    let user = new UserEntity();
    user.id = 1;
    user.name = "User1";
    user.respect = 10;
    users.addUser(user);

    describe("Simple challenge results", async () => {

        let challenge = challenges.repo.create();
        challenge.cost = 5;
        challenge.endTime = Date.now() + 100000;
        challenge.id = 1;
        challenge.name = "Event 1";
        challenge.type = Models.ChallengeType.SIMPLE;
        challenge.succesfulTries = 0;
        challenge.prizesLeft = 3;
        challenge.criteria = JSON.stringify([<Models.ChallengeCriteria>{
            stat: "score",
            operator: Models.ChallengeCriteriaOperator.MORE,
            value: 100
        }]);
        challenge.config = <any>{ duration: 10 };
        challenge.parseDuration();

        let data = <Models.GameData>{
            stats: <Models.GameStats>{
                score: 200
            },
            challenge: 1,
            input: {}
        }
        // let results = await Processors.Game.handleGame(data, user.id);

        it("should be SUCCES if player match the criteria", async () => {
            let results = await Processors.Game.handleGame(data, user.id);
            let expected = Models.ChallengeResult.SUCCES;
            let actual = results.overall.challengeResult;
            assert.equal(actual, expected);
        });


    });


    describe("opis", () => {

        it("shoudl work", () => {

            assert.equal(2, 3);
        })
    });

    console.log("Run!");
    run();
}

let db = new  DB(test, true);

*/