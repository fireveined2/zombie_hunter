﻿/*import * as Models from "../../../Shared/Shared"
processors/processors
import { DB } from "../databases/Database"
import { ChallengeEntity } from "../databases/tests/Challenges"
import {  UserEntity } from "../databases/tests/Users"
import { GameData, GameDataEtinity } from "../databases/tests/GameData"
import { Prize } from "../entities/Prize"
import * as Processors from "../processors/processors"
import { Challenges, Users }from "../routes/Routes"

import { assert } from "chai"
import "mocha"
import { TestClass, Tests } from "./Tests"

export class TestsChallenges implements TestClass {

    private user: UserEntity;
    private challenge: ChallengeEntity;

    public async prepare() {

        let user = this.user = DB.users.repo.create();
        user.id = 1;
        user.name = "Adde";
        user.respect = 100;
        await DB.users.save(user);

        let challenge = this.challenge = DB.challenges.repo.create();
        challenge.id = 1;
        challenge.configId = 1;
        challenge.cost = 50;
        challenge.endTime = Date.now() + 10000;
        challenge.prizesLeft = 5;
        challenge.minutesLeft = 10;
        challenge.name = "Wyzwanie"
        challenge.prizeId = 1;
        challenge.criteriaCompressed = Processors.ChallengeCriteria.compress(
            [<Models.ChallengeCriteria>{ stat: "score", operator: 0, value: 90 }]
        );
        challenge.type = Models.ChallengeType.SIMPLE;
        await DB.challenges.save(challenge);


        let prize = DB.instance.repoFor(Prize).create();
        prize.id = 1;
        prize.name = "Nagroda1";
        prize.type = 1;
        await DB.instance.repoFor(Prize).persist(prize);
    }

    public async run() {
        describe("User", async () => {
            let users = new Users();
            let challenges = new Challenges();


            await DB.gamesData.repo.remove(await DB.gamesData.repo.find());
            let acces = await users._getAccesToPlay(1, 1);
            it("should get proper acces for challenge", () => {
                let expected = 1;
                let actual = acces.challengeId;
                assert.deepEqual(actual, expected);
            });

            console.log("e");

            let gameData = <Models.GameData>{
                input: 0,
                challenge: 1,
                stats: <Models.GameStats>{
                    score: 200
                }
            }
            let results = await users._handleUserGame(1, gameData);

            it("should win if match the criteria", () => {
                let expected = Models.ChallengeResult.SUCCES;
                let actual = results.overall.challengeResult;
                assert.deepEqual(actual, expected);
            });

            ////////////////////////////////
            console.log("e")
            if (await DB.gamesData.repo.count()>0)
                await DB.gamesData.repo.find()
           // await DB.gamesData.repo.remove();
            console.log("e");
            this.user.respect = this.challenge.cost;
            await DB.users.save(this.user);
      
            acces = await users._getAccesToPlay(1, 1);
            it("should  not get proper acces for challenge when not enough respeect", () => {
                let expected = null;
                let actual = acces;
                assert.deepEqual(actual, expected);
            });

            results = await users._handleUserGame(1, gameData);
            it("should not  be possible to post data if has no acces", () => {
                let expected = null;
                let actual = results;
                assert.deepEqual(actual, expected);
            });

            console.log("end");
            //////////////////////////////////////////////

          //  return Promise.resolve(0);

        });
    }
}
setTimeout(()=>Tests.tests.push(new TestsChallenges()), 0);




/*


let test = () => {


    let users = DB.users as Users;
    let challenges = DB.challenges as Challenges;
    let games = DB.gamesData as GameData;


    let user = new UserEntity();
    user.id = 1;
    user.name = "User1";
    user.respect = 10;
    users.addUser(user);

    describe("Simple challenge results", async () => {

        let challenge = challenges.repo.create();
        challenge.cost = 5;
        challenge.endTime = Date.now() + 100000;
        challenge.id = 1;
        challenge.name = "Event 1";
        challenge.type = Models.ChallengeType.SIMPLE;
        challenge.succesfulTries = 0;
        challenge.prizesLeft = 3;
        challenge.criteria = JSON.stringify([<Models.ChallengeCriteria>{
            stat: "score",
            operator: Models.ChallengeCriteriaOperator.MORE,
            value: 100
        }]);
        challenge.config = <any>{ duration: 10 };
        challenge.parseDuration();

        let data = <Models.GameData>{
            stats: <Models.GameStats>{
                score: 200
            },
            challenge: 1,
            input: {}
        }
        // let results = await Processors.Game.handleGame(data, user.id);

        it("should be SUCCES if player match the criteria", async () => {
            let results = await Processors.Game.handleGame(data, user.id);
            let expected = Models.ChallengeResult.SUCCES;
            let actual = results.overall.challengeResult;
            assert.equal(actual, expected);
        });


    });


    describe("opis", () => {

        it("shoudl work", () => {

            assert.equal(2, 3);
        })
    });

    console.log("Run!");
    run();
}

let db = new  DB(test, true);

*/
