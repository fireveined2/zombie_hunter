﻿
import { FactorySettings, ICuttableFactory } from "../../AgarFront/scripts/game/logic/cuttable/Generator"
import { Config } from "../../AgarFront/scripts/utils/Config"
import { ICuttableInterface, ICuttable, Cuttable } from "../../AgarFront/scripts/game/logic/cuttable/Cuttable"
import { CuttablesDatabase } from "../../AgarFront/scripts/beforeGame/CuttablesDatabase"


export class CuttableFactory implements ICuttableFactory {
    constructor( ) {
  
    }

    public create(obj: ICuttable, cuttable?: ICuttableInterface): ICuttableInterface {
        if (!cuttable) 
            cuttable = new Cuttable();

        cuttable.createFromObj(obj);   
        return cuttable;
    }

}