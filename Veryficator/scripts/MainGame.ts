﻿
import { Config } from "../../AgarFront/scripts/utils/Config"
import { Blade } from "./Blade"

import { Replay } from "../../AgarFront/scripts/game/Replay"
import { Collector } from "../../AgarFront/scripts/game/InputCollector"

import { CuttablesDatabase } from "../../AgarFront/scripts/beforeGame/CuttablesDatabase"
import { GameLogic } from "../../AgarFront/scripts/game/logic/Logic"
import { GameFabric } from "./GameFabric"
import { GameEvents } from "../../AgarFront/scripts/game/logic/GameEvents"
import * as Models from "../../Shared/Shared"


export class MainGame {
    private logic: GameLogic;
    private replay: Replay;
    private fabric: GameFabric;

    private collector: Collector;

    public onEnd: (stats: Models.GameStats, debugPointsInfo: number[], input?: any) => void;

    public init(acces: Models.AccesToPlay, input: any, width: number, height: number, collectInput: boolean, speed = 0.00000005) {
        let database = new CuttablesDatabase();
        database.loadAll();

        let fabric = this.fabric= new GameFabric();
        this.replay = new Replay(input);

        this.logic = new GameLogic(fabric, width, height, speed);

        if (collectInput)
        this.collector = new Collector();

        if (this.collector)
        GameEvents.on("mode.rage", (frame, duration) => {
            this.collector.push("rage", frame);
            this.collector.reset();
        })

    

        this.logic.onUpdate = (now, elapsed) => {
            if (this.replay && this.logic.gameRunning)
                this.replay.update(now);
        }

    //    GameEvents.on("scored", (score) => console.log(score))
        // 
     //   GameEvents.on("durationChanged", (seconds) => {
          //  console.log(seconds +"s left");
   //     });
 


        this.logic.init(acces);
        this.logic.onEnd = this.end.bind(this);
        this.start()  
    }

    private start() {
        if (this.replay)
            this.initReplay();
        else
            this.run();
    }

    private run() {
        this.logic.run();
        this.replay.useSkill = (id, x, y) => this.logic.skills.useSkill(id, { x: x, y: y });

        if (this.collector)
            this.logic.forEachNewInput = this.collector.addCut.bind(this.collector);
        GameEvents.emit("startAfterCounting");
    }

    private initReplay(fileName?: string) {
        this.replay.load(fileName, () => {
            this.run();
            this.replay.start(this.logic.addInput.bind(this.logic), () => GameEvents.emit("userEnabledRage"));
        });
    }


    private remove() {
        this.logic.remove();    
    }

    public end(frame: number, secondsLeft: number, accesToPlay: Models.AccesToPlay) {

        this.logic.delayStop(frame + Config.secondsToFrames(0.8), () => {
            this.remove();
            GameEvents.removeAllListeners();
            this.onEnd(GameLogic.stats, GameLogic.debugPointsInfo, this.collector ? this.collector.getCompressed() : undefined);

        });
    }
}