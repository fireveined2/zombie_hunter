﻿import { Blade } from "./Blade"
import { CuttableFactory } from "./Factory"



export class GameFabric {

    constructor() {

    }

    createBlade(): Blade {
        return new Blade();

    }

    createCuttableFactory(): CuttableFactory {
        return new CuttableFactory()
    }

}