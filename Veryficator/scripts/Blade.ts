﻿import { Config } from "../../AgarFront/scripts/utils/Config"
import { BladeLogic, IBlade, IBladeView, Segment } from "../../AgarFront/scripts/game/logic/Blade"


class DummyBladeView implements IBladeView{
    clearCanvas() { }
    update(segments: Segment[]) { }
    render(segments: Segment[]) { }
}

export class Blade implements IBlade {

    private model: BladeLogic;

    set forEachNewInput(func: (x: number, y: number, frame: number) => void) {
        this.model.forEachNewInput = func;
    }

    constructor() {
        this.model = new BladeLogic(new DummyBladeView());
    }

    public run() {
      
    }


    set disableMouseInput(disable: boolean) {

    }

 
    public update(frame: number, elapsed: number): Phaser.Point[] {
        return this.model.update(frame, elapsed);
    }

    public render() {
 
    }

    public addPoint(pos: Phaser.Point, newSlide?: boolean) {
        this.model.addPoint(pos, newSlide);
    }
}