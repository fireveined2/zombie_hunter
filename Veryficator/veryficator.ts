﻿
declare function require(name: string);

import { MainGame } from "./scripts/MainGame"
import * as Models from "../Shared/Shared"

export function Validate(acces: Models.AccesToPlay, input: any, width: number, height: number, onEnd?: Function, collectInput?: boolean, speed?) {

    let start = Date.now();;
    let game = new MainGame();
    game.onEnd = (stats, debugPointsInfo, input) => {
        if (onEnd)
            onEnd(stats, Date.now() - start, debugPointsInfo, input);
    }
    game.init(acces, input, width, height, collectInput, speed);

}


